LOCAL_PATH := $(call my-dir)

CVROOT := /opt/opencv-3.4.2-android-sdk/sdk/native/jni

include $(CLEAR_VARS)
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=STATIC
include $(CVROOT)/OpenCV.mk

LOCAL_MODULE += app

#LOCAL_C_INCLUDES += path_to_your_code/bar.h
#LOCAL_SRC_FILES += path_to_your_code/bar.cpp 

LOCAL_CFLAGS += -std=c++11 -frtti -fexceptions -fopenmp -w
LOCAL_LDLIBS += -llog -L$(SYSROOT)/usr/lib
LOCAL_LDFLAGS += -fopenmp

include $(BUILD_SHARED_LIBRARY)
