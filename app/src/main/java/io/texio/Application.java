package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hypertrack.hyperlog.HyperLog;

import io.texio.util.RandomString;

/**
 * Application-level helpers
 */
public class Application
{
    public static final String TAG = Application.class.getSimpleName();

    public static final String KEY_DEBUG_MODE = "pref_debugMode";
    public static final String KEY_APP_ID = "pref_applicationId";
    public static final String KEY_DEFAULT_CURRENCY = "pref_defaultCurrency";
    public static final String KEY_SEND_SNAPS_TO_SERVER = "pref_sendSnapsToServer";

    private static Boolean debugModeCache = null;

    public static boolean getDebugMode(Context context)
    {
        if (debugModeCache == null)
        {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
            debugModeCache = sharedPref.getBoolean(KEY_DEBUG_MODE, false);
        }

        return debugModeCache;
    }

    public static void setDebugMode(Context context, boolean state)
    {
        debugModeCache = state;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(KEY_DEBUG_MODE, state);
        editor.apply();

        HyperLog.i(TAG, "Application debug mode has been set to " + state);
    }

    public static String getApplicationId(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(KEY_APP_ID, null);
    }

    public static void generateNewId(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_APP_ID, RandomString.generate(20));
        editor.apply();
    }

    /**
     * Returns the default currency specified in settings
     */
    public static String getDefaultCurrency(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        return sharedPref.getString(KEY_DEFAULT_CURRENCY, "CZK");
    }

    public static void setHermesEnabled(Context context, boolean value)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(KEY_SEND_SNAPS_TO_SERVER, value);
        editor.apply();

        HyperLog.i(TAG, "Hermes has been set to " + value);
    }

    public static boolean getHermesEnabled(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(KEY_SEND_SNAPS_TO_SERVER, false);
    }
}
