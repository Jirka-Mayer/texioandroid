package io.texio.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import io.texio.Application;
import io.texio.PrivacyPolicy;
import io.texio.R;

public class PrivacyPolicyActivity extends AppCompatActivity
{
    private TextView text;
    private CheckBox agreeCheckbox;
    private Button okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_privacy_policy);

        this.text = this.findViewById(R.id.text);
        this.agreeCheckbox = this.findViewById(R.id.agree_checkbox);
        this.okButton = this.findViewById(R.id.ok_button);

        this.agreeCheckbox.setOnCheckedChangeListener((v, b) -> this.agreeChanged());

        this.text.setText(Html.fromHtml(
            this.getResources().getString(
                R.string.privacyPolicyActivity_description
            )
        ));

        this.text.setOnClickListener((v) -> this.linkClicked());

        // default result is "canceled"
        this.setResult(RESULT_CANCELED);
    }

    public void linkClicked()
    {
        String url = "https://texio.io/zasady-ochrany-osobnich-udaju";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        this.startActivity(intent);
    }

    public void agreeChanged()
    {
        if (this.agreeCheckbox.isChecked())
        {
            this.okButton.setEnabled(true);
        }
        else
        {
            this.okButton.setEnabled(false);
        }
    }

    public void onOkClick(View view)
    {
        if (!this.agreeCheckbox.isChecked())
            return;

        PrivacyPolicy.accept(this);
        Application.setHermesEnabled(this, true);

        this.setResult(RESULT_OK);
        this.finish();
    }

    public void onCancelClick(View view)
    {
        this.setResult(RESULT_CANCELED);
        this.finish();
    }
}
