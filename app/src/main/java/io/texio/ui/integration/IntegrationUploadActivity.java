package io.texio.ui.integration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hypertrack.hyperlog.HyperLog;

import io.texio.CredentialStorage;
import io.texio.IntentExtras;
import io.texio.R;
import io.texio.integration.Integration;
import io.texio.integration.UploadTask;

/**
 * Performs upload (and all the associated checks) of a receipt to uctenkovka or other integration
 *
 * Starting intent:
 * EXTRA_INTEGRATION - id of the integration to upload to
 * EXTRA_RECEIPT_ID - id of the receipt to upload
 *
 * Behavior:
 * 1) Start upload without explicitCredentials (tries to obtain for itself from storage)
 * 2) Else ask for explicitCredentials and upload with those one-time provided
 * 3) Else repeat 2
 *
 * if RESULT_OK:
 *      the upload was successful, reload the receipt from database - may have changed
 * else:
 *      nothing required
 */
public class IntegrationUploadActivity extends AppCompatActivity implements UploadTask.UploadTaskCallback {

    public static final String TAG = IntegrationUploadActivity.class.getSimpleName();

    public static final int REQUEST_LOGIN = 1;

    private TextView statusText;
    private ProgressBar spinner;

    private Integration integration;
    private String receiptId;
    private CredentialStorage.CredentialPair credentials;

    private UploadTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integration_upload);

        findViewById(R.id.cancel_button).setOnClickListener(v -> cancelClicked());
        statusText = findViewById(R.id.status_text);
        spinner = findViewById(R.id.spinner);

        // load intent
        Intent intent = getIntent();
        integration = Integration.getIntegration(
            intent.getIntExtra(IntentExtras.EXTRA_INTEGRATION, -1),
            this
        );
        receiptId = intent.getStringExtra(IntentExtras.EXTRA_RECEIPT_ID);

        // start
        startUpload();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("io.texio.UPLOADING_RECEIPT", task != null);
        task.cancel(true);
        task = null;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.getBoolean("io.texio.UPLOADING_RECEIPT", false))
            startUpload();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode == RESULT_OK) {
                credentials = new CredentialStorage.CredentialPair(
                    data.getStringExtra(IntentExtras.EXTRA_LOGIN),
                    data.getStringExtra(IntentExtras.EXTRA_PASSWORD)
                );
                startUpload();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    /**
     * Starts the integration login activity
     */
    private void promptForCredentials() {
        Intent i = new Intent(this, IntegrationLoginActivity.class);
        i.putExtra(IntentExtras.EXTRA_INTEGRATION, integration.getId());
        i.putExtra(IntentExtras.EXTRA_FORCE_REMEMBERING, false);
        startActivityForResult(i, REQUEST_LOGIN);
    }

    /**
     * Starts the uploading task
     */
    private void startUpload() {
        if (task != null)
            task.cancel(true);

        task = integration.createUploadTask(this, receiptId, this);

        if (credentials != null)
            task.setExplicitCredentials(credentials);

        task.execute();

        statusText.setText(R.string.integrationUpload_uploading);
        spinner.setVisibility(View.VISIBLE);
        HyperLog.i(TAG, "Uploading receipt into integration with id " + integration.getId() + "...");
    }

    public void cancelClicked() {
        if (task != null) {
            task.cancel(true);
            task = null;
        }
        setResult(RESULT_CANCELED);
        finish();
        HyperLog.i(TAG, "Upload was canceled.");
    }

    @Override
    public void uploadHasInvalidCredentials() {
        promptForCredentials();
    }

    @Override
    public void uploadFailed(String message) {
        statusText.setText(message);
        spinner.setVisibility(View.GONE);
        HyperLog.i(TAG, "Upload failed: " + message.replaceAll("\\n", "<br>"));
    }

    @Override
    public void uploadDone() {
        Toast.makeText(this, R.string.integrationUpload_uploaded, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
        HyperLog.i(TAG, "Upload succeeded.");
    }
}
