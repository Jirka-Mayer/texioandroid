package io.texio.ui.integration;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.function.Function;
import java.util.function.Predicate;

import io.texio.R;
import io.texio.integration.Integration;

public class IntegrationCard extends CardView {

    private TextView title;
    private TextView login;
    private ImageView icon;
    private Button button;

    private Integration integration;

    public interface RequestLoginDelegate {
        void request(int integrationId);
    }

    private RequestLoginDelegate requestLogin;

    public void setRequestLogin(RequestLoginDelegate requestLogin) {
        this.requestLogin = requestLogin;
    }

    public IntegrationCard(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_integration_card, this);

        TypedArray a = context.getTheme().obtainStyledAttributes(
            attrs,
            R.styleable.IntegrationCard,
            0, 0
        );

        try {
            integration = Integration.getIntegration(
                a.getInteger(R.styleable.IntegrationCard_integrationId, -1),
                context
            );
        } finally {
            a.recycle();
        }

        // view references
        title = findViewById(R.id.title);
        login = findViewById(R.id.login);
        icon = findViewById(R.id.icon);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> buttonClicked());

        // set proper content
        title.setText(integration.titleResourceId());
        icon.setImageResource(integration.iconResourceId());

        updateLoginTextAndButtonText();
    }

    private void updateLoginTextAndButtonText() {
        String text = integration.getLogin();

        if (text == null) {
            login.setText(R.string.integrationActivity_noCredentials);
            button.setText(R.string.integrationActivity_set);
        } else {
            login.setText(text);
            button.setText(R.string.integrationActivity_forget);
        }
    }

    private void buttonClicked() {
        String text = integration.getLogin();

        if (text == null) {
            requestLogin.request(integration.getId());
        } else {
            integration.setCredentials(null);
            updateLoginTextAndButtonText();
        }
    }

    public void credentialsMayHaveChanged(int integrationId) {
        if (integrationId != integration.getId())
            return;

        updateLoginTextAndButtonText();
    }
}
