package io.texio.ui.integration;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.lang.ref.WeakReference;

import io.texio.CredentialStorage;
import io.texio.IntentExtras;
import io.texio.R;
import io.texio.integration.Integration;
import io.texio.networking.NoInternetAccessException;

/**
 * Activity for obtaining and verifying login explicitCredentials for a given integrated service
 *
 * Starting intent:
 * EXTRA_INTEGRATION - id of the integration to login to
 * EXTRA_FORCE_REMEMBERING - if true, remember box is checked and invisible
 *
 * Credentials are remembered based on the value of the checkbox
 * Credentials are always verified before returning
 *
 * Returned values if RESULT_OK:
 * EXTRA_LOGIN - value of the login (email) field
 * EXTRA_PASSWORD - value of the password field (both verified)
 */
public class IntegrationLoginActivity extends AppCompatActivity {

    public static final int LAYOUT_DEFAULT = 1;
    public static final int LAYOUT_DEFAULT_WITH_ERROR = 2;
    public static final int LAYOUT_VERIFYING = 3;

    private int layoutState;

    private Button loginButton;
    private Button cancelButton;
    private ImageView banner;
    private EditText loginField;
    private EditText passwordField;
    private TextView infoText;
    private TextView errorText;
    private CheckBox rememberBox;
    private ProgressBar progressBar;

    private boolean forceRemembering;
    private Integration integration;

    private VerifyCredentialsTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integration_login);

        // setup app bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // view references
        banner = findViewById(R.id.banner);
        loginField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_field);
        rememberBox = findViewById(R.id.remember);
        progressBar = findViewById(R.id.progress_bar);
        infoText = findViewById(R.id.info_text);
        errorText = findViewById(R.id.error_text);
        loginButton = findViewById(R.id.login_button);
        cancelButton = findViewById(R.id.cancel_button);

        // events
        cancelButton.setOnClickListener(v -> onSupportNavigateUp());
        loginButton.setOnClickListener(v -> login());

        // submit from the password field
        passwordField.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login();
                return true;
            }
            return false;
        });

        // load intent
        Intent intent = getIntent();
        integration = Integration.getIntegration(
            intent.getIntExtra(IntentExtras.EXTRA_INTEGRATION, -1),
            this
        );
        forceRemembering = intent.getBooleanExtra(IntentExtras.EXTRA_FORCE_REMEMBERING, false);

        // customize look based on the integration
        banner.setImageResource(integration.loginBannerResourceId());
        infoText.setText(integration.loginInfoTextResourceId());

        if (forceRemembering) {
            rememberBox.setChecked(true);
            rememberBox.setVisibility(View.GONE);
        }

        setLayout(LAYOUT_DEFAULT);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("io.texio.ACTIVITY_LAYOUT_STATE", layoutState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        setLayout(savedInstanceState.getInt("io.texio.ACTIVITY_LAYOUT_STATE", LAYOUT_DEFAULT));

        // restart the task
        if (layoutState == LAYOUT_VERIFYING)
            login();
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        // NOTE: called also on cancel button click

        // stop the task if running and don't exit
        if (task != null) {
            task.cancel(true);
            task = null;
            setLayout(LAYOUT_DEFAULT);
            return true;
        }

        // act as the back button
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }

    public void setLayout(int index) {
        layoutState = index;

        switch (index) {
            case LAYOUT_DEFAULT:
                setEnabledForViews(true);
                progressBar.setVisibility(View.GONE);
                infoText.setVisibility(View.VISIBLE);
                errorText.setVisibility(View.GONE);
                break;

            case LAYOUT_VERIFYING:
                setEnabledForViews(false);
                progressBar.setVisibility(View.VISIBLE);
                infoText.setVisibility(View.GONE);
                errorText.setVisibility(View.GONE);
                break;

            case LAYOUT_DEFAULT_WITH_ERROR:
                setLayout(LAYOUT_DEFAULT);
                errorText.setVisibility(View.VISIBLE);
                infoText.setVisibility(View.GONE);
                break;
        }
    }

    public void setEnabledForViews(boolean value) {
        loginField.setEnabled(value);
        passwordField.setEnabled(value);
        rememberBox.setEnabled(value);
        loginButton.setEnabled(value);
    }

    /**
     * Login button clicked
     */
    public void login() {
        setLayout(LAYOUT_VERIFYING);

        task = new VerifyCredentialsTask(this);
        task.execute(new CredentialStorage.CredentialPair(
            loginField.getText().toString(),
            passwordField.getText().toString()
        ));
    }

    /**
     * After the explicitCredentials have been successfully verified
     */
    public void credentialsVerified() {
        task = null;

        CredentialStorage.CredentialPair pair = new CredentialStorage.CredentialPair(
            loginField.getText().toString(),
            passwordField.getText().toString()
        );

        if (forceRemembering || rememberBox.isChecked())
            integration.setCredentials(pair);

        Intent data = new Intent();
        data.putExtra(IntentExtras.EXTRA_LOGIN, pair.login);
        data.putExtra(IntentExtras.EXTRA_PASSWORD, pair.password);
        setResult(RESULT_OK, data);
        finish();
    }

    public void credentialVerificationFailed(int errorMessageResourceId) {
        task = null;
        setLayout(LAYOUT_DEFAULT_WITH_ERROR);
        errorText.setText(errorMessageResourceId);
    }

    static class VerifyCredentialsTask extends AsyncTask<CredentialStorage.CredentialPair, Void, Void> {

        private WeakReference<IntegrationLoginActivity> weakActivity;
        private Integration integration;

        private int errorMessageResourceId;
        private boolean verifiedSuccessfully;

        public VerifyCredentialsTask(IntegrationLoginActivity activity) {
            weakActivity = new WeakReference<>(activity);
            integration = activity.integration;
        }

        @Override
        protected Void doInBackground(CredentialStorage.CredentialPair... credentialPairs) {
            try {
                verifiedSuccessfully = false;
                verifiedSuccessfully = integration.areCredentialsValid(credentialPairs[0]);
                if (!verifiedSuccessfully)
                    errorMessageResourceId = R.string.integrationLoginActivity_error_invalidCredentials;
            } catch (NoInternetAccessException e) {
                errorMessageResourceId = R.string.integrationLoginActivity_error_noConnection;
            } catch (IOException e) {
                errorMessageResourceId = R.string.integrationLoginActivity_error_unknown;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            IntegrationLoginActivity activity = weakActivity.get();

            if (activity == null)
                return;

            if (verifiedSuccessfully)
                activity.credentialsVerified();
            else
                activity.credentialVerificationFailed(errorMessageResourceId);
        }
    }
}
