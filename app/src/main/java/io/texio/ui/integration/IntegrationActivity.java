package io.texio.ui.integration;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.integration.Integration;
import io.texio.ui.NavigationActivity;

public class IntegrationActivity extends NavigationActivity {

    private static final int REQUEST_LOGIN = 1;

    private LinearLayout cards;
    private int integrationId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integration);

        // setup app bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle(this.getResources().getString(R.string.integrationActivity_title));
        this.integrateNavigationWithActionBar();

        cards = findViewById(R.id.cards);

        // attach reference for login request to each child
        for (int i = 0; i < cards.getChildCount(); i++) {
            View c = cards.getChildAt(i);
            if (c instanceof IntegrationCard) {
                ((IntegrationCard) c).setRequestLogin(this::requestLogin);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("io.texio.INTEGRATION_ID", integrationId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        integrationId = savedInstanceState.getInt("io.texio.INTEGRATION_ID", -1);
    }

    private void requestLogin(int integrationId) {
        this.integrationId = integrationId;
        Intent intent = new Intent(this, IntegrationLoginActivity.class);
        intent.putExtra(IntentExtras.EXTRA_FORCE_REMEMBERING, true);
        intent.putExtra(IntentExtras.EXTRA_INTEGRATION, integrationId);
        this.startActivityForResult(intent, REQUEST_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode == RESULT_OK) {
                noteThatCredentialsMayHaveChanged();
            }
        }
    }

    private void noteThatCredentialsMayHaveChanged() {
        for (int i = 0; i < cards.getChildCount(); i++) {
            View c = cards.getChildAt(i);
            if (c instanceof IntegrationCard) {
                ((IntegrationCard) c).credentialsMayHaveChanged(this.integrationId);
            }
        }
    }
}
