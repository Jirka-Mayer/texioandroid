package io.texio.ui.trash;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.ui.NavigationActivity;
import io.texio.ui.ViewFadingAnimator;
import io.texio.ui.receiptDetail.ReceiptDetailActivity;
import io.texio.ui.receiptList.LoadMoreReceipts;
import io.texio.ui.receiptList.RefreshReceipt;
import io.texio.ui.receiptListView.DateSortedFeeder;
import io.texio.ui.receiptListView.ReceiptListAdapter;
import io.texio.ui.receiptListView.ReceiptListView;

/**
 * Lists all removed receipts before they are completely deleted
 */
public class TrashActivity extends NavigationActivity {

    public static final String TAG = TrashActivity.class.getSimpleName();

    private static final int REQUEST_RECEIPT_DETAIL = 1;

    public CoordinatorLayout coordinatorLayout;

    // action bars
    private Toolbar mainToolbar;
    private Toolbar selectionToolbar;

    // list stuff
    private ReceiptListView listView;
    public ReceiptListAdapter adapter;
    public DateSortedFeeder feeder;

    private View emptyListOverlay;

    /**
     * Id of the currently opened receipt
     */
    private String openedReceiptId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash);

        // elements
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        mainToolbar = findViewById(R.id.toolbar);
        selectionToolbar = findViewById(R.id.selection_toolbar);
        listView = findViewById(R.id.receipt_list);
        emptyListOverlay = findViewById(R.id.empty_list_overlay);

        // setup app bar
        selectionToolbar.setNavigationOnClickListener(v -> adapter.clearSelection());
        selectionToolbar.inflateMenu(R.menu.appbar_trash_selection);
        selectionToolbar.setOnMenuItemClickListener(this::onOptionsItemSelected);
        setSupportActionBar(mainToolbar);
        integrateNavigationWithActionBar();

        // setup receipt list
        adapter = listView.getAdapter();
        feeder = new DateSortedFeeder(adapter);
        feeder.setContext(this);

        adapter.setLoadMoreReceiptsListener(() -> {
            new LoadMoreReceipts(
                this,
                adapter,
                feeder,
                ReceiptListView.RECOMMENDED_LOADING_BATCH_SIZE,
                LoadMoreReceipts.RECEIPT_SOURCE_TRASH
            ).execute();
        });
        adapter.setReceiptClickListener(r -> openReceiptDetail(r.id));
        adapter.setSelectionChangeListener(this::onReceiptSelectionChange);
        adapter.setItemsChangedListener(this::onReceiptListItemsChanged);

        // init this
        onReceiptListItemsChanged();

        // load first batch of receipts
        adapter.requestLoading();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // NOTE: selection is not saved

        outState.putInt("io.texio.LOADED_RECEIPT_COUNT", feeder.countReceipts());
        outState.putString("io.texio.OPENED_RECEIPT_ID", openedReceiptId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int receiptsToLoad = savedInstanceState.getInt("io.texio.LOADED_RECEIPT_COUNT", 0);
        if (receiptsToLoad > 0) {
            new LoadMoreReceipts(
                this, adapter, feeder, receiptsToLoad,
                LoadMoreReceipts.RECEIPT_SOURCE_TRASH
            ).execute();
        }

        openedReceiptId = savedInstanceState.getString("io.texio.OPENED_RECEIPT_ID");
    }

    @Override
    public void onBackPressed() {
        if (adapter.getSelectedReceipts().length > 0) {
            adapter.clearSelection();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.appbar_trash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_restore:
                restoreSelectedReceipts();
                return true;

            case R.id.action_emptyTrash:
                emptyTrash();
                return true;

            case R.id.action_deletePermanently:
                deleteSelectedReceipts();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RECEIPT_DETAIL) {
            if (openedReceiptId != null) {
                new RefreshReceipt(this, feeder, openedReceiptId).execute();
                openedReceiptId = null;
            }
        }
    }

    public void onReceiptListItemsChanged() {
        if (adapter.getItemCount() == 0) {
            emptyListOverlay.setVisibility(View.VISIBLE);
        } else {
            emptyListOverlay.setVisibility(View.GONE);
        }
    }

    /**
     * Opens the receipt detail activity
     */
    public void openReceiptDetail(String receiptId) {
        if (receiptId == null)
            return;

        Intent intent = new Intent(this, ReceiptDetailActivity.class);
        intent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, receiptId);
        startActivityForResult(intent, REQUEST_RECEIPT_DETAIL);
        openedReceiptId = receiptId;
    }

    private void onReceiptSelectionChange(Receipt[] receipts) {
        selectionToolbar.setTitle(Integer.toString(receipts.length));

        // appear
        if (receipts.length > 0 && selectionToolbar.getVisibility() == View.INVISIBLE) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorSecondaryDark));
            ViewFadingAnimator.fadeIn(selectionToolbar, 200);
        }

        // disappear
        if (receipts.length == 0 && selectionToolbar.getVisibility() == View.VISIBLE) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            ViewFadingAnimator.fadeOut(selectionToolbar, 200);
        }
    }

    public void restoreSelectedReceipts() {
        Receipt[] receipts = adapter.getSelectedReceipts();

        if (receipts.length == 0)
            return;

        new RestoreReceipts(this, receipts).execute();
    }

    public void deleteSelectedReceipts() {
        Receipt[] receipts = adapter.getSelectedReceipts();

        if (receipts.length == 0)
            return;

        new DeleteReceipts(this, receipts).execute();
    }

    public void emptyTrash() {
        new DeleteReceipts(this, null).execute();
    }
}
