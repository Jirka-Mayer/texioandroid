package io.texio.ui.trash;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;

import com.hypertrack.hyperlog.HyperLog;

import java.lang.ref.WeakReference;

import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptFileManager;

/**
 * Used for both selection deletion and entire trash deletion
 */
class DeleteReceipts extends AsyncTask<Void, Void, Void> {

    public static final String TAG = DeleteReceipts.class.getSimpleName();

    private WeakReference<TrashActivity> weakActivity;
    private AppDatabase db;

    private Context context;
    private Receipt[] receipts;

    public DeleteReceipts(TrashActivity activity, Receipt[] receipts) {
        weakActivity = new WeakReference<>(activity);
        db = AppDatabaseFactory.getInstance(activity);
        this.receipts = receipts;
        context = activity.getApplicationContext();
    }

    @Override
    protected Void doInBackground(Void... voids) {

        // if receipts are null it means all receipts should be deleted
        if (receipts == null) {
            HyperLog.i(TAG, "Emptying entire trash bin.");
            receipts = db.receiptDao().allDeleted();
        }

        for (int i = 0; i < receipts.length; i++) {
            if (isCancelled())
                break;

            // clear filesystem
            ReceiptFileManager fileManager = new ReceiptFileManager(context, receipts[i]);
            fileManager.deleteReceiptDirectory();

            // clear states otherwise database throws constraint violation
            db.receiptStateChangeDao().clearFor(receipts[i].id);

            // now remove the receipt from database
            db.receiptDao().delete(receipts[i]);
        }

        HyperLog.i(TAG, "Receipts (" + receipts.length + ") have been permanently deleted.");

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        TrashActivity activity = weakActivity.get();

        if (activity == null || isCancelled())
            return;

        activity.adapter.clearSelection();

        for (Receipt r : receipts)
            activity.feeder.removeReceipt(r.id);

        Snackbar.make(
            activity.coordinatorLayout,
            R.string.trashActivity_receiptsDeleted,
            Snackbar.LENGTH_LONG
        ).show();
    }
}
