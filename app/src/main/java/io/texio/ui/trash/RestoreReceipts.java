package io.texio.ui.trash;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;

import com.hypertrack.hyperlog.HyperLog;

import java.lang.ref.WeakReference;

import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;

class RestoreReceipts extends AsyncTask<Void, Void, Void> {

    public static final String TAG = RestoreReceipts.class.getSimpleName();

    private WeakReference<TrashActivity> weakActivity;
    private AppDatabase db;

    private Receipt[] receipts;

    public RestoreReceipts(TrashActivity activity, Receipt[] receipts) {
        weakActivity = new WeakReference<>(activity);
        db = AppDatabaseFactory.getInstance(activity);
        this.receipts = receipts;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (Receipt r : receipts)
            r.deletedAt = null;
        db.receiptDao().update(receipts);
        HyperLog.i(TAG, "Receipts (" + receipts.length + ") have been restored from trash.");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        TrashActivity activity = weakActivity.get();

        if (isCancelled() || activity == null)
            return;

        activity.adapter.clearSelection();

        for (Receipt r : receipts)
            activity.feeder.removeReceipt(r.id);

        Snackbar.make(
            activity.coordinatorLayout,
            R.string.trashActivity_receiptsRestored,
            Snackbar.LENGTH_SHORT
        ).show();
    }
}
