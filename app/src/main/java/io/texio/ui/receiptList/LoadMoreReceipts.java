package io.texio.ui.receiptList;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.ui.receiptListView.DateSortedFeeder;
import io.texio.ui.receiptListView.ReceiptListAdapter;
import io.texio.ui.receiptListView.ReceiptListView;

/**
 * Loads next batch of receipts
 */
public class LoadMoreReceipts extends AsyncTask<Void, Void, Void> {

    public static final int RECEIPT_SOURCE_RECEIPT_LIST = 1;
    public static final int RECEIPT_SOURCE_TRASH = 2;

    private AppDatabase db;

    private DateSortedFeeder feeder;
    private ReceiptListAdapter adapter;

    private List<Receipt> receipts;
    private List<ReceiptState> states;

    private int batchSize;
    private int source;

    public LoadMoreReceipts(
        Context context,
        ReceiptListAdapter adapter,
        DateSortedFeeder feeder,
        int batchSize,
        int source
    ) {
        db = AppDatabaseFactory.getInstance(context);
        this.adapter = adapter;
        this.feeder = feeder;
        this.batchSize = batchSize;
        this.source = source;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        switch (source) {
            case RECEIPT_SOURCE_RECEIPT_LIST:
                receipts = db.receiptDao().receiptList_loadBatch(
                    batchSize,
                    feeder.countReceipts()
                );
                break;

            case RECEIPT_SOURCE_TRASH:
                receipts = db.receiptDao().trash_loadBatch(
                    batchSize,
                    feeder.countReceipts()
                );
                break;
        }

        states = new ArrayList<>(receipts.size());
        for (int i = 0; i < receipts.size(); i++) {
            states.add(ReceiptState.fromStateChanges(
                db.receiptStateChangeDao().of(receipts.get(i).id)
            ));
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        if (isCancelled()) {
            adapter.itemsHaveBeenLoaded(false);
            return;
        }

        boolean wasEmptyBefore = feeder.countReceipts() == 0;

        for (int i = 0; i < receipts.size(); i++)
            feeder.insertReceipt(receipts.get(i), states.get(i));

        // because if it was empty, the spinner was the only item so it stayed
        // at the top of the screen and when removed the view looks like
        // it has been scrolled to bottom (so we scroll back up)
        if (wasEmptyBefore && receipts.size() > 0)
            feeder.scrollToReceipt(receipts.get(0).id);

        adapter.itemsHaveBeenLoaded(receipts.size() < batchSize);
    }
}
