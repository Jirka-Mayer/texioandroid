package io.texio.ui.receiptList;

import android.os.AsyncTask;

import com.hypertrack.hyperlog.HyperLog;

import java.lang.ref.WeakReference;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptDate;

public class CreateNewReceipt extends AsyncTask<Void, Void, Void> {

    private static final String TAG = CreateNewReceipt.class.getSimpleName();

    private WeakReference<ReceiptListActivity> weakActivity;
    private AppDatabase db;

    private Receipt receipt;

    public CreateNewReceipt(ReceiptListActivity activity) {
        weakActivity = new WeakReference<>(activity);
        db = AppDatabaseFactory.getInstance(activity);

        receipt = Receipt.createNew(activity);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        db.receiptDao().insert(receipt);
        HyperLog.i(TAG, "New receipt has been manually created.");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        ReceiptListActivity activity = weakActivity.get();

        if (isCancelled() || activity == null)
            return;

        activity.openReceiptDetail(receipt.id);
    }
}
