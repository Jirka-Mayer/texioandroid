package io.texio.ui.receiptList;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.getbase.floatingactionbutton.FloatingActionsMenu;

import io.texio.Application;
import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.ui.NavigationActivity;
import io.texio.ui.ViewFadingAnimator;
import io.texio.ui.receiptDetail.ReceiptDetailActivity;
import io.texio.ui.receiptListView.DateSortedFeeder;
import io.texio.ui.receiptListView.ReceiptListAdapter;
import io.texio.ui.receiptListView.ReceiptListView;
import io.texio.ui.snappingPipeline.SnappingPipelineActivity;

/**
 * Provides access to all non-deleted receipts in a list sorted by date
 *
 * Opening intent:
 * EXTRA_OPEN_RECEIPT - if true, we should immediately open a receipt detail
 * EXTRA_RECEIPT_ID - the receipt id to be opened
 */
public class ReceiptListActivity extends NavigationActivity
    implements FloatingActionsMenu.OnFloatingActionsMenuUpdateListener {

    public static final String TAG = ReceiptListActivity.class.getSimpleName();

    private static final int REQUEST_RECEIPT_DETAIL = 1;

    public CoordinatorLayout coordinatorLayout;

    // action bars
    private Toolbar mainToolbar;
    private Toolbar selectionToolbar;

    // list stuff
    private ReceiptListView listView;
    public ReceiptListAdapter adapter;
    public DateSortedFeeder feeder;

    private View emptyListOverlay;

    /**
     * Id of the currently opened receipt
     */
    private String openedReceiptId;

    private View fabCurtain;
    private FloatingActionsMenu fabMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_list);

        // elements
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        mainToolbar = findViewById(R.id.toolbar);
        selectionToolbar = findViewById(R.id.selection_toolbar);
        listView = findViewById(R.id.receipt_list);
        emptyListOverlay = findViewById(R.id.empty_list_overlay);

        // setup app bar
        selectionToolbar.setNavigationOnClickListener(v -> adapter.clearSelection());
        selectionToolbar.inflateMenu(R.menu.appbar_receipt_list_selection);
        selectionToolbar.setOnMenuItemClickListener(this::onOptionsItemSelected);
        setSupportActionBar(mainToolbar);
        integrateNavigationWithActionBar();

        // FABs
        fabCurtain = findViewById(R.id.fab_curtain);
        fabMenu = findViewById(R.id.fab_menu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(this);
        fabCurtain.setOnClickListener(v -> this.onFabCurtainClicked());
        findViewById(R.id.fab_newReceipt).setOnClickListener(v -> this.createReceipt());
        findViewById(R.id.fab_newSnap).setOnClickListener(v -> this.snapNewReceipt());

        if (!Application.getDebugMode(this)) {
            fabMenu.removeButton(findViewById(R.id.fab_boom));
        } else {
            findViewById(R.id.fab_boom).setOnClickListener(v -> this.boomReceipts());
        }

        // setup receipt list
        adapter = listView.getAdapter();
        feeder = new DateSortedFeeder(adapter);
        feeder.setContext(this);

        adapter.setLoadMoreReceiptsListener(() -> {
            new LoadMoreReceipts(
                this,
                adapter,
                feeder,
                ReceiptListView.RECOMMENDED_LOADING_BATCH_SIZE,
                LoadMoreReceipts.RECEIPT_SOURCE_RECEIPT_LIST
            ).execute();
        });
        adapter.setReceiptClickListener(r -> openReceiptDetail(r.id));
        adapter.setSelectionChangeListener(this::onReceiptSelectionChange);
        adapter.setItemsChangedListener(this::onReceiptListItemsChanged);

        // init this
        onReceiptListItemsChanged();

        // handle opening of receipt detail
        Intent intent = getIntent();
        if (intent.getBooleanExtra(IntentExtras.EXTRA_OPEN_RECEIPT, false))
            openReceiptDetail(intent.getStringExtra(IntentExtras.EXTRA_RECEIPT_ID));

        // load first batch of receipts
        adapter.requestLoading();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // NOTE: selection is not saved

        outState.putInt("io.texio.LOADED_RECEIPT_COUNT", feeder.countReceipts());
        outState.putString("io.texio.OPENED_RECEIPT_ID", openedReceiptId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int receiptsToLoad = savedInstanceState.getInt("io.texio.LOADED_RECEIPT_COUNT", 0);
        if (receiptsToLoad > 0) {
            new LoadMoreReceipts(
                this, adapter, feeder, receiptsToLoad,
                LoadMoreReceipts.RECEIPT_SOURCE_RECEIPT_LIST
            ).execute();
        }

        openedReceiptId = savedInstanceState.getString("io.texio.OPENED_RECEIPT_ID");
    }

    @Override
    public void onBackPressed() {
        if (adapter.getSelectedReceipts().length > 0) {
            adapter.clearSelection();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.appbar_receipt_list, menu);

        /*
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        */

        // Configure the search info and add any event listeners...

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.action_search:
                Toast.makeText(this, "Searching!", Toast.LENGTH_LONG).show();
                return true;*/

            case R.id.action_trash:
                moveSelectedReceiptsToTrash();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RECEIPT_DETAIL) {
            if (openedReceiptId != null) {
                new RefreshReceipt(this, feeder, openedReceiptId).execute();
                openedReceiptId = null;
            }
        }
    }

    public void onReceiptListItemsChanged() {
        if (adapter.getItemCount() == 0) {
            emptyListOverlay.setVisibility(View.VISIBLE);
        } else {
            emptyListOverlay.setVisibility(View.GONE);
        }
    }

    /**
     * Opens the receipt detail activity
     */
    public void openReceiptDetail(String receiptId) {
        if (receiptId == null)
            return;

        Intent intent = new Intent(this, ReceiptDetailActivity.class);
        intent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, receiptId);
        startActivityForResult(intent, REQUEST_RECEIPT_DETAIL);
        openedReceiptId = receiptId;
    }

    private void onReceiptSelectionChange(Receipt[] receipts) {
        selectionToolbar.setTitle(Integer.toString(receipts.length));

        // appear
        if (receipts.length > 0 && selectionToolbar.getVisibility() == View.INVISIBLE) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorSecondaryDark));
            ViewFadingAnimator.fadeIn(selectionToolbar, 200);
        }

        // disappear
        if (receipts.length == 0 && selectionToolbar.getVisibility() == View.VISIBLE) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            ViewFadingAnimator.fadeOut(selectionToolbar, 200);
        }
    }

    public void moveSelectedReceiptsToTrash() {
        Receipt[] receipts = adapter.getSelectedReceipts();

        if (receipts.length == 0)
            return;

        new MoveReceiptsToTrash(this, receipts).execute();
    }

    //////////////
    // FAB menu //
    //////////////

    @Override
    public void onMenuExpanded() {
        ViewFadingAnimator.fadeIn(fabCurtain, 200);
    }

    @Override
    public void onMenuCollapsed() {
        ViewFadingAnimator.fadeOut(fabCurtain, 200);
    }

    public void onFabCurtainClicked() {
        fabMenu.collapse();
    }

    public void createReceipt() {
        fabMenu.collapse();

        // receipt is not inserted into the list now,
        // but on refresh, when the detail activity finishes
        // (detail activity is launched by the task)
        new CreateNewReceipt(this).execute();
    }

    /**
     * Debugging tool: Creates a few of random receipts
     */
    public void boomReceipts() {
        fabMenu.collapse();

        AppDatabase db = AppDatabaseFactory.getInstance(this);

        for (int i = 0; i < 3; i++) {
            Receipt receipt = ReceiptFactory.make();
            db.receiptDao().insert(receipt);
            new RefreshReceipt(this, feeder, receipt.id).execute();
        }
    }

    public void snapNewReceipt() {
        fabMenu.collapse();

        Intent intent = new Intent(this, SnappingPipelineActivity.class);
        startActivity(intent);
    }
}
