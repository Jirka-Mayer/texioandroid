package io.texio.ui.receiptList;

import android.content.Context;
import android.os.AsyncTask;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.ui.receiptListView.DateSortedFeeder;

/**
 * Updates receipt data or removes the receipt if deleted
 */
public class RefreshReceipt extends AsyncTask<Void, Void, Void> {

    private AppDatabase db;
    private DateSortedFeeder feeder;

    private String receiptId;
    private Receipt receipt;
    private ReceiptState state;

    public RefreshReceipt(Context context, DateSortedFeeder feeder, String receiptId) {
        db = AppDatabaseFactory.getInstance(context);
        this.feeder = feeder;
        this.receiptId = receiptId;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        receipt = db.receiptDao().find(receiptId);
        state = ReceiptState.fromStateChanges(db.receiptStateChangeDao().of(receiptId));
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (isCancelled())
            return;

        if (receipt == null) {
            feeder.removeReceipt(receiptId);
        } else {
            if (feeder.containsReceipt(receiptId))
                feeder.updateReceipt(receipt, state);
            else
                feeder.insertReceipt(receipt, state);

            feeder.scrollToReceipt(receiptId);
        }
    }
}
