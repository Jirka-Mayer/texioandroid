package io.texio.ui.receiptList;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;

import java.lang.ref.WeakReference;

import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.receiptState.ReceiptState;

class UndoReceiptTrashing extends AsyncTask<Void, Void, Void> {

    private WeakReference<ReceiptListActivity> weakActivity;
    private AppDatabase db;

    private Receipt[] receipts;
    private ReceiptState[] states;

    public UndoReceiptTrashing(ReceiptListActivity activity, Receipt[] receipts) {
        weakActivity = new WeakReference<>(activity);
        db = AppDatabaseFactory.getInstance(activity);
        this.receipts = receipts;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (Receipt r : receipts)
            r.deletedAt = null;
        db.receiptDao().update(receipts);

        states = new ReceiptState[receipts.length];
        for (int i = 0; i < receipts.length; i++) {
            states[i] = ReceiptState.fromStateChanges(
                db.receiptStateChangeDao().of(receipts[i].id)
            );
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        ReceiptListActivity activity = weakActivity.get();

        if (isCancelled() || activity == null)
            return;

        for (int i = 0; i < receipts.length; i++)
            activity.feeder.insertReceipt(receipts[i], states[i]);

        Snackbar.make(
            activity.coordinatorLayout,
            R.string.receiptListActivity_receiptTrashingUndone,
            Snackbar.LENGTH_SHORT
        ).show();
    }
}
