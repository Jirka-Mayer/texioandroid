package io.texio.ui.receiptList;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;

import com.hypertrack.hyperlog.HyperLog;

import java.lang.ref.WeakReference;

import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;

class MoveReceiptsToTrash extends AsyncTask<Void, Void, Void> {

    public static final String TAG = MoveReceiptsToTrash.class.getSimpleName();

    private WeakReference<ReceiptListActivity> weakActivity;
    private AppDatabase db;

    private Receipt[] receipts;

    public MoveReceiptsToTrash(ReceiptListActivity activity, Receipt[] receipts) {
        weakActivity = new WeakReference<>(activity);
        db = AppDatabaseFactory.getInstance(activity);
        this.receipts = receipts;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for (Receipt r : receipts)
            r.deletedNow();
        db.receiptDao().update(receipts);
        HyperLog.i(TAG, "Receipts (" + receipts.length + ") have been moved to trash.");
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        ReceiptListActivity activity = weakActivity.get();

        if (isCancelled() || activity == null)
            return;

        activity.adapter.clearSelection();

        for (Receipt r : receipts)
            activity.feeder.removeReceipt(r.id);

        Snackbar.make(
            activity.coordinatorLayout,
            R.string.receiptListActivity_receiptsTrashed,
            Snackbar.LENGTH_LONG
        ).setAction(R.string.receiptListActivity_receiptsTrashingUndo, v -> {
            new UndoReceiptTrashing(activity, receipts).execute();
        }).show();
    }
}
