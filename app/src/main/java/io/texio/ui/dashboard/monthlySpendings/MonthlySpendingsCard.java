package io.texio.ui.dashboard.monthlySpendings;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import io.texio.Application;
import io.texio.R;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;

public class MonthlySpendingsCard extends CardView
{
    private BarChart chart;

    private String defaultCurrency = null;

    public MonthlySpendingsCard(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.card_monthly_spendings, null));

        this.chart = this.findViewById(R.id.chart);

        new LoadCardTask().execute();
    }

    //////////////////
    // LoadCardTask //
    //////////////////

    private class LoadCardTask extends AsyncTask<Void, Void, List<Float>>
    {
        protected Context context;

        public LoadCardTask()
        {
            this.context = MonthlySpendingsCard.this.getContext();
        }

        @Override
        protected List<Float> doInBackground(Void... args)
        {
            MonthlySpendingsCard.this.defaultCurrency = Application.getDefaultCurrency(this.context);

            // last 6 month receipt
            Calendar c = Calendar.getInstance();
            c.set(Calendar.MONTH, c.get(Calendar.MONTH) - 5);
            c.set(Calendar.DAY_OF_MONTH, 1);
            ReceiptDate date = ReceiptDate.fromCalendar(c);

            List<Receipt> receipts = AppDatabaseFactory
                .getInstance(this.context)
                .receiptDao()
                .newerThan(date.toString());

            // prepare bins
            List<Integer> months = new ArrayList<>(6);
            List<Float> totals = new ArrayList<>(6);

            for (int i = 0; i < 6; i++)
            {
                c = Calendar.getInstance();
                c.set(Calendar.MONTH, c.get(Calendar.MONTH) - i);
                months.add(c.get(Calendar.MONTH));
                totals.add(0.0f);
            }

            // sum spendings in default currency
            for (Receipt receipt : receipts)
            {
                if (!MonthlySpendingsCard.this.defaultCurrency.equals(receipt.currency))
                    continue;

                if (receipt.date == null)
                    continue;

                int index = months.indexOf(receipt.date.month);

                if (index == -1) // for example a receipt in the future
                    continue;

                totals.set(
                    index,
                    totals.get(index) + receipt.total / CurrencyAmount.FIXED_POINT_SHIFT
                );
            }

            // months are now backwards, fix it
            Collections.reverse(totals);

            return totals;
        }

        @Override
        protected void onPostExecute(List<Float> result)
        {
            MonthlySpendingsCard.this.chart.setValues(result);
        }
    }
}
