package io.texio.ui.dashboard.monthlySpendings;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.texio.R;

public class BarChart extends View
{
    private static float PADDING_DP = 10.0f; // only horizontal!
    private static float BAR_SPACE_DP = 5.0f;
    private static float TEXT_SIZE_DP = 14.0f;
    private static float TEXT_SPACE_DP = 26.0f;

    private float padding, barSpace, textSize, textSpace;

    private List<Float> values = null;
    private float maxValue;

    private Paint axisPaint, barPaint, activeBarPaint, textPaint, bedPaint;

    public BarChart(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.padding = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, PADDING_DP,
            this.getContext().getResources().getDisplayMetrics()
        );

        this.barSpace = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, BAR_SPACE_DP,
            this.getContext().getResources().getDisplayMetrics()
        );

        this.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DP,
            this.getContext().getResources().getDisplayMetrics()
        );

        this.textSpace = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SPACE_DP,
            this.getContext().getResources().getDisplayMetrics()
        );

        this.bedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.bedPaint.setStyle(Paint.Style.FILL);
        this.bedPaint.setColor(this.getContext().getResources().getColor(R.color.colorBackground));

        this.barPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.barPaint.setStyle(Paint.Style.FILL);
        this.barPaint.setColor(this.getContext().getResources().getColor(R.color.colorSecondary));

        this.activeBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.activeBarPaint.setStyle(Paint.Style.FILL);
        this.activeBarPaint.setColor(this.getContext().getResources().getColor(R.color.colorSecondaryLight));

        this.textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.textPaint.setStyle(Paint.Style.FILL);
        this.textPaint.setColor(Color.DKGRAY);
        this.textPaint.setTextSize(textSize);
    }

    public void setValues(List<Float> values)
    {
        this.values = values;

        this.maxValue = this.values.get(0);
        for (int i = 0; i < this.values.size(); i++)
        {
            if (this.values.get(i) > this.maxValue)
                this.maxValue = this.values.get(i);
        }

        // prevent division by zero
        if (this.maxValue < 1.0f)
            this.maxValue = 1.0f;

        this.invalidate();
    }

    ///////////////////
    // Drawing logic //
    ///////////////////

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if (this.values == null)
            return;

        // with space
        float barWidth = ((float)this.getMeasuredWidth() + barSpace - 2 * padding) / (float)this.values.size();

        for (int i = 0; i < this.values.size(); i++)
        {
            // bar
            canvas.drawRect(
                padding + barWidth * i,
                (this.getMeasuredHeight() - textSpace)
                    * (1.0f - this.values.get(i) / this.maxValue),
                padding + barWidth * (i + 1) - barSpace,
                this.getMeasuredHeight() - textSpace,
                i == this.values.size() - 1 ? this.activeBarPaint : this.barPaint
            );

            // bed
            canvas.drawRect(
                padding + barWidth * i,
                this.getMeasuredHeight() - textSpace - 5.0f,
                padding + barWidth * (i + 1) - barSpace,
                this.getMeasuredHeight() - textSpace,
                this.bedPaint
            );

            // month
            this.drawTextCentred(
                canvas,
                this.getMonthText(i),
                padding + barWidth * i + (barWidth - this.barSpace) / 2.0f,
                this.getMeasuredHeight() - this.textSpace / 2.0f
            );
        }
    }

    private String getMonthText(int i)
    {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) - (this.values.size() - i - 1));
        return new SimpleDateFormat("MMM").format(c.getTime());
    }

    private final Rect textBounds = new Rect();

    // helper to render centered text
    public void drawTextCentred(Canvas canvas, String text, float cx, float cy)
    {
        this.textPaint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(
            text,
            cx - textBounds.exactCenterX(),
            cy + textSize / 2.0f,
            this.textPaint
        );
    }
}
