package io.texio.ui.dashboard.budget;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import io.texio.Application;
import io.texio.R;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.ui.dialogs.TextFieldFragment;

public class BudgetCard extends CardView
{
    public static final String PREF_KEY_BUDGET = "budgetCard_monthlyBudget";

    protected TextView spentText;
    protected TextView budgetText;

    protected View noBudgetBody;
    protected View defaultBody;

    protected BudgetCardProgress budgetBar;
    protected TextView overviewText;

    protected Button setBudgetButton;
    protected Button clearBudgetButton;

    /**
     * What's the budget and how much is spent this month?
     */
    protected long budget = 0;
    protected long spent = 0;

    protected String defaultCurrency = "USD";

    public BudgetCard(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.card_budget, null));

        this.spentText = this.findViewById(R.id.spent_text);
        this.budgetText = this.findViewById(R.id.budget_text);
        this.noBudgetBody = this.findViewById(R.id.no_budget_body);
        this.defaultBody = this.findViewById(R.id.default_body);
        this.budgetBar = this.findViewById(R.id.budget_bar);
        this.overviewText = this.findViewById(R.id.overview_text);
        this.setBudgetButton = this.findViewById(R.id.set_budget_button);
        this.clearBudgetButton = this.findViewById(R.id.clear_budget_button);

        this.setBudgetButton.setOnClickListener(v -> this.showBudgetUpdateDialog());

        this.clearBudgetButton.setOnClickListener(v -> {
            this.budget = 0;
            this.updateUI();
        });

        this.updateUI();

        new LoadCardTask().execute();
    }

    public void updateUI()
    {
        this.spentText.setText(
            CurrencyAmount.formatHumanReadable(
                this.getContext(),
                this.spent,
                this.defaultCurrency
            )
        );

        if (this.budget == 0)
        {
            this.noBudgetBody.setVisibility(View.VISIBLE);
            this.defaultBody.setVisibility(View.GONE);
            this.budgetText.setText("");
        }
        else
        {
            this.noBudgetBody.setVisibility(View.GONE);
            this.defaultBody.setVisibility(View.VISIBLE);

            this.budgetText.setText("/ " + CurrencyAmount.formatHumanReadable(
                this.getContext(),
                this.budget,
                this.defaultCurrency
            ));

            if (this.spent <= this.budget)
            {
                this.budgetBar.setState(1.0f - (float)this.spent / (float)this.budget);

                this.overviewText.setText(Html.fromHtml(
                    this.getResources().getString(
                        R.string.budgetCard_overviewText,
                        CurrencyAmount.formatHumanReadable(
                            this.getContext(),
                            this.budget - this.spent,
                            this.defaultCurrency
                        )
                    )
                ));
            }
            else
            {
                this.budgetBar.setState(0);

                this.overviewText.setText(Html.fromHtml(
                    this.getResources().getString(
                        R.string.budgetCard_overviewTextOverBudget,
                        CurrencyAmount.formatHumanReadable(
                            this.getContext(),
                            this.spent - this.budget,
                            this.defaultCurrency
                        )
                    )
                ));
            }
        }
    }

    public void showBudgetUpdateDialog()
    {
        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_NUMBER);
        args.putString("title", this.getResources().getString(R.string.budgetCard_setBudget));
        args.putString("defaultContent", "");

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;

            this.budget = CurrencyAmount.parse(text);

            // round to units
            this.budget -= this.budget % CurrencyAmount.FIXED_POINT_SHIFT;

            this.saveBudget();
            this.updateUI();
        });

        dialog.show(((AppCompatActivity)this.getContext()).getSupportFragmentManager(), "budget");
    }

    public void saveBudget()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getContext());

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(PREF_KEY_BUDGET, this.budget);
        editor.apply();
    }

    //////////////////
    // LoadCardTask //
    //////////////////

    private class LoadCardTask extends AsyncTask<Void, Void, Void>
    {
        protected Context context;

        public LoadCardTask()
        {
            this.context = BudgetCard.this.getContext();
        }

        @Override
        protected Void doInBackground(Void... args)
        {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
            BudgetCard.this.defaultCurrency = Application.getDefaultCurrency(this.context);
            BudgetCard.this.budget = sharedPref.getLong(PREF_KEY_BUDGET, 0);

            // this month receipts
            ReceiptDate date = ReceiptDate.today();
            date.day = 1;

            List<Receipt> receipts = AppDatabaseFactory
                .getInstance(this.context)
                .receiptDao()
                .newerThan(date.toString());

            // sum spendings in default currency
            long total = 0;
            for (Receipt receipt : receipts)
            {
                if (!BudgetCard.this.defaultCurrency.equals(receipt.currency))
                    continue;

                if (receipt.date == null)
                    continue;

                total += receipt.total;
            }

            // round to units (down, because who cares...)
            total -= total % CurrencyAmount.FIXED_POINT_SHIFT;

            BudgetCard.this.spent = total;

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            BudgetCard.this.updateUI();
        }
    }
}
