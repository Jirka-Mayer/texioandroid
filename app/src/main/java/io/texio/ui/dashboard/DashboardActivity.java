package io.texio.ui.dashboard;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import io.texio.R;
import io.texio.ui.NavigationActivity;
import io.texio.ui.receiptList.ReceiptListActivity;
import io.texio.ui.snappingPipeline.SnappingPipelineActivity;

public class DashboardActivity extends NavigationActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_dashboard);

        // setup app bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle(this.getResources().getString(R.string.dashboardActivity_title));
        this.integrateNavigationWithActionBar();
    }

    /**
     * FAB click
     */
    public void snapNewReceipt(View v)
    {
        Intent intent = new Intent(this, SnappingPipelineActivity.class);
        this.startActivity(intent);
    }
}
