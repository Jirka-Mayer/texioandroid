package io.texio.ui.dashboard.budget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import io.texio.R;

public class BudgetCardProgress extends View
{
    /**
     * Progress state (0 to 1)
     */
    private float state = 0.0f;

    private Paint paint;

    public BudgetCardProgress(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setColor(this.getContext().getResources().getColor(R.color.colorSecondaryDark));
        this.paint.setStyle(Paint.Style.FILL);
    }

    public void setState(float state)
    {
        this.state = state;

        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawRect(
            0, 0,
            this.getMeasuredWidth() * this.state,
            this.getMeasuredHeight(),
            this.paint
        );
    }
}
