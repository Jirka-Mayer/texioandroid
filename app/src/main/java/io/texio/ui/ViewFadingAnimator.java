package io.texio.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/**
 * Animates view fade-in and fade-out by animating alpha and setting visibility
 * to {@link android.view.View#VISIBLE} or {@link android.view.View#INVISIBLE}
 */
public class ViewFadingAnimator {

    public static void fadeIn(View view, int millis) {
        if (view.getVisibility() == View.VISIBLE)
            return;

        view.setVisibility(View.VISIBLE);
        view.setAlpha(0);

        view.animate().setDuration(millis).alpha(1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }
        });
    }

    public static void fadeOut(View view, int millis) {
        if (view.getVisibility() == View.INVISIBLE)
            return;

        view.animate().setDuration(millis).alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                view.setVisibility(View.INVISIBLE);
            }
        });
    }

}
