package io.texio.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import io.texio.R;
import io.texio.ui.NavigationActivity;

public class SettingsActivity extends NavigationActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_settings);

        // setup app bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle(this.getResources().getString(R.string.settingsActivity_title));
        this.integrateNavigationWithActionBar();

        // show settings fragment
        this.getFragmentManager().beginTransaction()
            .add(R.id.fragment_container, new SettingsFragment())
            .commit();
    }
}
