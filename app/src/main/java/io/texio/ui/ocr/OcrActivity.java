package io.texio.ui.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptFileManager;
import io.texio.db.receipt.ReceiptSnapManager;
import io.texio.engine.OcrPipeline;
import io.texio.engine.TextPipeline;
import io.texio.engine.text.ShardBag;

public class OcrActivity extends AppCompatActivity
{
    protected ConstraintLayout processingScreen;
    protected ConstraintLayout dataScreen;
    protected ConstraintLayout noDataScreen;

    protected TextView statusText;
    protected ProgressBar progressBar;
    protected RecyclerView dataList;

    protected DataListAdapter adapter;

    protected Receipt receipt;
    protected ShardBag extractedData = null;

    protected OcrTask ocrTask;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_ocr);

        this.processingScreen = this.findViewById(R.id.processing_screen);
        this.dataScreen = this.findViewById(R.id.data_screen);
        this.noDataScreen = this.findViewById(R.id.no_data_screen);

        this.statusText = this.findViewById(R.id.status_text);
        this.progressBar = this.findViewById(R.id.progress_bar);
        this.dataList = this.findViewById(R.id.data_list);

        // load receipt
        this.receipt = Receipt.fromBundle(
            this.getIntent().getBundleExtra(IntentExtras.EXTRA_RECEIPT),
            this
        );

        // setup UI
        this.processingScreen.setVisibility(View.VISIBLE);
        this.dataScreen.setVisibility(View.GONE);
        this.noDataScreen.setVisibility(View.GONE);

        this.progressBar.setIndeterminate(true);

        // start the process
        this.ocrTask = new OcrTask();
        this.ocrTask.execute();
    }

    public void cancelButtonClick(View view)
    {
        if (this.ocrTask != null)
            this.ocrTask.cancel(true);

        Intent data = new Intent();
        data.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());

        // not really a cancel, since there may have been some receipt.text extracted
        this.setResult(RESULT_FIRST_USER, data);
        this.finish();
    }

    @Override
    protected void onDestroy()
    {
        if (this.ocrTask != null)
            this.ocrTask.cancel(true);

        super.onDestroy();
    }

    public void displayExtractedData()
    {
        if (this.extractedData.binCount() == 0)
        {
            this.processingScreen.setVisibility(View.GONE);
            this.dataScreen.setVisibility(View.GONE);
            this.noDataScreen.setVisibility(View.VISIBLE);
            return;
        }

        this.adapter = new DataListAdapter(this.extractedData);

        this.dataList = this.findViewById(R.id.data_list);
        this.dataList.setHasFixedSize(false);
        this.dataList.setLayoutManager(new LinearLayoutManager(this));
        this.dataList.addItemDecoration(
            new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        );
        this.dataList.setAdapter(this.adapter);

        this.processingScreen.setVisibility(View.GONE);
        this.dataScreen.setVisibility(View.VISIBLE);
    }

    public void takeButtonClick(View view)
    {
        this.extractedData.apply(this.receipt);

        new SaveReceiptTask().execute();

        Intent data = new Intent();
        data.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());

        this.setResult(RESULT_OK, data);
        this.finish();
    }

    /////////////////////
    // Processing Task //
    /////////////////////

    /**
     * The task that performs the data extraction on all provided snaps
     */
    private class OcrTask extends AsyncTask<Void, OcrTask.TaskProgress, ShardBag>
    {
        /**
         * Holds data about progress that should be displayed to the user
         */
        public class TaskProgress
        {
            public String word;
            public double progress;

            public TaskProgress(String word, double progress)
            {
                this.word = word;
                this.progress = progress;
            }
        }

        /**
         * Actual implementation of an extraction controller that provides communication
         * between the extraction process and the UI that controls the process.
         */
        private class TaskExtractionController implements OcrPipeline.ExtractionController
        {
            private int snapsDone, snapCount;

            private int wordCount = 0;
            private int classifiedWordCount = 0;

            public TaskExtractionController(int snapCount, int snapsDone)
            {
                this.snapCount = snapCount;
                this.snapsDone = snapsDone;
            }

            @Override
            public void setWordCount(int count)
            {
                this.wordCount = count;
                this.classifiedWordCount = 0;
                this.updateUI(null);
            }

            @Override
            public void wordHasBeenClassified(String word)
            {
                this.classifiedWordCount++;
                this.updateUI(word);
            }

            @Override
            public boolean isCancelled()
            {
                return OcrTask.this.isCancelled();
            }

            private void updateUI(String word)
            {
                double snapProgress = (double)this.classifiedWordCount / (double)this.wordCount;
                double totalProgress = (snapProgress + this.snapsDone) / (double)this.snapCount;

                OcrTask.this.publishProgress(
                    new TaskProgress(word, totalProgress)
                );
            }
        }

        @Override
        protected ShardBag doInBackground(Void... arguments)
        {
            OcrPipeline ocrPipeline = new OcrPipeline(OcrActivity.this);

            StringBuilder receiptText = new StringBuilder();

            // process each snap separately
            ReceiptFileManager fileManager = new ReceiptFileManager(
                OcrActivity.this,
                OcrActivity.this.receipt
            );
            ReceiptSnapManager snapManager = new ReceiptSnapManager(fileManager);
            List<String> snaps = snapManager.getSnapList();
            for (int snapsDone = 0; snapsDone < snaps.size(); snapsDone++)
            {
                String snap = snaps.get(snapsDone);

                // load snap as bitmap
                byte[] snapData = fileManager.readFileBytes(snap);
                Bitmap bitmap = BitmapFactory.decodeByteArray(snapData, 0, snapData.length);

                // extract text from the snap
                String extractedText = ocrPipeline.extractText(
                    bitmap,
                    new TaskExtractionController(snaps.size(), snapsDone)
                );

                // extracted text can be null only if the process has been canceled
                if (extractedText == null || this.isCancelled())
                    return null;

                // append the extracted text to the receipt text
                receiptText.append(extractedText);
                receiptText.append("\n\n");
            }

            // save receipt text
            OcrActivity.this.receipt.text = receiptText.toString();
            AppDatabaseFactory.getInstance(OcrActivity.this).receiptDao().update(
                OcrActivity.this.receipt
            );

            // extract information
            TextPipeline textPipeline = new TextPipeline(
                OcrActivity.this,
                OcrActivity.this.receipt.text
            );
            return textPipeline.extractAll();
        }

        @Override
        protected void onProgressUpdate(TaskProgress... values)
        {
            if (values.length == 0)
                return;

            // text
            if (values[0].word != null)
                OcrActivity.this.statusText.setText(values[0].word);

            // progress bar
            OcrActivity.this.progressBar.setIndeterminate(false);
            OcrActivity.this.progressBar.setMax(1000);
            OcrActivity.this.progressBar.setProgress((int)(values[0].progress * 1000));
        }

        @Override
        protected void onPostExecute(ShardBag result)
        {
            // task cancelled
            if (result == null || this.isCancelled())
                return;

            OcrActivity.this.extractedData = result;

            OcrActivity.this.displayExtractedData();
        }
    }

    /////////////////
    // Saving task //
    /////////////////

    private class SaveReceiptTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... arguments)
        {
            AppDatabaseFactory.getInstance(OcrActivity.this).receiptDao().update(
                OcrActivity.this.receipt
            );

            return null;
        }
    }
}
