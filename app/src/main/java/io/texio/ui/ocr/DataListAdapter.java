package io.texio.ui.ocr;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import io.texio.R;
import io.texio.engine.text.ShardBag;

public class DataListAdapter extends RecyclerView.Adapter<DataListAdapter.ViewHolder>
{
    protected ShardBag data;

    public DataListAdapter(ShardBag data)
    {
        this.data = data;
    }

    @NonNull
    @Override
    public DataListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(
            R.layout.view_ocr_data_list_item, parent, false
        );
        return new DataListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.setItem(this.data.getBin(position));
    }

    @Override
    public int getItemCount()
    {
        return this.data.binCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private ShardBag.Bin item;

        private TextView name;
        private TextView value;
        private CheckBox checkbox;

        public ViewHolder(View itemView)
        {
            super(itemView);

            this.name = itemView.findViewById(R.id.data_name);
            this.value = itemView.findViewById(R.id.data_value);
            this.checkbox = itemView.findViewById(R.id.accepted_checkbox);

            this.checkbox.setOnClickListener(v -> {
                this.item.accepted = this.checkbox.isChecked();
            });
        }

        public void setItem(ShardBag.Bin item)
        {
            this.item = item;

            // update UI
            this.name.setText(item.get(0).getTitle(this.name.getContext()));

            if (this.item.size() == 0)
            {
                this.value.setText("<error: no values>");
            }
            else
            {
                this.value.setText(this.item.get(0).getValue(this.value.getContext()));
            }

            this.checkbox.setChecked(this.item.accepted);
        }
    }
}
