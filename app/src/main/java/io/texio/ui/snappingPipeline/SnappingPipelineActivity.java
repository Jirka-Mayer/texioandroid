package io.texio.ui.snappingPipeline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.Snaps;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptSnapManager;
import io.texio.ui.camera.CameraActivity;
import io.texio.ui.camera.ExternalCameraActivity;
import io.texio.ui.ocr.OcrActivity;
import io.texio.ui.receiptDetail.SnapDetailActivity;
import io.texio.ui.receiptList.ReceiptListActivity;

/**
 * Snaps a new receipt and creates the record
 *
 * User taps camera icon ->
 *  -> SnappingPipelineActivity is launched
 *      -> CameraActivity is launched
 *          -> Let's the user take multiple snaps and crop them
 *          -> returns cropped snaps
 *      -> creates new receipt with the provided snaps
 *      -> launches OcrActivity
 *          -> it reads snaps and fills data into the receipt
 *      -> done
 */
public class SnappingPipelineActivity extends AppCompatActivity
{
    public static final int REQUEST_CAMERA_TAKE_SNAPS = 1;
    public static final int REQUEST_PERFORM_OCR = 2;

    /**
     * The receipt that's being constructed
     */
    private Receipt receipt = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_snapping_pipeline);

        // prepare the space
        Snaps.clearDirectory(this);

        // start the pipeline
        this.openCameraActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CAMERA_TAKE_SNAPS)
        {
            // camera activity took no snaps and exited -> exit completely
            if (resultCode == RESULT_CANCELED)
            {
                this.finish();
                return;
            }

            // camera activity returned some snaps, let's continue to the ocr activity
            if (resultCode == RESULT_OK)
            {
                new CreateReceiptTask(
                    data.getStringArrayExtra(IntentExtras.EXTRA_SNAPS)
                ).execute();
            }
        }

        if (requestCode == REQUEST_PERFORM_OCR)
        {
            // pull updated receipt if not total fail
            if (resultCode != RESULT_CANCELED)
            {
                this.receipt.soakUpBundle(data.getBundleExtra(IntentExtras.EXTRA_RECEIPT));
            }

            // open receipt detail regardless of the OCR success because we have
            // the receipt created now anyway
            this.goToReceiptDetail();
        }
    }

    /**
     * Starts camera activity to take the snaps
     */
    protected void openCameraActivity()
    {
        Intent intent = new Intent(this, CameraActivity.class);
        this.startActivityForResult(intent, REQUEST_CAMERA_TAKE_SNAPS);
    }

    /**
     * Starts the OCR activity
     */
    private void performOCR()
    {
        Intent intent = new Intent(this, OcrActivity.class);
        intent.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());
        this.startActivityForResult(intent, REQUEST_PERFORM_OCR);
    }

    /**
     * Opens receipt detail and closes this activity
     */
    private void goToReceiptDetail()
    {
        // close this activity
        this.finish();

        // tell receipt list activity to open receipt detail
        // (also make sure the back stack to the receipt list activity is correct)
        Intent receiptListIntent = new Intent(this, ReceiptListActivity.class);
        receiptListIntent.putExtra(IntentExtras.EXTRA_OPEN_RECEIPT, true);
        receiptListIntent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, this.receipt.id);
        receiptListIntent.setFlags(receiptListIntent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        this.startActivities(
            TaskStackBuilder.create(this)
                .addNextIntentWithParentStack(receiptListIntent)
                .getIntents()
        );
    }

    ///////////////////////
    // CreateReceiptTask //
    ///////////////////////

    private class CreateReceiptTask extends AsyncTask<Void, Void, Void>
    {
        private String[] snapNames;

        public CreateReceiptTask(String[] snapNames)
        {
            this.snapNames = snapNames;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            // create new receipt
            Receipt receipt = Receipt.createNew(SnappingPipelineActivity.this);
            AppDatabaseFactory.getInstance(SnappingPipelineActivity.this).receiptDao().insert(receipt);

            // and put the snaps into the receipt folder
            ReceiptSnapManager snapManager = new ReceiptSnapManager(
                SnappingPipelineActivity.this,
                receipt
            );
            for (String name : snapNames)
                snapManager.addSnap(Snaps.file(SnappingPipelineActivity.this, name));

            // put receipt into the activity instance
            SnappingPipelineActivity.this.receipt = receipt;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            SnappingPipelineActivity.this.performOCR();
        }
    }
}
