package io.texio.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import io.texio.Application;
import io.texio.R;
import io.texio.ui.dashboard.DashboardActivity;
import io.texio.ui.integration.IntegrationActivity;
import io.texio.ui.receiptList.ReceiptListActivity;
import io.texio.ui.settings.SettingsActivity;
import io.texio.ui.trash.TrashActivity;

/**
 * Super class for any activity that contains the main navigation drawer
 */
public class NavigationActivity extends AppCompatActivity {

    private FrameLayout activityBody;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView headerBackground;

    private int debugModeClicks = 0;
    private static final int REQUIRED_DEBUG_MODE_CLICKS = 7;
    private static final int HIDDEN_DEBUG_MODE_CLICKS = 2;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_navigation);

        activityBody = this.findViewById(R.id.activity_body);
        drawerLayout = this.findViewById(R.id.drawer_layout);
        navigationView = this.findViewById(R.id.navigation_view);
        headerBackground = navigationView.getHeaderView(0).findViewById(R.id.navigation_header_background);

        headerBackground.setOnClickListener(v -> this.handleDebugModeClick());

        activityBody.addView(
            this.getLayoutInflater().inflate(layoutResID, null)
        );

        navigationView.setNavigationItemSelectedListener(this::menuItemClicked);

        makeStatusBarTranslucent();
        highlightProperMenuItem();
    }

    @Override
    public void setContentView(View view) {
        throw new RuntimeException("Method not overloaded.");
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        throw new RuntimeException("Method not overloaded.");
    }

    private void makeStatusBarTranslucent() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setTranslucentStatusFlag(true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setTranslucentStatusFlag(false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setTranslucentStatusFlag(boolean on) {
        if (Build.VERSION.SDK_INT >= 19) {
            Window win = getWindow();
            WindowManager.LayoutParams winParams = win.getAttributes();
            final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            if (on) {
                winParams.flags |= bits;
            } else {
                winParams.flags &= ~bits;
            }
            win.setAttributes(winParams);
        }
    }

    /**
     * Sets up the menu icon instead of the back arrow icon
     */
    protected void integrateNavigationWithActionBar() {
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    private void highlightProperMenuItem() {
        if (this instanceof DashboardActivity)
            this.navigationView.getMenu().findItem(R.id.nav_dashboard).setChecked(true);

        if (this instanceof ReceiptListActivity)
            this.navigationView.getMenu().findItem(R.id.nav_receipts).setChecked(true);

        if (this instanceof TrashActivity)
            this.navigationView.getMenu().findItem(R.id.nav_trash).setChecked(true);

        if (this instanceof IntegrationActivity)
            this.navigationView.getMenu().findItem(R.id.nav_integration).setChecked(true);

        if (this instanceof SettingsActivity)
            this.navigationView.getMenu().findItem(R.id.nav_settings).setChecked(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigation opening by tapping the menu icon
        if (item.getItemId() == android.R.id.home && drawerLayout != null) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // closing navigation by pressing the back button
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    /**
     * Closes the navigation drawer if opened
     */
    public void closeNavigationDrawer() {
        if (drawerLayout != null)
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    private boolean menuItemClicked(@NonNull MenuItem menuItem) {
        // close drawer when item is tapped
        closeNavigationDrawer();

        Intent intent;

        if (!(this instanceof DashboardActivity))
            finish();

        // goto a given activity
        switch (menuItem.getItemId()) {
            case R.id.nav_dashboard:
                intent = new Intent(this, DashboardActivity.class);
                break;

            case R.id.nav_receipts:
                intent = new Intent(this, ReceiptListActivity.class);
                break;

            case R.id.nav_trash:
                intent = new Intent(this, TrashActivity.class);
                break;

            case R.id.nav_integration:
                intent = new Intent(this, IntegrationActivity.class);
                break;

            case R.id.nav_settings:
                intent = new Intent(this, SettingsActivity.class);
                break;

            default:
                throw new RuntimeException("Given menu item click is not implemented.");
        }

        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);

        return true;
    }

    private void handleDebugModeClick() {
        this.debugModeClicks += 1;

        if (this.debugModeClicks > HIDDEN_DEBUG_MODE_CLICKS
            && this.debugModeClicks < REQUIRED_DEBUG_MODE_CLICKS)
        {
            if (!Application.getDebugMode(this)) {
                Toast.makeText(
                    this,
                    (REQUIRED_DEBUG_MODE_CLICKS - this.debugModeClicks) + " more clicks to become a tester.",
                    Toast.LENGTH_SHORT
                ).show();
            } else {
                Toast.makeText(
                    this,
                    (REQUIRED_DEBUG_MODE_CLICKS - this.debugModeClicks) + " more clicks to stop being tester.",
                    Toast.LENGTH_SHORT
                ).show();
            }
        }

        if (this.debugModeClicks >= REQUIRED_DEBUG_MODE_CLICKS) {
            if (Application.getDebugMode(this)) {
                Toast.makeText(
                    this,
                    "Goodbye, tester.",
                    Toast.LENGTH_SHORT
                ).show();

                Application.setDebugMode(this, false);
            } else {
                Toast.makeText(
                    this,
                    "Now you are a tester!",
                    Toast.LENGTH_SHORT
                ).show();

                Application.setDebugMode(this, true);
            }

            this.debugModeClicks = 0;
        }
    }
}
