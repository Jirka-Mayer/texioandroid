package io.texio.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import io.texio.R;

public class ReceiptCropper extends View
{
    private static final float FRAME_STROKE_WIDTH = 5.0f;
    private static final float FRAME_PAD_RADIUS = 30.0f;
    private static final float FRAME_PAD_TOUCH_RADIUS = 120.0f; // 4x

    /**
     * Bitmap of the snap at full resolution
     */
    protected Bitmap snapBitmap = null;

    /**
     * Pads of the cropping frame
     * Ordered clockwise beginning with the top left corner (in the bitmap space)
     * (0-3 ... corners, ordered like said)
     * (4-7 ... edges, ordered same way, only on edges)
     */
    private Pad[] pads;

    /**
     * Matrix to transform from bitmap space to view space
     * (and the inverse)
     */
    private Matrix displayMatrix;
    private Matrix displayMatrixInverse;

    /**
     * Image rotation in 90 degree turns clockwise (0, 1, 2, 3) are possible values
     */
    private int snapRotation = 0;

    private Paint frameStrokePaint;
    private Paint framePadPaint;

    public ReceiptCropper(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.pads = new Pad[8];
        for (int i = 0; i < this.pads.length; i++)
            this.pads[i] = new Pad(new Point(0, 0), i);

        this.displayMatrix = new Matrix();
        this.displayMatrixInverse = new Matrix();

        this.frameStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.frameStrokePaint.setColor(
            this.getContext().getResources().getColor(R.color.colorSecondary)
        );
        this.frameStrokePaint.setStyle(Paint.Style.STROKE);

        this.framePadPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.framePadPaint.setColor(
            this.getContext().getResources().getColor(R.color.colorBackground)
        );
        this.framePadPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    public void setBitmap(Bitmap bitmap)
    {
        this.snapBitmap = bitmap;
        this.resetPads();

        this.invalidate();
    }

    public void setCorners(Point[] corners)
    {
        if (corners == null)
        {
            this.resetPads();
        }
        else
        {
            for (int i = 0; i < 4; i++)
                this.pads[i].moveTo(corners[i].x, corners[i].y);
        }

        this.invalidate();
    }

    public Point[] getCorners()
    {
        Point[] out = new Point[4];

        for (int i = 0; i < 4; i++)
            out[i] = new Point(this.pads[i].position);

        return out;
    }

    public void setSnapRotation(int rotation)
    {
        this.snapRotation = rotation % 4;
        this.invalidate();
    }

    public int getSnapRotation()
    {
        return this.snapRotation;
    }

    /////////////////////
    // Transformations //
    /////////////////////

    private void updateDisplayMatrices()
    {
        this.displayMatrix.reset();

        float bitmapWidth = this.snapBitmap.getWidth();
        float bitmapHeight = this.snapBitmap.getHeight();

        float viewWidth = this.getMeasuredWidth();
        float viewHeight = this.getMeasuredHeight();

        float scale, top, left, tmp;

        // rotate (+ adjust width/height)
        this.displayMatrix.postRotate(90 * this.snapRotation);

        switch (this.snapRotation)
        {
            case 0:
                break;

            case 1:
                tmp = bitmapWidth; bitmapWidth = bitmapHeight; bitmapHeight = tmp;
                this.displayMatrix.postTranslate(bitmapWidth, 0);
                break;

            case 2:
                this.displayMatrix.postTranslate(bitmapWidth, bitmapHeight);
                break;

            case 3:
                tmp = bitmapWidth; bitmapWidth = bitmapHeight; bitmapHeight = tmp;
                this.displayMatrix.postTranslate(0, bitmapHeight);
                break;
        }

        // fit to screen

        // bitmap too tall so fit vertically
        if ((viewWidth / viewHeight) > (bitmapWidth / bitmapHeight))
        {
            scale = viewHeight / bitmapHeight;
            top = 0;
            left = viewWidth / 2.0f - bitmapWidth * scale / 2.0f;
        }
        // bitmap too wide so fit horizontally
        else
        {
            scale = viewWidth / bitmapWidth;
            left = 0;
            top = viewHeight / 2.0f - bitmapHeight * scale / 2.0f;
        }

        this.displayMatrix.postScale(scale, scale);
        this.displayMatrix.postTranslate(left, top);

        // calculate the inverse
        this.displayMatrix.invert(this.displayMatrixInverse);
    }

    ////////////////////
    // Touch handling //
    ////////////////////

    /**
     * Currently moved pad
     */
    private Pad activePad = null;

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float[] pt;
        int action = event.getActionMasked();

        if (event.getActionIndex() != 0)
            return false;

        switch (action)
        {
            case MotionEvent.ACTION_DOWN:
                // transform to bitmap space
                pt = new float[2];
                pt[0] = event.getX();
                pt[1] = event.getY();
                this.displayMatrixInverse.mapPoints(pt);

                // get the closest pad to the click
                this.activePad = this.pads[0];
                float d = this.pads[0].distanceSquared(pt[0], pt[1]);
                float tmp;

                for (int i = 1; i < 4; i++)
                {
                    tmp = this.pads[i].distanceSquared(pt[0], pt[1]);
                    if (tmp < d)
                    {
                        this.activePad = this.pads[i];
                        d = tmp;
                    }
                }

                // click is in the pad range (squared distances)
                if (d <= FRAME_PAD_TOUCH_RADIUS * FRAME_PAD_TOUCH_RADIUS)
                {
                    this.invalidate();
                    return true;
                }

                this.activePad = null;
                return false;

            case MotionEvent.ACTION_MOVE:
                if (this.activePad == null)
                    return false;

                // transform to bitmap space
                pt = new float[2];
                pt[0] = event.getX();
                pt[1] = event.getY();
                this.displayMatrixInverse.mapPoints(pt);

                // move the pad
                this.activePad.moveTo(pt[0], pt[1]);

                this.invalidate();
                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_OUTSIDE:
                this.activePad = null;
                this.invalidate();
                return true;
        }

        return false;
    }

    private class Pad
    {
        public Point position;
        public int index;

        public Pad(Point position, int index)
        {
            this.position = position;
            this.index = index;
        }

        /**
         * Returns distance squared to the pad
         */
        public float distanceSquared(float x, float y)
        {
            return (x - this.position.x) * (x - this.position.x) + (y - this.position.y) * (y - this.position.y);
        }

        public void moveTo(float x, float y)
        {
            x = ReceiptCropper.clamp(x, FRAME_PAD_RADIUS, ReceiptCropper.this.snapBitmap.getWidth() - FRAME_PAD_RADIUS);
            y = ReceiptCropper.clamp(y, FRAME_PAD_RADIUS, ReceiptCropper.this.snapBitmap.getHeight() - FRAME_PAD_RADIUS);

            this.position.x = (int)x;
            this.position.y = (int)y;

            if (this.index < 4)
                ReceiptCropper.this.repositionEdgePads();
        }
    }

    /**
     * Position pads to the corners of the image
     */
    private void resetPads()
    {
        this.pads[0].moveTo(0, 0);
        this.pads[1].moveTo(this.snapBitmap.getWidth(), 0);
        this.pads[2].moveTo(this.snapBitmap.getWidth(), this.snapBitmap.getHeight());
        this.pads[3].moveTo(0, this.snapBitmap.getHeight());
    }

    private void repositionEdgePads()
    {
        // do stuff in the next versions
    }

    /**
     * Helper function for clamping value between boundaries
     */
    private static float clamp(float value, float min, float max)
    {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }

    ///////////////////
    // Drawing logic //
    ///////////////////

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if (this.snapBitmap == null)
            return;

        this.updateDisplayMatrices();

        // apply the display matrix
        canvas.save();
        canvas.concat(this.displayMatrix);

        // draw snap bitmap
        canvas.drawBitmap(this.snapBitmap, 0, 0, null);

        // draw cropping frame
        this.drawCroppingFrame(canvas);

        // restore previous canvas matrix
        canvas.restore();
    }

    /**
     * Draws the frame that represents the cropped area
     */
    private void drawCroppingFrame(Canvas canvas)
    {
        this.frameStrokePaint.setStrokeWidth(
            this.displayMatrixInverse.mapRadius(FRAME_STROKE_WIDTH)
        );

        // draw frame lines
        for (int i = 0; i < 4; i++)
            this.drawLine(
                canvas,
                this.pads[i].position,
                this.pads[(i + 1) % 4].position,
                this.frameStrokePaint
            );

        // currently draw only the first 4 pads
        for (int i = 0; i < 4; i++)
            canvas.drawCircle(
                this.pads[i].position.x,
                this.pads[i].position.y,
                this.displayMatrixInverse.mapRadius(FRAME_PAD_RADIUS),
                this.framePadPaint
            );
    }

    /**
     * Line drawing helper (Point to float)
     */
    private void drawLine(Canvas canvas, Point a, Point b, Paint p)
    {
        canvas.drawLine(a.x, a.y, b.x, b.y, p);
    }
}
