package io.texio.ui.receiptListView;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.ui.receiptDetail.ReceiptStateBadge;

/**
 * Holder for a view displaying a receipt
 */
public class ReceiptViewHolder extends RecyclerView.ViewHolder {

    private ReceiptListItem item;

    private Selection selection;
    private OnClickListener clickListener;

    private FrameLayout frame;
    private TextView storeText;
    private TextView dateText;
    private TextView totalText;
    private ImageView thumbnail;
    private ImageView selectedThumbnail;
    private ReceiptStateBadge stateBadge;

    public interface OnClickListener {
        void onClick(Receipt receipt);
    }

    /**
     * Creates new receipt view holder instance
     * @param itemView the inflated item view
     * @param clickListener called if the user clicks the receipt
     * @param selection if null, selection is disabled for this list
     */
    public ReceiptViewHolder(View itemView, OnClickListener clickListener, Selection selection) {
        super(itemView);

        this.selection = selection;
        this.clickListener = clickListener;

        View content = itemView.findViewById(R.id.content);
        content.setOnClickListener(v -> itemClicked());

        frame = itemView.findViewById(R.id.frame);
        storeText = itemView.findViewById(R.id.store_group);
        dateText = itemView.findViewById(R.id.date);
        totalText = itemView.findViewById(R.id.total);
        thumbnail = itemView.findViewById(R.id.thumbnail);
        selectedThumbnail = itemView.findViewById(R.id.selected_thumbnail);
        stateBadge = itemView.findViewById(R.id.receipt_state);

        if (selection != null) {
            thumbnail.setClickable(true);
            thumbnail.setOnClickListener(v -> toggleSelection());
            content.setOnLongClickListener(v -> {
                toggleSelection();
                return true;
            });
        }
    }

    private void toggleSelection() {
        if (item == null)
            return;

        this.selection.toggle(item.getReceipt());
        updateSelectionDisplay();
    }

    private void itemClicked() {
        if (item == null)
            return;

        if (this.selection != null && !this.selection.isEmpty()) {
            toggleSelection();
            return;
        }

        clickListener.onClick(item.getReceipt());
    }

    /**
     * Set the item to be displayed
     */
    public void setReceiptListItem(ReceiptListItem receiptListItem) {
        item = receiptListItem;

        setReceipt(item.getReceipt());
        setState(item.getReceiptState());

        updateSelectionDisplay();
    }

    private void setReceipt(Receipt receipt) {
        if (receipt == null)
            return;

        // store name
        if (receipt.store == null) {
            storeText.setText(R.string.receiptDetail_unknownStore);
            storeText.setTypeface(null, Typeface.NORMAL);
        } else {
            storeText.setText(receipt.store);
            storeText.setTypeface(null, Typeface.BOLD);
        }

        // datetime
        String datetime = "";
        ReceiptDate date = receipt.date;
        //ReceiptTime time = receipt.getTime();
        if (date != null)
            datetime += date.formatHumanReadable();
            /*if (time != null)
                datetime += " " + time.formatHumanReadable();*/
        dateText.setText(datetime.trim());

        // total amount
        if (receipt.total == 0)
            totalText.setText("");
        else
            totalText.setText(
                CurrencyAmount.formatHumanReadable(
                    totalText.getContext(),
                    receipt.total,
                    receipt.currency == null ? "CZK" : receipt.currency
                )
            );
    }

    private void setState(ReceiptState receiptState) {
        ReceiptStateEnum state = receiptState == null ? ReceiptStateEnum.UNUSED : receiptState.getState();

        stateBadge.setState(state);

        if (state == ReceiptStateEnum.UNUSED) {
            stateBadge.setVisibility(View.INVISIBLE);
        } else {
            stateBadge.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Updates how the view looks based on the selection state of the displayed receipt
     * May be called from outside the class
     */
    public void updateSelectionDisplay() {
        if (selection == null) // selection is disabled
            return;

        boolean selected = selection.isSelected(item.getReceipt());

        // appear
        if (selected && selectedThumbnail.getVisibility() == View.INVISIBLE) {
            selectedThumbnail.setVisibility(View.VISIBLE);

            Animation anim = new AlphaAnimation(0, 1);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.setDuration(100);
            selectedThumbnail.startAnimation(anim);

            frame.setBackgroundColor(Color.rgb(245, 245, 245));
        }

        // disappear
        if (!selected && selectedThumbnail.getVisibility() == View.VISIBLE) {
            Animation anim = new AlphaAnimation(1, 0);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.setDuration(100);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    selectedThumbnail.setVisibility(View.INVISIBLE);
                }
                public void onAnimationRepeat(Animation animation) {}
                public void onAnimationStart(Animation animation) {}
            });
            selectedThumbnail.startAnimation(anim);

            frame.setBackgroundColor(Color.rgb(255, 255, 255));
        }
    }
}
