package io.texio.ui.receiptListView;

public interface ListItem {

    /**
     * Returns one of the types in ReceiptListAdapter.VIEW_TYPE_?
     */
    int getViewType();
}
