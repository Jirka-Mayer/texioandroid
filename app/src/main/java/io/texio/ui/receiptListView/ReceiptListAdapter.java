package io.texio.ui.receiptListView;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.texio.R;
import io.texio.db.models.Receipt;

public class ReceiptListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @VisibleForTesting
    public boolean NOTIFY_RECYCLER_VIEW = true;

    public static final int VIEW_TYPE_RECEIPT = 1;
    public static final int VIEW_TYPE_HEADING = 2;
    public static final int VIEW_TYPE_LOADER = 3;

    private List<ListItem> items = new ArrayList<>();

    private RecyclerView recyclerView;

    private ReceiptViewHolder.OnClickListener receiptClickListener = null;

    private Selection selection = new Selection();

    private ItemsChangedListener itemsChangedListener = null;

    /**
     * Holds references to receipt holders to be able to poke them
     * when they should redraw, e.g. when the selection is emptied
     */
    private List<ReceiptViewHolder> receiptViewHolders = new ArrayList<>();

    public interface ItemsChangedListener {
        void onChange();
    }

    public ReceiptListAdapter() { }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    /**
     * Set the listener that should be called when a receipt is clicked
     */
    public void setReceiptClickListener(ReceiptViewHolder.OnClickListener receiptClickListener) {
        this.receiptClickListener = receiptClickListener;
    }

    /**
     * Listener that should be called when the list of items changes
     */
    public void setItemsChangedListener(ItemsChangedListener itemsChangedListener) {
        this.itemsChangedListener = itemsChangedListener;
    }

    /**
     * Listener to be called when the selection changes
     */
    public void setSelectionChangeListener(Selection.SelectionChangeListener listener) {
        selection.setChangeListener(listener);
    }

    /**
     * Returns receipt in the selection
     */
    public Receipt[] getSelectedReceipts() {
        return selection.getSelectedReceipts();
    }

    /**
     * Clears the selection
     */
    public void clearSelection() {
        /*List<Integer> itemsToNotify = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (
                getItemViewType(i) == VIEW_TYPE_RECEIPT
                    && selection.isSelected(((ReceiptListItem) getItem(i)).getReceipt())
                ) {
                itemsToNotify.add(i);
            }
        }*/

        selection.clear();

        /*for (int i : itemsToNotify)
            notifyItemChanged(i);*/

        for (ReceiptViewHolder holder : receiptViewHolders)
            holder.updateSelectionDisplay();
    }

    /**
     * Returns view type of the item at a given position
     */
    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    /**
     * Creates new holder of a given type
     */
    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_RECEIPT:
                ReceiptViewHolder holder = new ReceiptViewHolder(
                    inflater.inflate(
                        R.layout.view_receipt_list_receipt, parent, false
                    ),
                    (receipt) -> {
                        if (receiptClickListener != null)
                            receiptClickListener.onClick(receipt);
                    },
                    selection
                );
                receiptViewHolders.add(holder); // keep holder reference
                return holder;

            case VIEW_TYPE_HEADING:
                return new HeadingViewHolder(
                    inflater.inflate(
                        R.layout.view_receipt_list_heading, parent, false
                    )
                );

            case VIEW_TYPE_LOADER:
                return new LoaderViewHolder(
                    inflater.inflate(
                        R.layout.view_receipt_list_loader, parent, false
                    )
                );
        }

        throw new RuntimeException(
            "Receipt list requested a view of type " + viewType + ", which is unknown."
        );
    }

    /**
     * Bind a holder to a new data item
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();

        switch (viewType) {
            case VIEW_TYPE_RECEIPT:
                ((ReceiptViewHolder)holder).setReceiptListItem(
                    ((ReceiptListItem)items.get(position))
                );
                break;

            case VIEW_TYPE_HEADING:
                ((HeadingViewHolder)holder).setText(
                    ((HeadingListItem)items.get(position)).getText()
                );
                break;

            case VIEW_TYPE_LOADER:
                // nothing needs to be set
                break;

            default:
                throw new RuntimeException(
                    "Receipt list requested a view of type " + viewType + ", which is unknown."
                );
        }
    }

    /**
     * Returns total number of items
     */
    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Scrolls the view to a given item
     */
    public void scrollToItem(int index) {
        recyclerView.scrollToPosition(index);
    }

    ///////////////////////
    // Item list editing //
    ///////////////////////

    public ListItem getItem(int index) {
        return items.get(index);
    }

    public void clearItems() {
        items.clear();

        if (NOTIFY_RECYCLER_VIEW)
            notifyDataSetChanged();

        if (itemsChangedListener != null)
            itemsChangedListener.onChange();
    }

    public void insertItem(int index, ListItem item) {
        items.add(index, item);

        if (NOTIFY_RECYCLER_VIEW)
            notifyItemInserted(index);

        if (itemsChangedListener != null)
            itemsChangedListener.onChange();
    }

    public void removeItem(int index) {
        items.remove(index);

        if (NOTIFY_RECYCLER_VIEW)
            notifyItemRemoved(index);

        if (itemsChangedListener != null)
            itemsChangedListener.onChange();
    }

    /**
     * Moves an item from one place to another
     * The target index is automatically shifted when the item is removed if needed
     * @return returns the true index where the item actually ended up
     */
    public int moveItem(int from, int to) {
        ListItem item = items.remove(from);
        if (to > from)
            to--;
        items.add(to, item);

        if (NOTIFY_RECYCLER_VIEW)
            notifyItemMoved(from, to);

        if (itemsChangedListener != null)
            itemsChangedListener.onChange();

        return to;
    }

    /**
     * Replaces an items and notifies change
     */
    public void updateItem(ListItem item, int index) {
        items.remove(index);
        items.add(index, item);

        if (NOTIFY_RECYCLER_VIEW)
            notifyItemChanged(index);

        if (itemsChangedListener != null)
            itemsChangedListener.onChange();
    }

    //////////////////
    // Item loading //
    //////////////////

    private boolean everythingLoaded = false;
    private boolean loadingWasRequested = false;

    private LoadMoreReceiptsListener loadMoreReceiptsListener = null;

    public interface LoadMoreReceiptsListener {
        /**
         * Implementation:
         * Do the loading, insert items to the adapter and call {@link #itemsHaveBeenLoaded(boolean)}
         */
        void load();
    }

    /**
     * Set the listener that should be called, when loading of more receipts is requested
     */
    public void setLoadMoreReceiptsListener(LoadMoreReceiptsListener loadMoreReceiptsListener) {
        this.loadMoreReceiptsListener = loadMoreReceiptsListener;
    }

    /**
     * Displays the spinner and starts the loading process
     */
    public void requestLoading() {
        if (loadMoreReceiptsListener == null || everythingLoaded || loadingWasRequested)
            return;

        insertItem(items.size(), new LoaderListItem());

        loadingWasRequested = true;
        loadMoreReceiptsListener.load();
    }

    /**
     * Call this after the items have been loaded and inserted to make the spinner disappear
     * Can be called even without first requesting the loading
     * @param noMoreItems true if there are no more items to load
     */
    public void itemsHaveBeenLoaded(boolean noMoreItems) {
        everythingLoaded = noMoreItems;
        loadingWasRequested = false;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getViewType() == VIEW_TYPE_LOADER) {
                removeItem(i);
                break;
            }
        }
    }
}
