package io.texio.ui.receiptListView;

import io.texio.db.models.Receipt;
import io.texio.db.receipt.receiptState.ReceiptState;

public class ReceiptListItem implements ListItem {

    private Receipt receipt;
    private ReceiptState receiptState;

    public ReceiptListItem(Receipt receipt, ReceiptState receiptState) {
        this.receipt = receipt;
        this.receiptState = receiptState;
    }

    @Override
    public int getViewType() {
        return ReceiptListAdapter.VIEW_TYPE_RECEIPT;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public ReceiptState getReceiptState() {
        return receiptState;
    }
}
