package io.texio.ui.receiptListView;

import java.util.HashSet;
import java.util.Set;

import io.texio.db.models.Receipt;

public class Selection {

    private Set<Receipt> selectedReceipts = new HashSet<>();

    private SelectionChangeListener changeListener;

    public interface SelectionChangeListener {
        void onChange(Receipt[] receipts);
    }

    public void setChangeListener(SelectionChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    private void selectionHasChanged() {
        if (changeListener == null)
            return;

        changeListener.onChange(getSelectedReceipts());
    }

    public void select(Receipt receipt) {
        if (receipt == null)
            return;

        selectedReceipts.add(receipt);
        selectionHasChanged();
    }

    public void deselect(Receipt receipt) {
        selectedReceipts.remove(receipt);
        selectionHasChanged();
    }

    public boolean isSelected(Receipt receipt) {
        return selectedReceipts.contains(receipt);
    }

    public void toggle(Receipt receipt) {
        if (isSelected(receipt))
            deselect(receipt);
        else
            select(receipt);
    }

    public void clear() {
        selectedReceipts.clear();
        selectionHasChanged();
    }

    public boolean isEmpty() {
        return selectedReceipts.isEmpty();
    }

    public Receipt[] getSelectedReceipts() {
        return selectedReceipts.toArray(new Receipt[0]);
    }
}
