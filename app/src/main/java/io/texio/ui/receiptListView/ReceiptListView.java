package io.texio.ui.receiptListView;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import io.texio.R;

public class ReceiptListView extends ConstraintLayout {

    /**
     * How many receipts to ideally load in one loading request
     */
    public static final int RECOMMENDED_LOADING_BATCH_SIZE = 20;

    /**
     * How many list items should be below screen when loading starts?
     */
    public static final int ITEM_LOADING_THRESHOLD = 5;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ReceiptListAdapter adapter;

    public ReceiptListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_receipt_list, this);

        recyclerView = this.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new ItemDecoration(context));

        adapter = new ReceiptListAdapter();
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (
                    layoutManager.getItemCount()
                        <= layoutManager.findLastVisibleItemPosition() + ITEM_LOADING_THRESHOLD
                    ) {
                    adapter.requestLoading(); // can be called when still loading
                }
            }
        });
    }

    public ReceiptListAdapter getAdapter() {
        return adapter;
    }
}
