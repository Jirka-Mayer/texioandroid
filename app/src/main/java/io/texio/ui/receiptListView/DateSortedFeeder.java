package io.texio.ui.receiptListView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.hypertrack.hyperlog.HyperLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Locale;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.other.ReceiptDateComparator;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.hyperlog.HyperLogExtended;
import io.texio.ui.receiptList.ReceiptListActivity;

/**
 * Abstracts away individual recycler view items and presents itself as holding
 * a collection of sorted receipts
 *
 * Receipts are displayed sorted by date (and other stuff if dates equal)
 */
public class DateSortedFeeder {

    public static final String TAG = DateSortedFeeder.class.getSimpleName();

    @VisibleForTesting
    public boolean LOG_MISSING_CONTEXT = true;

    private ReceiptListAdapter adapter;
    private Comparator<Receipt> comparator = new ReceiptDateComparator();

    // context may or may not be set
    // it's used only for header creation (translation of text)
    private Context context = null;

    public DateSortedFeeder(ReceiptListAdapter adapter) {
        this.adapter = adapter;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Counts currently loaded receipts
     */
    public int countReceipts() {
        int count = 0;

        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (adapter.getItemViewType(i) == ReceiptListAdapter.VIEW_TYPE_RECEIPT)
                count++;
        }

        return count;
    }

    /**
     * Scrolls the view to a given receipt
     */
    public void scrollToReceipt(@NonNull String receiptId) {
        int itemIndex = getReceiptItemIndex(receiptId);

        // if there's a header before the receipt, scroll to the header instead
        if (itemIndex >= 1
            && adapter.getItemViewType(itemIndex - 1) == ReceiptListAdapter.VIEW_TYPE_HEADING
        ) {
            itemIndex--;
        }

        adapter.scrollToItem(itemIndex);
    }

    /**
     * Updates receipt data and position
     */
    public void updateReceipt(Receipt receipt, ReceiptState receiptState) {
        // prevent receipt edits outside to leak inside
        receipt = receipt.cloneReceiptInstance();

        int position = moveReceiptToNewPosition(receipt);

        // receipt was not found in the list
        if (position == -1)
            return;

        adapter.updateItem(new ReceiptListItem(receipt, receiptState), position);
    }

    /**
     * Returns the new position of the receipt or -1 if it wasn't found
     */
    private int moveReceiptToNewPosition(Receipt receipt) {
        int oldPosition = getReceiptItemIndex(receipt.id);

        // receipt not in the list
        if (oldPosition == -1)
            return -1;

        // where to insert it
        int newPosition = receiptToInsertBefore(receipt);

        // position of the receipt didn't change
        // (because the receipt was found in the list by date & time primarily; only then by id)
        if (newPosition == -2)
            return oldPosition;

        // heading removal
        boolean headingRemoved = false;
        if (
            // previous item is a heading
            oldPosition != 0
                && adapter.getItemViewType(oldPosition - 1) == ReceiptListAdapter.VIEW_TYPE_HEADING

                // next item is not a receipt (so the group ends here) (including no next item)
                && !(
                oldPosition + 1 < adapter.getItemCount()
                    && adapter.getItemViewType(oldPosition + 1) == ReceiptListAdapter.VIEW_TYPE_RECEIPT
            )
            ) {
            adapter.removeItem(oldPosition - 1);
            oldPosition--;

            if (newPosition > oldPosition)
                newPosition--;

            headingRemoved = true;
        }

        // heading addition
        boolean addHeading = shouldHeadingBeAdded(receipt, newPosition);

        // heading was removed, but the receipt didn't move. The add the heading back
        if (headingRemoved && oldPosition + 1 == newPosition)
            addHeading = true;

        if (addHeading) {
            adapter.insertItem(
                newPosition,
                new HeadingListItem(createHeading(receipt))
            );

            if (oldPosition >= newPosition)
                oldPosition++;

            newPosition++;
        }

        // move
        return adapter.moveItem(oldPosition, newPosition);
    }

    /**
     * Remove receipt with given id from the list
     */
    public void removeReceipt(@NonNull String receiptId) {
        // find the item
        int itemIndex = getReceiptItemIndex(receiptId);
        if (itemIndex == -1)
            return;

        // heading removal
        if (
            // previous item is a heading
            itemIndex != 0
            && adapter.getItemViewType(itemIndex - 1) == ReceiptListAdapter.VIEW_TYPE_HEADING

            // next item is not a receipt (so the group ends here) (including no next item)
            && !(
                itemIndex + 1 < adapter.getItemCount()
                && adapter.getItemViewType(itemIndex + 1) == ReceiptListAdapter.VIEW_TYPE_RECEIPT
            )
        ) {
            adapter.removeItem(itemIndex - 1);
            itemIndex--;
        }

        // remove the item
        adapter.removeItem(itemIndex);
    }

    /**
     * Returns index of the receipt item or -1 if not in the list
     */
    private int getReceiptItemIndex(@NonNull String receiptId) {
        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (
                adapter.getItemViewType(i) == ReceiptListAdapter.VIEW_TYPE_RECEIPT
                    && receiptId.equals(((ReceiptListItem)adapter.getItem(i)).getReceipt().id)
                ) {
                return i;
            }
        }

        return -1;
    }

    public boolean containsReceipt(@NonNull String receiptId) {
        return getReceiptItemIndex(receiptId) != -1;
    }

    /**
     * Inserts a receipt into the list at the proper location
     */
    public void insertReceipt(Receipt receipt, ReceiptState receiptState) {
        // prevent receipt edits outside to leak inside
        receipt = receipt.cloneReceiptInstance();

        int insertBeforeIndex = receiptToInsertBefore(receipt);

        // already present, do not insert
        if (insertBeforeIndex == -2)
            return;

        boolean addHeading = shouldHeadingBeAdded(receipt, insertBeforeIndex);

        if (addHeading) {
            adapter.insertItem(
                insertBeforeIndex,
                new HeadingListItem(createHeading(receipt))
            );
            insertBeforeIndex++;
        }

        adapter.insertItem(insertBeforeIndex, new ReceiptListItem(receipt, receiptState));
    }

    /**
     * Returns index of the receipt list item to insert a new receipt before
     * If no such receipt exists (appending to end), method returns the list length
     * Returns -2 if the receipt is already present
     */
    private int receiptToInsertBefore(Receipt insertedReceipt) {
        int insertBefore = -1;
        int lastReceiptItem = -1;
        Receipt olderReceipt = null;

        // find the first older receipt
        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (adapter.getItem(i).getViewType() != ReceiptListAdapter.VIEW_TYPE_RECEIPT)
                continue;

            Receipt r = ((ReceiptListItem)adapter.getItem(i)).getReceipt();
            lastReceiptItem = i;
            int comparison = comparator.compare(r, insertedReceipt);
            if (comparison == 0) {
                return -2; // receipt already present
            }
            if (comparison > 0) {
                insertBefore = i;
                olderReceipt = r;
                break;
            }
        }

        // if no older receipt found, then the new receipt should go right after the last
        // receipt item (fo make sure we go before the loader item)
        if (insertBefore == -1)
            insertBefore = lastReceiptItem + 1;

        // if it's too old (different month), there's a header in front of it so
        // put the new receipt before the header
        if (
            olderReceipt != null
                && insertBefore > 0
                && hasDifferentMonth(olderReceipt, insertedReceipt)
                && adapter.getItemViewType(insertBefore - 1) == ReceiptListAdapter.VIEW_TYPE_HEADING
        ) {
            insertBefore--;
        }

        return insertBefore;
    }

    /**
     * Returns true if a new heading should be inserted before the added receipt
     */
    private boolean shouldHeadingBeAdded(Receipt insertedReceipt, int insertBefore) {
        // if the receipt is gonna be the first one
        if (insertBefore == 0) {
            // if null date, then no heading, else yep heading
            return insertedReceipt.date != null;
        }

        // if there already is a heading right before, then nope it's not needed
        if (
            insertBefore > 0
                && adapter.getItemViewType(insertBefore - 1) == ReceiptListAdapter.VIEW_TYPE_HEADING
        ) {
            return false;
        }

        // if the list item right before is not a heading, nor a receipt we aren't gonna crash,
        // but we won't insert a heading either
        if (adapter.getItemViewType(insertBefore - 1) != ReceiptListAdapter.VIEW_TYPE_RECEIPT) {
            HyperLog.w(TAG, "Receipt should be inserted after an unknown item type ("
                + adapter.getItemViewType(insertBefore - 1) + ").");
            return false;
        }

        // if the month of the receipt right before differs, then yes, header is needed
        Receipt rightBefore = ((ReceiptListItem)adapter.getItem(insertBefore - 1)).getReceipt();
        return hasDifferentMonth(rightBefore, insertedReceipt);
    }

    /**
     * Returns true if the two receipts have different months
     * (meaning they fall into different groups (headers))
     */
    private boolean hasDifferentMonth(Receipt a, Receipt b) {
        ReceiptDate da = a.date;
        ReceiptDate db = b.date;

        if (da == null && db == null)
            return false;

        if (da == null || db == null)
            return true;

        return da.year != db.year || da.month != db.month;
    }

    /**
     * Creates the heading string for a given receipt
     */
    private String createHeading(Receipt receipt) {
        ReceiptDate date = receipt.date;

        if (date == null) {
            HyperLog.w(TAG, "It was requested to create heading text for a null-date receipt.");
            return "null";
        }

        if (context == null) {
            if (LOG_MISSING_CONTEXT)
                HyperLog.w(TAG, "It was requested to create heading, but no context was set.");
            return "noContext";
        }

        String[] months = context.getResources().getStringArray(R.array.fullMonthNames);

        if (date.year == ReceiptDate.today().year) {
            return months[date.month];
        } else {
            return months[date.month] + " " + date.year;
        }
    }
}
