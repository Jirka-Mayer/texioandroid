package io.texio.ui.receiptListView;

public class HeadingListItem implements ListItem {

    private String text;

    public HeadingListItem(String text) {
        this.text = text;
    }

    @Override
    public int getViewType() {
        return ReceiptListAdapter.VIEW_TYPE_HEADING;
    }

    public String getText() {
        return text;
    }
}
