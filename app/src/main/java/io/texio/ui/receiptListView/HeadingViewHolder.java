package io.texio.ui.receiptListView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class HeadingViewHolder extends RecyclerView.ViewHolder {

    private TextView textView;

    public HeadingViewHolder(View itemView) {
        super(itemView);

        textView = (TextView)itemView;
    }

    /**
     * Set the text to be displayed
     */
    public void setText(String text) {
        textView.setText(text);
    }
}
