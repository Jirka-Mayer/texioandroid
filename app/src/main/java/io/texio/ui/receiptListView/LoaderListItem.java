package io.texio.ui.receiptListView;

public class LoaderListItem implements ListItem {

    public LoaderListItem() { }

    @Override
    public int getViewType() {
        return ReceiptListAdapter.VIEW_TYPE_LOADER;
    }
}
