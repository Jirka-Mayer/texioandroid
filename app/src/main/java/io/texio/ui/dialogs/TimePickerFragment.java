package io.texio.ui.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment
{
    public interface SubmitListener
    {
        void onTimePicked(int hour, int minute);
    }

    protected SubmitListener listener = null;

    public void setSubmitListener(SubmitListener listener)
    {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        return new TimePickerDialog(
            this.getActivity(),
            (TimePicker view, int hour, int minute) -> {
                this.listener.onTimePicked(hour, minute);
            },
            this.getArguments().getInt("hour"),
            this.getArguments().getInt("minute"),
            DateFormat.is24HourFormat(this.getActivity())
        );
    }
}
