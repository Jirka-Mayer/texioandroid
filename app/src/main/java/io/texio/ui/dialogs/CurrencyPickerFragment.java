package io.texio.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import io.texio.R;

public class CurrencyPickerFragment extends DialogFragment
{
    public interface SubmitListener
    {
        void onCurrencyPicked(String currency);
    }

    protected SubmitListener listener = null;

    public void setSubmitListener(SubmitListener listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        String[] currencies = this.getResources().getStringArray(R.array.currencies);

        builder.setTitle(R.string.currencyPickerDialog_title)
            .setItems(currencies, (DialogInterface dialog, int which) -> {
                if (this.listener == null)
                    return;
                this.listener.onCurrencyPicked(currencies[which]);
            });

        return builder.create();
    }
}
