package io.texio.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import io.texio.R;
import io.texio.db.models.Receipt;

public class EetModePickerFragment extends DialogFragment
{
    public interface SubmitListener
    {
        void onModePicked(String mode);
    }

    protected SubmitListener listener = null;

    public void setSubmitListener(SubmitListener listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        String[] modeNames = new String[] {
            this.getResources().getString(R.string.receiptDetail_mode_default),
            this.getResources().getString(R.string.receiptDetail_mode_reduced),
            this.getResources().getString(R.string.receiptDetail_mode_null),
        };

        String[] modeValues = new String[] {
            Receipt.EET_MODE_DEFAULT,
            Receipt.EET_MODE_REDUCED,
            null
        };

        builder.setTitle(R.string.receiptDetail_mode)
            .setItems(modeNames, (DialogInterface dialog, int which) -> {
                if (this.listener == null)
                    return;
                this.listener.onModePicked(modeValues[which]);
            });

        return builder.create();
    }
}
