package io.texio.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import io.texio.R;

public class TextFieldFragment extends DialogFragment
{
    public interface SubmitListener
    {
        void onTextFieldDialogSubmit(String text);
    }

    protected SubmitListener listener = null;

    public void setSubmitListener(SubmitListener listener)
    {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle args = this.getArguments();

        LayoutInflater inflater = this.getActivity().getLayoutInflater();

        ConstraintLayout content = (ConstraintLayout) inflater.inflate(R.layout.dialog_text_field, null);
        EditText textField = content.findViewById(R.id.text_field);

        // set input type
        textField.setInputType(
            args.getInt("inputType", InputType.TYPE_CLASS_TEXT)
        );

        // set content
        textField.setText(
            args.getString("defaultContent", "")
        );

        // set hint
        textField.setHint(args.getString("hint", ""));

        // show keyboard on focus
        textField.setOnFocusChangeListener((View v, boolean hasFocus) -> {
            textField.post(() -> {
                Activity activity = this.getActivity();

                if (activity == null)
                    return;

                InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(textField, InputMethodManager.SHOW_IMPLICIT);
            });
        });
        textField.requestFocus();

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setTitle(args.getString("title", "Untitled"))
            .setView(content)
            .setPositiveButton(R.string.textFieldDialog_ok, (DialogInterface dialog, int id) -> {
                if (this.listener != null)
                    this.listener.onTextFieldDialogSubmit(textField.getText().toString());
            })
            .setNegativeButton(R.string.textFieldDialog_cancel, (DialogInterface dialog, int id) -> {
                // User cancelled the dialog
            });

        return builder.create();
    }
}
