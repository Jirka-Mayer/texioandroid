package io.texio.ui.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment
{
    public interface SubmitListener
    {
        void onDatePicked(int year, int month, int day);
    }

    protected SubmitListener listener = null;

    public void setSubmitListener(SubmitListener listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        return new DatePickerDialog(
            this.getActivity(),
            (DatePicker view, int year, int month, int day) -> {
                if (this.listener != null)
                    this.listener.onDatePicked(year, month, day);
            },
            this.getArguments().getInt("year"),
            this.getArguments().getInt("month"),
            this.getArguments().getInt("day")
        );
    }
}
