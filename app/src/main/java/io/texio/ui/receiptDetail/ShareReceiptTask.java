package io.texio.ui.receiptDetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import io.texio.R;
import io.texio.Snaps;
import io.texio.db.receipt.ReceiptFileManager;
import io.texio.db.receipt.ReceiptSnapManager;
import io.texio.util.Str;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptDate;

public class ShareReceiptTask extends AsyncTask<Void, Void, File>
{
    private WeakReference<Context> weakContext;
    private ReceiptFileManager fileManager;
    private ReceiptSnapManager snapManager;

    private Receipt receipt;
    private Bitmap snapBitmap;
    private boolean isZipped;

    public ShareReceiptTask(Context context, Receipt receipt, Bitmap snapBitmap)
    {
        this.weakContext = new WeakReference<>(context);

        fileManager = new ReceiptFileManager(context, receipt);
        snapManager = new ReceiptSnapManager(fileManager);

        this.receipt = receipt;
        this.snapBitmap = snapBitmap;

        if (snapManager.getSnapList().size() == 0)
        {
            Toast.makeText(context, R.string.shareReceiptTask_noSnaps, Toast.LENGTH_SHORT).show();
            this.cancel(true);
            return;
        }

        Toast.makeText(context, R.string.shareReceiptTask_preparingSnap, Toast.LENGTH_SHORT).show();

        Snaps.clearDirectory(context);
    }

    @Override
    protected File doInBackground(Void... voids)
    {
        File bundledFile;

        if (this.snapBitmap != null)
            bundledFile = this.bundleSingleSnap(this.snapBitmap);
        else
            bundledFile = this.bundleAllSnaps();

        return bundledFile;
    }

    private File bundleSingleSnap(Bitmap bitmap)
    {
        this.isZipped = false;

        String snapName = this.getBundleFileName(".jpg");
        File bundledFile = this.createTempFile(snapName);

        if (bundledFile == null)
            return null;

        try (FileOutputStream out = new FileOutputStream(bundledFile))
        {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        return bundledFile;
    }

    private File bundleAllSnaps()
    {
        String snapName = this.getBundleFileName(".zip");
        File bundledFile = this.createTempFile(snapName);

        List<String> snapList = snapManager.getSnapList();

        // single snap will not be zipped
        if (snapList.size() == 1)
        {
            byte[] snapData = fileManager.readFileBytes(snapList.get(0));
            Bitmap bitmap = BitmapFactory.decodeByteArray(snapData, 0, snapData.length);
            return this.bundleSingleSnap(bitmap);
        }

        this.isZipped = true;

        boolean result = this.zipSnaps(
            this.getBundleFileName(""),
            snapList,
            bundledFile
        );

        if (!result)
            return null;

        return bundledFile;
    }

    private String getBundleFileName(String extension)
    {
        String store = this.receipt.store;
        ReceiptDate receiptDate = this.receipt.date;
        String date;

        if (store == null || store.length() == 0)
            store = "-";

        if (receiptDate == null)
            date = "-";
        else
            date = receiptDate.toString();

        return Str.slug(store) + "_" + date + extension;
    }

    private File createTempFile(String name)
    {
        Context context = this.weakContext.get();

        if (context == null)
            return null;

        return Snaps.file(context, name);
    }

    private boolean zipSnaps(String namePrefix, List<String> snaps, File zipFile)
    {
        try
        {
            ZipOutputStream out = new ZipOutputStream(
                new BufferedOutputStream(
                    new FileOutputStream(
                        zipFile.getAbsolutePath()
                    )
                )
            );

            for (String snap : snaps)
            {
                ZipEntry entry = new ZipEntry(namePrefix + "_" + snap);
                out.putNextEntry(entry);
                out.write(fileManager.readFileBytes(snap));
            }

            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(File bundledFile)
    {
        if (this.isCancelled())
            return;

        Context context = this.weakContext.get();

        if (context == null)
            return;

        if (bundledFile == null)
        {
            Toast.makeText(context, R.string.shareReceiptTask_error, Toast.LENGTH_LONG).show();
            return;
        }

        Uri uri = FileProvider.getUriForFile(
            context, "io.texio.fileprovider", bundledFile
        );

        if (uri == null)
        {
            Toast.makeText(context, R.string.shareReceiptTask_error, Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(Intent.ACTION_SEND);

        if (this.isZipped)
            intent.setType("application/zip");
        else
            intent.setType("image/png");

        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, uri);

        context.startActivity(
            Intent.createChooser(
                intent,
                context.getResources().getString(R.string.shareReceiptTask_share)
            )
        );
    }
}
