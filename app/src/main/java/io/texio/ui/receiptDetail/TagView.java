package io.texio.ui.receiptDetail;

import android.content.Context;
import android.graphics.ColorFilter;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.Toast;

import io.texio.db.other.TagId;

public class TagView extends AppCompatTextView
{
    protected TagId tag;
    protected boolean selected;

    public TagView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        // toggle selection by click
        this.setOnClickListener(v -> {
            this.setSelected(!this.selected);
        });
    }

    public void setTag(TagId tag)
    {
        this.tag = tag;

        this.setText(this.tag.id);
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;

        this.getBackground().setLevel(selected ? 1 : 0);
    }
}
