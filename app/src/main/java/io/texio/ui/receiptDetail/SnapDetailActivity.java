package io.texio.ui.receiptDetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptFileManager;
import io.texio.ui.TouchImageView;

public class SnapDetailActivity extends AppCompatActivity
{
    protected Receipt receipt;
    protected String snapName;

    protected Bitmap snapBitmap = null;

    protected TouchImageView touchImageView;
    protected ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_snap_detail);

        // setup app bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);

        // load intent data
        Intent intent = this.getIntent();
        this.receipt = Receipt.fromBundle(intent.getBundleExtra(IntentExtras.EXTRA_RECEIPT), this);
        this.snapName = intent.getStringExtra(IntentExtras.EXTRA_SNAP_NAME);

        // elements
        this.touchImageView = this.findViewById(R.id.touch_image_view);
        this.progressBar = this.findViewById(R.id.progress_bar);

        // load image
        new LoadImageTask().execute();
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        // act as the back button
        this.finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.getMenuInflater().inflate(R.menu.appbar_snap_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_delete:
                // NOTE: the actual snap removal is done in the ReceiptDetail activity
                Intent data = new Intent();
                data.putExtra(IntentExtras.EXTRA_SNAP_DELETED, true);
                data.putExtra(IntentExtras.EXTRA_SNAP_NAME, this.snapName);
                this.setResult(RESULT_FIRST_USER, data);
                this.finish();
                return true;

            case R.id.action_share:
                if (this.snapBitmap != null)
                {
                    new ShareReceiptTask(this, this.receipt, this.snapBitmap).execute();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ////////////////////////
    // Image loading task //
    ////////////////////////

    private class LoadImageTask extends AsyncTask<Void, Void, Bitmap>
    {
        @Override
        protected Bitmap doInBackground(Void... voids)
        {
            ReceiptFileManager fileManager = new ReceiptFileManager(
                SnapDetailActivity.this,
                SnapDetailActivity.this.receipt
            );

            byte[] snapData = fileManager.readFileBytes(
                SnapDetailActivity.this.snapName
            );

            return BitmapFactory.decodeByteArray(snapData, 0, snapData.length);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap)
        {
            SnapDetailActivity.this.progressBar.setVisibility(View.GONE);

            SnapDetailActivity.this.touchImageView.setImageBitmap(bitmap);
            SnapDetailActivity.this.snapBitmap = bitmap;

            // A bug fix. View refused to redraw on my lenovo. Invalidating didn't help.
            SnapDetailActivity.this.touchImageView.setZoom(1.0f, 0.5f, 0.5f);
        }
    }
}
