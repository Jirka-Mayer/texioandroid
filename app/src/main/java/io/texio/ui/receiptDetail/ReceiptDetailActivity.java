package io.texio.ui.receiptDetail;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hypertrack.hyperlog.HyperLog;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.List;

import io.texio.Application;
import io.texio.IntentExtras;
import io.texio.R;
import io.texio.Snaps;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.other.DateTime;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptField;
import io.texio.db.receipt.ReceiptSnapManager;
import io.texio.db.receipt.ReceiptTime;
import io.texio.db.receipt.receiptState.BazarReceiptState;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.db.receipt.receiptState.UctenkovkaReceiptState;
import io.texio.integration.Integration;
import io.texio.ui.camera.CameraActivity;
import io.texio.ui.dialogs.CurrencyPickerFragment;
import io.texio.ui.dialogs.DatePickerFragment;
import io.texio.ui.dialogs.EetModePickerFragment;
import io.texio.ui.dialogs.TextFieldFragment;
import io.texio.ui.dialogs.TimePickerFragment;
import io.texio.ui.integration.IntegrationUploadActivity;
import io.texio.ui.ocr.OcrActivity;

/**
 * This activity allows you to view and edit data of a receipt
 * It also allows you to trigger other actions like sharing, viewing snap detail, etc.
 *
 * Opening intent:
 * EXTRA_RECEIPT_ID - id of the receipt to act upon
 */
public class ReceiptDetailActivity extends AppCompatActivity {

    public static final String TAG = ReceiptDetailActivity.class.getSimpleName();

    public static final int REQUEST_TAKE_NEW_SNAP = 1;
    public static final int REQUEST_DO_OCR = 2;
    public static final int REQUEST_SNAP_DETAIL = 3;
    public static final int REQUEST_UPLOAD_INTEGRATION = 4;

    private View content;
    private ProgressBar spinner;

    private ConstraintLayout storeGroup;
    private ConstraintLayout dateGroup;
    private ConstraintLayout timeGroup;
    private ConstraintLayout totalGroup;
    private ConstraintLayout stateGroup;
    private ConstraintLayout fikGroup;
    private ConstraintLayout bkpGroup;
    private ConstraintLayout dicGroup;
    private ConstraintLayout modeGroup;
    private ConstraintLayout noteGroup;
    private ConstraintLayout snapsGroup;
    private ConstraintLayout ocrGroup;

    private TextView storeField;
    private TextView dateField;
    private TextView timeField;
    private TextView totalField;
    private ReceiptStateBadge stateBadge;
    private TextView fikField;
    private TextView bkpField;
    private TextView dicField;
    private TextView modeField;
    private TextView noteField;
    private TextView ocrField;

    private ImageButton clearDateButton;
    private ImageButton clearTimeButton;
    private Button currencyButton;

    private RecyclerView snapList;
    private SnapListAdapter snapListAdapter;

    /**
     * The receipt and it's state we are looking at loaded during activity startup
     */
    private Receipt receipt;
    private ReceiptState receiptState;

    private LoadReceiptTask loaderTask;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_receipt_detail);

        // setup app bar
        Toolbar myToolbar = this.findViewById(R.id.toolbar);
        this.setSupportActionBar(myToolbar);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);

        // view references
        content = findViewById(R.id.content);
        spinner = findViewById(R.id.spinner);

        storeGroup = findViewById(R.id.store_group);
        dateGroup = findViewById(R.id.date_group);
        timeGroup = findViewById(R.id.time_group);
        totalGroup = findViewById(R.id.total_group);
        stateGroup = findViewById(R.id.state_group);
        fikGroup = findViewById(R.id.fik_group);
        bkpGroup = findViewById(R.id.bkp_group);
        dicGroup = findViewById(R.id.dic_group);
        modeGroup = findViewById(R.id.mode_group);
        noteGroup = findViewById(R.id.note_group);
        snapsGroup = findViewById(R.id.snaps_group);
        ocrGroup = findViewById(R.id.ocr_group);

        storeField = findViewById(R.id.store_field);
        dateField = findViewById(R.id.date_field);
        timeField = findViewById(R.id.time_field);
        totalField = findViewById(R.id.total_field);
        noteField = findViewById(R.id.note_field);
        stateBadge = findViewById(R.id.state_badge);
        fikField = findViewById(R.id.fik_field);
        bkpField = findViewById(R.id.bkp_field);
        dicField = findViewById(R.id.dic_field);
        modeField = findViewById(R.id.mode_field);
        ocrField = findViewById(R.id.ocr_field);

        clearDateButton = findViewById(R.id.clear_date_button);
        clearTimeButton = findViewById(R.id.clear_time_button);
        currencyButton = findViewById(R.id.currency_button);

        // setup listeners
        this.storeGroup.setOnClickListener(v -> this.onStoreGroupClick());
        this.dateGroup.setOnClickListener(v -> this.onDateGroupClick());
        this.timeGroup.setOnClickListener(v -> this.onTimeGroupClick());
        this.totalGroup.setOnClickListener(v -> this.onTotalGroupClick());
        this.fikGroup.setOnClickListener(v -> this.onFikGroupClick());
        this.bkpGroup.setOnClickListener(v -> this.onBkpGroupClick());
        this.dicGroup.setOnClickListener(v -> this.onDicGroupClick());
        this.modeGroup.setOnClickListener(v -> this.onModeGroupClick());
        this.noteGroup.setOnClickListener(v -> this.onNoteGroupClick());

        this.clearDateButton.setOnClickListener(v -> this.onClearDateClick());
        this.clearTimeButton.setOnClickListener(v -> this.onClearTimeClick());
        this.currencyButton.setOnClickListener(v -> this.onCurrencyButtonClick());

        // snap list
        snapList = findViewById(R.id.snap_list);
        snapListAdapter = new SnapListAdapter(this);
        snapListAdapter.setSnapClickListener(this::onSnapClick);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);

        snapList.setHasFixedSize(false);
        snapList.setLayoutManager(manager);
        snapList.setAdapter(this.snapListAdapter);

        // prepare UI look
        spinner.setVisibility(View.VISIBLE);
        content.setVisibility(View.INVISIBLE);

        // start loading the receipt
        loadReceiptFromDatabase();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (loaderTask != null)
            loaderTask.cancel(true);
        loaderTask = null;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // reload the receipt
        loadReceiptFromDatabase();
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish(); // act as the back button
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.appbar_receipt_detail, menu);

        // ocr button only visible in debug mode
        if (!Application.getDebugMode(this)) {
            MenuItem item = menu.findItem(R.id.action_ocr);
            if (item != null)
                item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (receipt == null)
            return super.onOptionsItemSelected(item);

        Intent intent;
        switch (item.getItemId())
        {
            case R.id.action_eet:
                HyperLog.i(TAG, "User wants to upload a receipt to uctenkovka.");
                intent = new Intent(this, IntegrationUploadActivity.class);
                intent.putExtra(IntentExtras.EXTRA_INTEGRATION, Integration.UCTENKOVKA);
                intent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, this.receipt.id);
                this.startActivityForResult(intent, REQUEST_UPLOAD_INTEGRATION);
                return true;

            case R.id.action_share:
                HyperLog.i(TAG, "User wants to share a receipt.");
                new ShareReceiptTask(this, this.receipt, null).execute();
                return true;

            case R.id.action_ocr:
                intent = new Intent(this, OcrActivity.class);
                intent.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());
                this.startActivityForResult(intent, REQUEST_DO_OCR);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Loads the receipt from database
     */
    private void loadReceiptFromDatabase() {
        loaderTask = new LoadReceiptTask(
            this,
            getIntent().getStringExtra(IntentExtras.EXTRA_RECEIPT_ID)
        );
        loaderTask.execute();
    }

    /**
     * Called after the receipt has been loaded
     */
    private void receiptHasBeenLoaded() {
        spinner.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);

        loaderTask = null;
        snapListAdapter.setReceipt(receipt);
        updateDisplayedData();
    }

    /**
     * Updates displayed values to correspond to the receipt data
     */
    protected void updateDisplayedData() {
        // store
        storeField.setText(receipt.store == null ? "": receipt.store);

        // date
        if (receipt.date == null) {
            dateField.setText("");
            clearDateButton.setVisibility(View.GONE);
        } else {
            dateField.setText(receipt.date.formatHumanReadable());
            clearDateButton.setVisibility(View.VISIBLE);
        }

        // time
        if (receipt.time == null) {
            timeField.setText("");
            clearTimeButton.setVisibility(View.GONE);
        } else {
            timeField.setText(receipt.time.formatHumanReadable());
            clearTimeButton.setVisibility(View.VISIBLE);
        }

        // currency
        String currency = receipt.currency;
        if (currency == null)
            currency = "CZK";
        currencyButton.setText(currency);

        // total
        totalField.setText(
            receipt.total == 0 ? "" :
            CurrencyAmount.formatHumanReadable(this, receipt.total, currency)
        );

        // state
        if (receiptState != null && !receiptState.is(ReceiptStateEnum.UNUSED)) {
            stateGroup.setVisibility(View.VISIBLE);
            stateBadge.setState(receiptState.getState());
        } else {
            stateGroup.setVisibility(View.GONE);
        }

        // FIK
        fikField.setText(receipt.fik == null ? "" : receipt.fik);

        // BKP
        bkpField.setText(receipt.bkp == null ? "" : receipt.bkp);

        // DIC
        dicField.setText(receipt.dic == null ? "" : receipt.dic);

        // EET mode
        switch (receipt.eetMode == null ? "" : receipt.eetMode) {
            case Receipt.EET_MODE_DEFAULT:
                this.modeField.setText(R.string.receiptDetail_mode_default);
                break;

            case Receipt.EET_MODE_REDUCED:
                this.modeField.setText(R.string.receiptDetail_mode_reduced);
                break;

            default:
                this.modeField.setText("");
                break;
        }

        // note
        noteField.setText(receipt.note == null ? "" : receipt.note);

        // snaps
        ReceiptSnapManager snapManager = new ReceiptSnapManager(this, receipt);
        if (snapManager.getSnapList().size() == 0)
            this.snapsGroup.setVisibility(View.GONE);
        else
            this.snapsGroup.setVisibility(View.VISIBLE);

        // ocr
        if (Application.getDebugMode(this)) {
            String ocr = this.receipt.text;
            if (ocr == null) {
                this.ocrGroup.setVisibility(View.GONE);
            } else {
                this.ocrGroup.setVisibility(View.VISIBLE);
                this.ocrField.setText(ocr);
            }
        } else {
            this.ocrGroup.setVisibility(View.GONE);
        }
    }

    /**
     * Saves receipt to the database
     * (called after some value gets edited)
     */
    public void saveReceipt(ReceiptField field) {
        new SaveReceiptTask(this, field).execute();
    }

    /**
     * Called on floating button click
     */
    public void takeNewSnap(View view)
    {
        Intent intent = new Intent(this, CameraActivity.class);
        this.startActivityForResult(intent, REQUEST_TAKE_NEW_SNAP);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TAKE_NEW_SNAP)
        {
            if (resultCode == RESULT_OK)
            {
                ReceiptSnapManager snapManager = new ReceiptSnapManager(this, receipt);

                String[] snapNames = data.getStringArrayExtra(IntentExtras.EXTRA_SNAPS);

                for (String name : snapNames)
                    snapManager.addSnap(Snaps.file(this, name));

                this.updateDisplayedData();
                this.snapListAdapter.refreshDisplayedSnaps();

                // re-run OCR
                Intent intent = new Intent(this, OcrActivity.class);
                intent.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());
                this.startActivityForResult(intent, REQUEST_DO_OCR);
            }
        }

        if (requestCode == REQUEST_DO_OCR)
        {
            if (resultCode != RESULT_CANCELED)
            {
                // update the local receipt instance
                this.receipt.soakUpBundle(
                    data.getBundleExtra(IntentExtras.EXTRA_RECEIPT)
                );

                this.updateDisplayedData();
                this.snapListAdapter.refreshDisplayedSnaps();
            }
        }

        if (requestCode == REQUEST_SNAP_DETAIL)
        {
            if (resultCode == RESULT_FIRST_USER
                && data.getBooleanExtra(IntentExtras.EXTRA_SNAP_DELETED, false))
            {
                ReceiptSnapManager snapManager = new ReceiptSnapManager(this, receipt);
                snapManager.removeSnap(
                    data.getStringExtra(IntentExtras.EXTRA_SNAP_NAME)
                );

                this.updateDisplayedData();
                this.snapListAdapter.refreshDisplayedSnaps();
            }
        }

        if (requestCode == REQUEST_UPLOAD_INTEGRATION) {
            loadReceiptFromDatabase();
        }
    }

    /**
     * Called when the user attempts to edit a field
     * @return True if the user can perform the edit
     */
    private boolean attemptToEditField(ReceiptField field) {
        if (receipt == null || receiptState == null)
            return false;

        if (receiptState.isUctenkovka() && field.isForUctenkovka()) {
            UctenkovkaReceiptState state = receiptState.asUctenkovka();
            if (!state.canEditUctenkovkaField()) {
                Toast.makeText(
                    this,
                    R.string.receiptDetail_cannotEditField,
                    Toast.LENGTH_SHORT
                ).show();
                return false;
            }
        }

        if (receiptState.isBazar() && field.isForBazar()) {
            BazarReceiptState state = receiptState.asBazar();
            if (!state.canEditBazarField()) {
                Toast.makeText(
                    this,
                    R.string.receiptDetail_cannotEditField,
                    Toast.LENGTH_SHORT
                ).show();
                return false;
            }
        }

        return true;
    }

    //////////////////
    // Group clicks //
    //////////////////

    public void onStoreGroupClick()
    {
        if (!attemptToEditField(ReceiptField.STORE))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_TEXT);
        args.putString("hint", this.getResources().getString(R.string.receiptDetail_unknownStore));
        args.putString("title", this.getResources().getString(R.string.receiptDetail_store));
        args.putString("defaultContent", this.receipt.store);

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;

            this.receipt.store = text;

            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.STORE);
        });

        dialog.show(this.getSupportFragmentManager(), "storeField");
    }

    public void onDateGroupClick()
    {
        if (!attemptToEditField(ReceiptField.DATE))
            return;

        Calendar c = Calendar.getInstance();
        ReceiptDate date = receipt.date;

        Bundle args = new Bundle();

        if (date == null)
        {
            args.putInt("year", c.get(Calendar.YEAR));
            args.putInt("month", c.get(Calendar.MONTH));
            args.putInt("day", c.get(Calendar.DAY_OF_MONTH));
        }
        else
        {
            args.putInt("year", date.year);
            args.putInt("month", date.month);
            args.putInt("day", date.day);
        }

        DatePickerFragment dialog = new DatePickerFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener(((year, month, day) -> {
            this.receipt.date = new ReceiptDate(year, month, day);
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.DATE);
        }));

        dialog.show(this.getSupportFragmentManager(), "dateField");
    }

    public void onClearDateClick()
    {
        if (!attemptToEditField(ReceiptField.DATE))
            return;

        this.receipt.date = null;
        this.updateDisplayedData();
        this.saveReceipt(ReceiptField.DATE);
    }

    public void onTimeGroupClick()
    {
        if (!attemptToEditField(ReceiptField.TIME))
            return;

        Calendar c = Calendar.getInstance();
        ReceiptTime time = this.receipt.time;

        Bundle args = new Bundle();

        if (time == null)
        {
            args.putInt("hour", c.get(Calendar.HOUR_OF_DAY));
            args.putInt("minute", c.get(Calendar.MINUTE));
        }
        else
        {
            args.putInt("hour", time.hour);
            args.putInt("minute", time.minute);
        }

        TimePickerFragment dialog = new TimePickerFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener(((hour, minute) -> {
            this.receipt.time = new ReceiptTime(hour, minute);
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.TIME);
        }));

        dialog.show(this.getSupportFragmentManager(), "timeField");
    }

    public void onClearTimeClick()
    {
        if (!attemptToEditField(ReceiptField.TIME))
            return;

        this.receipt.time = null;
        this.updateDisplayedData();
        this.saveReceipt(ReceiptField.TIME);
    }

    public void onTotalGroupClick()
    {
        if (!attemptToEditField(ReceiptField.TOTAL))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        args.putString("hint", this.getResources().getString(R.string.receiptDetail_unknownTotal));
        args.putString("title", this.getResources().getString(R.string.receiptDetail_total));

        if (receipt.total == 0)
            args.putString("defaultContent", "");
        else
            args.putString("defaultContent", CurrencyAmount.toString(
                receipt.total, ".", ""
            ));

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;
            this.receipt.total = CurrencyAmount.parse(text);
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.TOTAL);
        });

        dialog.show(this.getSupportFragmentManager(), "totalField");
    }

    public void onCurrencyButtonClick()
    {
        if (!attemptToEditField(ReceiptField.CURRENCY))
            return;

        CurrencyPickerFragment dialog = new CurrencyPickerFragment();

        dialog.setSubmitListener((String currency) -> {
            this.receipt.currency = currency;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.CURRENCY);
        });

        dialog.show(this.getSupportFragmentManager(), "currencyField");
    }

    public void onFikGroupClick()
    {
        if (!attemptToEditField(ReceiptField.FIK))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_TEXT);
        args.putString("hint", "");
        args.putString("title", this.getResources().getString(R.string.receiptDetail_fik));
        args.putString("defaultContent", this.receipt.fik);

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;
            this.receipt.fik = text;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.FIK);
        });

        dialog.show(this.getSupportFragmentManager(), "fikField");
    }

    public void onBkpGroupClick()
    {
        if (!attemptToEditField(ReceiptField.BKP))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_TEXT);
        args.putString("hint", "");
        args.putString("title", this.getResources().getString(R.string.receiptDetail_bkp));
        args.putString("defaultContent", this.receipt.bkp);

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;
            this.receipt.bkp = text;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.BKP);
        });

        dialog.show(this.getSupportFragmentManager(), "bkpField");
    }

    public void onDicGroupClick()
    {
        if (!attemptToEditField(ReceiptField.DIC))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_TEXT);
        args.putString("hint", "");
        args.putString("title", this.getResources().getString(R.string.receiptDetail_dic));
        args.putString("defaultContent", this.receipt.dic);

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;
            this.receipt.dic = text;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.DIC);
        });

        dialog.show(this.getSupportFragmentManager(), "dicField");
    }

    public void onModeGroupClick()
    {
        if (!attemptToEditField(ReceiptField.EET_MODE))
            return;

        EetModePickerFragment dialog = new EetModePickerFragment();

        dialog.setSubmitListener((String mode) -> {
            this.receipt.eetMode = mode;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.EET_MODE);
        });

        dialog.show(this.getSupportFragmentManager(), "modeField");
    }

    public void onNoteGroupClick()
    {
        if (!attemptToEditField(ReceiptField.NOTE))
            return;

        Bundle args = new Bundle();
        args.putInt("inputType", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        args.putString("hint", "");
        args.putString("title", this.getResources().getString(R.string.receiptDetail_note));
        args.putString("defaultContent", this.receipt.note);

        TextFieldFragment dialog = new TextFieldFragment();
        dialog.setArguments(args);

        dialog.setSubmitListener((String text) -> {
            if (text != null && text.length() == 0)
                text = null;
            this.receipt.note = text;
            this.updateDisplayedData();
            this.saveReceipt(ReceiptField.NOTE);
        });

        dialog.show(this.getSupportFragmentManager(), "noteField");
    }

    public void onSnapClick(String snap)
    {
        if (receipt == null)
            return;

        Intent intent = new Intent(this, SnapDetailActivity.class);

        intent.putExtra(IntentExtras.EXTRA_RECEIPT, this.receipt.toBundle());
        intent.putExtra(IntentExtras.EXTRA_SNAP_NAME, snap);

        this.startActivityForResult(intent, REQUEST_SNAP_DETAIL);
    }

    /////////////////
    // Async tasks //
    /////////////////

    /**
     * Loads the receipt data and state, given the receipt id
     */
    private static class LoadReceiptTask extends AsyncTask<Void, Void, Void> {
        private WeakReference<ReceiptDetailActivity> weakActivity;
        private AppDatabase db;

        private String id;
        private Receipt receipt;
        private ReceiptState receiptState;

        public LoadReceiptTask(ReceiptDetailActivity activity, String receiptId) {
            db = AppDatabaseFactory.getInstance(activity);
            weakActivity = new WeakReference<>(activity);
            id = receiptId;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            receipt = db.receiptDao().find(id);

            if (receipt == null || this.isCancelled())
                return null;

            List<ReceiptStateChange> stateChanges = db.receiptStateChangeDao().of(receipt.id);
            receiptState = ReceiptState.fromStateChanges(stateChanges.toArray(new ReceiptStateChange[0]));

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ReceiptDetailActivity activity = weakActivity.get();

            if (activity == null)
                return;

            activity.receipt = receipt;
            activity.receiptState = receiptState;

            activity.receiptHasBeenLoaded();
        }
    }

    /**
     * Stores the receipt, updates the receipt timestamp
     * Sets state to UNUSED based on the edited field:
     * - state is not unused already
     * - receipt is in uctenkovka and eet field was edited
     * - receipt is in bazar and eet or dic field was edited
     */
    private static class SaveReceiptTask extends AsyncTask<Void, Void, Void> {
        private WeakReference<ReceiptDetailActivity> weakActivity;
        private AppDatabase db;

        private Receipt receipt;
        private ReceiptState state;
        private ReceiptField field;

        public SaveReceiptTask(ReceiptDetailActivity activity, ReceiptField field) {
            db = AppDatabaseFactory.getInstance(activity);
            weakActivity = new WeakReference<>(activity);

            receipt = activity.receipt;
            state = activity.receiptState;
            this.field = field;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            if (receipt == null || state == null) {
                cancel(true);
                return null;
            }

            // update receipt
            receipt.updatedNow();
            db.receiptDao().update(receipt);

            // update state if needed
            if (!state.is(ReceiptStateEnum.UNUSED)) {
                if (
                    (state.isUctenkovka() && field.isForUctenkovka())
                    || (state.isBazar() && field.isForBazar())
                ) {
                    ReceiptStateChange change = new ReceiptStateChange(receipt, ReceiptStateEnum.UNUSED);
                    change.metadata = new Metadata()
                        .put("reason", "EDITED")
                        .put("editedField", field.getName());

                    db.receiptStateChangeDao().insert(change);

                    state = ReceiptState.fromStateChanges(
                        db.receiptStateChangeDao().of(receipt.id)
                    );
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ReceiptDetailActivity activity = weakActivity.get();

            if (activity == null || isCancelled())
                return;

            // state has been updated
            activity.receiptState = state;
            activity.updateDisplayedData();
        }
    }
}
