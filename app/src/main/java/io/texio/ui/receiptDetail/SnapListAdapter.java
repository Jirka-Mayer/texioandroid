package io.texio.ui.receiptDetail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptFileManager;
import io.texio.db.receipt.ReceiptSnapManager;

public class SnapListAdapter extends RecyclerView.Adapter<SnapListAdapter.ViewHolder>
{
    public static final int SNAP_WIDTH_DP = 150;
    public static final int IMAGE_PADDING_DP = 32;

    public interface SnapClickListener
    {
        void onSnapClick(String snap);
    }

    private Context context;
    private Receipt receipt;

    /**
     * List of available snaps
     * (has to be kept in sync with the receipt)
     */
    private List<String> snaps = new ArrayList<String>();

    /**
     * Stores loaded snap thumbnails
     */
    private Map<String , Bitmap> snapThumbnails = new HashMap<>(20);

    private SnapClickListener listener = null;

    private int snapPixelWidth;

    public SnapListAdapter(Context context)
    {
        this.context = context;

        this.snapPixelWidth = (int) TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            SNAP_WIDTH_DP,
            this.context.getResources().getDisplayMetrics()
        );
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
        refreshDisplayedSnaps();
    }

    public void setSnapClickListener(SnapClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        ImageView imageView = new ImageView(this.context);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            this.snapPixelWidth,
            ViewGroup.LayoutParams.WRAP_CONTENT
        );
        params.gravity = Gravity.CENTER;
        imageView.setLayoutParams(params);

        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(
            IMAGE_PADDING_DP,
            IMAGE_PADDING_DP,
            IMAGE_PADDING_DP,
            IMAGE_PADDING_DP
        );

        return new ViewHolder(imageView, this.listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.setSnap(this.snaps.get(position));
    }

    @Override
    public int getItemCount()
    {
        return this.snaps.size();
    }

    /**
     * Updates displayed snaps
     * (does not clear the thumbnail cache for better performance)
     */
    public void refreshDisplayedSnaps() {
        if (receipt == null)
            return;
        ReceiptSnapManager snapManager = new ReceiptSnapManager(context, receipt);
        snaps = snapManager.getSnapList();
        notifyDataSetChanged();
    }

    /////////////////
    // View holder //
    /////////////////

    public class ViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener
    {
        protected SnapListAdapter.SnapClickListener listener;
        protected String snap = null;

        public ViewHolder(View itemView, SnapListAdapter.SnapClickListener listener)
        {
            super(itemView);

            itemView.setOnClickListener(this);

            this.listener = listener;
        }

        public void setSnap(String snap)
        {
            this.snap = snap;

            Bitmap bitmap = SnapListAdapter.this.snapThumbnails.get(this.snap);

            if (bitmap != null)
            {
                ((ImageView)this.itemView).setImageBitmap(bitmap);
            }
            else
            {
                int position = SnapListAdapter.this.snaps.indexOf(this.snap);

                if (position == -1)
                    return;

                new SnapLoadingTask(this.snap, position).execute();
            }
        }

        @Override
        public void onClick(View view)
        {
            if (this.listener != null)
                this.listener.onSnapClick(this.snap);
        }
    }

    /////////////////////
    // SnapLoadingTask //
    /////////////////////

    private class SnapLoadingTask extends AsyncTask<Void, Void, Void>
    {
        private String snapName;
        private int position;

        public SnapLoadingTask(String snapName, int position)
        {
            this.snapName = snapName;
            this.position = position;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            // get snap width
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            ReceiptFileManager fileManager = new ReceiptFileManager(
                SnapListAdapter.this.context,
                SnapListAdapter.this.receipt
            );
            byte[] snapData = fileManager.readFileBytes(this.snapName);
            BitmapFactory.decodeByteArray(snapData, 0, snapData.length, opts);

            // calculate down-sampling rate
            int sampleSize = opts.outWidth / SnapListAdapter.this.snapPixelWidth;

            if (sampleSize < 1)
                sampleSize = 1;

            // load snap
            opts = new BitmapFactory.Options();
            opts.inSampleSize = sampleSize;
            Bitmap bitmap = BitmapFactory.decodeByteArray(snapData, 0, snapData.length, opts);

            // put to cache
            SnapListAdapter.this.snapThumbnails.put(this.snapName, bitmap);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            // reload item to pull snap from cache
            SnapListAdapter.this.notifyItemChanged(this.position);
        }
    }
}
