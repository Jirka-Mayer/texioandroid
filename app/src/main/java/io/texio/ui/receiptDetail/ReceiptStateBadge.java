package io.texio.ui.receiptDetail;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.Toast;

import io.texio.R;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;

public class ReceiptStateBadge extends AppCompatTextView {

    /**
     * State that's being displayed
     */
    private ReceiptStateEnum state = ReceiptStateEnum.UNUSED;

    /**
     * Is the appearance simple?
     */
    private final boolean simple;

    public ReceiptStateBadge(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
            attrs,
            R.styleable.ReceiptStateBadge,
            0, 0
        );

        try {
            simple = a.getBoolean(R.styleable.ReceiptStateBadge_isSimple, false);
        } finally {
            a.recycle();
        }

        if (simple)
            setUpSimpleAppearance();
        else
            setUpRichAppearance();

        updateUI();
    }

    private void setUpRichAppearance() {
        setTextColor(Color.rgb(255, 255, 255));
        setTextAlignment(TEXT_ALIGNMENT_CENTER);

        // https://stackoverflow.com/questions/9685658/add-padding-on-view-programmatically
        float scale = getResources().getDisplayMetrics().density;
        int verticalPadding = (int) (2f * scale + 0.5f); // 2dp
        int horizontalPadding = (int) (10f * scale + 0.5f); // 10dp
        setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
        setMinWidth((int) (150f * scale + 0.5f)); // 150dp
    }

    private void setUpSimpleAppearance() {
        setTypeface(null, Typeface.BOLD);
    }

    public void setState(ReceiptStateEnum state) {
        this.state = state;

        if (state != null) {
            updateUI();
        }
    }

    private void updateUI() {
        if (simple)
            updateSimpleUI();
        else
            updateRichUI();
    }

    private void updateRichUI() {
        setText(state.getMessageResourceId());
        setBackgroundColor(getResources().getColor(state.getColorResourceId()));
    }

    private void updateSimpleUI() {
        int color = getResources().getColor(state.getColorResourceId());
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        String msg = getResources().getString(state.getMessageResourceId());
        String text = msg + " <font color=" + hexColor + ">&#x2B24;</font>";
        setText(Html.fromHtml(text));
    }
}
