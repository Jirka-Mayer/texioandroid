package io.texio.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;

import io.texio.AppInitializer;
import io.texio.PrivacyPolicy;
import io.texio.R;
import io.texio.ui.dashboard.DashboardActivity;

public class SplashActivity extends AppCompatActivity
{
    static
    {
        // allows vector drawables in some places
        // https://stackoverflow.com/questions/36741036/android-selector-drawable-with-vectordrawables-srccompat
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static final int REQUEST_PRIVACY_POLICY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        this.setTheme(R.style.AppTheme_Launcher);
        super.onCreate(savedInstanceState);

        // initialize application stuff
        AppInitializer initializer = new AppInitializer(this);
        initializer.initializeApplication();

        if (!PrivacyPolicy.hasBeenAccepted(this))
        {
            this.displayPrivacyPolicy();
            return;
        }

        this.startTheApp();
    }

    public void displayPrivacyPolicy()
    {
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        this.startActivityForResult(intent, REQUEST_PRIVACY_POLICY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_PRIVACY_POLICY)
        {
            if (resultCode == RESULT_OK)
                this.startTheApp();
            else
                this.finish();
        }
    }

    public void startTheApp()
    {
        Intent intent = new Intent(this, DashboardActivity.class);
        this.startActivity(intent);
        this.finish();
    }
}
