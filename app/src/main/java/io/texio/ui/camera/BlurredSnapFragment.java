package io.texio.ui.camera;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import io.texio.R;

public class BlurredSnapFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = this.getActivity().getLayoutInflater();

        View content = inflater.inflate(R.layout.dialog_blurred_snap, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setView(content)
            .setPositiveButton(R.string.blurredSnapDialog_ok, (DialogInterface dialog, int id) -> {
                // do nothing
            });

        return builder.create();
    }
}
