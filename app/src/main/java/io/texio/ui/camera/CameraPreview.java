package io.texio.ui.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.hypertrack.hyperlog.HyperLog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.texio.Application;
import io.texio.hyperlog.HyperLogExtended;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback
{
    private static final String TAG = "CameraPreview";

    private SurfaceHolder holder;
    private Camera camera;

    private double previewAspectRatio = -1;

    public CameraPreview(Context context, Camera camera)
    {
        super(context);

        this.camera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        holder = getHolder();
        holder.addCallback(this);

        // deprecated setting, but required on Android versions prior to 3.0
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        holder.setFormat(PixelFormat.TRANSPARENT);
    }

    /**
     * Chooses the proper preview size
     */
    private void setupPreviewSize()
    {
        Camera.Parameters p = this.camera.getParameters();
        List<Camera.Size> sizes = p.getSupportedPreviewSizes();

        Camera.Size pictureSize = p.getPictureSize();

        //double targetAspectRatio = (double)this.getMeasuredWidth() / (double)this.getMeasuredHeight();
        double targetAspectRatio = (double)pictureSize.height / (double)pictureSize.width;
        int targetWidth = this.getMeasuredWidth();
        final double epsilon = 0.001;

        Collections.sort(sizes, (a, b) -> {

            // first sort by proper aspect ratio (in screen coordinates)
            double aspect_a = (double)a.height / (double)a.width;
            double aspect_b = (double)b.height / (double)b.width;
            boolean a_fits = Math.abs(targetAspectRatio - aspect_a) < epsilon;
            boolean b_fits = Math.abs(targetAspectRatio - aspect_b) < epsilon;

            // if one does not fit, take the one that fits more
            if (!a_fits || !b_fits)
                return Double.compare(
                    Math.abs(targetAspectRatio - aspect_a),
                    Math.abs(targetAspectRatio - aspect_b)
                );

            // if both fit, compare the absolute size and get the one with closest (screen) width
            return Integer.compare(
                Math.abs(a.height - targetWidth),
                Math.abs(b.height - targetWidth)
            );
        });

        p.setPreviewSize(sizes.get(0).width, sizes.get(0).height);

        if (Application.getDebugMode(this.getContext()))
            this.logPreviewSizeSelectionInfo(sizes, sizes.get(0));

        // get the aspect ratio
        this.previewAspectRatio = (double)sizes.get(0).width / (double)sizes.get(0).height;
        if (this.previewAspectRatio < 1.0)
            this.previewAspectRatio = 1 / this.previewAspectRatio;

        this.camera.setParameters(p);
    }

    private void logPreviewSizeSelectionInfo(List<Camera.Size> sizes, Camera.Size selected)
    {
        Log.d(TAG, "Selecting camera preview size...");
        Log.d(TAG, "Available sizes are:");

        for (Camera.Size size : sizes)
            Log.d(TAG, size.width + "x" + size.height);

        Log.d(TAG, "Selected size is: " + selected.width + "x" + selected.height);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        try
        {
            this.setupPreviewSize();
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error setting camera preview size: " + e.getMessage());
        }

        // The Surface has been created, now tell the camera where to draw the preview.
        try
        {
            this.camera.setPreviewDisplay(holder);
            this.camera.startPreview();
        }
        catch (IOException e)
        {
            Log.e(TAG, "Error setting camera preview: " + e.getMessage());
        }

        // focus on the center of the frame (slightly delayed)
        new Handler().postDelayed(() -> this.focusCameraTo(0.5f, 0.5f), 500);
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (this.holder.getSurface() == null)
        {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try
        {
            this.camera.stopPreview();
        }
        catch (Exception e)
        {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        this.setupPreviewSize();

        // start preview with new settings
        try
        {
            this.camera.setPreviewDisplay(this.holder);
            this.camera.startPreview();
        }
        catch (Exception e)
        {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int w, h;

        // no preview yet
        if (this.previewAspectRatio < 0)
        {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        // preview initialized
        else
        {
            // fit height (to fill the entire screen, usually)
            h = ((View) this.getParent()).getHeight();
            w = (int) (h / this.previewAspectRatio);
        }

        this.setMeasuredDimension(w, h);
    }

    /////////////////////////////////
    // Touch handling & auto focus //
    /////////////////////////////////

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        if (event.getActionIndex() != 0)
            return false;

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                this.focusCameraTo(
                    event.getX() / (float)this.getMeasuredWidth(),
                    event.getY() / (float)this.getMeasuredHeight()
                );
                break;
        }

        return true;
    }

    /**
     * Transforms a point (x, y) in [0, 1] to a focus area instance for the camera
     */
    private Camera.Area focusPointToArea(float x, float y) {
        final int size = 50; // half of the square side, where 2000 is screen size
        // (not a square, but that's ok)

        x = 1f - x; // due to camera rotation, the x is flipped
        x = x * 2000f - 1000f;
        y = y * 2000f - 1000f;

        // clamp area center to [-1000 + size, 1000 - size]
        if (x < -1000 + size) x = -1000 + size;
        if (x > 1000 - size) x = 1000 - size;
        if (y < -1000 + size) y = -1000 + size;
        if (y > 1000 - size) y = 1000 - size;

        return new Camera.Area(
            // swap x, y because of screen rotation
            new Rect((int)y - size, (int)x - size, (int)y + size, (int)x + size),
            1000
        );
    }

    /**
     * Focuses on a region (x, y in 0 to 1 interval)
     */
    private void focusCameraTo(float x, float y) {

        // CANCEL AUTOFOCUS

        try {
            camera.cancelAutoFocus();
        } catch (RuntimeException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
        }

        // SET FOCUS MODE

        try {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            camera.setParameters(parameters);
        } catch (RuntimeException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
        }

        // SET FOCUS AREA

        List<Camera.Area> areas = new ArrayList<>();
        areas.add(focusPointToArea(x, y));

        try {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getMaxNumFocusAreas() > 0)
                parameters.setFocusAreas(areas);
            camera.setParameters(parameters);
        } catch (RuntimeException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
        }

        // SET METTERING AREA

        try {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getMaxNumFocusAreas() > 0)
                parameters.setMeteringAreas(areas);
            camera.setParameters(parameters);
        } catch (RuntimeException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
        }

        // START AUTOFOCUS

        try {
            this.camera.autoFocus((boolean success, Camera camera) -> {
                if (!success) {
                    HyperLog.i(TAG, "Autofocus failed.");
                    return;
                }

                // SET FOCUS MODE TO CONTINUOUS IF AVAILABLE

                try {
                    Camera.Parameters parameters = camera.getParameters();
                    List<String> supportedFocusModes = parameters.getSupportedFocusModes();

                    if (supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                        camera.setParameters(parameters);
                    }
                } catch (RuntimeException e) {
                    HyperLogExtended.exceptionWithTrace(TAG, e);
                }
            });
        } catch (RuntimeException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
        }
    }
}

