package io.texio.ui.camera;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;

import io.texio.IntentExtras;
import io.texio.Snaps;

public class ExternalCameraActivity extends AppCompatActivity
{
    public static final int REQUEST_TAKE_EXTERNAL_PHOTO = 1;

    private File snapFile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();
        this.snapFile = new File(intent.getStringExtra(IntentExtras.EXTRA_FILE_PATH));

        this.openCameraApp();
    }

    protected void openCameraApp()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // is there a camera app?
        ComponentName targetComponent = intent.resolveActivity(this.getPackageManager());
        if (targetComponent == null)
        {
            Toast.makeText(this, "No camera app found.", Toast.LENGTH_LONG).show();
            this.finish();
            return;
        }

        // add uri to the intent
        Uri snapUri = FileProvider.getUriForFile(
            this, "io.texio.fileprovider", this.snapFile
        );
        intent.putExtra(MediaStore.EXTRA_OUTPUT, snapUri);

        // grant permissions
        this.grantUriPermission(
            targetComponent.getPackageName(),
            snapUri,
            Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        );

        // start the activity
        this.startActivityForResult(intent, REQUEST_TAKE_EXTERNAL_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TAKE_EXTERNAL_PHOTO)
        {
            if (resultCode == RESULT_OK)
            {
                Intent intent = new Intent();
                intent.putExtra(IntentExtras.EXTRA_INITIAL_ROTATION, 0);
                this.setResult(RESULT_OK, intent);
            }
            else
            {
                this.setResult(resultCode);
            }

            this.finish();
        }
    }
}
