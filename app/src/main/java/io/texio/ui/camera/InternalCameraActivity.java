package io.texio.ui.camera;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import io.texio.Application;
import io.texio.IntentExtras;
import io.texio.R;

public class InternalCameraActivity extends AppCompatActivity
{
    static
    {
        // allows vector drawables in some places
        // https://stackoverflow.com/questions/36741036/android-selector-drawable-with-vectordrawables-srccompat
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private static final String TAG = "InternalCameraActivity";

    public static final int REQUEST_CAMERA_PERMISSION = 1;

    private File snapFile;

    private boolean hasCameraPermissions = false;

    protected Camera camera;
    protected CameraPreview cameraPreview;

    private FrameLayout cameraPreviewFrame;
    private ImageButton takePhotoButton;
    private ImageButton lightButton;
    private ImageButton backButton;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        this.checkCameraPermissions();

        super.onCreate(savedInstanceState);

        // no camera on this device
        // (this activity shouldn't have been launched in the first place)
        if (!this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            this.setResult(RESULT_CANCELED);
            this.finish();
            return;
        }

        // remove title bar at the top of the screen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );

        // load content
        this.setContentView(R.layout.activity_internal_camera);

        // get interesting elements
        this.cameraPreviewFrame = this.findViewById(R.id.camera_preview);
        this.takePhotoButton = this.findViewById(R.id.take_photo_button);
        this.backButton = this.findViewById(R.id.back_button);
        this.lightButton = this.findViewById(R.id.light_button);
        this.spinner = this.findViewById(R.id.spinner);

        this.takePhotoButton.setOnClickListener(v -> this.takeTheSnap());
        this.backButton.setOnClickListener(v -> this.finish());
        this.lightButton.setOnClickListener(v -> this.toggleTheLight());

        // light button visibility
        if (!this.isLightAvailable())
            this.lightButton.setVisibility(View.GONE);

        // get target file
        Intent intent = this.getIntent();
        this.snapFile = new File(intent.getStringExtra(IntentExtras.EXTRA_FILE_PATH));
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        this.openCamera();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        this.closeCamera();
    }

    /////////////////
    // Permissions //
    /////////////////

    private void checkCameraPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (this.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            {
                if (this.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA))
                {
                    Toast.makeText(
                        this,
                        "Camera permission is needed to take a picture of the receipt.",
                        Toast.LENGTH_SHORT
                    ).show();
                }

                this.requestPermissions(new String[] { Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
                return;
            }
        }

        this.hasCameraPermissions = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (requestCode == REQUEST_CAMERA_PERMISSION)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                this.hasCameraPermissions = true;
                this.openCamera();
            }
            else
            {
                this.setResult(RESULT_CANCELED);
                this.finish();
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /////////////////////////////////////
    // Camera creation and destruction //
    /////////////////////////////////////

    private void openCamera()
    {
        if (!this.hasCameraPermissions)
            return;

        this.maximizeScreenBrightness();

        try
        {
            this.camera = Camera.open(this.getBackFacingCameraIndex());
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Unable to access the camera.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Unable to access the camera.", e);
            this.setResult(RESULT_CANCELED);
            this.finish();
            return;
        }

        // set orientation to portrait (to orient the preview)
        // (activity orientation is fixed in the manifest file)
        this.camera.setDisplayOrientation(90);

        try
        {
            this.setupCameraPictureProperties();
        }
        catch (Exception e)
        {
            Log.e(TAG, "Camera picture size selection error:", e);
        }

        this.startPreview();
    }

    private void maximizeScreenBrightness()
    {
        WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
        layoutParams.screenBrightness = 1f;
        this.getWindow().setAttributes(layoutParams);
    }

    private int getBackFacingCameraIndex()
    {
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo info = new Camera.CameraInfo();

        for (int i = 0; i < numberOfCameras; i++)
        {
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                return i;
        }

        throw new RuntimeException("No back-facing camera found!");
    }

    private void setupCameraPictureProperties()
    {
        Camera.Parameters p = this.camera.getParameters();

        // select largest resolution
        // (to preserve lots of detail and so we can choose to fill the screen with preview)
        List<Camera.Size> sizes = p.getSupportedPictureSizes();
        Collections.sort(sizes, (a, b) -> -Integer.compare(a.width * a.height, b.width * b.height));
        p.setPictureSize(sizes.get(0).width, sizes.get(0).height);

        if (Application.getDebugMode(this))
            this.logPictureSizeSelectionInfo(sizes, sizes.get(0));

        p.setJpegQuality(90);

        this.camera.setParameters(p);
    }

    private void logPictureSizeSelectionInfo(List<Camera.Size> sizes, Camera.Size selected)
    {
        Log.d(TAG, "Selecting camera picture size...");
        Log.d(TAG, "Available sizes are:");

        for (Camera.Size size : sizes)
            Log.d(TAG, size.width + "x" + size.height);

        Log.d(TAG, "Selected size is: " + selected.width + "x" + selected.height);
    }

    private void startPreview()
    {
        this.cameraPreview = new CameraPreview(this, this.camera);
        this.cameraPreviewFrame.addView(this.cameraPreview);
    }

    private void closeCamera()
    {
        if (this.camera == null)
            return;

        this.camera.release();
        this.camera = null;

        this.cameraPreviewFrame.removeAllViews();
        this.cameraPreview = null;
    }

    ////////////////////
    // Light handling //
    ////////////////////

    private boolean isLightAvailable()
    {
        return this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    private boolean isTheLightOn()
    {
        Camera.Parameters parameters = this.camera.getParameters();
        return Camera.Parameters.FLASH_MODE_TORCH.equals(parameters.getFlashMode());
    }

    private void toggleTheLight()
    {
        if (!this.isLightAvailable())
        {
            Log.e(TAG, "Cannot toggle light: Light is not available.");
            return;
        }

        if (this.isTheLightOn())
            this.turnOffTheLight();
        else
            this.turnOnTheLight();
    }

    private void turnOnTheLight()
    {
        Camera.Parameters parameters = this.camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        this.camera.setParameters(parameters);
    }

    private void turnOffTheLight()
    {
        Camera.Parameters parameters = this.camera.getParameters();
        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        this.camera.setParameters(parameters);
    }

    /////////////////
    // Snap taking //
    /////////////////

    private SaveSnapTask snapSavingTask;

    public void takeTheSnap()
    {
        if (this.camera == null)
            return;

        if (snapSavingTask != null)
            return;

        this.spinner.setVisibility(View.VISIBLE);

        this.camera.takePicture(null, null, (data, camera) -> {
            this.snapSavingTask = new SaveSnapTask(this.snapFile, this);
            this.snapSavingTask.execute(data);
        });
    }

    public void snapSavingFinished(boolean result)
    {
        this.snapSavingTask = null;
        this.spinner.setVisibility(View.GONE);

        if (result)
        {
            Intent intent = new Intent();

            // android camera returns a landscape picture, rotate it for cropping
            intent.putExtra(IntentExtras.EXTRA_INITIAL_ROTATION, 1);

            this.setResult(RESULT_OK, intent);
        }
        else
        {
            this.setResult(RESULT_CANCELED);
        }

        this.finish();
    }

    private static class SaveSnapTask extends AsyncTask<byte[], Void, Boolean>
    {
        private File snapFile;
        private WeakReference<InternalCameraActivity> activity;

        public SaveSnapTask(File snapFile, InternalCameraActivity activity)
        {
            this.snapFile = snapFile;
            this.activity = new WeakReference<>(activity);
        }

        @Override
        protected Boolean doInBackground(byte[]... data)
        {
            if (this.snapFile == null)
                return false;

            try
            {
                FileOutputStream fos = new FileOutputStream(this.snapFile);
                fos.write(data[0]);
                fos.close();
            }
            catch (IOException e)
            {
                Log.e(TAG, "Error occurred while saving the snap:");
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            InternalCameraActivity a = this.activity.get();

            if (a != null)
                a.snapSavingFinished(result);
        }
    }
}
