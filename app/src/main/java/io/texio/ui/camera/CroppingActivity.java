package io.texio.ui.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import java.io.File;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.engine.BlurDetector;
import io.texio.engine.ExifOrientation;
import io.texio.hermes.HermesService;
import io.texio.hermes.SnapQueue;
import io.texio.ui.ReceiptCropper;
import io.texio.Snaps;
import io.texio.engine.CroppingPipeline;
import io.texio.engine.EnhancingPipeline;

public class CroppingActivity extends AppCompatActivity
{
    private ReceiptCropper cropper = null;
    private ProgressBar spinner = null;

    /**
     * If true, buttons are inactive
     */
    private boolean loading = true;

    /**
     * Snap that we are working on
     */
    private File snap;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_cropping);

        // get views
        this.cropper = this.findViewById(R.id.receipt_cropper);
        this.spinner = this.findViewById(R.id.spinner);

        // button click handlers
        this.findViewById(R.id.back_button).setOnClickListener(v -> this.backButtonClicked());
        this.findViewById(R.id.turn_right_button).setOnClickListener(v -> this.turnRightButtonClicked());
        this.findViewById(R.id.next_snap_button).setOnClickListener(v -> this.nextSnapButtonClicked());
        this.findViewById(R.id.continue_button).setOnClickListener(v -> this.continueButtonClicked());

        // intent
        Intent intent = this.getIntent();

        // load snap
        this.snap = Snaps.file(this, intent.getStringExtra(IntentExtras.EXTRA_SNAP_NAME));
        new LoadSnapTask().execute();

        // load initial rotation
        this.cropper.setSnapRotation(intent.getIntExtra(IntentExtras.EXTRA_INITIAL_ROTATION, 0));
    }

    protected void backButtonClicked()
    {
        if (this.loading)
            return;

        this.setResult(RESULT_CANCELED);
        this.finish();
    }

    protected void turnRightButtonClicked()
    {
        if (this.loading)
            return;

        this.cropper.setSnapRotation(this.cropper.getSnapRotation() + 1);
    }

    protected void nextSnapButtonClicked()
    {
        if (this.loading)
            return;

        new CropSnapTask(RESULT_FIRST_USER).execute();
    }

    protected void continueButtonClicked()
    {
        if (this.loading)
            return;

        new CropSnapTask(RESULT_OK).execute();
    }

    ////////////////////
    // Load snap task //
    ////////////////////

    // load receipt and crop it
    private class LoadSnapTask extends AsyncTask<Void, Void, Void>
    {
        private Bitmap bitmap = null;
        private Point[] corners = null;

        private boolean isBlurred = false;

        @Override
        protected void onPreExecute()
        {
            CroppingActivity.this.loading = true;
            CroppingActivity.this.spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... arguments)
        {
            // get rid of exif orientation
            // what I think is happening:
            // - Bitmap considers exif and performs rotation
            // - OpenCV does not and thus returns wrong corner orientation
            ExifOrientation.apply(CroppingActivity.this.snap);

            // load snap bitmap
            this.bitmap = BitmapFactory.decodeFile(
                CroppingActivity.this.snap.getAbsolutePath()
            );

            // get crop corners
            CroppingPipeline pipeline = new CroppingPipeline();
            this.corners = pipeline.calculateCropCorners(CroppingActivity.this.snap);

            // get blurriness
            BlurDetector blurDetector = new BlurDetector();
            isBlurred = blurDetector.isSnapBlurry(CroppingActivity.this.snap);

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            CroppingActivity.this.cropper.setBitmap(this.bitmap);
            CroppingActivity.this.cropper.setCorners(this.corners);

            CroppingActivity.this.loading = false;
            CroppingActivity.this.spinner.setVisibility(View.GONE);

            if (isBlurred) {
                BlurredSnapFragment dialog = new BlurredSnapFragment();
                dialog.show(CroppingActivity.this.getSupportFragmentManager(), "blurredSnap");
            }
        }
    }

    ////////////////////
    // Crop snap task //
    ////////////////////

    private class CropSnapTask extends AsyncTask<Void, Void, Void>
    {
        // Activity result code
        private int resultCode;

        public CropSnapTask(int resultCode)
        {
            this.resultCode = resultCode;
        }

        @Override
        protected void onPreExecute()
        {
            CroppingActivity.this.loading = true;
            CroppingActivity.this.spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            SnapQueue snapQueue = new SnapQueue(CroppingActivity.this);

            // enqueue raw snap to be sent to the server
            snapQueue.enqueueRawSnap(CroppingActivity.this.snap);

            // crop
            CroppingPipeline cp = new CroppingPipeline();
            cp.crop(
                CroppingActivity.this.snap,
                CroppingActivity.this.cropper.getCorners(),
                CroppingActivity.this.cropper.getSnapRotation()
            );

            // enhance
            EnhancingPipeline ep = new EnhancingPipeline();
            ep.enhance(CroppingActivity.this.snap);

            // enqueue enhanced snap to be sent to the server
            snapQueue.enqueueEnhancedSnap(CroppingActivity.this.snap);

            // start hermes service (if not running already)
            CroppingActivity.this.startService(new Intent(CroppingActivity.this, HermesService.class));

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            // activity is done
            CroppingActivity.this.setResult(this.resultCode);
            CroppingActivity.this.finish();
        }
    }
}
