package io.texio.ui.camera;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hypertrack.hyperlog.HyperLog;

import java.io.File;
import java.util.ArrayList;

import io.texio.IntentExtras;
import io.texio.Snaps;

/**
 * Creates and crops a list of snaps
 */
public class CameraActivity extends AppCompatActivity
{
    private static final String TAG = CameraActivity.class.getSimpleName();

    public static final int REQUEST_TAKE_SNAP = 1;
    public static final int REQUEST_CROP_SNAP = 2;

    public static final String PREF_USE_EXTERNAL_CAMERA = "pref_useExternalCamera";

    /**
     * Currently processed snap
     */
    protected File currentSnapFile = null;

    /**
     * List of snaps taken
     */
    protected ArrayList<File> snaps = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.snaps = new ArrayList<>();

        this.openCamera();
    }

    private void openCamera()
    {
        this.currentSnapFile = Snaps.createNew(this);

        Intent intent;
        if (this.useExternalCameraApp()) {
            intent = new Intent(this, ExternalCameraActivity.class);
            HyperLog.i(TAG, "Opening external camera.");
        } else {
            intent = new Intent(this, InternalCameraActivity.class);
            HyperLog.i(TAG, "Opening internal camera.");
        }

        intent.putExtra(IntentExtras.EXTRA_FILE_PATH, this.currentSnapFile.getAbsolutePath());
        this.startActivityForResult(intent, REQUEST_TAKE_SNAP);
    }

    private boolean useExternalCameraApp()
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getBoolean(PREF_USE_EXTERNAL_CAMERA, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TAKE_SNAP)
        {
            if (resultCode == RESULT_CANCELED)
            {
                // remove the empty snap file
                this.currentSnapFile.delete();
                this.currentSnapFile = null;

                // and no more snapping is probably wanted
                this.snappingDone();
            }

            if (resultCode == RESULT_OK)
            {
                int initialRotation = 0;

                if (data != null)
                    initialRotation = data.getIntExtra(IntentExtras.EXTRA_INITIAL_ROTATION, 0);

                this.snapTakenByCamera(initialRotation);
            }
        }

        if (requestCode == REQUEST_CROP_SNAP)
        {
            if (resultCode == RESULT_CANCELED)
            {
                // go back to camera
                this.currentSnapFile.delete();
                this.currentSnapFile = null;

                this.openCamera();
            }

            if (resultCode == RESULT_FIRST_USER)
            {
                // take next snap
                this.snapCropped();

                this.openCamera();
            }

            if (resultCode == RESULT_OK)
            {
                // return with success
                this.snapCropped();
                this.snappingDone();
            }
        }
    }

    /**
     * When a new snap is taken
     */
    public void snapTakenByCamera(int initialRotation)
    {
        if (this.currentSnapFile == null)
            throw new RuntimeException("No snap available while wanting to handle it.");

        HyperLog.i(TAG, "Snap taken, now proceeding to cropping.");

        // open cropping activity
        Intent intent = new Intent(this, CroppingActivity.class);
        intent.putExtra(IntentExtras.EXTRA_SNAP_NAME, this.currentSnapFile.getName());
        intent.putExtra(IntentExtras.EXTRA_INITIAL_ROTATION, initialRotation);

        this.startActivityForResult(intent, REQUEST_CROP_SNAP);
    }

    /**
     * Handle when a snap is cropped successfully by the cropping activity
     */
    public void snapCropped()
    {
        this.snaps.add(this.currentSnapFile);
        this.currentSnapFile = null;
    }

    /**
     * Finish the activity and return to caller
     */
    protected void snappingDone()
    {
        // successful snaps
        if (this.snaps.size() > 0)
        {
            Intent intent = new Intent();

            String[] snapNames = new String[this.snaps.size()];
            for (int i = 0; i < this.snaps.size(); i++)
                snapNames[i] = this.snaps.get(i).getName();
            intent.putExtra(IntentExtras.EXTRA_SNAPS, snapNames);

            this.setResult(RESULT_OK, intent);
            this.finish();
        }

        // no snaps taken
        else
        {
            this.setResult(RESULT_CANCELED);
            this.finish();
        }
    }
}
