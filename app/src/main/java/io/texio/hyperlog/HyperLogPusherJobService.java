package io.texio.hyperlog;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;

import io.texio.Application;
import io.texio.JobIds;
import io.texio.backend.BackEnd;

/**
 * Job that pushes log to server
 */
public class HyperLogPusherJobService extends JobService {

    public static final int PERIOD_IN_MILLISECONDS = 24 * 3600 * 1000; // 1 day

    private PushLogs task;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        task = new PushLogs(getApplicationContext()) {
            @Override
            protected void onPostExecute(Boolean success) {
                jobFinished(jobParameters, !success);
            }
        };
        task.execute();
        return true; // true = we have a background task running, don't kill me yet
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        // this method is called by the system to kill the job when running

        if (task != null) {
            task.cancel(true);
        }

        return true; // reschedule the job (because it didn't finish)
    }

    /**
     * Schedules the job if it isn't already
     * Call this during app startup
     */
    public static void scheduleIfNeeded(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        // return if scheduled already
        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            if (jobInfo.getId() == JobIds.HYPER_LOG_PUSHER_JOB) {

                // if the interval differs, cancel it and reschedule it
                if (jobInfo.getIntervalMillis() != PERIOD_IN_MILLISECONDS) {
                    jobScheduler.cancel(JobIds.HYPER_LOG_PUSHER_JOB);
                } else {
                    // if everything matches, it's already scheduled so don't do anything
                    return;
                }
            }
        }

        JobInfo jobInfo = new JobInfo.Builder(
            JobIds.HYPER_LOG_PUSHER_JOB,
            new ComponentName(context, HyperLogPusherJobService.class)
        )
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
            .setPeriodic(PERIOD_IN_MILLISECONDS)
            .build();

        jobScheduler.schedule(jobInfo);
    }

    /**
     * Performs the actual push
     */
    public static class PushLogs extends AsyncTask<Void, Void, Boolean> {

        private String appId;
        private boolean hermesEnabled;

        public PushLogs(Context context) {
            this.appId = Application.getApplicationId(context);
            this.hermesEnabled = Application.getHermesEnabled(context);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // if the user doesn't want to send usage data, it covers logs as well
            // so if not, then this task succeeds, but does nothing
            if (!hermesEnabled)
                return true;

            if (appId == null)
                return false;

            String domain = BackEnd.getDomain();

            if (domain == null)
                return false;

            String url = domain + "api/1.0/hyperlog/push/" + appId;

            return HyperLogManager.pushLogsToServer(url);
        }
    }
}
