package io.texio.hyperlog;

import android.util.Log;

import com.hypertrack.hyperlog.HyperLog;

/**
 * Extension methods on top of the hyper log library
 */
public class HyperLogExtended {
    public static void exceptionWithTrace(String tag, Exception exception) {
        HyperLog.e(tag, "EXCEPTION: " + Log.getStackTraceString(exception));
    }

    public static void exceptionWithTrace(String tag, String message, Exception exception) {
        HyperLog.e(tag, message + " EXCEPTION: " + Log.getStackTraceString(exception));
    }
}
