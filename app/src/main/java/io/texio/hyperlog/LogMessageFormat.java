package io.texio.hyperlog;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.hypertrack.hyperlog.LogFormat;

import io.texio.Application;

public class LogMessageFormat extends LogFormat {

    private String appId;
    private String appVersion;

    public LogMessageFormat(Context context) {
        super(context);

        appId = Application.getApplicationId(context);

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "?";
        }
    }

    @Override
    public String getFormattedLogMessage(String logLevelName, String tag, String message, String timeStamp,
                                         String senderName, String osVersion, String deviceUUID) {
        if (tag == null)
            tag = "";
        else
            tag = "/" + tag;

        return timeStamp + " | "
            + appVersion + " : "
            + osVersion + " | "
            + appId + " | [" + logLevelName + tag + "]: "
            + message;
    }
}
