package io.texio.hyperlog;

import android.content.Context;
import android.util.JsonToken;
import android.util.Log;

import com.hypertrack.hyperlog.DeviceLogModel;
import com.hypertrack.hyperlog.HyperLog;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.File;
import java.util.List;

/**
 * Helper for hyper log
 */
public class HyperLogManager {

    public static final String TAG = HyperLogManager.class.getSimpleName();

    /**
     * Log record expiry time in seconds
     */
    public static final int LOG_EXPIRY_TIME = 4 * 7 * 24 * 60 * 60; // 4 weeks

    /**
     * Called on application startup
     */
    public static void initializeHyperLog(Context context) {
        HyperLog.initialize(context, LOG_EXPIRY_TIME);
        HyperLog.setLogFormat(new LogMessageFormat(context));
        HyperLog.setLogLevel(Log.VERBOSE);
    }

    /**
     * Sends logs to server and clears them on success
     * @return Returns true on success
     */
    public static boolean pushLogsToServer(String url) {
        while (HyperLog.getDeviceLogBatchCount() > 0) {
            List<String> logs = HyperLog.getDeviceLogsAsStringList(true, 0);

            StringBuilder payload = new StringBuilder();
            for (String l : logs) {
                payload.append(l);
                payload.append('\n');
            }

            try {
                HttpResponse<JsonNode> response = Unirest.post(url)
                    .field("payload", payload.toString())
                    .asJson();

                if (response.getCode() != 200)
                    throw new UnirestException(
                        new RuntimeException(
                            "Response code was not 200, but " + response.getCode() + " instead."
                        )
                    );
            } catch (UnirestException e) {
                HyperLogExtended.exceptionWithTrace(
                    TAG, "Problem during log pushing. A batch of records has been lost.", e
                );
                return false;
            }
        }

        return true;
    }
}
