package io.texio.backend;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Represents the backend server
 */
public class BackEnd {

    private static final String DOMAIN = "texio.io";

    /**
     * Returns the first working domain (with https:// and ending with slash)
     * @return null if no reachable domain found
     */
    public static String getDomain() {
        try {
            InetAddress ipAddr = InetAddress.getByName(DOMAIN);

            if (ipAddr.equals(""))
                return null;

            return "https://" + DOMAIN + "/";
        } catch (UnknownHostException e) {
            return null;
        }
    }
}
