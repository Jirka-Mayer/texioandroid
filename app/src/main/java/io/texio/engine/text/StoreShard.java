package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;

public class StoreShard implements Shard
{
    private String value;
    private int priority;

    public StoreShard(String value, int priority)
    {
        this.value = value;
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        return context.getResources().getString(R.string.ocrActivity_item_store);
    }

    @Override
    public String getValue(Context context)
    {
        return this.value;
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.store = value;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.value.equals(((StoreShard) obj).value);
    }
}
