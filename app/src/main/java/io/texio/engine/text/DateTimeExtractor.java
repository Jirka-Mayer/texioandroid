package io.texio.engine.text;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Finds the first occurrence of time or date or both
 */
public class DateTimeExtractor
{
    private String text;
    private ShardBag bag;

    private int datePriority = 1000;
    private int timePriority = 1000;

    public DateTimeExtractor(String text, ShardBag bag)
    {
        this.text = text;
        this.bag = bag;
    }

    public void run()
    {
        //////////////
        // DATETIME //
        //////////////

        // higher priority to override possible detection of shop (opening) hours

        // 17.8.2018 15:13:42
        this.processPattern(
            "(^|\\s)(\\d{1,2})\\.(\\d{1,2})\\.(\\d{4})\\s+(\\d{1,2}):(\\d{1,2}):\\d{1,2}(\\s|$)",
            "$4-$3-$2T$5:$6"
        );

        // 17.8.2018 15:13
        this.processPattern(
            "(^|\\s)(\\d{1,2})\\.(\\d{1,2})\\.(\\d{4})\\s+(\\d{1,2}):(\\d{1,2})(\\s|$)",
            "$4-$3-$2T$5:$6"
        );

        //////////
        // DATE //
        //////////

        // 17.8.2018
        this.processPattern(
            "(^|\\s)(\\d{1,2})\\.(\\d{1,2})\\.(\\d{4})(\\s|$)",
            "$4-$3-$2T"
        );

        // 17/8/2018
        this.processPattern(
            "(^|\\s)(\\d{1,2})/(\\d{1,2})/(\\d{4})(\\s|$)",
            "$4-$3-$2T"
        );

        // 17.8.18
        this.processPattern(
            "(^|\\s)(\\d{1,2})\\.(\\d{1,2})\\.(\\d{2})(\\s|$)",
            "20$4-$3-$2T"
        );

        // 17/8/18
        this.processPattern(
            "(^|\\s)(\\d{1,2})/(\\d{1,2})/(\\d{2})(\\s|$)",
            "20$4-$3-$2T"
        );

        //////////
        // TIME //
        //////////

        // 15:13:42
        this.processPattern(
            "(^|\\s)(\\d{1,2}):(\\d{1,2}):\\d{1,2}(\\s|$)",
            "T$2:$3"
        );

        // 15:13
        this.processPattern(
            "(^|\\s)(\\d{1,2}):(\\d{1,2})(\\s|$|:)",
            "T$2:$3"
        );
    }

    private void processPattern(String searchPattern, String transformationPattern)
    {
        // prepare pattern and matcher
        Pattern pattern = Pattern.compile(searchPattern);
        Matcher matcher = pattern.matcher(this.text);

        // try to find the pattern in the text
        if (!matcher.find())
            return;

        // transform found group into proper format
        String transformed = matcher.group().replaceFirst(searchPattern, transformationPattern);

        // split to date and time
        int tIndex = transformed.indexOf('T');

        if (tIndex == -1)
            return;

        String[] parts = new String[] {
            transformed.substring(0, tIndex),
            transformed.substring(tIndex + 1)
        };

        // date
        if (parts[0].length() > 0)
        {
            this.bag.add(new DateShard(parts[0], datePriority));
            datePriority--;
        }

        // time
        if (parts[1].length() > 0)
        {
            this.bag.add(new TimeShard(parts[1], timePriority));
            timePriority--;
        }
    }
}
