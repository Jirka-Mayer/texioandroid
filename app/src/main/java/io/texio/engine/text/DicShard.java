package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.other.Dic;

public class DicShard implements Shard {

    private int priority;
    private String value;

    public DicShard(String value, int priority) {
        this.value = value;
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context) {
        return context.getResources().getString(R.string.ocrActivity_item_dic);
    }

    @Override
    public String getValue(Context context) {
        return Dic.toCzForm(this.value);
    }

    @Override
    public boolean isValid() {
        return Dic.isValid(value);
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt) {
        receipt.dic = Dic.toCzForm(this.value);
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.value.equals(obj);
    }
}
