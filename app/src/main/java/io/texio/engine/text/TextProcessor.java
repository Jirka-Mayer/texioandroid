package io.texio.engine.text;

public class TextProcessor
{
    private StringBuilder subject;

    public TextProcessor(String subject)
    {
        this.subject = new StringBuilder(subject);
    }

    @Override
    public String toString()
    {
        return this.subject.toString();
    }

    public void replaceChar(char source, char target)
    {
        for (int i = 0; i < this.subject.length(); i++)
        {
            char foo = this.subject.charAt(i);
            if (foo == source)
                this.subject.setCharAt(i, target);
        }
    }

    public void removeChar(char character)
    {
        for (int i = 0; i < this.subject.length(); i++)
        {
            if (this.subject.charAt(i) == character)
            {
                this.subject.deleteCharAt(i);
                i--;
            }
        }
    }

    /////////////
    // Actions //
    /////////////

    public void joinLines()
    {
        this.removeChar('\n');
    }

    public void toLowerCase()
    {
        this.subject = new StringBuilder(this.subject.toString().toLowerCase());
    }

    public void removeLowercaseDiacritics()
    {
        this.replaceChar('ě', 'e');
        this.replaceChar('š', 's');
        this.replaceChar('č', 'c');
        this.replaceChar('ř', 'r');
        this.replaceChar('ž', 'z');
        this.replaceChar('ý', 'y');
        this.replaceChar('á', 'a');
        this.replaceChar('í', 'i');
        this.replaceChar('é', 'e');
        this.replaceChar('ů', 'u');
        this.replaceChar('ú', 'u');
        this.replaceChar('ó', 'o');
        this.replaceChar('ť', 't');
        this.replaceChar('ň', 'n');
    }

    public void enhanceNumbers()
    {
        this.replaceChar('O', '0');
        this.replaceChar('l', '1');
        this.replaceChar('I', '1');
    }

    public void supressNumbers()
    {
        this.replaceChar('0', 'O');
        this.replaceChar('1', 'l');
    }
}
