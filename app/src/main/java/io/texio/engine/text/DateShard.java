package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptDate;

public class DateShard implements Shard
{
    private ReceiptDate value;
    private int priority;

    public DateShard(String value, int priority)
    {
        this.value = ReceiptDate.parse(value);
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        return context.getResources().getString(R.string.ocrActivity_item_date);
    }

    @Override
    public String getValue(Context context)
    {
        return this.value.formatHumanReadable();
    }

    @Override
    public boolean isValid()
    {
        if (this.value.day < 1 || this.value.day > 31)
            return false;

        if (this.value.month < 0 || this.value.month > 11)
            return false;

        if (this.value.year < 1990 || this.value.year >= 2100)
            return false;

        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.date = value;
    }

    @Override
    public boolean equals(Object that)
    {
        return this.value.equals(((DateShard) that).value);
    }
}
