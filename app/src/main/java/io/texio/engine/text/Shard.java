package io.texio.engine.text;

import android.content.Context;

import io.texio.db.models.Receipt;

/**
 * Shard holds a piece of information found in the text.
 * It does not do any logic, it's only a holder.
 * Logic should be performed in extractors.
 */
public interface Shard
{
    /**
     * Returns human-readable title of the shard
     */
    String getTitle(Context context);

    /**
     * Returns human-readable value of the shard
     */
    String getValue(Context context);

    /**
     * Check for validity
     * Some validation may be performed in the extractor, some may be left for this method
     * ShardBag will remove invalid shards immediately during insertion
     */
    boolean isValid();

    /**
     * Priority for sorting shards of same kind
     */
    int getPriority();

    /**
     * Apply data to a receipt
     */
    void apply(Receipt receipt);

    // WARNING! Don't forget to override .equals(Object obj)
}
