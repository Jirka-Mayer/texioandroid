package io.texio.engine.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FikBkpExtractor
{
    private String text;
    private ShardBag bag;

    public FikBkpExtractor(String text, ShardBag bag)
    {
        this.text = text;
        this.bag = bag;
    }

    public void run()
    {
        // prepare the text for extraction
        TextProcessor processor = new TextProcessor(this.text);
        processor.joinLines(); // codes are often split into two lines
        processor.enhanceNumbers();
        processor.replaceChar('.', '-'); // sometimes it can be misclassified
        processor.toLowerCase(); // to simplify regex patterns
        processor.removeLowercaseDiacritics();
        String processedText = processor.toString();

        this.findFik(processedText);
        this.findPartialFik(processedText);
        this.findBkp(processedText);
        this.findPartialBkp(processedText);
    }

    private void findFik(String processedText)
    {
        // pattern
        Pattern pattern = Pattern.compile(
            "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}-[0-9a-f]{2}"
        );
        Matcher matcher = pattern.matcher(processedText);

        // try to find the pattern in the text
        if (matcher.find())
        {
            this.bag.add(new FikShard(matcher.group(), 2));
        }
    }

    private void findPartialFik(String processedText)
    {
        // pattern
        Pattern pattern = Pattern.compile(
            "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}"
        );
        Matcher matcher = pattern.matcher(processedText);

        // try to find the pattern in the text
        if (matcher.find())
        {
            this.bag.add(new FikShard(matcher.group(), 1));
        }
    }

    private void findBkp(String processedText)
    {
        // pattern
        Pattern pattern = Pattern.compile(
            "[0-9a-f]{8}-[0-9a-f]{8}-[0-9a-f]{8}-[0-9a-f]{8}-[0-9a-f]{8}"
        );
        Matcher matcher = pattern.matcher(processedText);

        // try to find the pattern in the text
        if (matcher.find())
        {
            this.bag.add(new BkpShard(matcher.group(), 2));
        }
    }

    private void findPartialBkp(String processedText)
    {
        // pattern
        Pattern pattern = Pattern.compile(
            "[0-9a-f]{8}-[0-9a-f]{8}"
        );
        Matcher matcher = pattern.matcher(processedText);

        // try to find the pattern in the text
        if (matcher.find())
        {
            this.bag.add(new BkpShard(matcher.group(), 1));
        }
    }
}
