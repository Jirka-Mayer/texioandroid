package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;

public class PriceShard implements Shard
{
    private long value;
    private String currency;
    private int priority;

    public PriceShard(long value, String currency, int priority)
    {
        this.value = value;
        this.currency = currency;
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        return context.getResources().getString(R.string.ocrActivity_item_total);
    }

    @Override
    public String getValue(Context context)
    {
        return CurrencyAmount.formatHumanReadable(context, this.value, this.currency);
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.currency = currency;
        receipt.total = value;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.value == ((PriceShard)obj).value;
    }
}
