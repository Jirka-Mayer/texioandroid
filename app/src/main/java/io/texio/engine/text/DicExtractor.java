package io.texio.engine.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DicExtractor {

    private String text;
    private ShardBag bag;

    public DicExtractor(String text, ShardBag bag) {
        this.text = text;
        this.bag = bag;
    }

    public void run() {
        // prepare the text for extraction
        TextProcessor processor = new TextProcessor(this.text);
        processor.enhanceNumbers();
        processor.toLowerCase(); // to simplify regex patterns
        processor.removeLowercaseDiacritics();
        String processedText = processor.toString();

        this.findDic(processedText);
    }

    private void findDic(String processedText) {
        // pattern
        // due to number replacement, I gets turned into 1
        Pattern pattern = Pattern.compile(
            "d[i1]c:\\s?(cz\\d+)"
        );
        Matcher matcher = pattern.matcher(processedText);

        // try to find the pattern in the text
        if (matcher.find()) {
            String dic = matcher.group(1).toUpperCase();
            this.bag.add(new DicShard(dic, 2));
        }
    }
}
