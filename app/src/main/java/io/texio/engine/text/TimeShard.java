package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptTime;

public class TimeShard implements Shard
{
    private ReceiptTime value;
    private int priority;

    public TimeShard(String value, int priority)
    {
        this.value = ReceiptTime.parse(value);
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        return context.getResources().getString(R.string.ocrActivity_item_time);
    }

    @Override
    public String getValue(Context context)
    {
        return this.value.formatHumanReadable();
    }

    @Override
    public boolean isValid()
    {
        if (this.value.minute < 0 || this.value.minute > 59)
            return false;

        if (this.value.hour < 0 || this.value.hour > 23)
            return false;

        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.time = value;
    }

    @Override
    public boolean equals(Object that)
    {
        return this.value.equals(((TimeShard) that).value);
    }
}
