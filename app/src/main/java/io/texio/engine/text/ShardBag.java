package io.texio.engine.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.texio.db.models.Receipt;

public class ShardBag
{
    private Map<Class, Bin> bins;
    private List<Class> binOrder;

    public ShardBag()
    {
        this.bins = new HashMap<>();
        this.binOrder = new ArrayList<>();
    }

    public boolean add(Shard item)
    {
        if (!item.isValid())
            return false;

        return this.getBin(item.getClass()).add(item);
    }

    /**
     * Returns the number of bins
     */
    public int binCount()
    {
        return this.bins.size();
    }

    /**
     * Returns bin by index
     */
    public Bin getBin(int index)
    {
        return this.bins.get(this.binOrder.get(index));
    }

    /**
     * Gets or creates a bin
     */
    public Bin getBin(Class type)
    {
        Bin bin = this.bins.get(type);

        if (bin == null)
        {
            bin = new Bin();
            this.bins.put(type, bin);
            this.binOrder.add(type);
        }

        return bin;
    }

    /**
     * Returns the best shard of a given type
     */
    public Shard getBestOfType(Class type)
    {
        List<Shard> bin = this.bins.get(type);

        if (bin == null)
            return null;

        if (bin.size() == 0)
            return null;

        return bin.get(0);
    }

    /**
     * Apply selected shards to a receipt
     */
    public void apply(Receipt receipt)
    {
        for (Class key : this.binOrder)
        {
            Bin bin = this.getBin(key);

            if (!bin.accepted)
                continue;

            Shard shard = bin.get(bin.shardChoice);

            if (shard != null)
                shard.apply(receipt);
        }
    }

    /**
     * Removes bins that contain no shards
     */
    public void removeEmptyBins() {
        for (int i = 0; i < binOrder.size(); i++) {
            if (bins.get(binOrder.get(i)).size() == 0) {
                bins.remove(binOrder.get(i));
                binOrder.remove(i);
                i--;
            }
        }
    }

    /**
     * Returns list of classes _Shard that were found
     */
    public List<Class> getBinTypes() {
        return Collections.unmodifiableList(binOrder);
    }

    ///////////////
    // Bin class //
    ///////////////

    public class Bin extends ArrayList<Shard>
    {
        /**
         * Is the bin accepted by the user?
         */
        public boolean accepted = true;

        /**
         * Which shard did the user choose?
         */
        public int shardChoice = 0;

        @Override
        public boolean add(Shard shard)
        {
            // remove duplicities
            if (this.contains(shard))
                return false;

            boolean out = super.add(shard);

            // keep the bin sorted
            Collections.sort(this, (a, b) -> {
                if (a.getPriority() == b.getPriority())
                    return 0;
                if (a.getPriority() < b.getPriority())
                    return 1;
                else
                    return -1;
            });

            return out;
        }
    }
}
