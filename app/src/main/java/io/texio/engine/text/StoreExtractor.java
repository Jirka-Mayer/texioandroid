package io.texio.engine.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StoreExtractor
{
    private String text, simpleText;
    private ShardBag bag;

    public StoreExtractor(String text, ShardBag bag)
    {
        this.text = text;
        this.bag = bag;

        this.simpleText = this.simplifyText(text);
    }

    private String simplifyText(String text)
    {
        TextProcessor processor = new TextProcessor(text);

        processor.supressNumbers();
        processor.toLowerCase();
        processor.removeLowercaseDiacritics();

        return processor.toString();
    }

    public void run()
    {
        // try to find an exact name
        List<String> exactNames = this.findExactStoreNames();

        for (String name : exactNames)
            this.bag.add(new StoreShard(name, 2));

        if (exactNames.size() > 0)
            return;

        // resort to guessing
        List<String> typeNames = this.findStoreTypeNames();

        for (String name : typeNames)
            this.bag.add(new StoreShard(name, 1));
    }

    ///////////////////////
    // Exact store names //
    ///////////////////////

    private static final String[] STORE_NAMES = new String[] {
        // general
        "Albert", "BILLA", "COOP", "Kaufland", "Penny", "Tesco", "Globus",

        // electro
        "Datart", "Electro world", "Euronics",

        // hobby
        "Hornbach", "OBI", "Baumax",

        // wear
        "KiK", "Sportisimo", "Tacco", "ZOOT", "Tally weijl", "Alpine pro",
        "Baťa", "Calzedonia", "Deichmann", "Hannah", "H&M", "Inter sport",
        "Lindex", "NewYorker", "Orsay", "Promod", "Reserved",

        // food
        "KFC", "Costa coffee", "Costa", "Starbucks", "UGO", "Top lunch",

        // transport
        "České dráhy", "Regiojet", "Leo express", "IREDO", "Arriva", "ICOM",

        // oil
        "MOL", "Benzina", "Shell", "Agip",

        // other
        "Bambule", "dm", "Drogerie markt", "Pepco", "Rossmann", "Cinema city",
        "Fokus optik", "Intimissimi", "Sephora", "Tchibo"
    };

    private List<String> findExactStoreNames()
    {
        List<String> out = new ArrayList<>();

        for (String keyword : STORE_NAMES)
        {
            String simpleKeyword = this.simplifyText(keyword);

            Pattern p = Pattern.compile(
                keyword.length() <= 4 ? // short keywords match with space
                "(^|\\s)(" + simpleKeyword + ")(\\s|$)" : "()(" + simpleKeyword +")()"
            );
            Matcher m = p.matcher(this.simpleText);
            int index = 0;
            while (m.find(index))
            {
                index = m.end(2);
                out.add(keyword);
            }
        }

        // order by length DESC
        Collections.sort(out, (a, b) -> -Integer.compare(a.length(), b.length()));

        return out;
    }

    /////////////////////////
    // Names by store type //
    /////////////////////////

    private static final String[] STORE_TYPES = new String[] {
        "potraviny", "drogerie", "papírnictví", "pekárna", "lékárna", "restaurace",
        "penzion", "obuv", "hotel", "menza", "bus"
    };

    private static final String[] STORE_PREPOSITIONS = new String[] {
        "u", "na", "za", "v", "pod", "o", "do", "z", "s", "vedle", "naproti", "mezi", "pro"
    };

    private List<String> findStoreTypeNames()
    {
        List<String> out = new ArrayList<>();

        for (String keyword : STORE_TYPES)
        {
            String simpleKeyword = this.simplifyText(keyword);

            Pattern p = Pattern.compile("(" + simpleKeyword + ")\\s([a-z]+)(\\s([a-z]+))?");
            Matcher m = p.matcher(this.simpleText);
            int index = 0;
            while (m.find(index))
            {
                // has preposition?
                if (this.isPreposition(m.group(2))) {
                    out.add(
                        this.capitalize(keyword) + " "
                        + this.capitalize(m.group(2)) + " "
                        + this.capitalize(m.group(4))
                    );
                } else { // no preposition
                    out.add(this.capitalize(keyword) + " " + this.capitalize(m.group(2)));
                }

                index = m.end(1);
            }
        }

        // order by length DESC
        Collections.sort(out, (a, b) -> -Integer.compare(a.length(), b.length()));

        return out;
    }

    private boolean isPreposition(String s) {
        if (s == null)
            return false;

        if (s.length() == 0)
            return false;

        for (String preposition : STORE_PREPOSITIONS) {
            if (s.toLowerCase().equals(preposition))
                return true;
        }

        return false;
    }

    /**
     * Turns "ranDoMstring" into "Randomstring"
     */
    private String capitalize(String subject)
    {
        if (subject.length() == 0)
            return "";

        return subject.substring(0, 1).toUpperCase() + subject.substring(1, subject.length()).toLowerCase();
    }
}
