package io.texio.engine.text;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.texio.Application;
import io.texio.db.other.CurrencyAmount;

public class PriceExtractor
{
    private static final String[] KEYWORDS = new String[] {
        "celkem", "suma", "k platbe", "k úhradě", "součet", "zaplatit", "částka"
    };

    private String text;
    private ShardBag bag;

    private String currency;

    public PriceExtractor(String text, ShardBag bag, Context context)
    {
        this.text = text;
        this.bag = bag;

        this.currency = Application.getDefaultCurrency(context);
    }

    public void run()
    {
        // by keyword
        List<Long> prices = this.getPossiblePricesByKeywordSearch();
        for (long price : prices)
            this.bag.add(new PriceShard(price, this.currency, 2));

        // by magnitude (lesser priority)
        prices = this.getPossiblePricesByMagnitude();
        for (long price : prices)
            this.bag.add(new PriceShard(price, this.currency, 1));
    }

    //////////////////
    // By magnitude //
    //////////////////

    private List<Long> getPossiblePricesByMagnitude()
    {
        TextProcessor processor = new TextProcessor(this.text);
        processor.enhanceNumbers();

        List<Long> prices = this.parseNumberList(this.findPriceStrings(processor.toString()));
        Collections.sort(prices);

        if (prices.size() < 2)
            return new ArrayList<>();

        List<Long> out = new ArrayList<>();
        out.add(prices.get(prices.size() - 2)); // second biggest number
        return out;
    }

    /////////////////
    // By keywords //
    /////////////////

    /**
     * Returns prices on lines with keywords
     */
    private List<Long> getPossiblePricesByKeywordSearch()
    {
        List<String> lines = this.getKeywordLines(this.text);
        List<String> priceStrings = new ArrayList<>();

        for (String line : lines) {
            TextProcessor processor = new TextProcessor(line);
            processor.enhanceNumbers();
            priceStrings.addAll(this.findPriceStrings(processor.toString()));
        }

        List<Long> prices = this.parseNumberList(priceStrings);
        Collections.sort(prices);

        if (prices.size() == 0)
            return new ArrayList<>();

        List<Long> out = new ArrayList<>();
        out.add(prices.get(prices.size() - 1)); // get the biggest
        return out;
    }

    private String simplifyForKeywords(String subject)
    {
        TextProcessor processor = new TextProcessor(subject);

        processor.toLowerCase();
        processor.removeLowercaseDiacritics();

        return processor.toString();
    }

    /**
     * Returns list of lines that contain a keyword
     */
    private List<String> getKeywordLines(String text)
    {
        String[] originalLines = text.split("\n");
        String[] lines = this.simplifyForKeywords(text).split("\n");

        List<String> keywordLines = new ArrayList<>();

        if (originalLines.length != lines.length)
            return keywordLines;

        for (String keyword : KEYWORDS)
        {
            Pattern p = Pattern.compile(".*" + this.simplifyForKeywords(keyword) + ".*");

            for (int i = 0; i < lines.length; i++) {
                Matcher m = p.matcher(lines[i]);
                if (m.find(0)) {
                    keywordLines.add(originalLines[i]);
                }
            }
        }

        return keywordLines;
    }

    /////////////
    // Helpers //
    /////////////

    /**
     * Finds price strings inside a given string
     */
    private List<String> findPriceStrings(String text)
    {
        ArrayList<String> prices = new ArrayList<>();

        // 123,00
        // 123.00
        // 18
        // 18,-
        // 24,     (dash not recognised by CNN)
        Pattern p = Pattern.compile("(^|\\s)(\\d+\\s?[.,]\\s?(\\d{1,2}|-)?)(\\s|$)");

        Matcher m = p.matcher(text);
        int index = 0;

        while (m.find(index))
        {
            index = m.end(2); // end of the number itself, excluding surrounding space

            prices.add(m.group());
        }

        return prices;
    }

    private List<Long> parseNumberList(List<String> text)
    {
        List<Long> out = new ArrayList<>(text.size());

        for (String item : text)
        {
            long parsed = this.parseNumber(item);

            if (parsed != 0)
                out.add(parsed);
        }

        return out;
    }

    private long parseNumber(String text)
    {
        // remove all that isn't a number or decimal point
        text = text.replaceAll("[^\\d.,]", "");

        // only decimal point
        text = text.replaceAll(",", ".");

        // returns 0 on error
        return CurrencyAmount.parse(text, "\\.");
    }
}
