package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;

public class BkpShard implements Shard
{
    private int priority;
    private String value;

    public BkpShard(String value, int priority)
    {
        this.value = value;
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        if (this.value.length() < 44)
            return context.getResources().getString(R.string.ocrActivity_item_partial_bkp);
        else
            return context.getResources().getString(R.string.ocrActivity_item_bkp);
    }

    @Override
    public String getValue(Context context)
    {
        return this.value;
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.bkp = value;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.value.equals(obj);
    }
}
