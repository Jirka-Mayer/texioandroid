package io.texio.engine.text;

import android.content.Context;

import io.texio.R;
import io.texio.db.models.Receipt;

public class FikShard implements Shard
{
    private int priority;
    private String value;

    public FikShard(String value, int priority)
    {
        this.value = value;
        this.priority = priority;
    }

    @Override
    public String getTitle(Context context)
    {
        if (value.length() < 39)
            return context.getResources().getString(R.string.ocrActivity_item_partial_fik);
        else
            return context.getResources().getString(R.string.ocrActivity_item_fik);
    }

    @Override
    public String getValue(Context context)
    {
        return this.value;
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public int getPriority()
    {
        return this.priority;
    }

    @Override
    public void apply(Receipt receipt)
    {
        receipt.fik = value;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.value.equals(obj);
    }
}
