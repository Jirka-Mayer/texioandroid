package io.texio.engine;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WordDetector
{
    public static final String TAG = "WordDetector";

    public static final int NORMALIZED_WIDTH = 930;
    public static final int BIN_SIZE = 100;

    private List<Circle> circles;
    private List<Circle>[][] circleBins;
    private int binsX, binsY; // number of bins in each dimension
    private List<Component> components;
    private List<Line> lines;
    private List<Word> words;

    public double medianComponentHeight = -1;

    private TeeStore teeStore = null;

    public WordDetector()
    {
        Log.i(TAG, "Loading OpenCV...");
        if(!OpenCVLoader.initDebug())
            Log.e(TAG, "Unable to load OpenCV!");
    }

    public WordDetector(TeeStore teeStore)
    {
        this();
        this.teeStore = teeStore;
    }

    /**
     * Finds words inside a cropped and enhanced grayscale image
     * Also fuses words into lines
     */
    public void findWords(Bitmap bitmap)
    {
        // check that the bitmap is normalized
        // (normalization is done prior to word detection within the ocr pipeline)
        if (bitmap.getWidth() != NORMALIZED_WIDTH)
            throw new IllegalArgumentException("Provided bitmap does not have normalized size.");

        // convert bitmap to matrix
        Mat img = new Mat();
        Bitmap bitmap32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bitmap, img);

        // convert to grayscale
        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);

        // get edges
        Mat edges = new Mat();
        Imgproc.Canny(img, edges, 30, 50);

        // get contours
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(
            edges, contours, hierarchy,
            Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE
        );

        // now do a lot of clever stuff:
        this.getCircles(contours, bitmap.getHeight());
        this.groupCirclesIntoComponents();
        this.findAndProcessWeirdos();

        // tee word boxes
        if (this.teeStore != null)
        {
            Mat tee_img = img.clone();

            for (Component c : components)
                c.draw(tee_img);

            this.teeStore.store("components", tee_img);
        }

        // some more clever stuff
        this.buildLines();

        // cut out image pieces
        for (Word word : this.words)
            word.cutFrom(img);
    }

    /**
     * Converts contours into circles
     */
    @SuppressWarnings("unchecked")
    protected void getCircles(List<MatOfPoint> contours, int imageHeight)
    {
        this.circles = new ArrayList<>(1024);

        this.binsX = (int)Math.ceil((double)NORMALIZED_WIDTH / (double)BIN_SIZE);
        this.binsY = (int)Math.ceil((double)imageHeight / (double)BIN_SIZE);
        this.circleBins = new List[this.binsX][this.binsY];

        for (int x = 0; x < binsX; x++)
            for (int y = 0; y < binsY; y++)
                this.circleBins[x][y] = new ArrayList<>(128);

        for (MatOfPoint c : contours)
        {
            MatOfPoint2f contour = new MatOfPoint2f(c.toArray());

            // circle parameters
            float[] radius = new float[1];
            Point center = new Point();
            Imgproc.minEnclosingCircle(contour, center, radius);

            // rectangle parameters
            RotatedRect rectangle = Imgproc.minAreaRect(contour);

            // remove circles touching an edge of the image (probably some background noise)
            if (center.x - radius[0] <= 0) continue;
            if (center.x + radius[0] >= NORMALIZED_WIDTH) continue;
            if (center.y - radius[0] <= 0) continue;
            if (center.y + radius[0] >= imageHeight) continue;

            // remove line segments (narrow rectangles, large circles)
            double circleArea = radius[0] * radius[0] * Math.PI;
            double rectangleArea = rectangle.size.width * rectangle.size.height;
            if (circleArea > rectangleArea * 5.0) // magic constant
                continue;

            // remove tiny noise
            if (radius[0] < 5)
                continue;

            // remove huge mess
            if (radius[0] > 50)
                continue;

            // create the circle
            Circle circle = new Circle(center, radius[0], contour);
            this.circles.add(circle);

            // register the circle into a bin
            int x = (int)Math.floor(circle.center.x / BIN_SIZE);
            int y = (int)Math.floor(circle.center.y / BIN_SIZE);
            this.circleBins[x][y].add(circle);
        }
    }

    /**
     * Groups circles into components - initial simple grouping using DFS
     */
    protected void groupCirclesIntoComponents()
    {
        this.components = new ArrayList<>(512);
        this.dfsOver(this.circles);
    }

    // helper method to iterate dfs over some circles and append results to the global components list
    private void dfsOver(List<Circle> circles)
    {
        for (Circle circle : this.circles)
        {
            // skip closed vertices (circles)
            if (circle.component != null)
                continue;

            // skip outlier circles
            if (circle.outlier)
                continue;

            // create a component from this free circle
            Component component = new Component();
            this.dfs(circle, component);

            // initialize component properties once it has finished growing
            component.calculateRect();

            // components that are too thin ignore immediately
            if (component.rect.size.height <= 5.0)
                continue;

            // now the component is fine, let's add it to the list
            this.components.add(component);
        }
    }

    // helper dfs recursive step
    // (recursively branch from a given circle and add those circles to a given component)
    private void dfs(Circle circle, Component component)
    {
        component.add(circle);
        circle.component = component;

        int x = (int)Math.floor(circle.center.x / BIN_SIZE);
        int y = (int)Math.floor(circle.center.y / BIN_SIZE);

        for (int bx = Math.max(0, x - 1); bx < Math.min(this.binsX, x + 2); bx++)
        for (int by = Math.max(0, y - 1); by < Math.min(this.binsY, y + 2); by++)
        for (Circle c : this.circleBins[bx][by])
        {
            // skipping similar to dfsOver(...) method
            if (c.component != null) continue;
            if (c.outlier) continue;

            // recursion to neighbor
            if (circle.intersectsWith(c))
                this.dfs(c, component);
        }
    }

    /**
     * Removes too small components and splits too big components (if appropriate)
     */
    private void findAndProcessWeirdos()
    {
        this.calculateMedianComponentHeight();

        // find weird components
        for (Component c : this.components)
            c.calculateWeirdness();

        // separate big components and remove small ones
        List<Component> bigComponents = new ArrayList<>();
        for (int i = 0; i < this.components.size(); i++)
        {
            Component component = this.components.get(i);

            if (component.big)
            {
                this.components.remove(i);
                i--;
                bigComponents.add(component);
            }

            if (component.small)
            {
                this.components.remove(i);
                i--;
            }
        }

        // find outlier circles in big components
        for (Component component : bigComponents)
        {
            // normal vector to project circle position to
            double normalX = -Math.sin(component.angleRad);
            double normalY = Math.cos(component.angleRad);

            // get positions and radii of the circles
            double[] positions = new double[component.circles.size()];
            double[] radii = new double[component.circles.size()];
            for (int i = 0; i < component.circles.size(); i++)
            {
                Circle circle = component.circles.get(i);
                radii[i] = circle.radius;

                // project position along the normal vector (dot product)
                positions[i] = (circle.center.x - component.rect.center.x) * normalX
                    + (circle.center.y - component.rect.center.y) * normalY;
            }

            // find outliers (not enough neighbors in a manhattan distance circle (square)
            // around a given point = outlier) in a radius-position 2D space
            for (int i = 0; i < positions.length; i++)
            {
                int neighbors = 0;

                for (int j = 0; j < positions.length; j++)
                {
                    if (i == j)
                        continue;

                    // manhattan distance with threshold of 10.0 units (px)
                    if (Math.abs(positions[i] - positions[j]) + Math.abs(radii[i] - radii[j]) < 10.0)
                        neighbors++;
                }

                if (neighbors < 5) // neighbour count threshold
                    component.circles.get(i).outlier = true;
            }

            // free up component circles and rebuild new components with DFS
            for (Circle c : component.circles)
                c.component = null;

            this.dfsOver(component.circles);
        }
    }

    private void calculateMedianComponentHeight()
    {
        // get a list of heights
        double[] heights = new double[this.components.size()];
        for (int i = 0; i < heights.length; i++)
            heights[i] = this.components.get(i).rect.size.height;

        // sort
        Arrays.sort(heights);

        // get the middle element
        if (heights.length % 2 == 0)
            this.medianComponentHeight = (heights[heights.length / 2] + heights[heights.length / 2 + 1]) / 2;
        else
            this.medianComponentHeight = heights[heights.length / 2];
    }

    /**
     * Connects words into lines
     */
    private void buildLines()
    {
        // sort components roughly by vertical position first
        Collections.sort(this.components, (a, b) -> Double.compare(a.rect.center.y, b.rect.center.y));

        // calculate local skew coefficients ("angles" of the lines)

        /*
            The following mess computes average coefficient (=tan(angle)) over a
            sliding window. To get sums over the window faster we also calculate cumulative sums.
         */

        double windowSemiHeightPx = this.medianComponentHeight * 5.0;

        double[] yPositions = new double[this.components.size()];
        double[] weights = new double[this.components.size()];
        double[] weightsSum = new double[this.components.size()];
        double[] weightedCoeffs = new double[this.components.size()];
        double[] weightedCoeffsSum = new double[this.components.size()];

        double weightsAccum = 0;
        double weightedCoeffsAccum = 0;
        for (int i = 0; i < this.components.size(); i++)
        {
            Component component = this.components.get(i);
            yPositions[i] = component.rect.center.y;
            weights[i] = component.rect.size.width;
            weightedCoeffs[i] = Math.tan(component.angleRad) * component.rect.size.width;

            weightsAccum += weights[i];
            weightedCoeffsAccum += weightedCoeffs[i];

            weightsSum[i] = weightsAccum;
            weightedCoeffsSum[i] = weightedCoeffsAccum;
        }

        for (int i = 0; i < this.components.size(); i++)
        {
            int start_i = this.bisect(yPositions, yPositions[i] - windowSemiHeightPx, 0, i);
            int end_i = this.bisect(yPositions, yPositions[i] + windowSemiHeightPx, i, components.size() - 1);

            double coef;
            if (start_i != end_i)
                coef = (weightedCoeffsSum[end_i] - weightedCoeffsSum[start_i]) / (weightsSum[end_i] - weightsSum[start_i]);
            else
                coef = weightedCoeffs[end_i] / weights[end_i];

            components.get(i).skewCoeff = coef;
        }

        // now sort components properly with horizontal consideration
        Collections.sort(this.components, (a, b) -> a.compareTo(b));

        // find lines
        this.lines = new ArrayList<>();
        this.words = new ArrayList<>();
        Line currentLine = new Line();
        Component lastComponent = null;
        for (Component component : this.components)
        {
            if (lastComponent == null)
            {
                currentLine.addWord(component);
                lastComponent = component;
                continue;
            }

            if (component.onSameLineAs(lastComponent))
            {
                currentLine.addWord(component);
                lastComponent = component;
            }
            else
            {
                this.lines.add(currentLine);
                currentLine = new Line();
                currentLine.addWord(component);
                lastComponent = component;
            }
        }
        this.lines.add(currentLine);
    }

    private int bisect(double[] array, double targetValue, int lowIndex, int highIndex)
    {
        int index = Arrays.binarySearch(array, lowIndex, highIndex + 1, targetValue);

        // fix "not found" index
        if (index < 0)
            index = -index - 1;

        // clamp
        if (index < 0) return 0;
        if (index > highIndex) return highIndex;

        return index;
    }

    public List<Line> getLines()
    {
        return this.lines;
    }

    public List<Word> getWords()
    {
        return this.words;
    }

    //////////////////
    // Circle class //
    //////////////////

    public class Circle
    {
        public Point center;
        public float radius;
        public MatOfPoint2f contour;
        public Component component = null;
        public boolean outlier = false;

        public Circle(Point center, float radius, MatOfPoint2f contour)
        {
            this.center = center;
            this.radius = radius;
            this.contour = contour;
        }

        public boolean intersectsWith(Circle that)
        {
            // shrink horizontal distance (3x)
            // grow vertical distance (120%)
            double d_squared =
                ((this.center.x - that.center.x) / 3) * ((this.center.x - that.center.x) / 3) // tzn. ^2
                + ((this.center.y - that.center.y) * 1.2) * ((this.center.y - that.center.y) * 1.2);
            double r_squared = (this.radius + that.radius) * (this.radius + that.radius);
            return d_squared <= r_squared;
        }
    }

    /////////////////////
    // Component class //
    /////////////////////

    public class Component
    {
        public List<Circle> circles;

        public RotatedRect rect = null;
        public double angleRad = 0;

        public boolean big = false;
        public boolean small = false;

        public double skewCoeff = 0;

        public Component()
        {
            this.circles = new ArrayList<>(16);
        }

        public void add(Circle c)
        {
            this.circles.add(c);
        }

        public void calculateRect()
        {
            // map this.circles: c -> c.contour
            List<Mat> contourList = new ArrayList<>();
            for (Circle c : this.circles)
                contourList.add(c.contour);

            // concat contour points into a single matrix
            MatOfPoint2f points = new MatOfPoint2f();
            Core.vconcat(contourList, points);

            // get the rectangle
            this.rect = Imgproc.minAreaRect(points);

            // repair orientation (deg)
            if (this.rect.angle < -45)
            {
                this.rect.angle += 90;
                this.rect.size = new Size(this.rect.size.height, this.rect.size.width);
            }

            if (this.rect.angle > 45)
            {
                this.rect.angle -= 90;
                this.rect.size = new Size(this.rect.size.height, this.rect.size.width);
            }

            // rect angle in radians
            this.angleRad = this.rect.angle / 180 * Math.PI;
        }

        public void calculateWeirdness()
        {
            this.big = this.rect.size.height > WordDetector.this.medianComponentHeight * 2.0;
            this.small = this.rect.size.height < WordDetector.this.medianComponentHeight / 2.0;
        }

        public void draw(Mat img)
        {
            Point[] points = new Point[4];
            this.rect.points(points);
            MatOfPoint mat = new MatOfPoint(points[0], points[1], points[2], points[3]);
            List<MatOfPoint> matList = new ArrayList<>(1);
            matList.add(mat);
            Imgproc.drawContours(img, matList, -1, new Scalar(0, 0, 255), 2);
        }

        public boolean onSameLineAs(Component that)
        {
            double dx = that.rect.center.x - this.rect.center.x;
            double dy = this.skewCoeff * dx;
            return Math.abs(that.rect.center.y - this.rect.center.y - dy) <= WordDetector.this.medianComponentHeight;
        }

        public int compareTo(Component that)
        {
            if (this.onSameLineAs(that)) // is vertical position roughly equal?
                return Double.compare(this.rect.center.x, that.rect.center.x); // then order horizontally
            else
                return Double.compare(this.rect.center.y, that.rect.center.y); // else order vertically
        }
    }

    ////////////////
    // Word class //
    ////////////////

    public class Word
    {
        public Component component;
        public Mat image = null;
        public String text = null;

        public Word(Component component)
        {
            this.component = component;
        }

        public void cutFrom(Mat img)
        {
            final int padding = 4;
            final int h = 32;
            int w = (int)(h * (this.component.rect.size.width / this.component.rect.size.height));

            Point[] points = new Point[4];
            this.component.rect.points(points);

            Mat sourceRect = new Mat(4, 1, CvType.CV_32FC2);
            Mat destRect = new Mat(4, 1, CvType.CV_32FC2);
            sourceRect.put(
                0, 0,

                points[1].x, points[1].y,
                points[0].x, points[0].y,
                points[2].x, points[2].y,
                points[3].x, points[3].y
            );
            destRect.put(
                0, 0,

                padding, padding,
                padding, h - 2*padding,
                w - 2*padding, padding,
                w - 2*padding, h - 2*padding
            );

            Mat m = Imgproc.getPerspectiveTransform(
                sourceRect,
                destRect
            );

            this.image = new Mat();
            Imgproc.warpPerspective(img, this.image, m, new Size(w, h));
        }
    }

    ////////////////
    // Line class //
    ////////////////

    public class Line
    {
        public List<Word> words;

        public Line()
        {
            this.words = new ArrayList<>();
        }

        public void addWord(Component component)
        {
            Word word = new Word(component);
            this.words.add(word);
            WordDetector.this.words.add(word);
        }
    }
}
