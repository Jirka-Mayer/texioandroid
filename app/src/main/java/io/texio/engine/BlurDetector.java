package io.texio.engine;

import com.hypertrack.hyperlog.HyperLog;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;

public class BlurDetector {

    public static final String TAG = BlurDetector.class.getSimpleName();

    public static final int BLUR_THRESHOLD = 300; // magic constant
    public static final int NORMALIZED_WIDTH = 800; // px

    public BlurDetector() {
        // empty
    }

    /**
     * Returns true if the variance of the laplacian on a
     * normalized-with image is below the threshold value
     *
     * https://www.pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
     */
    public boolean isSnapBlurry(File snap) {
        Mat img = Imgcodecs.imread(
            snap.getAbsolutePath(),
            Imgcodecs.IMREAD_GRAYSCALE
        );

        // normalize size (width is considered to be the smaller dimension)
        float scaleFactor = (float)NORMALIZED_WIDTH / (float)Math.min(img.width(), img.height());
        Imgproc.resize(img, img, new Size(scaleFactor * img.width(), scaleFactor * img.height()));

        // calculate laplacian
        Mat lap = new Mat();
        Imgproc.Laplacian(img, lap, CvType.CV_16S);

        // calculate its variance
        MatOfDouble mean = new MatOfDouble();
        MatOfDouble stddev = new MatOfDouble();
        Core.meanStdDev(lap, mean, stddev);
        double variance = stddev.toArray()[0] * stddev.toArray()[0]; // square the stddev

        // check threshold
        boolean blurry = variance < BLUR_THRESHOLD;

        if (blurry) {
            HyperLog.i(TAG, "Receipt '" + snap.getName() + "' detected as blurry (" + (int)variance + ").");
        }

        return blurry;
    }
}
