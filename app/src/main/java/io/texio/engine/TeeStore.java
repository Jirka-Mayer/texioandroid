package io.texio.engine;

import android.content.Context;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.texio.Application;

/**
 * Stores tees from image processing pipelines for pipeline debugging
 */
public class TeeStore
{
    private Context context;

    public TeeStore(Context context)
    {
        if (!Application.getDebugMode(context))
            throw new RuntimeException("Tee store cannot be instantiated outside the debug mode.");

        this.context = context;

        this.clear();
    }

    /**
     * Returns the directory containing all tees
     */
    public File directory()
    {
        File dir = new File(this.context.getFilesDir(), "tees");

        dir.mkdirs();

        return dir;
    }

    /**
     * Returns file representing a tee
     */
    public File file(String name)
    {
        return new File(this.directory(), name);
    }

    /**
     * Clears the tee directory
     */
    public void clear()
    {
        File[] contents = this.directory().listFiles();

        for (File f : contents)
            f.delete();
    }

    /**
     * Stores a tee
     */
    public void store(String title, Mat img)
    {
        String dateString = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

        Imgcodecs.imwrite(
            this.file(dateString + "_" + title + ".png").getAbsolutePath(),
            img
        );
    }
}
