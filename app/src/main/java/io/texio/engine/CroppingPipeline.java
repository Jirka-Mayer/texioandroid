package io.texio.engine;

import android.util.Log;

import com.hypertrack.hyperlog.HyperLog;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.texio.db.models.Receipt;

public class CroppingPipeline
{
    private static final String TAG = "CroppingPipeline";
    private static final boolean TEE = false;

    public CroppingPipeline()
    {
        Log.i(TAG, "Loading OpenCV...");
        if(!OpenCVLoader.initDebug())
            Log.e(TAG, "Unable to load OpenCV!");
    }

    /**
     * Calculates the most likely cropping corners
     */
    public android.graphics.Point[] calculateCropCorners(File snap)
    {
        Mat img = Imgcodecs.imread(
            snap.getAbsolutePath(),
            Imgcodecs.IMREAD_COLOR
        );

        // grayscale
        Mat gray = new Mat();
        Imgproc.cvtColor(img, gray, Imgproc.COLOR_BGR2GRAY);

        // resize
        int height = 800;
        float scaleFactor = (float)height / (float)img.height();
        Imgproc.resize(gray, gray, new Size(scaleFactor * img.width(), height));

        // bilateral
        Mat bilateral = new Mat();
        Imgproc.bilateralFilter(gray, bilateral, 9, 75, 75);

        // adaptive threshold
        Mat threshed = new Mat();
        Imgproc.adaptiveThreshold(
            bilateral, threshed,
            255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
            Imgproc.THRESH_BINARY, 115, 4
        );

        // median filter
        Mat median = new Mat();
        Imgproc.medianBlur(threshed, median, 11);

        // add border
        Mat withBorder = new Mat();
        int borderWidth = 5;
        Core.copyMakeBorder(
            median, withBorder,
            borderWidth, borderWidth, borderWidth, borderWidth,
            Core.BORDER_CONSTANT,
            new Scalar(0, 0, 0)
        );

        // canny edges
        Mat edges = new Mat();
        Imgproc.Canny(withBorder, edges, 200, 250);

        // Contours
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(
            edges, contours, hierarchy,
            Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE
        );

        if (contours.size() == 0)
        {
            HyperLog.i(TAG, "No contours found.");
            return null;
        }

        // Find the proper contour
        // The one with the most children
        MatOfPoint bestContour = this.findBestContour(contours, hierarchy);

        // tee best contour
        /*
        if (CroppingPipeline.TEE)
        {
            Mat countourTee = new Mat();
            Imgproc.cvtColor(edges, countourTee, Imgproc.COLOR_GRAY2BGR);
            ArrayList<MatOfPoint> drawContours = new ArrayList<>();
            drawContours.add(bestContour);
            Imgproc.drawContours(countourTee, drawContours, -1, new Scalar(0, 255, 0), 5);
            this.receipt.storeFile("best-contour.png", countourTee);
        }
        */

        // hough lines
        Mat contourImg = Mat.zeros(edges.rows(), edges.cols(), edges.type());
        ArrayList<MatOfPoint> drawContours = new ArrayList<>();
        drawContours.add(bestContour);
        Imgproc.drawContours(contourImg, drawContours, -1, new Scalar(255, 255, 255), 1);

        Mat rawLines = new Mat();
        Imgproc.HoughLinesP(contourImg, rawLines, 1.0, Math.PI / 180.0, 50, 50.0, 30.0);

        if (rawLines.cols() == 0)
        {
            HyperLog.i(TAG, "No hough lines found.");
            return null;
        }

        // raw lines to line instances
        ArrayList<Line> lines = new ArrayList<>(rawLines.cols());
        for (int i = 0; i < rawLines.rows(); i++)
            lines.add(new Line(rawLines.get(i, 0)));

        /*
        if (CroppingPipeline.TEE)
        {
            Mat houghTee = new Mat();
            Imgproc.cvtColor(edges, houghTee, Imgproc.COLOR_GRAY2BGR);
            for (int i = 0; i < lines.size(); i++)
                Imgproc.line(houghTee, lines.get(i).a, lines.get(i).b, new Scalar(0, 255, 0), 5);
            this.receipt.storeFile("hough.png", houghTee);
        }
        */

        // global center of mass
        Point com = new Point(0, 0);
        for (int i = 0; i < lines.size(); i++)
        {
            com.x += lines.get(i).com.x;
            com.y += lines.get(i).com.y;
        }
        com.x /= lines.size();
        com.y /= lines.size();

        // get extremes
        ArrayList<Line> tops = new ArrayList<>();
        ArrayList<Line> bottoms = new ArrayList<>();
        ArrayList<Line> lefts = new ArrayList<>();
        ArrayList<Line> rights = new ArrayList<>();

        for (int i = 1; i < lines.size(); i++)
        {
            if (lines.get(i).vertical)
            {
                if (lines.get(i).com.x < com.x) {
                    lefts.add(lines.get(i));
                }

                if (lines.get(i).com.x > com.x) {
                    rights.add(lines.get(i));
                }
            }
            else
            {
                if (lines.get(i).com.y < com.y) {
                    tops.add(lines.get(i));
                }

                if (lines.get(i).com.y > com.y) {
                    bottoms.add(lines.get(i));
                }
            }
        }

        if (tops.size() == 0 || bottoms.size() == 0 || lefts.size() == 0 || rights.size() == 0)
        {
            HyperLog.i(TAG, "Not all borders found.");
            return null;
        }

        // scale up
        for (int i = 0; i < lines.size(); i++)
            lines.get(i).scaleUp(scaleFactor, borderWidth);

        // find best lines
        Comparator<Line> comp = (Line x, Line y) -> (int)(y.norm - x.norm);
        Line top = Collections.max(tops, comp);
        Line bottom = Collections.max(bottoms, comp);
        Line left = Collections.max(lefts, comp);
        Line right = Collections.max(rights, comp);

        /*
        if (CroppingPipeline.TEE)
        {
            Mat borderTee = new Mat();
            Imgproc.cvtColor(gray, borderTee, Imgproc.COLOR_GRAY2BGR);
            Imgproc.line(borderTee, top.a, top.b, new Scalar(0, 255, 0), 5);
            Imgproc.line(borderTee, bottom.a, bottom.b, new Scalar(255, 255, 0), 5);
            Imgproc.line(borderTee, left.a, left.b, new Scalar(0, 0, 255), 5);
            Imgproc.line(borderTee, right.a, right.b, new Scalar(0, 255, 255), 5);
            this.receipt.storeFile("border.png", borderTee);
        }
        */

        Point a, b, c, d;
        try
        {
            /*
                A-B    in standard [x, y] coordinates
                | |
                D-C
            */

            a = this.lineIntersection(top, left);
            b = this.lineIntersection(top, right);
            c = this.lineIntersection(bottom, right);
            d = this.lineIntersection(bottom, left);
        }
        catch (IllegalArgumentException e)
        {
            HyperLog.i(TAG, "Some lines were parallel.");
            return null;
        }

        HyperLog.i(TAG, "Some corners were found.");

        return new android.graphics.Point[] {
            new android.graphics.Point((int) a.x, (int) a.y),
            new android.graphics.Point((int) b.x, (int) b.y),
            new android.graphics.Point((int) c.x, (int) c.y),
            new android.graphics.Point((int) d.x, (int) d.y)
        };
    }

    /**
     * Performs cropping given the frame and rotation
     */
    public void crop(File snap, android.graphics.Point[] corners, int rotation)
    {
        Mat img = Imgcodecs.imread(
            snap.getAbsolutePath(),
            Imgcodecs.IMREAD_COLOR
        );

        Line top = new Line(corners[0], corners[1]);
        Line right = new Line(corners[1], corners[2]);
        Line bottom = new Line(corners[2], corners[3]);
        Line left = new Line(corners[3], corners[0]);

        double w = (top.norm + bottom.norm) / 2.0;
        double h = (left.norm + right.norm) / 2.0;

        Mat sourceRect = new Mat(4, 1, CvType.CV_32FC2);
        Mat destRect = new Mat(4, 1, CvType.CV_32FC2);
        sourceRect.put(
            0, 0,
            
            corners[0].x, corners[0].y,
            corners[3].x, corners[3].y,
            corners[1].x, corners[1].y,
            corners[2].x, corners[2].y
        );
        destRect.put(
            0, 0,
            
            0, 0,
            0, h,
            w, 0,
            w, h
        );
        
        Mat m = Imgproc.getPerspectiveTransform(
            sourceRect,
            destRect
        );

        Mat cropped = new Mat();
        Imgproc.warpPerspective(img, cropped, m, new Size(w, h));

        // rotate image
        switch (rotation)
        {
            case 0:
                break;

            case 1:
                Core.transpose(cropped, cropped);
                Core.flip(cropped, cropped, 1);
                break;

            case 2:
                Core.flip(cropped, cropped, -1);
                break;

            case 3:
                Core.transpose(cropped, cropped);
                Core.flip(cropped, cropped, 0);
                break;
        }

        // save the cropped image
        Imgcodecs.imwrite(
            snap.getAbsolutePath(),
            cropped
        );
    }

    // extracted logic
    private MatOfPoint findBestContour(List<MatOfPoint> contours, Mat hierarchy)
    {
        MatOfPoint bestContour = contours.get(0);
        int bestContourChildren = this.countContourChildren(0, hierarchy);

        for (int i = 1; i < contours.size(); i++)
        {
            int c = this.countContourChildren(i, hierarchy);

            if (c > bestContourChildren)
            {
                bestContourChildren = c;
                bestContour = contours.get(i);
            }
        }

        return bestContour;
    }

    /**
     * Counts direct children of a contour
     */
    private int countContourChildren(int contourIndex, Mat hierarchy)
    {
        int count = 0;

        int child = (int)hierarchy.get(0, contourIndex)[2];
        while (child != -1)
        {
            count += 1;
            child = (int)hierarchy.get(0, child)[0];
        }
        
        return count;
    }

    /**
     * Calculates intersection of two lines
     */
    private Point lineIntersection(Line a, Line b)
    {
        double x1 = a.a.x;
        double x2 = a.b.x;
        double x3 = b.a.x;
        double x4 = b.b.x;

        double y1 = a.a.y;
        double y2 = a.b.y;
        double y3 = b.a.y;
        double y4 = b.b.y;

        double denominator = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);

        if (denominator == 0.0)
            throw new IllegalArgumentException("Lines are parallel.");

        double Px = ((x1*y2 - y1*x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3*x4)) / denominator;
        double Py = ((x1*y2 - y1*x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3*x4)) / denominator;

        return new Point(Px, Py);
    }

    //////////
    // Line //
    //////////

    private class Line
    {
        public Point a, b, com, dir;
        public double norm;
        public boolean vertical;

        public Line(double[] l)
        {
            this.a = new Point(l[0], l[1]);
            this.b = new Point(l[2], l[3]);

            this.init();
        }

        public Line(android.graphics.Point a, android.graphics.Point b)
        {
            this.a = new Point(a.x, a.y);
            this.b = new Point(b.x, b.y);

            this.init();
        }

        private void init()
        {
            this.com = new Point((this.a.x + this.b.x) / 2, (this.a.y + this.b.y) / 2);
            this.dir = new Point(this.a.x - this.b.x, this.a.y - this.b.y);
            this.norm = Math.sqrt(this.dir.x * this.dir.x + this.dir.y * this.dir.y);
            this.dir.x /= this.norm;
            this.dir.y /= this.norm;

            this.vertical = Math.abs(this.dir.x) < Math.sqrt(2) / 2.0;
        }

        public void scaleUp(float scaleFactor, int borderWidth)
        {
            this.a.x = (this.a.x - borderWidth) / scaleFactor;
            this.a.y = (this.a.y - borderWidth) / scaleFactor;
            this.b.x = (this.b.x - borderWidth) / scaleFactor;
            this.b.y = (this.b.y - borderWidth) / scaleFactor;
        }
    }
}
