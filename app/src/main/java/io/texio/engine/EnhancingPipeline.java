package io.texio.engine;

import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;

import io.texio.db.models.Receipt;

public class EnhancingPipeline
{
    private static final String TAG = "EnhancingPipeline";

    private static final int PATCH_SIZE = 200;

    public EnhancingPipeline()
    {
        Log.i(TAG, "Loading OpenCV...");
        if(!OpenCVLoader.initDebug())
            Log.e(TAG, "Unable to load OpenCV!");
    }

    /**
     * Enhances a snap and prepares it for the OCR
     */
    public void enhance(File snap)
    {
        // load image
        Mat img = Imgcodecs.imread(
            snap.getAbsolutePath(),
            Imgcodecs.IMREAD_COLOR
        );

        // gray scale
        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);

        // number of patches
        int patches_x = img.cols() / EnhancingPipeline.PATCH_SIZE + 1;
        int patches_y = img.rows() / EnhancingPipeline.PATCH_SIZE + 1;

        // calculate paper color for each patch
        Mat paperColorGrid = new Mat(patches_y, patches_x, CvType.CV_8U);

        for (int x = 0; x < patches_x; x++)
        for (int y = 0; y < patches_y; y++)
        {
            int a = y * EnhancingPipeline.PATCH_SIZE;
            int b = Math.min((y + 1) * PATCH_SIZE, img.rows());
            int c = x * EnhancingPipeline.PATCH_SIZE;
            int d = Math.min((x + 1) * PATCH_SIZE, img.cols());
            
            ArrayList<Mat> imgs = new ArrayList<>(1);
            imgs.add(img.submat(a, b, c, d));

            Mat histogram = new Mat();
            MatOfFloat ranges = new MatOfFloat(0f, 256f);

            Imgproc.calcHist(imgs, new MatOfInt(0), new Mat(), histogram, new MatOfInt(256), ranges);
            Core.MinMaxLocResult result = Core.minMaxLoc(histogram);

            // double to int with clipping (just for sure)
            int color = (int) result.maxLoc.y;
            if (color > 255) color = 255;
            if (color < 0) color = 0;

            paperColorGrid.put(y, x, color);
        }

        // remove outliers
        Imgproc.medianBlur(paperColorGrid, paperColorGrid, 7);

        // let's work with floats
        img.convertTo(img, CvType.CV_32F);

        // adjust levels based on the paper color
        for (int x = 0; x < patches_x; x++)
        for (int y = 0; y < patches_y; y++)
        {
            int a = y * EnhancingPipeline.PATCH_SIZE;
            int b = Math.min((y + 1) * PATCH_SIZE, img.rows());
            int c = x * EnhancingPipeline.PATCH_SIZE;
            int d = Math.min((x + 1) * PATCH_SIZE, img.cols());
            
            double paperColor = paperColorGrid.get(y, x)[0] / 255.0;
            double textColor = 0.0 / 255.0; // works nice this way

            // bias for paper color inaccuracy (it's not exactly one shade)
            paperColor -= 0.05;

            // change levels
            Mat temp = new Mat();
            Core.divide(img.submat(a, b, c, d), new Scalar(255.0), temp);
            Core.subtract(temp, new Scalar(textColor), temp);
            Core.multiply(temp, new Scalar(255.0 / paperColor), temp);

            temp.copyTo(img.submat(a, b, c, d));
        }

        // scale away from almost white (making the text darker)
        // (if factor was infinity, then this would be thresholding)
        Core.divide(img, new Scalar(255.0), img);
        Core.subtract(img, new Scalar(0.9), img);
        Core.multiply(img, new Scalar(3.0), img);
        Core.add(img, new Scalar(0.9), img);
        Core.multiply(img, new Scalar(255.0), img);

        // back to int8
        img.convertTo(img, CvType.CV_8U);

        // save
        Imgcodecs.imwrite(
            snap.getAbsolutePath(),
            img
        );
    }
}
