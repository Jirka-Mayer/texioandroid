package io.texio.engine;

import android.content.Context;

import com.hypertrack.hyperlog.HyperLog;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.other.Dic;
import io.texio.engine.text.BkpShard;
import io.texio.engine.text.DateShard;
import io.texio.engine.text.DateTimeExtractor;
import io.texio.engine.text.DicExtractor;
import io.texio.engine.text.DicShard;
import io.texio.engine.text.FikBkpExtractor;
import io.texio.engine.text.FikShard;
import io.texio.engine.text.PriceExtractor;
import io.texio.engine.text.PriceShard;
import io.texio.engine.text.Shard;
import io.texio.engine.text.ShardBag;
import io.texio.engine.text.StoreExtractor;
import io.texio.engine.text.StoreShard;
import io.texio.engine.text.TimeShard;

/**
 * Extracts information from text
 */
public class TextPipeline {

    public static final String TAG = TextPipeline.class.getSimpleName();

    private Context context;

    /**
     * Text from which we extract information
     */
    protected String text;

    /**
     * Data extracted so far
     */
    protected ShardBag bag;

    public TextPipeline(Context context, String text)
    {
        this.context = context;
        this.text = text;
        this.bag = new ShardBag();
    }

    /**
     * Extracts all possible data
     */
    public ShardBag extractAll()
    {
        // create bins to dictate their order
        bag.getBin(StoreShard.class);
        bag.getBin(DateShard.class);
        bag.getBin(TimeShard.class);
        bag.getBin(PriceShard.class);
        bag.getBin(FikShard.class);
        bag.getBin(BkpShard.class);
        bag.getBin(DicShard.class);

        // extract items
        this.extractStore();
        this.extractDateTime();
        this.extractPrices();
        this.extractFik();
        this.extractDic();
        this.deduceStoreByHistoryAndDic();

        // clean up the bag
        bag.removeEmptyBins();

        // log
        StringBuilder sb = new StringBuilder();
        for (Class c : bag.getBinTypes()) {
            sb.append(c.getSimpleName());
            sb.append(", ");
        }
        HyperLog.i(TAG, "Extracted data: " + sb.toString());

        return this.bag;
    }

    private void extractStore()
    {
        StoreExtractor extractor = new StoreExtractor(this.text, this.bag);
        extractor.run();
    }

    private void extractDateTime()
    {
        DateTimeExtractor extractor = new DateTimeExtractor(this.text, this.bag);
        extractor.run();
    }

    private void extractPrices()
    {
        PriceExtractor extractor = new PriceExtractor(this.text, this.bag, this.context);
        extractor.run();
    }

    private void extractFik()
    {
        FikBkpExtractor extractor = new FikBkpExtractor(this.text, this.bag);
        extractor.run();
    }

    private void extractDic() {
        DicExtractor extractor = new DicExtractor(text, bag);
        extractor.run();
    }

    private void deduceStoreByHistoryAndDic() {
        DicShard s = (DicShard) bag.getBestOfType(DicShard.class);
        if (s == null)
            return;
        String dicNumbers = Dic.normalize(s.getValue(context));
        AppDatabase db = AppDatabaseFactory.getInstance(context);
        Receipt receipt = db.receiptDao().textPipeline_latestWithDic("%" + dicNumbers + "%");
        if (receipt == null || receipt.store == null)
            return;
        bag.add(new StoreShard(receipt.store, 3)); // higher priority than extraction
    }
}
