package io.texio.engine;

import android.content.Context;
import android.graphics.Bitmap;

import com.hypertrack.hyperlog.HyperLog;

import java.util.List;

import io.texio.Application;
import io.texio.engine.classifier.ParallelClassifier;

public class OcrPipeline
{
    private static final String TAG = "OcrPipeline";

    /**
     * Tesseract requires 300dpi and let's ignore font size (receipts have
     * similar font size to books)
     * A standard receipt is 79mm wide, that's 3.1 inches, that's 930 pixels
     *
     * EDIT:
     * Now no tesseract anymore, but the normalization is useful and the value stuck.
     */
    private static final int NORMALIZED_WIDTH = 930;

    protected Context context;

    public OcrPipeline(Context context)
    {
        this.context = context;
    }

    /**
     * Provides communication between the extraction process and the caller
     */
    public interface ExtractionController extends ParallelClassifier.ClassifierController
    {
        void setWordCount(int count);

        @Override
        void wordHasBeenClassified(String word);

        @Override
        boolean isCancelled();
    }

    /**
     * Extracts text from an image
     */
    public String extractText(Bitmap bitmap, ExtractionController controller)
    {
        long startTime = System.nanoTime();

        // normalize image width
        bitmap = Bitmap.createScaledBitmap(
            bitmap,
            NORMALIZED_WIDTH,
            (int)(((float)NORMALIZED_WIDTH / (float)bitmap.getWidth()) * bitmap.getHeight()),
            false
        );

        // setup tee store
        TeeStore teeStore = null;
        if (Application.getDebugMode(this.context))
            teeStore = new TeeStore((this.context));

        // find words
        WordDetector wordDetector = new WordDetector(teeStore);
        wordDetector.findWords(bitmap);

        // classify words
        List<WordDetector.Word> words = wordDetector.getWords();
        controller.setWordCount(words.size());

        ParallelClassifier classifier = new ParallelClassifier(
            this.context,
            controller // extraction controller extends classifier controller, so just pass it
        );

        for (WordDetector.Word word : words)
            classifier.classify(word);

        classifier.waitUntilDone();

        // build the output text
        StringBuilder builder = new StringBuilder();
        for (WordDetector.Line line : wordDetector.getLines())
        {
            for (int i = 0; i < line.words.size(); i++)
            {
                builder.append(line.words.get(i).text);

                if (i < line.words.size() - 1)
                    builder.append(" ");
            }

            builder.append("\n");
        }

        // report time
        long endTime = System.nanoTime();
        long timeElapsedMs = (endTime - startTime) / 1000000;
        float timeElapsedSec = timeElapsedMs / 1000f;
        HyperLog.i(TAG, "OCR time in seconds: " + timeElapsedSec);

        return builder.toString();
    }
}
