package io.texio.engine.classifier;

import android.content.Context;
import android.util.Log;

public class ClassifierThread extends Thread
{
    private Context context;
    public WordClassifier classifier = null;

    public ClassifierThread(Runnable runnable, Context context)
    {
        super(runnable);

        this.context = context;
    }

    @Override
    public void run()
    {
        // initialize TF instance
        this.classifier = new WordClassifier(this.context);

        // run the thread stuff
        super.run();

        // finish TF
        this.classifier.closeTf();
    }
}
