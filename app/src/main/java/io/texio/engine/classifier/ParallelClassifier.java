package io.texio.engine.classifier;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.texio.engine.OcrPipeline;
import io.texio.engine.WordDetector;

public class ParallelClassifier
{
    public static final int MESSAGE_WORD_CLASSIFIED = 1;

    // parameters
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private Handler handler;
    private BlockingQueue<Runnable> workQueue;
    private ThreadPoolExecutor threadPool;

    /**
     * Provides communication between the caller and the process
     */
    public interface ClassifierController
    {
        void wordHasBeenClassified(String word);
        boolean isCancelled();
    }

    public ParallelClassifier(Context context, ClassifierController controller)
    {
        // message handler
        // (receives messages sent by ClassificationRunnable instances)
        this.handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                // check cancellation
                // (because it's a good opportunity now, no other reason)
                if (controller.isCancelled())
                    ParallelClassifier.this.clearQueue();

                // report word classification
                if (msg.what == MESSAGE_WORD_CLASSIFIED)
                    controller.wordHasBeenClassified((String)msg.obj);
            }
        };

        // task queue
        this.workQueue = new LinkedBlockingQueue<>();

        // thread factory
        ThreadFactory threadFactory = new ClassifierThreadFactory(context);

        // number of threads
        int threadCount = Runtime.getRuntime().availableProcessors() / 2;
        if (threadCount < 1)
            threadCount = 1;

        // thread pool manager
        this.threadPool = new ThreadPoolExecutor(
            threadCount, // initial pool size
            threadCount, // max pool size
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            this.workQueue,
            threadFactory
        );
    }

    /**
     * Registers a word to be classified
     */
    public void classify(WordDetector.Word word)
    {
        ClassificationRunnable runnable = new ClassificationRunnable(this.handler, word);
        this.threadPool.execute(runnable);
    }

    /**
     * Waits until all submitted words are classified
     */
    public void waitUntilDone()
    {
        try
        {
            // tell thread pool that we want to end the process
            this.threadPool.shutdown();

            // and continually wait for the currently running stuff to finish
            while (!this.threadPool.awaitTermination(10, TimeUnit.SECONDS)) {}
        }
        catch (InterruptedException e)
        {
            // the classification process has been interrupted (canceled)
            // by clearing the queue
        }
    }

    /**
     * Clears the queue so that after the currently running
     * classifications finishes, the whole thing stops.
     */
    public void clearQueue()
    {
        this.workQueue.clear();
    }
}
