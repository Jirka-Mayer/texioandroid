package io.texio.engine.classifier;

import android.os.Handler;
import android.os.Process;
import android.util.Log;

import io.texio.engine.WordDetector;

public class ClassificationRunnable implements Runnable
{
    public Handler handler;
    public WordDetector.Word word;

    public ClassificationRunnable(Handler handler, WordDetector.Word word)
    {
        this.handler = handler;
        this.word = word;
    }

    @Override
    public void run()
    {
        // thread priority
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_MORE_FAVORABLE);

        // get the thread instance
        ClassifierThread thread = (ClassifierThread) Thread.currentThread();

        // do the classification
        thread.classifier.classify(this.word);

        // report progress
        this.handler.obtainMessage(
            ParallelClassifier.MESSAGE_WORD_CLASSIFIED,
            this.word.text
        ).sendToTarget();
    }
}
