package io.texio.engine.classifier;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.concurrent.ThreadFactory;

public class ClassifierThreadFactory implements ThreadFactory
{
    private Context context;

    public ClassifierThreadFactory(Context context)
    {
        this.context = context;
    }

    @Override
    public Thread newThread(@NonNull Runnable runnable)
    {
        return new ClassifierThread(runnable, this.context);
    }
}
