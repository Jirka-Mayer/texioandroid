package io.texio.engine.classifier;

import android.content.Context;
import android.util.Log;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import io.texio.engine.WordDetector;

public class WordClassifier
{
    private Context context;
    private TensorFlowInferenceInterface tf;
    private String alphabet = null;

    public WordClassifier(Context context)
    {
        this.context = context;

        // load tensorflow and the model
        System.loadLibrary("tensorflow_inference");
        this.tf = new TensorFlowInferenceInterface(
            this.context.getAssets(),
            "model/graph.pb"
        );

        // load decoding alphabet
        try
        {
            InputStream stream = this.context.getAssets().open("model/alphabet.txt");
            byte bytes[] = new byte[500];
            int length = stream.read(bytes);
            stream.close();

            this.alphabet = new String(bytes, 0, length, StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Classifies a word and sets its .text property
     */
    public void classify(WordDetector.Word word)
    {
        if (word.image == null)
            throw new IllegalArgumentException("Provided word is missing image data.");

        if (word.image.height() != 32)
            throw new IllegalArgumentException("Provided word does not have normalized height.");

        // prepare image data
        byte[] imageData = new byte[word.image.width() * 32];
        float[] data = new float[imageData.length];
        word.image.get(0, 0, imageData);
        for (int i = 0; i < imageData.length; i++)
        {
            if (imageData[i] < 0) // java byte is signed no matter what...
                data[i] = (256 + imageData[i]) / 255.0f;
            else
                data[i] = imageData[i] / 255.0f;
        }

        // prepare image widths (inside a batch)
        int[] widths = new int[] { word.image.width() };

        // feed the data
        this.tf.feed("images", data, 1, 32, widths[0]);
        this.tf.feed("widths", widths, 1);

        // run the model
        tf.run(new String[] {
            "android_greedy_predictions_indices",
            "android_greedy_predictions_values",
            "android_greedy_predictions_dense_shape"
        });

        // fetch result length
        long[] denseShape = new long[2];
        tf.fetch("android_greedy_predictions_dense_shape", denseShape);

        // fetch results
        long[] results = new long[(int)denseShape[1]];
        tf.fetch("android_greedy_predictions_values", results);

        // decode results into a string
        word.text = this.decode(results).trim(); // + trim the word
    }

    /**
     * Decodes logit values returned by the model into a string
     */
    public String decode(long[] logits)
    {
        if (this.alphabet == null)
            throw new RuntimeException("Alphabet has not been loaded, cannot decode.");

        StringBuilder builder = new StringBuilder();

        for (long logit : logits)
            builder.append(this.alphabet.charAt((int)logit));

        return builder.toString();
    }

    /**
     * Destroys the TF stuff
     */
    public void closeTf()
    {
        this.tf.close();
    }
}
