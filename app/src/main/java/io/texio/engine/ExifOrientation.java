package io.texio.engine;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExifOrientation
{
    /*
        Credit:
        https://stackoverflow.com/questions/20478765/how-to-get-the-correct-orientation-of-the-image-selected-from-the-default-image
     */

    /**
     * Applies exif orientation to a bitmap file, resetting the
     * orientation value to 0
     */
    public static void apply(File file)
    {
        // get exif data
        ExifInterface exif = null;
        try
        {
            exif = new ExifInterface(file.getAbsolutePath());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if (exif == null)
            return;

        // get orientation
        int orientation = exif.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        );

        // load the bitmap
        Bitmap originalBitmap = BitmapFactory.decodeFile(
            file.getAbsolutePath()
        );

        // rotate the bitmap
        Bitmap rotatedBitmap = ExifOrientation.rotateBitmap(originalBitmap, orientation);

        if (rotatedBitmap == null)
            return;

        // save the bitmap
        try (FileOutputStream out = new FileOutputStream(file.getAbsolutePath()))
        {
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation)
    {
        Matrix matrix = new Matrix();

        switch (orientation)
        {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }

        try
        {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
