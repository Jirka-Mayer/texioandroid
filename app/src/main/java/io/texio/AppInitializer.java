package io.texio;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import io.texio.hermes.HermesService;
import io.texio.hyperlog.HyperLogManager;
import io.texio.hyperlog.HyperLogPusherJobService;

/**
 * This class is instantiated and called on application startup
 * to perform various one-time checks
 */
public class AppInitializer  {

    protected Context context;

    public AppInitializer(Context context) {
        this.context = context;
    }

    /**
     * Main method
     */
    public void initializeApplication() {
        // initialize preferences with default values if not initialized already
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);

        // make sure the app has an id
        this.generateApplicationId();

        // init the hyper log service (must be after app id generation)
        HyperLogManager.initializeHyperLog(context);

        // start the hermes service
        // (stops itself if disabled)
        context.startService(new Intent(context, HermesService.class));

        // schedule periodic jobs
        ReceiptStateUpdaterJobService.scheduleIfNeeded(context);
        HyperLogPusherJobService.scheduleIfNeeded(context);
    }

    private void generateApplicationId() {
        String id = Application.getApplicationId(context);

        if (id == null || id.length() == 0 || id.equals("null"))
            Application.generateNewId(context);
    }
}
