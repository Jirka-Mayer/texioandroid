package io.texio.db.migrations;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

public class Migration_2_3_AddReceiptStateChangeTable extends Migration {
    public Migration_2_3_AddReceiptStateChangeTable() {
        super(2, 3);
    }

    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
        // first add DIČ to Receipt table
        database.execSQL("ALTER TABLE Receipt ADD COLUMN `dic` TEXT DEFAULT NULL;");

        // create ReceiptStateChange table
        database.execSQL(
            "CREATE TABLE ReceiptStateChange (" +
                "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "`receiptId` TEXT NOT NULL," +
                "`state` TEXT NOT NULL," +
                "`datetime` TEXT NOT NULL," +
                "`metadata` TEXT," +
                "FOREIGN KEY(receiptId) REFERENCES Receipt(id)" +
            ");"
        );

        // create index under the foreign key
        database.execSQL("CREATE INDEX IF NOT EXISTS `receiptId_index` ON `ReceiptStateChange` (`receiptId`);");
    }
}
