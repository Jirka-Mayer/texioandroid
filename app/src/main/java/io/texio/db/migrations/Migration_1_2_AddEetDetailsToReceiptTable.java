package io.texio.db.migrations;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

/**
 * Adds details for EET
 */
public class Migration_1_2_AddEetDetailsToReceiptTable extends Migration {
    public Migration_1_2_AddEetDetailsToReceiptTable() {
        super(1, 2);
    }

    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
        database.execSQL("ALTER TABLE Receipt ADD COLUMN `fik` TEXT DEFAULT NULL;");
        database.execSQL("ALTER TABLE Receipt ADD COLUMN `bkp` TEXT DEFAULT NULL;");
        database.execSQL("ALTER TABLE Receipt ADD COLUMN `eetMode` TEXT DEFAULT NULL;");
    }
}
