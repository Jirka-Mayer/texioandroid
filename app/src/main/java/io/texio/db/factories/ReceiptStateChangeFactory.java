package io.texio.db.factories;

import java.time.Instant;

import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.DateTime;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;

public class ReceiptStateChangeFactory {

    private static int nextId = 1_001;

    public static ReceiptStateChange make(Receipt receipt, ReceiptStateEnum state, Metadata metadata) {
        ReceiptStateChange change = new ReceiptStateChange();
        change.id = nextId++;
        change.receiptId = receipt.id;
        change.state = state;
        change.datetime = DateTime.now();
        change.metadata = metadata;
        return change;
    }

    public static ReceiptStateChange make(Receipt receipt, ReceiptStateEnum state) {
        return make(receipt, state, null);
    }
}
