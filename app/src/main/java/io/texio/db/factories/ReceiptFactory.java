package io.texio.db.factories;

import java.util.Random;

import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptTime;

public class ReceiptFactory {
    /**
     * Creates a receipt that could be commonly used within the appliaction
     * with all available data set
     */
    public static Receipt make() {
        Receipt receipt = new Receipt();
        receipt.deletedAt = null;
        receipt.store = randomStoreName();
        receipt.date = ReceiptDate.today();
        receipt.time = ReceiptTime.now();
        receipt.total = CurrencyAmount.parse("24.00");
        receipt.currency = "CZK";
        receipt.fik = "fik";
        receipt.bkp = "bkp";
        receipt.eetMode = Receipt.EET_MODE_DEFAULT;
        receipt.note = "This is an awesome receipt";
        receipt.text = "text";
        return receipt;
    }

    /**
     * Returns random name of a store - a real name, no gibberish
     */
    public static String randomStoreName() {
        Random generator = new Random();
        String[] names = new String[] {
            "Tesco", "Kaufland", "OBI", "Večerka", "Málek", "Benzina", "Shell"
        };
        return names[generator.nextInt(names.length)];
    }
}
