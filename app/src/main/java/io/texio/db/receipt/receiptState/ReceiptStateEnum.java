package io.texio.db.receipt.receiptState;

import com.hypertrack.hyperlog.HyperLog;

import io.texio.R;

/**
 * Receipt state once more, as an enum to make some values bound together on the level of java
 * (to prevent forgetting to enter those values when adding more enum options)
 *
 * Represents state names bound with other data. Does not know anything about metadata or change
 * history. For that purpose use ReceiptState class.
 */
public enum ReceiptStateEnum {
    UNUSED("UNUSED", R.string.receiptStateBadge_unused, R.color.receiptStateBadge_unused, true, true),

    UCTENKOVKA_UPLOADED("UCTENKOVKA_UPLOADED", R.string.receiptStateBadge_verifying, R.color.receiptStateBadge_orange, false, false),
    UCTENKOVKA_NEW("UCTENKOVKA_NEW", R.string.receiptStateBadge_verifying, R.color.receiptStateBadge_orange, false, false),
    UCTENKOVKA_VERIFIED("UCTENKOVKA_VERIFIED", R.string.receiptStateBadge_verified, R.color.receiptStateBadge_orange, false, false),
    UCTENKOVKA_REJECTED("UCTENKOVKA_REJECTED", R.string.receiptStateBadge_notFound, R.color.receiptStateBadge_red, true, false),
    UCTENKOVKA_DUPLICATE("UCTENKOVKA_DUPLICATE", R.string.receiptStateBadge_duplicate, R.color.receiptStateBadge_red, true, false),
    UCTENKOVKA_VIO_1x1x1("UCTENKOVKA_VIO_1x1x1", R.string.receiptStateBadge_vio111, R.color.receiptStateBadge_red, true, true),
    UCTENKOVKA_IN_DRAW("UCTENKOVKA_IN_DRAW", R.string.receiptStateBadge_inDraw, R.color.receiptStateBadge_inDraw, false, false),
    UCTENKOVKA_WINNING("UCTENKOVKA_WINNING", R.string.receiptStateBadge_winning, R.color.receiptStateBadge_winning, true, false),
    UCTENKOVKA_NOT_WINNING("UCTENKOVKA_NOT_WINNING", R.string.receiptStateBadge_notWinning, R.color.receiptStateBadge_notWinning, true, false),

    BAZAR_UPLOADED("BAZAR_UPLOADED", R.string.receiptStateBadge_forSale, R.color.receiptStateBadge_forSale, false, false),
    BAZAR_FOR_SALE("BAZAR_FOR_SALE", R.string.receiptStateBadge_forSale, R.color.receiptStateBadge_forSale, false, false),
    BAZAR_SOLD("BAZAR_SOLD", R.string.receiptStateBadge_sold, R.color.receiptStateBadge_removed, true, false),
    BAZAR_REMOVED("BAZAR_REMOVED", R.string.receiptStateBadge_removed, R.color.receiptStateBadge_sold, true, true);

    public static final String TAG = ReceiptStateEnum.class.getSimpleName();

    private String state;
    private int messageResourceId;
    private int colorResourceId;
    private boolean dead;
    private boolean uploadable;

    ReceiptStateEnum(String state, int messageResourceId, int colorResourceId, boolean dead, boolean uploadable) {
        this.state = state;
        this.messageResourceId = messageResourceId;
        this.colorResourceId = colorResourceId;
        this.dead = dead;
        this.uploadable = uploadable;
    }

    /**
     * Returns the string representation of the enum to be stored in the database
     */
    public String stateAsString() {
        return state;
    }

    /**
     * Returns the enum instance, for a given string representation of the enum
     * Returns null for null string as input
     * Returns UNUSED for unknown string state
     */
    public static ReceiptStateEnum fromStringState(String state) {
        if (state == null)
            return null;

        for (ReceiptStateEnum e : ReceiptStateEnum.values()) {
            if (e.state.equals(state))
                return e;
        }

        HyperLog.e(TAG, "Someone tried to deserialize state, that was not known.");
        return UNUSED;
    }

    public int getMessageResourceId() {
        return messageResourceId;
    }

    public int getColorResourceId() {
        return colorResourceId;
    }

    /**
     * Is the state dead? Meaning it cannot change by itself, so there is no
     * need to poll the servers to check if it has changed or not.
     * @return
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Can the receipt in this state be uploaded to uctenkovka or bazar?
     */
    public boolean isUploadable() {
        return uploadable;
    }
}
