package io.texio.db.receipt.receiptState;

import com.hypertrack.hyperlog.HyperLog;

public class UctenkovkaReceiptState extends ReceiptState {

    public static final String TAG = UctenkovkaReceiptState.class.getSimpleName();

    /**
     * Hold id of the receipt within the uctenkovka system,
     * May not be set, if the receipt was rejected immediately, so be careful
     */
    public static final String METADATA_UCTENKOVKA_RECEIPT_ID = "uctenkovkaReceiptId";

    /**
     * Email address of the uctenkovka account that performed the registration
     */
    public static final String METADATA_EMAIL = "registererEmail";

    public UctenkovkaReceiptState(ReceiptState state) {
        super(state);

        if (!state.isUctenkovka())
            throw new IllegalArgumentException("Provided state is not an uctenkovka state.");
    }

    /**
     * Returns true, if an uctenkovka field can be edited and in turn would
     * cause the state to change to UNUSED
     */
    public boolean canEditUctenkovkaField() {
        switch (state) {
            case UCTENKOVKA_UPLOADED:
            case UCTENKOVKA_NEW:
            case UCTENKOVKA_VERIFIED:
            case UCTENKOVKA_IN_DRAW:
            case UCTENKOVKA_WINNING:
            case UCTENKOVKA_NOT_WINNING:
                return false;

            case UCTENKOVKA_REJECTED:
            case UCTENKOVKA_DUPLICATE:
            case UCTENKOVKA_VIO_1x1x1:
                return true;
        }

        HyperLog.e(TAG, "WTF: canEditUctenkovkaField() called, but the state was unknown.");
        return true;
    }

    /**
     * Returns email address of the user that registered the receipt
     */
    public String getEmail() {
        return lastImportantChange.metadata.getString(METADATA_EMAIL, null);
    }

    /**
     * Returns receipt id within the uctenkovka system, or -1 if no ID set
     */
    public long getUctenkovkaReceiptId() {
        return lastImportantChange.metadata.getLong(METADATA_UCTENKOVKA_RECEIPT_ID, -1);
    }

    /**
     * True if the state has the receipt id within uctenkovka system
     */
    public boolean hasUctenkovkaReceiptId() {
        return getUctenkovkaReceiptId() != -1;
    }
}
