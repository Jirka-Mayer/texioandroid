package io.texio.db.receipt.receiptState;

import com.hypertrack.hyperlog.HyperLog;

public class BazarReceiptState extends ReceiptState {

    public static final String TAG = BazarReceiptState.class.getSimpleName();

    public BazarReceiptState(ReceiptState state) {
        super(state);

        if (!state.isBazar())
            throw new IllegalArgumentException("Provided state is not a bazar state.");
    }

    /**
     * Returns true, if a bazar field can be edited and in turn would
     * cause the state to change to UNUSED
     */
    public boolean canEditBazarField() {
        switch (state) {
            case BAZAR_UPLOADED:
            case BAZAR_FOR_SALE:
            case BAZAR_SOLD:
                return false;

            case BAZAR_REMOVED:
                return true;
        }

        HyperLog.e(TAG, "WTF: canEditBazarField() called, but the state was unknown.");
        return true;
    }
}
