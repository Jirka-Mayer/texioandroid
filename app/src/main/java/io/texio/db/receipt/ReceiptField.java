package io.texio.db.receipt;

import io.texio.R;

public enum ReceiptField {
    STORE("STORE", false, true, R.string.receiptDetail_store),
    DATE("DATE", true, true, R.string.receiptDetail_date),
    TIME("TIME", true, true, R.string.receiptDetail_time),
    TOTAL("TOTAL", true, true, R.string.receiptDetail_total),
    CURRENCY("CURRENCY", false, false, R.string.receiptDetail_currency),
    FIK("FIK", true, true, R.string.receiptDetail_fik),
    BKP("BKP", true, true, R.string.receiptDetail_bkp),
    EET_MODE("EET_MODE", true, true, R.string.receiptDetail_mode),
    DIC("DIC", false, true, R.string.receiptDetail_dic),
    NOTE("NOTE", false, false, R.string.receiptDetail_note);

    private String name;
    private boolean forUctenkovka;
    private boolean forBazar;
    private int textResourceId;

    ReceiptField(String name, boolean forUctenkovka, boolean forBazar, int textResourceId) {
        this.name = name;
        this.forUctenkovka = forUctenkovka;
        this.forBazar = forBazar;
        this.textResourceId = textResourceId;
    }

    public String getName() {
        return name;
    }

    /**
     * Whether the field contains data for uctenkovka
     */
    public boolean isForUctenkovka() {
        return forUctenkovka;
    }

    /**
     * Whether the field contains data for bazar
     */
    public boolean isForBazar() {
        return forBazar;
    }

    /**
     * Returns text resource id of the field title in receipt detail activity
     */
    public int getTextResourceId() {
        return textResourceId;
    }
}
