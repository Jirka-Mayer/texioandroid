package io.texio.db.receipt;

import android.content.Context;
import android.support.annotation.NonNull;

import com.hypertrack.hyperlog.HyperLog;

import java.io.File;
import java.nio.charset.StandardCharsets;

import io.texio.Encrypter;
import io.texio.db.models.Receipt;
import io.texio.util.FileUtil;

/**
 * Manages files associated with a receipt and handles their encryption
 *
 * For historic reasons is the encryption not mandatory (for reading; for writing now is)
 */
public class ReceiptFileManager {

    public static final String TAG = ReceiptFileManager.class.getSimpleName();

    /**
     * The directory that contains all the receipts
     * (external files dir)
     */
    private static final String RECEIPTS_DIRECTORY = "receipts";

    /**
     * Encrypted files get this suffix to recognise that they are encrypted
     * Note: does not conform to any .aes file formats
     */
    private static final String ENCRYPTION_SUFFIX = ".aes";

    /**
     * The receipt directory
     * It's guaranteed to exist since if not it's created during construction of this instance
     */
    private File directory;

    /**
     * Encrypter used to encrypt the files
     */
    private Encrypter encrypter;

    public ReceiptFileManager(Context context, Receipt receipt) {
        directory = prepareDirectory(context, receipt);
        encrypter = Encrypter.getDefault(context);
    }

    /**
     * Returns the directory file of the receipt and creates it if doesn't exist
     */
    private static File prepareDirectory(Context context, Receipt receipt) {
        File dir = getReceiptDirectory(context, receipt);

        dir.mkdirs();

        if (!dir.exists())
            throw new RuntimeException("Receipt directory creation failed.");

        return dir;
    }

    /**
     * Returns a file representing the receipt directory
     */
    private static File getReceiptDirectory(Context context, Receipt receipt) {
        // group directory (2 symbol directory name)
        File groupDir = new File(
            context.getExternalFilesDir(RECEIPTS_DIRECTORY),
            receipt.id.substring(0, 2)
        );

        // directory
        return new File(groupDir, receipt.id);
    }

    /**
     * Deletes the receipt directory if it exists with all it's content
     * Warning: After this no other method on this instance should be called.
     */
    public void deleteReceiptDirectory() {
        if (!directory.exists())
            return;

        for (File f : directory.listFiles())
            f.delete();

        directory.delete();
    }

    /**
     * Returns true if a given file exists and so can be opened.
     */
    public boolean exists(@NonNull String fileName) {
        return getFileNameThatExists(fileName, new boolean[1]) != null;
    }

    /**
     * Reads contents of a file as byte array
     * Returns null on some error or if the file does not exist
     */
    public byte[] readFileBytes(@NonNull String fileName) {
        boolean[] encrypted = new boolean[1];
        File existingFile = getFileNameThatExists(fileName, encrypted);

        if (existingFile == null) {
            HyperLog.w(
                TAG, "Someone requested reading file '" + fileName + "' but it does not exist."
            );
            return null;
        }

        byte[] data = FileUtil.readAllBytes(existingFile);

        if (data == null) {
            HyperLog.e(
                TAG, "Reading the file '" + fileName + "' resulted in an error."
            );
            return null;
        }

        if (encrypted[0]) {
            data = encrypter.decryptBytes(data);

            if (data == null) {
                HyperLog.e(
                    TAG, "Decrypting the file '" + fileName + "' resulted in an error."
                );
            }
        }

        return data;
    }

    /**
     * Returns the filename.aes or filename or null if none of those exist
     * Handles reading of non-encrypted files
     * @param encrypted Output parameter -> is the returned file the encrypted version?
     */
    private File getFileNameThatExists(String fileName, boolean[] encrypted) {
        File encryptedFile = new File(directory, fileName + ENCRYPTION_SUFFIX);
        if (encryptedFile.exists()) {
            encrypted[0] = true;
            return encryptedFile;
        }

        File nonEncryptedFile = new File(directory, fileName);
        if (nonEncryptedFile.exists()) {
            encrypted[0] = false;
            return nonEncryptedFile;
        }

        return null;
    }

    /**
     * Writes data to a file and encrypts it
     * Returns true on success
     */
    public boolean writeFileBytes(@NonNull String fileName, @NonNull byte[] content) {
        File file = new File(directory, fileName + ENCRYPTION_SUFFIX);

        content = encrypter.encryptBytes(content);

        if (content == null) {
            HyperLog.e(TAG, "Encryption failed while writing to file '" + fileName + "'.");
            return false;
        }

        return FileUtil.writeAllBytes(file, content);
    }

    /**
     * Reads contents of a file as a UTF-8 string
     */
    public String readFileText(String fileName) {
        byte[] bytes = readFileBytes(fileName);

        if (bytes == null)
            return null;

        return new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * Writes contents of a file as a utf-8 string
     */
    public boolean writeFileText(String fileName, String text) {
        return writeFileBytes(
            fileName,
            text.getBytes(StandardCharsets.UTF_8)
        );
    }

    /**
     * Deletes a file and/or it's encrypted version
     */
    public void deleteFile(String fileName) {
        File encryptedFile = new File(this.directory, fileName + ENCRYPTION_SUFFIX);
        File plainFile = new File(this.directory, fileName);

        // if one doesn't exist, it does not cause any problems
        encryptedFile.delete();
        plainFile.delete();
    }
}
