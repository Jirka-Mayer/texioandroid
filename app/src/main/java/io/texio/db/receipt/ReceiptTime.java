package io.texio.db.receipt;

import android.support.annotation.NonNull;

import java.util.Calendar;

/**
 * Represents the time the receipt was made in format 'HH:mm'
 */
public class ReceiptTime implements Comparable<ReceiptTime>
{
    public int hour;
    public int minute;

    public ReceiptTime(int hour, int minute)
    {
        this.hour = hour;
        this.minute = minute;
    }

    public static ReceiptTime parse(String time)
    {
        String[] parts = time.split(":");

        if (parts.length < 2)
            return null;

        return new ReceiptTime(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
    }

    public static ReceiptTime now()
    {
        Calendar c = Calendar.getInstance();

        return new ReceiptTime(
            c.get(Calendar.HOUR_OF_DAY),
            c.get(Calendar.MINUTE)
        );
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof ReceiptTime))
            return false;

        ReceiptTime that = (ReceiptTime) obj;
        return this.hour == that.hour && this.minute == that.minute;
    }

    @Override
    public String toString()
    {
        String hourZero = this.hour < 10 ? "0" : "";
        String minuteZero = this.minute < 10 ? "0" : "";

        return hourZero + this.hour + ":" + minuteZero + this.minute;
    }

    @Override
    public int compareTo(@NonNull ReceiptTime receiptTime) {
        return toString().compareTo(receiptTime.toString());
    }

    public String formatHumanReadable()
    {
        return this.toString();
    }
}
