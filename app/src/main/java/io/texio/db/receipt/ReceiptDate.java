package io.texio.db.receipt;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.util.Calendar;

/**
 * Represents the date the receipt was made in format 'YYYY-MM-DD'
 */
public class ReceiptDate implements Comparable<ReceiptDate>
{
    /**
     * Year, eg. "2016"
     */
    public int year;

    /**
     * Zero-based month index
     */
    public int month; // january = 0

    /**
     * Day number (one-based)
     */
    public int day;

    public ReceiptDate(int year, int month, int day)
    {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public static ReceiptDate fromCalendar(Calendar c)
    {
        return new ReceiptDate(
            c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH)
        );
    }

    public static ReceiptDate today()
    {
        return fromCalendar(Calendar.getInstance());
    }

    public static ReceiptDate parse(String time)
    {
        String[] parts = time.split("-");

        if (parts.length != 3)
            return null;

        return new ReceiptDate(
            Integer.parseInt(parts[0]),
            Integer.parseInt(parts[1]) - 1,
            Integer.parseInt(parts[2])
        );
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof ReceiptDate))
            return false;

        ReceiptDate that = (ReceiptDate) obj;
        return this.year == that.year && this.month == that.month && this.day == that.day;
    }

    @Override
    public int compareTo(@NonNull ReceiptDate receiptDate) {
        return toString().compareTo(receiptDate.toString());
    }

    @Override
    public String toString()
    {
        String monthZero = this.month + 1 < 10 ? "0" : "";
        String dayZero = this.day < 10 ? "0" : "";

        return this.year + "-" + monthZero + (this.month + 1) + "-" + dayZero + this.day;
    }

    public String formatHumanReadable()
    {
        Calendar c = Calendar.getInstance();

        c.set(Calendar.YEAR, this.year);
        c.set(Calendar.MONTH, this.month);
        c.set(Calendar.DAY_OF_MONTH, this.day);

        return DateFormat.getDateInstance().format(c.getTime());
    }
}
