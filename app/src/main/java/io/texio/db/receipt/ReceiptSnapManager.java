package io.texio.db.receipt;

import android.content.Context;

import com.hypertrack.hyperlog.HyperLog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.texio.db.models.Receipt;
import io.texio.util.RandomString;
import io.texio.util.FileUtil;

/**
 * Manages snaps associated with a receipt
 * Snaps are identified by a name string which is the name of the snap file with extension
 */
public class ReceiptSnapManager {

    public static final String TAG = ReceiptSnapManager.class.getSimpleName();

    /**
     * Filename of the snap list
     */
    private static final String FILE_SNAP_LIST = "snap-list.txt";

    /**
     * Cache for the snapList
     * Don't forget to invalidate on list update
     */
    private List<String> snapCache = null;

    /**
     * Manager for receipt files
     */
    private ReceiptFileManager fileManager;

    public ReceiptSnapManager(ReceiptFileManager fileManager) {
        this.fileManager = fileManager;
    }

    public ReceiptSnapManager(Context context, Receipt receipt) {
        this.fileManager = new ReceiptFileManager(context, receipt);
    }

    /**
     * Returns the list of available snaps
     */
    public List<String> getSnapList() {
        if (snapCache != null)
            return snapCache;

        String content = null;
        if (fileManager.exists(FILE_SNAP_LIST)) // check existence to prevent warnings
            content = fileManager.readFileText(FILE_SNAP_LIST);

        List<String> list = new ArrayList<>();

        if (content != null) {
            String[] lines = content.split("\n");

            for (String line : lines)
                if (line.length() != 0)
                    list.add(line);
        }

        snapCache = list;

        return list;
    }

    /**
     * Writes list of snaps into the snap list file
     */
    private void storeSnapList(List<String> snapNames) {
        snapCache = null; // invalidate cache

        StringBuilder builder = new StringBuilder();

        for (String name : snapNames) {
            builder.append(name);
            builder.append("\n");
        }

        fileManager.writeFileText(FILE_SNAP_LIST, builder.toString());
    }

    /**
     * Adds a new snap at the end of the snap list
     * (The provided snap file gets copied into the receipt directory and encrypted)
     */
    public void addSnap(File snap) {
        this.snapCache = null; // invalidate cache

        // read snap data
        byte[] data = FileUtil.readAllBytes(snap);

        if (data == null) {
            HyperLog.e(TAG, "Couldn't read snap file to be added into the receipt.");
            return;
        }

        // put snap data into a receipt snap
        String name = "snap_" + RandomString.generate(5) + ".jpg";
        fileManager.writeFileBytes(name, data);

        // add snap to the list
        List<String> list = this.getSnapList();
        list.add(name);
        storeSnapList(list);
    }

    /**
     * Removes a snap from the list and deletes the snap file
     */
    public void removeSnap(String snapName) {
        if (snapName == null)
            return;

        snapCache = null; // invalidate cache

        // update snap list
        List<String> list = getSnapList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(snapName)) {
                list.remove(i);
                break;
            }
        }
        storeSnapList(list);

        // delete the file
        fileManager.deleteFile(snapName);
    }
}
