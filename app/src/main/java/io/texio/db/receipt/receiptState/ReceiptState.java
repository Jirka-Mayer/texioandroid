package io.texio.db.receipt.receiptState;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.List;

import io.texio.db.models.ReceiptStateChange;
import static io.texio.db.receipt.receiptState.ReceiptStateEnum.*;

/**
 * Represents a state of the receipt
 * IMMUTABLE
 *
 * Contains more information then the plain ReceiptStateEnum, eg. metadata, change history, ...
 */
public class ReceiptState {

    @NonNull
    protected ReceiptStateEnum state;

    @NonNull
    protected ReceiptStateChange[] changeHistory;

    @Nullable
    protected ReceiptStateChange lastImportantChange;

    /**
     * Cloning constructor
     * No copying happens, because instances are immutable
     */
    protected ReceiptState(ReceiptState that) {
        this.state = that.state;
        this.changeHistory = that.changeHistory;
        this.lastImportantChange = that.lastImportantChange;
    }

    private ReceiptState(@NonNull ReceiptStateChange[] changes) {
        changeHistory = changes.clone();
        Arrays.sort(changeHistory);

        state = UNUSED;

        lastImportantChange = null;
        for (ReceiptStateChange change : changeHistory)
            if (isChangeImportant(change))
                lastImportantChange = change;

        if (changeHistory.length > 0)
            state = changeHistory[changeHistory.length - 1].state;
    }

    /**
     * Infers current receipt state from state change history
     * @param changes History of changes, need not be sorted
     */
    public static ReceiptState fromStateChanges(ReceiptStateChange[] changes) {
        return new ReceiptState(changes);
    }

    public static ReceiptState fromStateChanges(List<ReceiptStateChange> changes) {
        return fromStateChanges(changes.toArray(new ReceiptStateChange[0]));
    }

    /**
     * Returns true, if the change is important
     * (uctenkovka or bazar upload, or setting back to unused)
     */
    private static boolean isChangeImportant(ReceiptStateChange change) {
        switch (change.state) {
            case UNUSED:
            case UCTENKOVKA_UPLOADED:
            case BAZAR_UPLOADED:
                return true;
        }
        return false;
    }

    @NonNull
    public ReceiptStateEnum getState() {
        return state;
    }

    /**
     * Returns true if this state instance does represent given string state
     */
    public boolean is(ReceiptStateEnum state) {
        return state == this.state;
    }

    /**
     * Returns true, if the receipt is uploaded in uctenkovka
     */
    public boolean isUctenkovka() {
        return lastImportantChange != null && UCTENKOVKA_UPLOADED == lastImportantChange.state;
    }

    /**
     * Returns the state as uctenkovka state, or null on failure
     */
    public UctenkovkaReceiptState asUctenkovka() {
        if (!isUctenkovka())
            return null;
        return new UctenkovkaReceiptState(this);
    }

    /**
     * Returns true, if the receipt is uploaded in bazar
     */
    public boolean isBazar() {
        return lastImportantChange != null && BAZAR_UPLOADED == lastImportantChange.state;
    }

    /**
     * Returns the state as bazar state, or null on failure
     */
    public BazarReceiptState asBazar() {
        if (!isBazar())
            return null;
        return new BazarReceiptState(this);
    }

    /**
     * Can the receipt in this state be uploaded to uctenkovka or bazar?
     */
    public boolean isUploadable() {
        return state.isUploadable();
    }
}
