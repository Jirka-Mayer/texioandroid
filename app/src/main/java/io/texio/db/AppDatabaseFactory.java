package io.texio.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import io.texio.db.migrations.Migration_1_2_AddEetDetailsToReceiptTable;
import io.texio.db.migrations.Migration_2_3_AddReceiptStateChangeTable;

public class AppDatabaseFactory {

    /**
     * Name of the database (filename without extension)
     */
    public static final String DATABASE_NAME = "texio-data";

    /**
     * Format for datetime fields
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"; // ISO 8601

    /**
     * Migrations available to the database
     *
     * When adding a migration:
     * - create migration class
     * - add it's instance here
     * - register the instance in getInstance() method
     * - create a test case for it
     *
     * A quick guide:
     * https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
     */
    public static final Migration MIGRATION_1_2 = new Migration_1_2_AddEetDetailsToReceiptTable();
    public static final Migration MIGRATION_2_3 = new Migration_2_3_AddReceiptStateChangeTable();

    private static AppDatabase databaseInstance = null;

    /**
     * Returns a singleton instance of the app database
     */
    public static AppDatabase getInstance(Context context) {
        if (AppDatabaseFactory.databaseInstance == null)
            AppDatabaseFactory.databaseInstance = Room.databaseBuilder(
                context.getApplicationContext(),
                AppDatabase.class,
                DATABASE_NAME
            )
                .allowMainThreadQueries()
                .addMigrations(
                    MIGRATION_1_2,
                    MIGRATION_2_3
                )
                .build();

        return AppDatabaseFactory.databaseInstance;
    }
}
