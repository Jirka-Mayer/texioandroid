package io.texio.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
    primaryKeys = {"tagId", "receiptId"},
    foreignKeys = @ForeignKey(
        entity = Receipt.class,
        parentColumns = "id",
        childColumns = "receiptId",
        onDelete = CASCADE,
        onUpdate = CASCADE
    ),
    indices = {@Index("receiptId")}
)
public class Tag
{
    @NonNull
    public String tagId;

    @NonNull
    public String receiptId;
}
