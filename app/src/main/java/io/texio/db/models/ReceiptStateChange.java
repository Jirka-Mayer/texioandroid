package io.texio.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.texio.db.other.DateTime;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;

@Entity(
    foreignKeys = @ForeignKey(
        entity = Receipt.class,
        parentColumns = "id",
        childColumns = "receiptId"
    ),
    indices = {
        @Index(
            value = {"receiptId"},
            name = "receiptId_index"
        )
    }
)
public class ReceiptStateChange implements Comparable<ReceiptStateChange> {

    /**
     * Primary key - auto incremented integer
     */
    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int id;

    /**
     * Id of the associated receipt
     */
    @NonNull
    public String receiptId;

    /**
     * State, that the receipt entered
     */
    @NonNull
    public ReceiptStateEnum state;

    /**
     * Timestamp of the state entrance
     */
    @NonNull
    public DateTime datetime;

    /**
     * JSON entity containing state-change metadata
     */
    @Nullable
    public Metadata metadata;

    /**
     * Empty constructor
     */
    public ReceiptStateChange() {}

    /**
     * Simple constructor that sets datetime to now and leaves metadata empty
     */
    public ReceiptStateChange(Receipt receipt, ReceiptStateEnum state) {
        receiptId = receipt.id;
        this.state = state;
        datetime = DateTime.now();
        metadata = null;
    }

    /**
     * Constructor that takes metadata
     */
    public ReceiptStateChange(Receipt receipt, ReceiptStateEnum state, Metadata metadata) {
        this(receipt, state);
        this.metadata = metadata;
    }

    /**
     * Sort by datetime, then by ID
     */
    @Override
    public int compareTo(@NonNull ReceiptStateChange that) {
        if (datetime == null && that.datetime == null)
            return 0;
        if (that.datetime == null)
            return 1;
        if (datetime == null)
            return -1;
        int result = datetime.compareTo(that.datetime);
        if (result == 0)
            result = Integer.compare(id, that.id);
        return result;
    }
}
