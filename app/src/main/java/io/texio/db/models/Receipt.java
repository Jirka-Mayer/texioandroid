package io.texio.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.texio.Application;
import io.texio.util.RandomString;
import io.texio.db.Convertors;
import io.texio.db.other.DateTime;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptTime;

@Entity
public class Receipt  {

    public static final String TAG = Receipt.class.getSimpleName();

    /**
     * Lengths of the receipt id in characters
     */
    private static final int ID_LENGTH = 20;

    /**
     * Primary key
     * A random string generated at receipt creation
     */
    @PrimaryKey
    @NonNull
    public String id = RandomString.generate(ID_LENGTH);

    /**
     * User that owns this receipt
     *
     * If null, it's not owned by anyone - imagine it as a default anonymous local user.
     *
     * Currently users are not supported so the value has no meaning. Maybe it's a local id,
     * maybe a global server-generated id, who knows.
     */
    @Nullable
    public String userId = null;

    /**
     * Timestamps
     */
    @Nullable
    public DateTime deletedAt = null;
    @Nullable
    public DateTime createdAt = DateTime.now();
    @Nullable
    public DateTime updatedAt = DateTime.now();

    /**
     * Name of the store that produced this receipt
     */
    @Nullable
    public String store = null;

    /**
     * Date on the receipt YYYY-MM-DD
     */
    @Nullable
    public ReceiptDate date = null;

    /**
     * Time on the receipt HH:MM
     */
    @Nullable
    public ReceiptTime time = null;

    /**
     * Total amount paid
     * (0 and NULL are identical, meaning the value is not known)
     */
    public long total = 0;

    /**
     * Receipt currency
     */
    @Nullable
    public String currency = "CZK";

    /**
     * FIK code
     */
    @Nullable
    public String fik = null;

    /**
     * BKP code
     */
    @Nullable
    public String bkp = null;

    /**
     * DIC of the receipt vendor
     * May or may not include the CZ prefix even in lowercase
     * (because the field can be directly edited by the user)
     */
    @Nullable
    public String dic = null;

    /**
     * EET mode
     */
    @Nullable
    public String eetMode = EET_MODE_DEFAULT;
    public static final String EET_MODE_DEFAULT = "eetMode.default"; // "běžný režim"
    public static final String EET_MODE_REDUCED = "eetMode.reduced"; // "zjednodušený režim"

    /**
     * Note for the receipt
     */
    @Nullable
    public String note = null;

    /**
     * Extracted raw OCR text
     */
    @Nullable
    public String text = null;

    /**
     * Creates a new receipt with id and timestamps
     */
    public Receipt() { }

    /**
     * Creates a receipt and sets default currency and possibly other stuff
     */
    public static Receipt createNew(Context context) {
        Receipt receipt = new Receipt();
        receipt.currency = Application.getDefaultCurrency(context);
        return receipt;
    }

    /**
     * Packs the receipt into a bundle
     */
    @Deprecated
    public Bundle toBundle() {
        Bundle b = new Bundle();

        b.putString("id", id);
        b.putString("createdAt", createdAt == null ? null : createdAt.toString());
        b.putString("updatedAt", updatedAt == null ? null : updatedAt.toString());
        b.putString("deletedAt", deletedAt == null ? null : deletedAt.toString());
        b.putString("store", store);
        b.putString("date", date == null ? null : date.toString());
        b.putString("time", time == null ? null : time.toString());
        b.putLong("total", total);
        b.putString("currency", this.currency);
        b.putString("fik", this.fik);
        b.putString("bkp", this.bkp);
        b.putString("dic", this.dic);
        b.putString("eetMode", this.eetMode);
        b.putString("note", this.note);
        b.putString("text", this.text);

        return b;
    }

    /**
     * Unpacks a receipt from a bundle
     */
    @Deprecated
    public static Receipt fromBundle(Bundle b, Context c) {
        if (b == null)
            return null;

        Receipt r = new Receipt();
        r.soakUpBundle(b);

        return r;
    }

    /**
     * Soaks up data from a receipt bundle into the current instance
     */
    @Deprecated
    public void soakUpBundle(Bundle b) {
        this.id = b.getString("id");
        this.createdAt = DateTime.fromString(b.getString("createdAt"));
        this.updatedAt = DateTime.fromString(b.getString("updatedAt"));
        this.deletedAt = DateTime.fromString(b.getString("deletedAt"));
        this.store = b.getString("store");
        this.date = Convertors.receiptDateFromText(b.getString("date"));
        this.time = Convertors.receiptTimeFromText(b.getString("time"));
        this.total = b.getLong("total");
        this.currency = b.getString("currency");
        this.fik = b.getString("fik");
        this.bkp = b.getString("bkp");
        this.dic = b.getString("dic");
        this.eetMode = b.getString("eetMode");
        this.note = b.getString("note");
        this.text = b.getString("text");
    }

    public Receipt cloneReceiptInstance() {
        return Receipt.fromBundle(toBundle(), null);
    }

    /**
     * Set updatedAt time to now
     */
    public void updatedNow() {
        updatedAt = DateTime.now();
    }

    /**
     * Set deletedAt time to now
     */
    public void deletedNow() {
        deletedAt = DateTime.now();
    }

    /**
     * Checks equality by comparing IDs only
     */
    @Override
    public boolean equals(Object that) {
        if (!(that instanceof Receipt))
            return false;

        return this.id.equals(((Receipt)that).id);
    }

    /**
     * Hash code of the ID
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
