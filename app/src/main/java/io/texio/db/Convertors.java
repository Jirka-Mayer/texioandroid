package io.texio.db;

import android.arch.persistence.room.TypeConverter;

import io.texio.db.other.DateTime;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptTime;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;

/**
 * Type convertors for database fields
 */
public class Convertors {

    // DateTime

    @TypeConverter
    public static DateTime dateTimeFromText(String value) {
        return value == null ? null : new DateTime(value);
    }

    @TypeConverter
    public static String dateTimeToText(DateTime value) {
        return value == null ? null : value.toString();
    }

    // ReceiptDate

    @TypeConverter
    public static ReceiptDate receiptDateFromText(String value) {
        return value == null ? null : ReceiptDate.parse(value);
    }

    @TypeConverter
    public static String receiptDateToText(ReceiptDate value) {
        return value == null ? null : value.toString();
    }

    // ReceiptTime

    @TypeConverter
    public static ReceiptTime receiptTimeFromText(String value) {
        return value == null ? null : ReceiptTime.parse(value);
    }

    @TypeConverter
    public static String receiptTimeToText(ReceiptTime value) {
        return value == null ? null : value.toString();
    }

    // Metadata

    @TypeConverter
    public static Metadata metadataFromText(String value) {
        return new Metadata(value);
    }

    @TypeConverter
    public static String metadataToText(Metadata value) {
        return value == null ? null : value.toString();
    }

    // ReceiptStateEnum

    @TypeConverter
    public static ReceiptStateEnum receiptStateEnumFromText(String value) {
        return ReceiptStateEnum.fromStringState(value);
    }

    @TypeConverter
    public static String receiptStateEnumToText(ReceiptStateEnum value) {
        return value == null ? null : value.stateAsString();
    }
}
