package io.texio.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.texio.db.models.Receipt;
import io.texio.db.other.DateTime;

@Dao
public interface ReceiptDao
{
    @Query("SELECT * FROM receipt")
    List<Receipt> all();

    @Query("SELECT * FROM receipt WHERE deletedAt IS NOT NULL")
    Receipt[] allDeleted();

    @Query("SELECT * FROM receipt LIMIT 1")
    Receipt first(); // debugging query

    @Query("SELECT * FROM receipt WHERE date >= :date")
    List<Receipt> newerThan(String date);

    @Query("SELECT * FROM receipt WHERE createdAt >= :datetime")
    List<Receipt> createdAfter(DateTime datetime);

    @Query("SELECT * FROM receipt WHERE id = :id")
    Receipt find(String id);

    @Insert
    void insert(Receipt... receipts);

    @Update
    void update(Receipt... receipts);

    @Delete
    void delete(Receipt... receipts);

    //////////////////////
    // Specific queries //
    //////////////////////

    @Query("SELECT * FROM receipt " +
        "WHERE deletedAt IS NULL " +
        "ORDER BY " +
        "IFNULL(date, '9999-99-99') DESC, " +
        "IFNULL(time, '99:99') DESC, " +
        "createdAt DESC, id ASC " +
        "LIMIT :size OFFSET :offset")
    List<Receipt> receiptList_loadBatch(int size, int offset);

    @Query("SELECT * FROM receipt " +
        "WHERE deletedAt IS NOT NULL " +
        "ORDER BY " +
        "IFNULL(date, '9999-99-99') DESC, " +
        "IFNULL(time, '99:99') DESC, " +
        "createdAt DESC, id ASC " +
        "LIMIT :size OFFSET :offset")
    List<Receipt> trash_loadBatch(int size, int offset);

    @Query("SELECT * FROM receipt " +
        "WHERE dic LIKE :dicLikePattern AND store IS NOT NULL " +
        "ORDER BY createdAt DESC LIMIT 1")
    Receipt textPipeline_latestWithDic(String dicLikePattern);
}
