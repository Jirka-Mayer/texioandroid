package io.texio.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.texio.db.models.Tag;

@Dao
public interface TagDao
{
    @Query("SELECT * FROM tag WHERE receiptId = :receiptId")
    List<Tag> allWithReceipt(String receiptId);

    @Query("SELECT * FROM tag WHERE tagId = :tagId")
    List<Tag> allWithTag(String tagId);
}
