package io.texio.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;

@Dao
public interface ReceiptStateChangeDao {
    @Query("SELECT * FROM ReceiptStateChange")
    List<ReceiptStateChange> all();

    @Query("SELECT * FROM ReceiptStateChange WHERE `receiptId` = :receiptId")
    List<ReceiptStateChange> of(String receiptId);

    @Query("DELETE FROM ReceiptStateChange WHERE `receiptId` = :receiptId")
    void clearFor(String receiptId);

    @Insert
    void insert(ReceiptStateChange... changes);

    @Update
    void update(ReceiptStateChange... changes);

    @Delete
    void delete(ReceiptStateChange... changes);
}
