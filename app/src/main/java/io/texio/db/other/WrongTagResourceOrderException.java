package io.texio.db.other;

public class WrongTagResourceOrderException extends RuntimeException
{
    public WrongTagResourceOrderException()
    {
        super("Tags are defined in faulty order. Cannot reference a parent before defining it.");
    }
}
