package io.texio.db.other;

import android.support.annotation.NonNull;

import java.util.Comparator;

import io.texio.db.models.Receipt;

/**
 * This comparator is used to sort receipt list activity content
 *
 * Note: should match the receipt list query
 *
 * Features:
 * - newer date is smaller then older date
 * - when dates equal, time is used
 * - null date receipts end up at the beginning (smallest)
 * - when same date and time, receipt created_at field is used (newer are smaller)
 * - if still equal, id is used to differentiate the receipts
 * - if even that mathes, 0 is returned
 */
public class ReceiptDateComparator implements Comparator<Receipt> {

    /**
     * If a > b, returns 1
     * If a = b, returns 0
     * else -1
     */
    @Override
    public int compare(Receipt a, Receipt b) {
        // handle null receipt
        if (a == null && b == null)
            return 0;

        if (a != null && b == null)
            return 1;
        else if (a == null)
            return -1;

        int dateComparison = compareByDate(a, b);
        if (dateComparison != 0)
            return dateComparison;

        int timeComparison = compareByTime(a, b);
        if (timeComparison != 0)
            return timeComparison;

        int createdAtComparison = compareByCreatedAt(a, b);
        if (createdAtComparison != 0)
            return createdAtComparison;

        return a.id.compareTo(b.id);
    }

    private int compareByDate(@NonNull Receipt a, @NonNull Receipt b) {
        if (a.date == null && b.date == null)
            return 0;

        if (a.date != null && b.date == null)
            return 1;
        else if (a.date == null)
            return -1;

        return -a.date.compareTo(b.date);
    }

    private int compareByTime(@NonNull Receipt a, @NonNull Receipt b) {
        if (a.time == null && b.time == null)
            return 0;

        if (a.time != null && b.time == null)
            return 1;

        else if (a.time == null)
            return -1;

        return -a.time.compareTo(b.time);
    }

    private int compareByCreatedAt(@NonNull Receipt a, @NonNull Receipt b) {
        if (a.createdAt == null && b.createdAt == null)
            return 0;

        if (a.createdAt != null && b.createdAt == null)
            return 1;

        else if (a.createdAt == null)
            return -1;

        return -a.createdAt.compareTo(b.createdAt);
    }
}
