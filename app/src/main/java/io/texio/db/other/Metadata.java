package io.texio.db.other;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Stores any metadata about a database record.
 * Data that are dynamic and will be serialized into a text field.
 *
 * Stores primitives only, no nested objects allowed (yet?).
 */
public class Metadata {

    protected JSONObject data;

    public Metadata() {
        data = new JSONObject();
    }

    public Metadata(String metadata) {
        if (metadata == null)
            metadata = "{}";

        try {
            data = new JSONObject(metadata);
        } catch (JSONException e) {
            data = new JSONObject();
        }
    }

    /**
     * Returns true if there are no metadata stored
     */
    public boolean isEmpty() {
        return data.length() == 0;
    }

    @Override
    public String toString() {
        return data.toString();
    }

    public Metadata put(String key, String value) {
        try {
            data.put(key, value);
        } catch (JSONException e) {}
        return this;
    }

    public Metadata put(String key, long value) {
        try {
            data.put(key, value);
        } catch (JSONException e) {}
        return this;
    }

    public String getString(String key, String def) {
        try {
            return data.getString(key);
        } catch (JSONException e) {
            return def;
        }
    }

    public long getLong(String key, long def) {
        try {
            return data.getLong(key);
        } catch (JSONException e) {
            return def;
        }
    }
}
