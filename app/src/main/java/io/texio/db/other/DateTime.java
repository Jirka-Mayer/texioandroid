package io.texio.db.other;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Locale;

/**
 * Wrapper for datetime value
 * It has precision from year to a millisecond
 * No timezone, Czech is assumed
 */
public class DateTime implements Comparable<DateTime> {

    // FORMAT ISO 8601
    // yyyy-MM-dd'T'HH:mm:ss.SSS'Z'

    private int year;
    private int month; // 1-based, as expected
    private int day;

    private int hour;
    private int minute;
    private int second;
    private int millisecond;

    public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.millisecond = millisecond;
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.millisecond = 0;
    }

    /**
     * Creates a datetime instance from and ISO 8601 format string
     * yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
     * Milliseconds may be omitted (.SSS)
     */
    public DateTime(String datetime) {
        String[] halves = datetime.split("T");
        String[] date = halves[0].split("-");
        String[] time = halves[1].split("[\\:\\.Z]");

        year = Integer.parseInt(date[0]);
        month = Integer.parseInt(date[1]);
        day = Integer.parseInt(date[2]);

        hour = Integer.parseInt(time[0]);
        minute = Integer.parseInt(time[1]);
        second = Integer.parseInt(time[2]);

        if (time.length >= 4)
            millisecond = Integer.parseInt(time[3]);
        else
            millisecond = 0;
    }

    /**
     * Does the same thing as the string constructor except it passes null through
     */
    @Nullable
    public static DateTime fromString(String datetime) {
        if (datetime == null)
            return null;
        return new DateTime(datetime);
    }

    /**
     * Creates a datetime object from calendar instance
     */
    public static DateTime fromCalendar(Calendar calendar) {
        return new DateTime(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH) + 1,
            calendar.get(Calendar.DAY_OF_MONTH),
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            calendar.get(Calendar.SECOND),
            calendar.get(Calendar.MILLISECOND)
        );
    }

    /**
     * Transforms the datetime object to calendar instance
     */
    public Calendar toCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, millisecond);
        return calendar;
    }

    /**
     * Creates new datetime instance representing this moment
     */
    public static DateTime now() {
        return fromCalendar(Calendar.getInstance());
    }

    @Override
    public String toString() {
        return String.format(
            Locale.ENGLISH,
            "%d-%02d-%02dT%02d:%02d:%02d.%03dZ",
            year, month, day, hour, minute, second, millisecond
        );
    }

    @Override
    public int compareTo(@NonNull DateTime that) {
        return this.toString().compareTo(that.toString());
    }

    /////////////
    // Getters //
    /////////////

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public int getMillisecond() {
        return millisecond;
    }

    ////////////////
    // Operations //
    ////////////////

    /**
     * Add a given number of days to the datetime; can be negative for subtracting
     */
    public DateTime addDays(int days) {
        Calendar calendar = toCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return fromCalendar(calendar);
    }
}
