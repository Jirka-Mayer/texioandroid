package io.texio.db.other;

import android.content.Context;

import io.texio.R;

public class CurrencyAmount
{
    /**
     * Number of (fixed) decimal places
     */
    public static final int DECIMAL_PLACES = 3;
    public static final long FIXED_POINT_SHIFT = 1000; // 10^3

    public static long parse(String text)
    {
        return CurrencyAmount.parse(text, "\\.");
    }

    public static long parse(String text, String regexDecimalSeparator)
    {
        if (text == null)
            return 0;

        // remove itchy characters (thousands separators)
        if (regexDecimalSeparator == "\\.")
            text = text.replaceAll("\\,", "");
        text = text.replaceAll("\\s", "");

        if (text.length() == 0)
            return 0;

        String[] parts = text.split(regexDecimalSeparator);
        String decimalPart = "0";

        switch (parts.length)
        {
            case 2:
                if (parts[1].length() > DECIMAL_PLACES)
                    decimalPart = parts[1].substring(0, DECIMAL_PLACES);
                else
                    decimalPart = parts[1];

            case 1:
                // trailing zeros
                while (decimalPart.length() < DECIMAL_PLACES)
                    decimalPart += "0";

                return Long.parseLong(parts[0]) * FIXED_POINT_SHIFT + Long.parseLong(decimalPart);

            default:
                return 0;
        }
    }

    public static String toString(long amount)
    {
        return CurrencyAmount.toString(amount, ",", " ");
    }

    public static String toString(long amount, String decimalSeparator, String thousandsSeparator)
    {
        long wholePart = amount / FIXED_POINT_SHIFT;
        long decimalPart = amount % FIXED_POINT_SHIFT;

        String decimalText = Long.toString(decimalPart);

        // add leading decimal zeros
        while (decimalText.length() < DECIMAL_PLACES)
            decimalText = "0" + decimalText;

        // remove trailing decimal zeros
        while (decimalText.length() > 0 && decimalText.charAt(decimalText.length() - 1) == '0')
            decimalText = decimalText.substring(0, decimalText.length() - 1);

        // dec separator
        if (decimalText.length() != 0)
            decimalText = decimalSeparator + decimalText;

        // wholes
        String wholeText = Long.toString(wholePart);
        StringBuilder wholeBuilder = new StringBuilder(wholeText);

        for (int i = 0; i < wholeText.length() / 3; i++)
            wholeBuilder.insert(wholeText.length() + i - (3 + i * 4), thousandsSeparator);

        // trim leading thousands separator (may occur)
        wholeText = wholeBuilder.toString().trim();

        return wholeText + decimalText;
    }

    public static String formatHumanReadable(Context context, long amount, String currency)
    {
        String amountText = CurrencyAmount.toString(amount);
        String[] formats = context.getResources().getStringArray(R.array.currencyFormats);

        String format = null;
        for (String f : formats)
        {
            if (f.split(":")[0].equals(currency))
            {
                format = f.substring(currency.length() + 1);
                break;
            }
        }

        if (format == null)
            return amountText;

        return format.replace("_", amountText);
    }
}
