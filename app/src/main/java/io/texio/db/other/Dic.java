package io.texio.db.other;

/**
 * Methods for working with DIC number
 */
public class Dic {

    /**
     * Returns just the number part of the dic
     */
    public static String normalize(String dic) {
        return dic.replaceAll("[a-zA-Z\\s]+", "");
    }

    /**
     * True if the dic is valid according to the specification of dic
     */
    public static boolean isValid(String dic) {
        dic = normalize(dic);

        // 8-10 digits
        if (dic.length() < 8 || dic.length() > 10)
            return false;

        // valid
        return true;
    }

    /**
     * Formats the dic properly with the "CZ" prefix
     */
    public static String toCzForm(String dic) {
        return "CZ" + normalize(dic);
    }
}
