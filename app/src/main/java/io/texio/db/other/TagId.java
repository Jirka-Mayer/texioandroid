package io.texio.db.other;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.texio.R;

public class TagId // TagId to differentiate from the Tag database entity
{
    public static final String TAG = "TagId";

    public String id = null;

    public TagId parent = null;
    public ArrayList<TagId> children = null;

    /**
     * Loaded tag structure (singleton)
     */
    private static ArrayList<TagId> allTags = null;

    private TagId()
    {

    }

    /**
     * Returns list of all tag ids and their relationship
     */
    public static List<TagId> getAll(Context context)
    {
        if (TagId.allTags == null)
        {
            TagId.allTags = new ArrayList<>();

            try
            {
                TagId.loadTagStructure(context);
            }
            catch (XmlPullParserException|IOException e)
            {
                Log.e(TAG, "Tag structure was not loaded.", e);
            }
        }

        return TagId.allTags;
    }

    private static void loadTagStructure(Context context)
        throws XmlPullParserException, IOException
    {
        XmlResourceParser xpp = context.getResources().getXml(R.xml.tags);
        xpp.next();

        boolean done = false;
        while (!done)
        {
            switch (xpp.getEventType())
            {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equals("tag"))
                    {
                        TagId.addTag(
                            xpp.getAttributeValue(null, "id"),
                            xpp.getAttributeValue(null, "parent")
                        );
                    }
                    xpp.next();
                    break;

                case XmlPullParser.END_DOCUMENT:
                    done = true;
                    break;

                default:
                    xpp.next();
                    break;
            }
        }

        xpp.close();
    }

    private static void addTag(String id, String parent)
    {
        TagId parentTag = TagId.findTag(parent);

        TagId tag = new TagId();
        tag.id = id;
        tag.parent = parentTag;

        if (parent == null)
        {
            TagId.allTags.add(tag);
        }
        else
        {
            if (parentTag == null)
                throw new WrongTagResourceOrderException();

            parentTag.children.add(tag);
        }
    }

    /**
     * Find TagId instance by id
     */
    public static TagId findTag(Context context, String id)
    {
        return TagId.findTag(id, TagId.getAll(context));
    }

    private static TagId findTag(String id)
    {
        return TagId.findTag(id, TagId.allTags);
    }

    private static TagId findTag(String id, List<TagId> collection)
    {
        TagId inner = null;

        for (TagId item : collection)
        {
            if (item.id.equals(id))
                return item;

            inner = TagId.findTag(id, item.children);

            if (inner != null)
                return inner;
        }

        return null;
    }

    /**
     * Returns the list of tags from this one, to the root one including this one
     */
    public List<TagId> getSuperTags()
    {
        ArrayList<TagId> out = new ArrayList<>();
        TagId subject = this;

        while (subject != null)
        {
            out.add(subject);
            subject = subject.parent;
        }

        return out;
    }
}
