package io.texio.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import io.texio.db.dao.ReceiptDao;
import io.texio.db.dao.ReceiptStateChangeDao;
import io.texio.db.dao.TagDao;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.models.Tag;

@Database(
    entities = {Receipt.class, Tag.class, ReceiptStateChange.class},
    version = 3
)
@TypeConverters({Convertors.class})
public abstract class AppDatabase extends RoomDatabase
{
    public abstract ReceiptDao receiptDao();

    public abstract TagDao tagDao();

    public abstract ReceiptStateChangeDao receiptStateChangeDao();
}
