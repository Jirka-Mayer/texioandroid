package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.hypertrack.hyperlog.HyperLog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import io.texio.hyperlog.HyperLogExtended;

public class Encrypter {

    public static final String TAG = Encrypter.class.getSimpleName();

    /**
     * Version number of the current encoding scheme
     */
    public static final int CURRENT_ENCODING_VERSION = 1;

    /**
     * Name of the key inside the preferences
     */
    public static final String AES_KEY_NAME = "io.texio.ENCRYPTOR";

    /**
     * Name of the key inside the preferences that is the RSA encrypted version
     */
    public static final String AES_KEY_ENCRYPTED_NAME = "io.texio.ENCRYPTED_ENCRYPTOR";

    /**
     * Name of the key inside the preferences
     */
    public static final String RSA_KEY_NAME = "io.texio.ENCRYPTOR_RSA_KEYS";

    /**
     * Name of the shared preferences file
     */
    public static final String PREFERENCES_FILENAME = "io.texio.KEYS";

    /**
     * Name of the keystore to use
     */
    public static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    private SecretKey key;

    public Encrypter(SecretKey key) {
        this.key = key;
    }

    /**
     * Builds an encrypter instance with keys found in preferences and keystore
     */
    public static Encrypter getDefault(Context context) {
        try {

            KeyStore keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
            keyStore.load(null);

            generateRsaKeysIfNeeded(keyStore, context);

            SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCES_FILENAME, Context.MODE_PRIVATE
            );

            // updates the AES key to the RSA encrypted version
            encryptAesKeyIfNeeded(preferences, keyStore);

            // loads or generates and stores AES key and returns the key RSA encrypted
            byte[] keyBytes = loadOrGenerateAndStoreAesKeyBytes(preferences, keyStore);
            keyBytes = rsaDecrypt(keyBytes, keyStore);
            SecretKey aesKey = new SecretKeySpec(keyBytes, "AES");

            return new Encrypter(aesKey);

        } catch (Exception e) {
            HyperLogExtended.exceptionWithTrace(TAG, "Building default encrypter failed.", e);

            // something failed and I need to have an idea why, so I will log,
            // what the stored and rsa encrypted aes key looks like. Maybe it's
            // null, maybe an empty string, maybe corrupted string, who knows.
            // That should help me diagnose the problem.
            // Since the key is RSA encrypted and probably corrupted and this exception
            // is not meant to happen often, I considered sending the whole
            // encrypted key as-is to be safe-enough.
            SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCES_FILENAME, Context.MODE_PRIVATE
            );
            String aesEncrypted = preferences.getString(AES_KEY_ENCRYPTED_NAME, null);
            if (aesEncrypted == null)
                HyperLog.i(TAG, "Stored encrypted AES key has null value.");
            else
                HyperLog.i(TAG, "Stored encrypted AES key has value '" + aesEncrypted + "'.");

            throw new RuntimeException(e);
        }
    }

    public byte[] encryptString(String text) {
        try {
            return encryptBytes(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public String decryptString(byte[] ciphertext) {
        try {
            return new String(decryptBytes(ciphertext), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public byte[] encryptBytes(byte[] text) {
        try
        {
            Header header = new Header();
            header.fileVersion = CURRENT_ENCODING_VERSION;
            header.iv = generateIv();

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(header.iv));
            byte[] cipherText = cipher.doFinal(text);

            header.encryptedContentLength = cipherText.length;
            header.mac = computeMac(text);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            header.writeTo(stream);
            stream.write(cipherText);
            return stream.toByteArray();
        }
        catch (
            NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeyException |
                IllegalBlockSizeException |
                BadPaddingException |
                InvalidAlgorithmParameterException |
                IOException e)
        {
            HyperLogExtended.exceptionWithTrace(TAG, "Error while encrypting bytes.", e);
            return null;
        }
    }

    public byte[] decryptBytes(byte[] cipherText) {
        try
        {
            ByteArrayInputStream stream = new ByteArrayInputStream(cipherText);

            Header header = new Header(stream);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(header.iv));

            byte[] encryptedContent = new byte[header.encryptedContentLength];
            stream.read(encryptedContent);

            byte[] text = cipher.doFinal(encryptedContent);

            if (!Arrays.equals(computeMac(text), header.mac))
                throw new IOException("MAC does not match.");

            return text;
        }
        catch (
            NoSuchAlgorithmException |
                NoSuchPaddingException |
                InvalidKeyException |
                IllegalBlockSizeException |
                BadPaddingException |
                InvalidAlgorithmParameterException |
                IOException e)
        {
            HyperLogExtended.exceptionWithTrace(TAG, "Error while decrypting bytes.", e);
            return null;
        }
    }

    private static byte[] computeMac(byte[] text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            return md.digest(text);
        } catch (NoSuchAlgorithmException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
            return null;
        }
    }

    ////////////
    // Header //
    ////////////

    /**
     * Header for the encoded data
     */
    private static class Header {
        public int fileVersion;
        public int encryptedContentLength;

        public byte[] iv;
        public byte[] mac;

        public Header() {

        }

        public Header(ByteArrayInputStream stream) throws IOException {
            // load header
            byte[] header = new byte[4 + 4 + 4 + 4];
            int count = stream.read(header);

            if (count != header.length)
                throw new IOException("File too short to contain a header.");

            // decode header
            ByteBuffer buffer = ByteBuffer.wrap(header);
            fileVersion = buffer.getInt();

            int iv_length = buffer.getInt();
            int mac_length = buffer.getInt();

            if (iv_length > 10_000 || mac_length > 10_000 || iv_length < 1 || mac_length < 1)
                throw new IOException("Provided data for decryption seems corrupted.");

            iv = new byte[iv_length];
            mac = new byte[mac_length];
            encryptedContentLength = buffer.getInt();

            // check version
            if (fileVersion != CURRENT_ENCODING_VERSION) {
                stream.close();
                throw new IOException("File encryption version '" + fileVersion + "' is not supported.");
            }

            // load stuff
            count = stream.read(iv);
            if (count != iv.length)
                throw new IOException("File too short to contain IV.");

            count = stream.read(mac);
            if (count != mac.length)
                throw new IOException("File too short to contain MAC.");
        }

        public void writeTo(OutputStream stream) throws IOException {
            byte[] header = ByteBuffer.allocate(4 + 4 + 4 + 4)
                .putInt(fileVersion)
                .putInt(iv.length)
                .putInt(mac.length)
                .putInt(encryptedContentLength)
                .array();
            stream.write(header);
            stream.write(iv);
            stream.write(mac);
        }
    }

    ////////////////
    // Generation //
    ////////////////

    @NonNull
    public static SecretKey generateAesKey()
    {
        try {
            KeyGenerator keygen = KeyGenerator.getInstance("AES");
            keygen.init(256);
            return keygen.generateKey();
        } catch (NoSuchAlgorithmException e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
            throw new RuntimeException(e);
        }
    }

    public static byte[] generateIv()
    {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return iv;
    }

    //////////////////////////////////
    // Key obtaining and encryption //
    //////////////////////////////////

    // https://medium.com/@ericfu/securely-storing-secrets-in-an-android-application-501f030ae5a3

    /**
     * Generates RSA key pair if not present in the keystore
     */
    public static void generateRsaKeysIfNeeded(KeyStore keyStore, Context context) throws Exception {
        if (keyStore.containsAlias(RSA_KEY_NAME))
            return;

        // Generate a key pair for encryption
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 30);
        KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
            .setAlias(RSA_KEY_NAME)
            .setSubject(new X500Principal("CN=" + RSA_KEY_NAME))
            .setSerialNumber(BigInteger.TEN)
            .setStartDate(start.getTime())
            .setEndDate(end.getTime())
            .build();
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEY_STORE);
        kpg.initialize(spec);
        kpg.generateKeyPair();

        HyperLog.i(TAG, "RSA keypair has been generated.");
    }

    public static byte[] rsaEncrypt(byte[] secret, KeyStore keyStore) throws Exception {
        PublicKey publicKey = keyStore.getCertificate(RSA_KEY_NAME).getPublicKey();

        // Encrypt the text
        Cipher inputCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        inputCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
        cipherOutputStream.write(secret);
        cipherOutputStream.close();

        return outputStream.toByteArray();
    }

    public static byte[] rsaDecrypt(byte[] encrypted, KeyStore keyStore) throws Exception {
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(RSA_KEY_NAME, null);

        Cipher output = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        output.init(Cipher.DECRYPT_MODE, privateKey);

        CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte)nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for(int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private static void encryptAesKeyIfNeeded(SharedPreferences preferences, KeyStore keyStore) throws Exception {
        if (!preferences.contains(AES_KEY_NAME))
            return;

        byte[] aesKeyBytes = loadBytesFromPreferences(preferences, AES_KEY_NAME);

        if (aesKeyBytes == null || aesKeyBytes.length == 0) {
            preferences.edit().remove(AES_KEY_NAME).apply();
            HyperLog.w(TAG, "Old AES key couldn't be loaded. So I deleted it.");
            return;
        }

        preferences.edit().putString(
            AES_KEY_ENCRYPTED_NAME,
            Base64.encodeToString(
                rsaEncrypt(aesKeyBytes, keyStore),
                Base64.DEFAULT
            )
        ).apply();

        preferences.edit().remove(AES_KEY_NAME).apply();

        HyperLog.i(TAG, "AES key has been encrypted.");
    }

    /**
     * Returns RSA encryped AES key, that is stored in preferences
     * (generates new one and stores it if no key present in preferences)
     */
    private static byte[] loadOrGenerateAndStoreAesKeyBytes(
        SharedPreferences preferences, KeyStore keyStore
    ) throws Exception {
        // LOADING

        // do not test "contains", test that it can be loaded instead
        byte[] storedKeyBytes = loadBytesFromPreferences(preferences, AES_KEY_ENCRYPTED_NAME);
        if (storedKeyBytes != null && storedKeyBytes.length > 0) {
            // key is present, no need to generate it
            // Return encrypted AES key
            return storedKeyBytes;
        } else {
            // key was weird, we need to generate it
            // if it really was weird, log that
            if (preferences.contains(AES_KEY_ENCRYPTED_NAME))
                HyperLog.w(TAG, "Encrypted AES key was present, but corrupted. So we generate a new one.");
        }

        // GENERATION

        SecretKey aesKey = generateAesKey();
        byte[] keyBytes = aesKey.getEncoded();

        HyperLog.i(
            TAG,
            "New AES key has been generated with the length of " + keyBytes.length + " bytes"
        );

        // RSA encrypt generated key
        keyBytes = rsaEncrypt(keyBytes, keyStore);

        preferences.edit().putString(
            AES_KEY_ENCRYPTED_NAME,
            Base64.encodeToString(
                keyBytes,
                Base64.DEFAULT
            )
        ).apply();

        // return encrypted AES key
        return keyBytes;
    }

    /**
     * Loads a non-empty array of bytes from preferences
     * (meaning it loads AES keys - both encrypted and decrypted)
     *
     * Returns null on failure either because the field does not exist, or cannot be parsed.
     * Empty byte array can be returned.
     */
    private static byte[] loadBytesFromPreferences(SharedPreferences preferences, String key) {
        String content = preferences.getString(key, null);

        if (content == null)
            return null;

        try {
            byte[] bytes = Base64.decode(content, Base64.DEFAULT);
            return bytes;
        } catch (IllegalArgumentException e) {
            HyperLogExtended.exceptionWithTrace(TAG, "Field " + key + " couldn't be parsed.", e);
            return null;
        }
    }
}
