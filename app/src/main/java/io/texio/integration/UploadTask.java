package io.texio.integration;

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import io.texio.CredentialStorage;
import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.receiptState.ReceiptState;

/**
 * Superclass for any integration receipt upload task
 * (meaning uploading to uctenkovka, bazar, ...)
 *
 * Returns action lambda that should be performed on the UI thread after execution
 */
public abstract class UploadTask extends AsyncTask<Void, Void, UploadTask.Action> {
    protected AppDatabase db;
    protected CredentialStorage credentialStorage;

    protected String receiptId;
    protected CredentialStorage.CredentialPair explicitCredentials;

    protected Receipt receipt;
    protected ReceiptState receiptState;

    private WeakReference<UploadTaskCallback> weakCallback;
    private WeakReference<Context> weakContext;

    public UploadTask(Context context, String receiptId, UploadTaskCallback callback) {
        this.receiptId = receiptId;
        weakCallback = new WeakReference<>(callback);
        weakContext = new WeakReference<>(context);
        credentialStorage = CredentialStorage.getDefault(context);
        db = AppDatabaseFactory.getInstance(context);
    }

    public void setExplicitCredentials(CredentialStorage.CredentialPair credentials) {
        this.explicitCredentials = credentials;
    }

    @Override
    protected Action doInBackground(Void... voids) {
        receipt = db.receiptDao().find(receiptId);
        receiptState = ReceiptState.fromStateChanges(db.receiptStateChangeDao().of(receiptId));

        return null;
    }

    @Override
    protected void onPostExecute(Action action) {
        UploadTaskCallback callback = weakCallback.get();

        if (callback == null)
            return;

        if (action != null)
            action.perform();
        else
            callback.uploadDone();
    }

    /**
     * Call this if the explicitCredentials in storage or explicitly provided are incorrect
     */
    protected void invalidCredentials() {
        UploadTaskCallback callback = weakCallback.get();
        if (callback != null)
            callback.uploadHasInvalidCredentials();
    }

    /**
     * Displays an error
     */
    protected void error(int textResourceId, Object... formatArgs) {
        UploadTaskCallback callback = weakCallback.get();
        Context context = weakContext.get();
        if (callback != null)
            callback.uploadFailed(context.getResources().getString(textResourceId, formatArgs));
    }

    /**
     * Displays an error
     */
    protected void fieldError(int textResourceId, int fieldNameResourceId) {
        UploadTaskCallback callback = weakCallback.get();
        Context context = weakContext.get();
        if (callback != null)
            callback.uploadFailed(
                context.getResources().getString(
                    textResourceId,
                    context.getResources().getString(fieldNameResourceId)
                )
            );
    }

    /**
     * Listener for return calls from the task
     */
    public interface UploadTaskCallback {
        void uploadHasInvalidCredentials();
        void uploadFailed(String message);
        void uploadDone();
    }

    public interface Action {
        void perform();
    }
}
