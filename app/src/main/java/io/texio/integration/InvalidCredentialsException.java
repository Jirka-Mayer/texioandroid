package io.texio.integration;

/**
 * Throw this when the provided credentials are invalid
 */
public class InvalidCredentialsException extends Exception {
    public InvalidCredentialsException() {
        super("Logging in with provided credentials failed.");
    }
}
