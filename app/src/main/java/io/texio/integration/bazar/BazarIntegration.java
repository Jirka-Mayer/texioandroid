package io.texio.integration.bazar;

import android.content.Context;
import android.support.annotation.Nullable;

import java.io.IOException;

import io.texio.CredentialStorage;
import io.texio.R;
import io.texio.integration.Integration;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.UploadTask;
import io.texio.integration.uctenkovka.Uctenkovka;
import io.texio.networking.NoInternetAccessException;

public class BazarIntegration implements Integration {

    private Context context;

    public BazarIntegration(Context context) {
        this.context = context;
    }

    @Override
    public int loginBannerResourceId() {
        return R.drawable.login_bazar;
    }

    @Override
    public int loginInfoTextResourceId() {
        return R.string.integrationLoginActivity_info_bazar;
    }

    @Override
    public int iconResourceId() {
        return R.drawable.ic_bazar;
    }

    @Override
    public int titleResourceId() {
        return R.string.integrationActivity_bazar_title;
    }

    @Nullable
    @Override
    public String getLogin() {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        CredentialStorage.CredentialPair pair = storage.loadPair(Bazar.LOGIN_PAIR_CSK);
        if (pair == null)
            return null;
        return pair.login;
    }

    @Override
    public boolean setCredentials(CredentialStorage.CredentialPair pair) {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        storage.forget(Bazar.SESSION_CSK);
        return storage.storePair(Bazar.LOGIN_PAIR_CSK, pair);
    }

    @Nullable
    @Override
    public CredentialStorage.CredentialPair getCredentials() {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        return storage.loadPair(Bazar.LOGIN_PAIR_CSK);
    }

    @Override
    public boolean areCredentialsValid(CredentialStorage.CredentialPair pair) throws IOException {
        if (!Integration.hasInternetConnectionTo(Bazar.DOMAIN))
            throw new NoInternetAccessException();

        try {
            // attempts one-time login
            new BazarApi(pair);
            return true;
        } catch (InvalidCredentialsException e) {
            return false;
        }
    }

    @Override
    public UploadTask createUploadTask(Context context, String receiptId, UploadTask.UploadTaskCallback callback) {
        throw new RuntimeException("Not implemented.");
    }
}
