package io.texio.integration.bazar;

/**
 * Holds metadata about the integrated service
 */
public class Bazar {
    // Warning: KEEP THE "www." THERE! (otherwise login breaks)
    public static final String DOMAIN = "www.bazaructenek.cz";

    // credential storage keys (CSK)
    public static final String LOGIN_PAIR_CSK = "io.texio.integration.BAZAR_LOGIN";
    public static final String SESSION_CSK = "io.texio.integration.BAZAR_SESSION";
}
