package io.texio.integration.bazar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.texio.CredentialStorage;
import io.texio.integration.CredentialsNotInStorageException;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.UnexpectedResponseException;
import io.texio.networking.HttpsClient;

/**
 * Methods for accessing the bazar uctenek website
 *
 * Api instance internally keeps a session. New instance creates a brand new session.
 */
public class BazarApi {

    /**
     * Client that holds the http session
     */
    private HttpsClient client = new HttpsClient(Bazar.DOMAIN);

    /**
     * Creates new API connection and tries to authenticate via
     * information stored within a credential storage
     *
     * First by session, then by credentials, then fails
     */
    public BazarApi(CredentialStorage storage)
        throws IOException, InvalidCredentialsException, CredentialsNotInStorageException {

        // at-least credentials have to be provided
        CredentialStorage.CredentialPair pair = storage.loadPair(Bazar.LOGIN_PAIR_CSK);

        if (pair == null)
            throw new CredentialsNotInStorageException("There are no credentials in the storage.");

        // try session
        client.setCookies(storage.load(Bazar.SESSION_CSK));

        // if session expired, re-login, store new session
        if (!isSessionProperlyAuthenticated(pair.login)) {
            login(pair);
            storage.store(Bazar.SESSION_CSK, client.getCookies());
        }
    }

    /**
     * Creates new API connection and tries to authenticate via provided one-time credentials
     */
    public BazarApi(CredentialStorage.CredentialPair pair) throws IOException, InvalidCredentialsException {
        login(pair);
    }

    /**
     * Requests the settings page and checks the email displayed
     */
    private boolean isSessionProperlyAuthenticated(String expectedEmail) throws IOException {
        return expectedEmail.equals(page_settings().getUserEmail());
    }

    /**
     * Tries to login
     * Checks that login was totally successful
     */
    private void login(CredentialStorage.CredentialPair pair) throws IOException, InvalidCredentialsException {
        // POST the login page
        Map<String, String> payload = new HashMap<>();
        payload.put("log_email", pair.login);
        payload.put("log_pass", pair.password);
        payload.put("login", "");

        // disable redirection following to catch XSS token cookie set with the 302 response
        client.followRedirects = false;
        client.post("/login", payload);
        client.followRedirects = true;

        // check user is logged in
        if (!isSessionProperlyAuthenticated(pair.login))
            throw new InvalidCredentialsException();
    }

    ///////////////////////
    // Raw page requests //
    ///////////////////////

    public BazarPage page_index() throws IOException {
        return new BazarPage(client.get("/"));
    }

    public BazarPage page_settings() throws IOException {
        return new BazarPage(client.get("/nastaveni"));
    }
}
