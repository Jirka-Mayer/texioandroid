package io.texio.integration.bazar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Contains page returned by bazar, allows you to query it for content
 */
public class BazarPage {

    /**
     * The underlying jsoup document
     */
    private Document document;

    public BazarPage(String html) {
        document = Jsoup.parse(html);
    }

    /**
     * Returns email of the logged in user, or null on failure
     *
     * Available only on the account detail page
     */
    public String getUserEmail() {
        Element e = document.select("div.form-group #email").first();
        if (e == null)
            return null;
        return e.attr("value");
    }
}
