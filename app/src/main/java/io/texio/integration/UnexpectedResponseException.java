package io.texio.integration;

import java.io.IOException;

/**
 * Throw this, when an API method gets unexpected response
 * (some html element not found, json has bad format, etc.)
 */
public class UnexpectedResponseException extends IOException {
    public UnexpectedResponseException(String msg) {
        super(msg);
    }

    public UnexpectedResponseException(String msg, Exception cause) {
        super(msg, cause);
    }
}
