package io.texio.integration;

import android.content.Context;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import io.texio.CredentialStorage;
import io.texio.integration.bazar.BazarIntegration;
import io.texio.integration.uctenkovka.UctenkovkaIntegration;

/**
 * Unified interface to manage any integration
 */
public interface Integration {

    // individual integration IDs
    int UCTENKOVKA = 1;
    int BAZAR = 2;

    /**
     * Returns an actual integration instance, given an integration ID
     */
    static Integration getIntegration(int id, Context context) {
        switch (id) {
            case UCTENKOVKA:
                return new UctenkovkaIntegration(context);

            case BAZAR:
                return new BazarIntegration(context);
        }

        throw new IllegalArgumentException("Integration with id " + id + " does not exist.");
    }

    /**
     * Returns ID of the integration
     */
    default int getId() {
        if (this instanceof UctenkovkaIntegration)
            return UCTENKOVKA;
        if (this instanceof BazarIntegration)
            return BAZAR;

        throw new RuntimeException("Integration has unknown id.");
    }

    /**
     * Checks if there is an internet connection to a given domain
     * @param domainName Domain to request, eg. "google.com"
     * @return True if the domain is reachable
     */
    static boolean hasInternetConnectionTo(String domainName)
    {
        try {
            InetAddress ipAddr = InetAddress.getByName(domainName);
            return !"".equals(ipAddr);
        } catch (UnknownHostException e) {
            return false;
        }
    }

    /**
     * Returns resource id of the banner displayed in the login activity
     */
    int loginBannerResourceId();

    /**
     * Returns resource id of the info text displayed in the login activity
     */
    int loginInfoTextResourceId();

    /**
     * Resource id of the icon in integration listing
     */
    int iconResourceId();

    /**
     * Resource id of the integration title
     */
    int titleResourceId();

    /**
     * Returns login credential of the integration or null of none is set
     */
    @Nullable
    String getLogin();

    /**
     * Sets explicitCredentials to use for this integration
     * @returns True on success
     */
    boolean setCredentials(CredentialStorage.CredentialPair pair);

    /**
     * Returns explicitCredentials set for this integration
     */
    @Nullable
    CredentialStorage.CredentialPair getCredentials();

    /**
     * Returns true if the provided explicitCredentials do login successfully
     * Operation will perform synchronous networking so don't call this on UI thread
     */
    boolean areCredentialsValid(CredentialStorage.CredentialPair pair) throws IOException;

    /**
     * Creates an upload task for the integration
     */
    UploadTask createUploadTask(Context context, String receiptId, UploadTask.UploadTaskCallback callback);
}
