package io.texio.integration;

/**
 * Signals that a credential store was passed as an argument, but no useful
 * explicitCredentials were found inside of it.
 */
public class CredentialsNotInStorageException extends Exception {
    public CredentialsNotInStorageException(String msg) {
        super(msg);
    }
}
