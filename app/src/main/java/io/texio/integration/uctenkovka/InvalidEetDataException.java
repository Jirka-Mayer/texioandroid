package io.texio.integration.uctenkovka;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.texio.db.receipt.ReceiptField;
import io.texio.integration.UnexpectedResponseException;

/**
 * Uctenkovka receipt registration returned 400 - provided data is weird, and it's not duplicity
 */
public class InvalidEetDataException extends Exception {

    public List<FieldError> errors = new ArrayList<>();

    public InvalidEetDataException(HttpResponse<JsonNode> response) throws UnexpectedResponseException {
        try {
            JSONArray array = response.getBody().getArray();

            for (int i = 0; i < array.length(); i++) {
                errors.add(new FieldError(array.getJSONObject(i)));
            }

        } catch (JSONException e) {
            throw new UnexpectedResponseException(
                "JSONException occured while parsing 401 receipt registration response.", e
            );
        }
    }

    /**
     * Represents a single error returned by the server
     */
    public static class FieldError {
        public ReceiptField field;
        public String code;
        public String message;

        public FieldError(JSONObject o) throws JSONException {
            code = o.getString("code");
            message = o.getString("message");

            switch (o.getString("field")) {
                case "date": field = ReceiptField.DATE; break;
                case "time": field = ReceiptField.TIME; break;
                case "amount": field = ReceiptField.TOTAL; break;
                case "fik": field = ReceiptField.FIK; break;
                case "bkp": field = ReceiptField.BKP; break;
                case "simpleMode": field = ReceiptField.EET_MODE; break;
                default: field = null; break;
            }
        }
    }
}
