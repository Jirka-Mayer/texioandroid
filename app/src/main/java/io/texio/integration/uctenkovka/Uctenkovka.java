package io.texio.integration.uctenkovka;

public class Uctenkovka {
    public static final String DOMAIN = "www.uctenkovka.cz";

    // credential storage keys (CSK)
    public static final String LOGIN_PAIR_CSK = "io.texio.integration.UCTENKOVKA_LOGIN";
}
