package io.texio.integration.uctenkovka;

import android.content.Context;

import com.mashape.unirest.http.exceptions.UnirestException;

import io.texio.R;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.db.receipt.receiptState.UctenkovkaReceiptState;
import io.texio.hyperlog.HyperLogExtended;
import io.texio.integration.CredentialsNotInStorageException;
import io.texio.integration.DuplicateReceiptException;
import io.texio.integration.EetData;
import io.texio.integration.EetFieldMissingException;
import io.texio.integration.Integration;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.UploadTask;

public class UploadToUctenkovkaTask extends UploadTask {

    public static final String TAG = UploadToUctenkovkaTask.class.getSimpleName();

    public UploadToUctenkovkaTask(Context context, String receiptId, UploadTaskCallback callback) {
        super(context, receiptId, callback);
    }

    @Override
    protected Action doInBackground(Void... voids) {
        super.doInBackground();

        if (!receiptState.isUploadable()) {
            return () -> error(R.string.integrationUpload_wrongState);
        }

        UctenkovkaApi api;
        String email;

        if (!Integration.hasInternetConnectionTo(Uctenkovka.DOMAIN)) {
            return () -> error(R.string.integrationUpload_internetConnection);
        }

        try {
            if (explicitCredentials != null)
                api = new UctenkovkaApi(explicitCredentials);
            else
                api = new UctenkovkaApi(credentialStorage);
            email = api.queryUserEmail();
        } catch (InvalidCredentialsException | CredentialsNotInStorageException e) {
            return this::invalidCredentials;
        } catch (UnirestException e) {
            HyperLogExtended.exceptionWithTrace(
                TAG, "Couldn't create " + UctenkovkaApi.class.getSimpleName() + " instance.", e
            );
            return () -> error(R.string.integrationUpload_error, e.getMessage());
        }

        EetData data;
        try {
            data = EetData.fromReceipt(receipt, false);
        } catch (EetFieldMissingException e) {
            return () -> fieldError(R.string.integrationUpload_missingField, e.getField().getTextResourceId());
        }

        UctenkovkaApi.RegistrationResult result;
        try {
            result = api.registerReceipt(data);
        } catch (DuplicateReceiptException e) {
            return () -> error(R.string.integrationUpload_duplicateReceipt);
        } catch (InvalidEetDataException e) {
            return () -> fieldError(
                R.string.integrationUpload_faultyField,
                e.errors.get(0).field.getTextResourceId()
            );
        } catch (UnirestException e) {
            HyperLogExtended.exceptionWithTrace(
                TAG, "Receipt registration to uctenkovka failed.", e
            );
            return () -> error(R.string.integrationUpload_error, e.getMessage());
        }

        // change receipt state
        ReceiptStateChange uploadChange = new ReceiptStateChange(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata()
                .put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, result.getReceiptEetId())
                .put(UctenkovkaReceiptState.METADATA_EMAIL, email)
        );
        ReceiptStateChange returnedState = new ReceiptStateChange(
            receipt,
            result.getState()
        );

        db.receiptStateChangeDao().insert(uploadChange, returnedState);

        return null;
    }
}
