package io.texio.integration.uctenkovka;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.texio.CredentialStorage;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.integration.CredentialsNotInStorageException;
import io.texio.integration.DuplicateReceiptException;
import io.texio.integration.EetData;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.UnexpectedResponseException;

/**
 * Represents a connection to the uctenkovka API
 *
 * Usage:
 * 1) create an instance (a "connection") by calling the constructor and providing
 *      either a credential pair, access token or credential storage instance
 *      - If the login fails due to invalid explicitCredentials, it throws {@link InvalidCredentialsException}
 *      - If the provided credential storage has no corresponding explicitCredentials,
 *          then {@link IllegalArgumentException} is thrown.
 *      - If it fails for any other reason, it throws corresponding exception
 * 2) now the instance holds valid access token and you can call methods on it
 */
public class UctenkovkaApi {

    // NOTE: don't forget to include token with every request

    /**
     * The access token used to authenticate requests
     */
    private String token;

    /**
     * Create a session from stored explicitCredentials inside a storage
     */
    public UctenkovkaApi(CredentialStorage storage)
        throws UnirestException, InvalidCredentialsException, CredentialsNotInStorageException {

        CredentialStorage.CredentialPair pair = storage.loadPair(Uctenkovka.LOGIN_PAIR_CSK);

        if (pair == null)
            throw new CredentialsNotInStorageException("There are no explicitCredentials in the storage.");

        login(pair);
    }

    /**
     * Create a one-time session with provided explicitCredentials
     */
    public UctenkovkaApi(CredentialStorage.CredentialPair pair) throws UnirestException, InvalidCredentialsException {
        login(pair);
    }

    /**
     * Creates a session with an existing token for a given email
     * This method is meant for testing
     */
    public UctenkovkaApi(String email, String token) throws UnirestException, InvalidCredentialsException {
        this.token = token;

        if (!isSessionProperlyAuthenticated(email))
            throw new InvalidCredentialsException();
    }

    /**
     * Sends a login request and sets the access token header or throws an exception
     */
    private void login(CredentialStorage.CredentialPair pair) throws UnirestException, InvalidCredentialsException {
        // build request body
        JSONObject body = new JSONObject();
        try {
            body.put("username", pair.login);
            body.put("password", pair.password);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        // make the request
        HttpResponse<String> httpResponse = Unirest.post("https://" + Uctenkovka.DOMAIN + "/api/web/auth/login")
            .header("Content-type", "application/json;encoding=UTF-8")
            .body(body.toString())
            .asString();

        // login refused
        if (httpResponse.getCode() == 401)
            throw new InvalidCredentialsException();

        // parse response as json
        JSONObject response;
        try {
            response = new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            throw new UnirestException(
                new UnexpectedResponseException("Login response wasn't a json object.", e)
            );
        }

        // store access token
        try {
            token = response.getString("accessToken");
        } catch (JSONException e) {
            throw new UnirestException(
                new UnexpectedResponseException("Login response did not contain access token.", e)
            );
        }

        // check user is logged in
        if (!isSessionProperlyAuthenticated(pair.login))
            throw new InvalidCredentialsException();
    }

    /**
     * Compares email addresses - the one stored and the one authenticated
     */
    private boolean isSessionProperlyAuthenticated(String expectedEmail) throws UnirestException {
        String queriedEmail = queryUserEmail();
        if (queriedEmail == null)
            return false;
        return queriedEmail.equals(expectedEmail);
    }

    //////////////////////////
    // Receipt registration //
    //////////////////////////

    /**
     * Does not check data validity, it just uploads the receipt as it is and returns the result
     * Does not modify database of anything like that
     */
    public RegistrationResult registerReceipt(EetData eetData)
        throws UnirestException, DuplicateReceiptException, InvalidEetDataException {

        // request body
        JSONObject body = buildRegistrationRequestBody(eetData);

        // make the request
        HttpResponse<JsonNode> httpResponse;
        try {
            httpResponse = Unirest.post("https://" + Uctenkovka.DOMAIN + "/api/web/player/receipts")
                .header("Content-type", "application/json;encoding=UTF-8")
                .header("Authorization", "Bearer " + token)
                .body(body.toString())
                .asJson();
        } catch (RuntimeException e) {
            throw new UnirestException(e);
        }

        /*
            https://www.uctenkovka.cz/docs/Uctenkovka-3rd-party-API.pdf

            // 201 - success responses

            // unknown, verified and not-verified (not found = rejected)
            {"receiptId":192658266,"receiptStatus":"NEW","playerAssignmentStatus":"ADDED_TO_FULL_PLAYER"}
            {"receiptId":192658266,"receiptStatus":"VERIFIED","playerAssignmentStatus":"ADDED_TO_FULL_PLAYER"}
            {"receiptId":192658266,"receiptStatus":"REJECTED","playerAssignmentStatus":"ADDED_TO_FULL_PLAYER"}

            // 400 - error responses

            // assigned to fields, or for duplicate assigned to null field
            [{"field": null, "code":"object.duplicate","message":"Receipt with FIK already registered"}]
            [{"field": "fik","code": "object.invalid", "message": "Invalid FIK format"}]
            [{"field":"date","code":"object.invalid","message":"Draw period for this receipt date is not opened."}]

            // NOTE: for this kind of duplicate we don't get to know the ID,
                but a duplicate might arise later if the initial response is "NEW" and then
                we do have receiptId and we need to handle it just like any other state.

                So if we get duplicate without a receiptId, we wont's set any state,
                we throw the DuplicateReceiptException
        */

        // handle success
        if (httpResponse.getCode() == 201) {
            try {
                return new RegistrationResult(httpResponse);
            } catch (UnexpectedResponseException e) {
                throw new UnirestException(e);
            }
        }

        // handle failure
        if (httpResponse.getCode() == 400) {
            if (responseStatesDuplicity(httpResponse))
                throw new DuplicateReceiptException();

            try {
                throw new InvalidEetDataException(httpResponse);
            } catch (UnexpectedResponseException e) {
                throw new UnirestException(e);
            }
        }

        throw new UnirestException(
            new UnexpectedResponseException("Response was neither 201, nor 400.")
        );
    }

    /**
     * Builds request payload for receipt registration
     * @return null on some failure
     */
    private JSONObject buildRegistrationRequestBody(EetData eetData) {
        try {
            JSONObject body = new JSONObject();

            body.put("date", eetData.getDate().toString()); // e.g. "2018-09-27"
            body.put("time", eetData.getTime().toString()); // e.g. "10:00"
            body.put("amount", eetData.getTotalInHellers()); // e.g. 128000 for "1 280,-"

            if (eetData.hasFik())
                body.put("fik", eetData.getFik());
            else
                body.put("bkp", eetData.getBkp());

            body.put("simpleMode", eetData.isSimpleMode());

            // web form sends this, so I will as well
            body.put("phone", null);

            return body;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns true if the response contains:
     * [{"field": null, "code":"object.duplicate","message":"Receipt with FIK already registered"}]
     */
    private boolean responseStatesDuplicity(HttpResponse<JsonNode> response) {
        if (!response.getBody().isArray())
            return false;

        JSONArray array = response.getBody().getArray();

        try {
            for (int i = 0; i < array.length(); i++) {
                if (!(array.get(i) instanceof JSONObject))
                    continue;

                JSONObject item = (JSONObject) array.get(i);

                if (!item.isNull("field"))
                    continue;

                if (item.getString("code").equals("object.duplicate"))
                    return true;
            }
        } catch (JSONException e) {
            return false;
        }

        return false;
    }

    /**
     * Represents the successful result of a receipt registration
     */
    public static class RegistrationResult {
        private long receiptEetId;
        private ReceiptStateEnum state;

        public RegistrationResult(HttpResponse<JsonNode> response) throws UnexpectedResponseException {
            try {
                JSONObject o = response.getBody().getObject();
                receiptEetId = o.getLong("receiptId");
                state = ReceiptStateEnum.fromStringState("UCTENKOVKA_" + o.getString("receiptStatus"));
                if (state == ReceiptStateEnum.UNUSED)
                    throw new UnexpectedResponseException(
                        "Unknown receipt state '" + o.getString("receiptStatus") + "'."
                    );
            } catch (JSONException e) {
                throw new UnexpectedResponseException("Parsing success threw json exception.", e);
            }
        }

        public long getReceiptEetId() {
            return receiptEetId;
        }

        public ReceiptStateEnum getState() {
            return state;
        }
    }

    /////////////
    // Queries //
    /////////////

    /**
     * Makes a query for the player details and returns the email address
     * @return null on failure (no json sent -> token wasn't valid)
     * @throws UnirestException on network problem
     */
    public String queryUserEmail() throws UnirestException {
        try {
            return Unirest.get("https://" + Uctenkovka.DOMAIN + "/api/web/player")
                .header("Authorization", "Bearer " + token)
                .asJson()
                .getBody()
                .getObject()
                .getString("email");
        } catch (RuntimeException | JSONException e) {
            return null;
        }
    }

    /**
     * Obtains state of the receipt
     * @param receiptUctenkovkaId id of the receipt in uctenkovka
     * @return null on weird problems
     * @throws UnirestException on networking issue
     */
    public ReceiptStateEnum queryReceiptState(long receiptUctenkovkaId) throws UnirestException {
        String status;
        try {
            status = Unirest.get(
                "https://" + Uctenkovka.DOMAIN + "/api/web/player/receipts/" + receiptUctenkovkaId
            )
                .header("Authorization", "Bearer " + token)
                .asJson()
                .getBody()
                .getObject()
                .getString("receiptStatus");
        } catch (RuntimeException | JSONException e) {
            return null;
        }

        // string to enum
        ReceiptStateEnum state = ReceiptStateEnum.fromStringState("UCTENKOVKA_" + status);
        if (state == ReceiptStateEnum.UNUSED)
            return null;
        return state;
    }
}
