package io.texio.integration.uctenkovka;

import android.content.Context;
import android.support.annotation.Nullable;

import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;

import io.texio.CredentialStorage;
import io.texio.R;
import io.texio.integration.Integration;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.UploadTask;
import io.texio.networking.NoInternetAccessException;

public class UctenkovkaIntegration implements Integration {

    private Context context;

    public UctenkovkaIntegration(Context context) {
        this.context = context;
    }

    @Override
    public int loginBannerResourceId() {
        return R.drawable.login_uctenkovka;
    }

    @Override
    public int loginInfoTextResourceId() {
        return R.string.integrationLoginActivity_info_uctenkovka;
    }

    @Override
    public int iconResourceId() {
        return R.drawable.ic_uctenkovka;
    }

    @Override
    public int titleResourceId() {
        return R.string.integrationActivity_uctenkovka_title;
    }

    @Nullable
    @Override
    public String getLogin() {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        CredentialStorage.CredentialPair pair = storage.loadPair(Uctenkovka.LOGIN_PAIR_CSK);
        if (pair == null)
            return null;
        return pair.login;
    }

    @Override
    public boolean setCredentials(CredentialStorage.CredentialPair pair) {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        return storage.storePair(Uctenkovka.LOGIN_PAIR_CSK, pair);
    }

    @Nullable
    @Override
    public CredentialStorage.CredentialPair getCredentials() {
        CredentialStorage storage = CredentialStorage.getDefault(context);
        return storage.loadPair(Uctenkovka.LOGIN_PAIR_CSK);
    }

    @Override
    public boolean areCredentialsValid(CredentialStorage.CredentialPair pair) throws IOException {
        if (!Integration.hasInternetConnectionTo(Uctenkovka.DOMAIN))
            throw new NoInternetAccessException();

        try {
            // attempts one-time login
            new UctenkovkaApi(pair);
            return true;
        } catch (InvalidCredentialsException e) {
            return false;
        } catch (UnirestException e) {
            throw new IOException(e);
        }
    }

    @Override
    public UploadTask createUploadTask(Context context, String receiptId, UploadTask.UploadTaskCallback callback) {
        return new UploadToUctenkovkaTask(context, receiptId, callback);
    }
}
