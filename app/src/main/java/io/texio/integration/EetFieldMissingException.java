package io.texio.integration;

import io.texio.db.receipt.ReceiptField;

/**
 * Contains info that a given field is missing while trying to construct
 * the EetData packet
 */
public class EetFieldMissingException extends Exception {
    private ReceiptField field;

    public EetFieldMissingException(ReceiptField field) {
        this.field = field;
    }

    public ReceiptField getField() {
        return field;
    }
}
