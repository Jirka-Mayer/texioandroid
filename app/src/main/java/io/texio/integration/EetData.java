package io.texio.integration;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptField;
import io.texio.db.receipt.ReceiptTime;

/**
 * Packet containing all EET data needed for registration
 * + optional dic (may be required)
 * + optional store name
 */
public class EetData {

    private ReceiptTime time;
    private ReceiptDate date;
    private String fik;
    private String bkp;
    private String dic; // may or may not have prefix, we don't validate values here (same with fik, bkp)
    private int totalInHellers;
    private boolean simpleMode;

    private String store; // optional, for the bazar

    private EetData() {}

    @NonNull
    public ReceiptTime getTime() {
        return time;
    }

    @NonNull
    public ReceiptDate getDate() {
        return date;
    }

    public String getFik() {
        return fik;
    }

    public String getBkp() {
        return bkp;
    }

    /**
     * False means it has bkp
     * True means it may have bkp
     * Cannot lack both values
     */
    public boolean hasFik() {
        return fik != null;
    }

    public String getDic() {
        return dic;
    }

    public boolean hasDic() {
        return dic != null;
    }

    public int getTotalInHellers() {
        return totalInHellers;
    }

    public boolean isSimpleMode() {
        return simpleMode;
    }

    public String getStore() {
        return store;
    }

    /**
     * Creates eet data packet from a receipt
     */
    public static EetData fromReceipt(Receipt receipt, boolean requireDic) throws EetFieldMissingException {
        EetData d = new EetData();

        d.time = receipt.time;
        if (d.time == null)
            throw new EetFieldMissingException(ReceiptField.TIME);

        d.date = receipt.date;
        if (d.date == null)
            throw new EetFieldMissingException(ReceiptField.DATE);

        if (receipt.total <= 0)
            throw new EetFieldMissingException(ReceiptField.TOTAL);
        double realTotal = receipt.total / CurrencyAmount.FIXED_POINT_SHIFT;
        d.totalInHellers = (int)(realTotal * 100); // kinda like "2 fixed decimal places"

        if (receipt.fik != null && receipt.fik.length() > 0)
            d.fik = getMinimalFik(receipt.fik);
        else if (receipt.bkp != null && receipt.bkp.length() > 0)
            d.bkp = getMinimalBkp(receipt.bkp);
        else
            throw new EetFieldMissingException(ReceiptField.FIK);

        // we will assume full mode to be the default if nothing is filled
        d.simpleMode = Receipt.EET_MODE_REDUCED.equals(receipt.eetMode);

        d.dic = receipt.dic;
        if (d.dic == null && requireDic)
            throw new EetFieldMissingException(ReceiptField.DIC);

        d.store = receipt.store;
        if (d.store != null && d.store.length() == 0)
            d.store = null;

        return d;
    }

    private static String getMinimalFik(String fik)
    {
        if (fik.length() >= 18)
            return fik.substring(0, 18);
        else
            return fik;
    }

    private static String getMinimalBkp(String bkp)
    {
        if (bkp.length() >= 17)
            return bkp.substring(0, 17);
        else
            return bkp;
    }
}
