package io.texio.integration;

/**
 * Thrown by the uctenkovka api when the receipt is refused as
 * a duplicate right at the moment of registration, before we have a chance to obtain
 * it's receiptId
 */
public class DuplicateReceiptException extends Exception {
}
