package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.hypertrack.hyperlog.HyperLog;

import io.texio.hyperlog.HyperLogExtended;

/**
 * Stores any kind of sensitive key-value string data, mainly access information
 *
 * It's primarily focused on texio.integration package, but not exclusively
 *
 * It can store explicitCredentials in pairs login, password
 * This is useful, because when we load a pair, we want either both parts or none to be loaded
 *
 * Implementation:
 * Data is stored in the "io.texio.KEYS" preferences file. The file is private to the application.
 * String values are first turned into bytes using UTF-8, then encrypted
 * and finally encoded with base64 and then saved.
 */
public class CredentialStorage {

    public static final String TAG = CredentialStorage.class.getSimpleName();

    /**
     * Prefix for each key
     */
    public static final String KEY_PREFIX = "io.texio.CredentialStorage.";

    /**
     * Suffixes for credential-pair storing
     */
    public static final String LOGIN_SUFFIX = "_LOGIN";
    public static final String PASSWORD_SUFFIX = "_PASSWORD";

    /**
     * Preferences that we use as the storage
     */
    protected SharedPreferences preferences;

    /**
     * Encrypter used for data encryption
     */
    protected Encrypter encrypter;

    /**
     * Creates new credential storage instance that uses the provided preferences
     */
    public CredentialStorage(@NonNull SharedPreferences preferences, @NonNull Encrypter encrypter) {
        this.preferences = preferences;
        this.encrypter = encrypter;
    }

    /**
     * Returns the default credential storage for the application
     */
    public static CredentialStorage getDefault(@NonNull Context context) {
        return new CredentialStorage(context.getSharedPreferences(
            "io.texio.KEYS",
            Context.MODE_PRIVATE
        ), Encrypter.getDefault(context));
    }

    /**
     * Loads a credential from the store.
     * @param key Key under which the credential is stored.
     * @return Loaded value or null if not present or on exception.
     */
    public String load(@NonNull String key) {
        String credential = preferences.getString(KEY_PREFIX + key, null);

        if (credential == null)
            return null;

        try {
            byte[] bytes = Base64.decode(credential, Base64.DEFAULT);
            bytes = encrypter.decryptBytes(bytes);

            if (bytes == null) {
                HyperLog.w(TAG, "Decryption of field " + key + " failed.");
                return null;
            }

            credential = new String(bytes, "UTF-8");
        } catch (Exception e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
            return null;
        }

        return credential;
    }

    /**
     * Loads entire credential pair. Returns null if any of the parts fail.
     */
    public CredentialPair loadPair(@NonNull String key) {
        CredentialPair pair = new CredentialPair(
            load(key + LOGIN_SUFFIX),
            load(key + PASSWORD_SUFFIX)
        );

        if (!pair.isValid())
            return null;

        return pair;
    }

    /**
     * Stores a credential inside the store.
     * @param key Key under which to save it.
     * @param credential Value to be saved.
     * @return True on success.
     */
    public boolean store(@NonNull String key, String credential) {
        if (credential == null) {
            return forget(key);
        }

        try {
            byte[] bytes = credential.getBytes();
            bytes = encrypter.encryptBytes(bytes);

            if (bytes == null) {
                HyperLog.w(TAG, "Encryption of field " + key + " failed.");
                return false;
            }

            credential = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception e) {
            HyperLogExtended.exceptionWithTrace(TAG, e);
            return false;
        }

        SharedPreferences.Editor e = preferences.edit();
        e.putString(KEY_PREFIX + key, credential);
        e.apply();

        return true;
    }

    /**
     * Stores a credential pair
     * @return True only if both were saved successfully
     */
    public boolean storePair(@NonNull String key, CredentialPair pair) {
        if (pair == null)
            return forgetPair(key);

        if (!pair.isValid())
            return false;

        if (!store(key + LOGIN_SUFFIX, pair.login))
            return false;

        if (!store(key + PASSWORD_SUFFIX, pair.password))
            return false;

        HyperLog.i(TAG, "Storing credential pair for '" + key + "'.");

        return true;
    }

    /**
     * Forgets a credential
     * @param key Credential key
     * @return True on success.
     */
    public boolean forget(@NonNull String key) {
        SharedPreferences.Editor e = preferences.edit();
        e.remove(KEY_PREFIX + key);
        e.apply();

        return true;
    }

    /**
     * Forgets a credential pair
     * @return True on success
     */
    public boolean forgetPair(@NonNull String key) {
        if (!forget(key + LOGIN_SUFFIX))
            return false;

        if (!forget(key + PASSWORD_SUFFIX))
            return false;

        HyperLog.i(TAG, "Forgetting credential pair for '" + key + "'.");

        return true;
    }

    /**
     * Pair of explicitCredentials - login and password
     */
    public static class CredentialPair {
        public String login;
        public String password;

        public CredentialPair(String login, String password) {
            this.login = login;
            this.password = password;
        }

        /**
         * True if none of the two explicitCredentials is null
         */
        public boolean isValid() {
            return login != null && password != null;
        }
    }
}
