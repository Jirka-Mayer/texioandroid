package io.texio;

/**
 * Shared make intent extras in one place
 *
 * (specialized ones should still be specified in the given activity)
 */
public class IntentExtras
{
    /**
     * String - id of a receipt
     */
    public static final String EXTRA_RECEIPT_ID = "io.texio.RECEIPT_ID";

    /**
     * Bundle - receipt.toBundle()
     */
    public static final String EXTRA_RECEIPT = "io.texio.RECEIPT";

    /**
     * String - the name of a snap associated with *some* receipt
     * Receipt is inferred from the context or sent as another extra value
     */
    public static final String EXTRA_SNAP_NAME = "io.texio.SNAP_NAME";

    /**
     * String[] - list of snap names
     */
    public static final String EXTRA_SNAPS = "io.texio.SNAPS";

    /**
     * int - steps of rotation for the cropping activity to initialize with (0, 1, 2, 3)
     */
    public static final String EXTRA_INITIAL_ROTATION = "io.texio.INITIAL_ROTATION";

    /**
     * String, an email address
     */
    public static final String EXTRA_EMAIL = "io.texio.EMAIL";

    /**
     * String, login credential
     */
    public static final String EXTRA_LOGIN = "io.texio.LOGIN";

    /**
     * String, a password
     */
    public static final String EXTRA_PASSWORD = "io.texio.PASSWORD";

    /**
     * String, informational text
     */
    public static final String EXTRA_INFO = "io.texio.INFO";

    /**
     * Bool, remember login explicitCredentials?
     */
    public static final String EXTRA_REMEMBER = "io.texio.REMEMBER";

    /**
     * String, path to a file
     */
    public static final String EXTRA_FILE_PATH = "io.texio.FILE_PATH";

    /**
     * Int, integration id
     */
    public static final String EXTRA_INTEGRATION = "io.texio.INTEGRATION_ID";

    /**
     * Boolean, whether IntegrationLoginActivity should force remembering
     */
    public static final String EXTRA_FORCE_REMEMBERING = "io.texio.FORCE_REMEMBERING";

    ///////////////////
    // Action extras //
    ///////////////////

    /**
     * Boolean - a snap was deleted
     */
    public static final String EXTRA_SNAP_DELETED = "io.texio.SNAP_DELETED";

    /**
     * Boolean - a receipt was deleted
     */
    public static final String EXTRA_RECEIPT_DELETED = "io.texio.RECEIPT_DELETED";

    /**
     * Boolean - open a given receipt
     * (argument - EXTRA_RECEIPT)
     * (targeted at ReceiptListActivity)
     */
    public static final String EXTRA_OPEN_RECEIPT = "io.texio.OPEN_RECEIPT";
}
