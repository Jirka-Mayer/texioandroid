package io.texio.hermes;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import javax.net.ssl.HttpsURLConnection;

import io.texio.BuildConfig;
import io.texio.util.RandomString;

public class UploadSnapTask extends AsyncTask<File, Void, Boolean>
{
    private static final String TAG = "UploadSnapTask";

    public interface OnDoneListener
    {
        void uploadTaskDone(boolean result);
    }

    private OnDoneListener listener;

    public UploadSnapTask(OnDoneListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(File... files)
    {
        if (!this.isInternetAvailable())
        {
            Log.i(TAG, "Internet (texio.io domain) not available.");
            return false;
        }

        for (File f : files)
        {
            if (!this.uploadSingleSnap(f))
                return false;

            f.delete();
        }

        return true;
    }

    public boolean isInternetAvailable()
    {
        try
        {
            InetAddress ipAddr = InetAddress.getByName("texio.io");
            return !ipAddr.equals("");
        }
        catch (UnknownHostException e)
        {
            Log.i(TAG, "Internet not available:");
            e.printStackTrace();
            return false;
        }
    }

    private boolean uploadSingleSnap(File file)
    {
        HttpsURLConnection connection;
        DataOutputStream outputStream;

        String twoHyphens = "--";
        String boundary = "*****" + RandomString.generate(20) + "*****";
        String lineEnd = "\r\n";

        String fileField = "snap";
        String fileName = file.getName();
        String fileMimeType = "image/jpeg";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        try
        {
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL("https://texio.io/api/1.0/hermes/save-snap");
            connection = (HttpsURLConnection) url.openConnection();

            connection.setDoInput(false); // we don't need the input stream, response code is enough
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "close");
            connection.setRequestProperty("User-Agent", "Texio mobile app " + BuildConfig.VERSION_NAME);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + fileField + "\"; filename=\"" + fileName + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0)
            {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            if (200 != connection.getResponseCode())
            {
                Log.e(TAG, "Non 200 response code!");
                return false;
            }

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
        }
        catch (IOException e)
        {
            Log.e(TAG, "Exception occurred during snap sending:");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result)
    {
        this.listener.uploadTaskDone(result);
    }
}
