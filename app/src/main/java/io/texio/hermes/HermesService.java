package io.texio.hermes;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.texio.Application;

public class HermesService extends Service implements UploadSnapTask.OnDoneListener
{
    private static final String TAG = "HermesService";

    private UploadSnapTask task = null;

    // for parallel async task execution
    private ThreadPoolExecutor threadPool = null;

    @Override
    public void onCreate()
    {
        super.onCreate();

        this.threadPool = new ThreadPoolExecutor(
            1, // initial pool size
            1, // max pool size
            1, // keep alive time
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>()
        );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (this.canStart())
        {
            this.task = new UploadSnapTask(this);

            Log.i(TAG, "Starting service...");

            SnapQueue queue = new SnapQueue(this);
            this.task.executeOnExecutor(this.threadPool, queue.items());
        }

        if (this.task == null)
            this.stopSelf();

        return super.onStartCommand(intent,flags,startId);
    }

    private boolean canStart()
    {
        if (this.task != null)
            return false;

        if (!Application.getHermesEnabled(this))
            return false;

        if (!this.isOnWifi())
            return false;

        return true;
    }

    private boolean isOnWifi()
    {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork == null)
            return false;

        if (!activeNetwork.isConnectedOrConnecting())
            return false;

        if (activeNetwork.getType() != ConnectivityManager.TYPE_WIFI)
            return false;

        return true;
    }

    @Override
    public void uploadTaskDone(boolean result)
    {
        Log.i(TAG, "Service finished with result: " + result);

        this.task = null;
        this.stopSelf();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}
