package io.texio.hermes;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import io.texio.Application;
import io.texio.db.receipt.ReceiptDate;

public class SnapQueue
{
    public static final String TAG = "SnapQueue";

    public static final int QUEUE_SIZE = 10;
    public static final String QUEUE_DIRECTORY = "hermes-snap-queue";

    private Context context;

    public SnapQueue(Context context)
    {
        this.context = context;
    }

    private static String now() {
        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        String hourZero = hour < 10 ? "0" : "";
        String minuteZero = minute < 10 ? "0" : "";
        String secondZero = second < 10 ? "0" : "";

        return hourZero + hour + ":" + minuteZero + minute + ":" + secondZero + second;
    }

    public void enqueueRawSnap(File snap)
    {
        if (!this.canEnqueue())
            return;

        this.copyFile(
            snap,
            new File(
                this.directory(),
                Application.getApplicationId(this.context) + "_"
                    + ReceiptDate.today().toString() + "_"
                    + now() + "_"
                    + "raw.jpg"
            )
        );
    }

    public void enqueueEnhancedSnap(File snap)
    {
        if (!this.canEnqueue())
            return;

        this.copyFile(
            snap,
            new File(
                this.directory(),
                Application.getApplicationId(this.context) + "_"
                    + ReceiptDate.today().toString() + "_"
                    + now() + "_"
                    + "enhanced.jpg"
            )
        );
    }

    public File directory()
    {
        File dir = new File(this.context.getFilesDir(), QUEUE_DIRECTORY);
        dir.mkdirs();
        return dir;
    }

    public boolean canEnqueue()
    {
        // is allowed and not full
        return Application.getHermesEnabled(this.context) && !this.isQueueFull();
    }

    public File[] items()
    {
        return this.directory().listFiles(File::isFile);
    }

    public boolean isQueueFull()
    {
        return this.items().length >= QUEUE_SIZE;
    }

    private void copyFile(File source, File dest)
    {
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try
        {
            try
            {
                inputChannel = new FileInputStream(source).getChannel();
                outputChannel = new FileOutputStream(dest).getChannel();
                outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
            }
            finally
            {
                if (inputChannel != null)
                    inputChannel.close();

                if (outputChannel != null)
                    outputChannel.close();
            }
        }
        catch (IOException e)
        {
            Log.e(TAG, "File has not been copied:");
            e.printStackTrace();
        }
    }
}
