package io.texio;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.hypertrack.hyperlog.HyperLog;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.util.List;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.DateTime;
import io.texio.db.receipt.receiptState.BazarReceiptState;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.db.receipt.receiptState.UctenkovkaReceiptState;
import io.texio.hyperlog.HyperLogExtended;
import io.texio.integration.CredentialsNotInStorageException;
import io.texio.integration.InvalidCredentialsException;
import io.texio.integration.bazar.BazarApi;
import io.texio.integration.uctenkovka.UctenkovkaApi;

/**
 * Job, that updates states of receipts (no all receipts, only those that could have changed)
 *
 * Tutorial on JobScheduler:
 * https://medium.com/google-developers/scheduling-jobs-like-a-pro-with-jobscheduler-286ef8510129
 */
public class ReceiptStateUpdaterJobService extends JobService {

    public static final String TAG = ReceiptStateUpdaterJobService.class.getSimpleName();

    public static final int PERIOD_IN_MILLISECONDS = 2 * 3600 * 1000; // 2 hours

    private UpdateReceiptStates task;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        try {

            task = new UpdateReceiptStates(this) {
                @Override
                protected void onPostExecute(Boolean success) {
                    jobFinished(jobParameters, !success);
                }
            };
            task.execute();
            return true; // true = we have a background task running, don't kill me yet

        } catch (Exception e) {
            // unschedule this service, since it crashed
            JobScheduler js = (JobScheduler) getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
            if (js != null)
                js.cancel(JobIds.RECEIPT_STATE_UPDATER_JOB);
            HyperLogExtended.exceptionWithTrace(TAG, "Receipt state updater crashed.", e);
            throw e;
        }
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        // this method is called by the system to kill the job when running

        if (task != null) {
            task.cancel(true);
        }

        return true; // reschedule the job (because it didn't finish)
    }

    /**
     * Schedules the job if it isn't already
     * Call this during app startup
     */
    public static void scheduleIfNeeded(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        // return if scheduled already
        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            if (jobInfo.getId() == JobIds.RECEIPT_STATE_UPDATER_JOB) {
                return;
            }
        }

        JobInfo jobInfo = new JobInfo.Builder(
            JobIds.RECEIPT_STATE_UPDATER_JOB,
            new ComponentName(context, ReceiptStateUpdaterJobService.class)
        )
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .setPeriodic(PERIOD_IN_MILLISECONDS)
            .build();

        jobScheduler.schedule(jobInfo);
    }

    /**
     * Checks and updates states of receipts and returns true on success
     */
    public static class UpdateReceiptStates extends AsyncTask<Void, Void, Boolean> {
        private AppDatabase db;
        private CredentialStorage storage;

        public UpdateReceiptStates(ReceiptStateUpdaterJobService service) {
            db = AppDatabaseFactory.getInstance(service.getApplicationContext());
            storage = CredentialStorage.getDefault(service.getApplicationContext());
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            UctenkovkaApi uctenkovkaApi = null;
            BazarApi bazarApi = null;

            try {
                uctenkovkaApi = new UctenkovkaApi(storage);
            } catch (UnirestException e) {
                HyperLogExtended.exceptionWithTrace(TAG, "Couldn't create uctenkovka api instance.", e);
                return false; // failure
            } catch (CredentialsNotInStorageException | InvalidCredentialsException e) {
                // this case is ok, keep it null, receipts for this api will be ignored
            }

            try {
                bazarApi = new BazarApi(storage);
            } catch (IOException e) {
                HyperLogExtended.exceptionWithTrace(TAG, "Couldn't create bazar api instance.", e);
                return false; // failure
            } catch (CredentialsNotInStorageException | InvalidCredentialsException e) {
                // this case is ok, keep it null, receipts for this api will be ignored
            }

            performTheActualUpdate(db, uctenkovkaApi, bazarApi);

            return true; // success
        }

        /**
         * This method does the actual work
         * Extracted so that it can be tested
         *
         * If a given api is null, their receipts will be skipped
         */
        public static void performTheActualUpdate(
            AppDatabase db, @Nullable UctenkovkaApi uctenkovkaApi, @Nullable BazarApi bazarApi
        ) {
            /*
                - get list of receipts that are not in a dead state
                - update their state by querying the corresponding api
                - don't care about expiry, don't care about deletedAt
                - check if the uctenkovka receipt id is set, may not be!
             */

            // NOTE: update only receipts less than 3 months old,
            // because older ones are dead anyway

            List<Receipt> receipts = db.receiptDao()
                .createdAfter(DateTime.now().addDays(-3 * 30));

            for (Receipt receipt : receipts) {
                ReceiptState state = ReceiptState.fromStateChanges(
                    db.receiptStateChangeDao().of(receipt.id)
                );

                // if in dead state, skip
                if (state.getState().isDead())
                    continue;

                // handle uctenkovka receipts
                if (state.isUctenkovka() && uctenkovkaApi != null) {
                    UctenkovkaReceiptState uctenkovkaState = state.asUctenkovka();

                    // skip those without an id
                    if (!uctenkovkaState.hasUctenkovkaReceiptId())
                        continue;

                    try {
                        // check for update
                        ReceiptStateEnum newState = uctenkovkaApi.queryReceiptState(
                            uctenkovkaState.getUctenkovkaReceiptId()
                        );

                        // update receipt state (if nothing weird happened and a change occured)
                        if (newState != null && newState != state.getState()) {
                            db.receiptStateChangeDao().insert(
                                new ReceiptStateChange(receipt, newState)
                            );

                            HyperLog.i(
                                TAG, "Receipt '" + receipt.id
                                    + "' has obtained state '" + newState.stateAsString() + "'"
                            );
                        }
                    } catch (UnirestException e) {
                        HyperLogExtended.exceptionWithTrace(
                            TAG, "Receipt state network query failed.", e
                        );
                    }
                }

                // handle bazar receipts
                if (state.isUctenkovka() && bazarApi != null) {
                    BazarReceiptState bazarState = state.asBazar();

                    // TODO: handle bazar receipts
                }
            }
        }
    }
}
