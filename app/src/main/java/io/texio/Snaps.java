package io.texio;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents the snaps directory ( = directory containing snaps with no receipt bound to yet)
 * (contains some helper functions)
 *
 * Snaps are not encrypted because they are stored on the internal storage
 */
public class Snaps
{
    public static final String SNAPS_DIRECTORY = "snaps";

    /**
     * Returns the directory containing all snaps
     */
    public static File snapsDirectory(Context context)
    {
        File dir = new File(context.getFilesDir(), Snaps.SNAPS_DIRECTORY);

        dir.mkdirs();

        return dir;
    }

    /**
     * Returns file representing a snap
     */
    public static File file(Context context, String name)
    {
        return new File(Snaps.snapsDirectory(context), name);
    }

    /**
     * Creates a new snap file for camera app to write to
     */
    public static File createNew(Context context)
    {
        try
        {
            String snapName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File snapFile = File.createTempFile(
                "snap_" + snapName + "_",
                ".jpg",
                Snaps.snapsDirectory(context)
            );

            return snapFile;
        }
        catch (IOException ex)
        {
            return null;
        }
    }

    /**
     * Removes any left snap files. The folder should be empty if camera activity is not running.
     * (Files may be left due to crashes or other errors)
     */
    public static void clearDirectory(Context context)
    {
        File dir = Snaps.snapsDirectory(context);

        File[] contents = dir.listFiles();

        for (File f : contents)
            f.delete();

        dir.delete();
    }
}
