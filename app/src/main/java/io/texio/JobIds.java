package io.texio;

/**
 * IDs of jobs for the JobScheduler
 */
public class JobIds {
    public static final int RECEIPT_STATE_UPDATER_JOB = 1;
    public static final int HYPER_LOG_PUSHER_JOB = 2;
}
