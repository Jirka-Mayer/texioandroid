package io.texio.networking;

import java.io.IOException;

public class NoInternetAccessException extends IOException {
}
