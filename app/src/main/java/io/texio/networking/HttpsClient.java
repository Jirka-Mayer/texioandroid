package io.texio.networking;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import io.texio.integration.UnexpectedResponseException;

/**
 * Wrapper for making https requests to a single domain
 *
 * Preserves cookies in between requests
 */
public class HttpsClient {

    // chromium, ubuntu
    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.80 Chrome/71.0.3578.80 Safari/537.36";

    /**
     * Headers to set to every request
     */
    private Map<String, String> headers = new HashMap<>();

    /**
     * Cookies stored in between requests
     */
    private CookieStore cookies = new CookieStore();

    /**
     * The domain we are requesting
     */
    private String domain;

    /**
     * All timeouts (ms)
     */
    private int timeout = 10_000;

    private int lastResponseCode = -1;

    public boolean followRedirects = true;

    public HttpsClient(String domain) {
        if (!domain.endsWith("/"))
            domain += "/";
        this.domain = domain;
    }

    @Nullable
    public String getCookies() {
        return cookies.serialize();
    }

    public void setCookies(@Nullable String cookies) {
        this.cookies = CookieStore.deserialize(cookies);
    }

    public void setHeader(String name, String value) {
        headers.put(name, value);
    }

    public void clearHeaders() {
        headers.clear();
    }

    public int getLastResponseCode() {
        return lastResponseCode;
    }

    public URL pathToUrl(String path) {
        if (path.startsWith("//"))
            throw new IllegalArgumentException("Path shouldn't start with multiple '/'.");

        String stringUrl = "https://" + domain;

        if (path.startsWith("/"))
            stringUrl += path.substring(1);
        else
            stringUrl += path;

        try {
            return new URL(stringUrl);
        } catch (MalformedURLException e) {
            // urls are hardcoded so this is not likely
            throw new RuntimeException(e);
        }
    }

    private void setConnectionHeaders(HttpsURLConnection connection) {
        for (String headerName : headers.keySet()) {
            connection.setRequestProperty(
                headerName,
                headers.get(headerName)
            );
        }
    }

    @NonNull
    public String get(String path) throws IOException {
        HttpsURLConnection connection = null;
        InputStream inStream = null;
        try {
            connection = (HttpsURLConnection) pathToUrl(path).openConnection();

            connection.setInstanceFollowRedirects(followRedirects);
            connection.setReadTimeout(timeout);
            connection.setConnectTimeout(timeout);
            connection.setRequestMethod("GET");

            if (!cookies.isEmpty())
                setHeader("Cookie", cookies.header_cookie());
            setHeader("User-Agent", USER_AGENT);
            setConnectionHeaders(connection);

            connection.setDoOutput(false); // no app output
            connection.setDoInput(true);

            connection.connect();

            lastResponseCode = connection.getResponseCode();

            // Retrieve the response body as an InputStream.
            if (lastResponseCode == 200)
                inStream = connection.getInputStream();
            else
                inStream = connection.getErrorStream();

            if (inStream == null)
                throw new IOException("No response stream found.");

            String response = this.readStream(inStream);

            receiveCookies(connection);

            return response;
        } finally {
            if (connection != null)
                connection.disconnect();

            if (inStream != null)
                inStream.close();
        }
    }

    @NonNull
    public String post(String path, String payload) throws IOException {
        HttpsURLConnection connection = null;
        InputStream inStream = null;
        try {
            connection = (HttpsURLConnection) pathToUrl(path).openConnection();

            connection.setInstanceFollowRedirects(followRedirects);
            connection.setReadTimeout(timeout);
            connection.setConnectTimeout(timeout);
            connection.setRequestMethod("POST");

            if (!cookies.isEmpty())
                setHeader("Cookie", cookies.header_cookie());
            setHeader("User-Agent", USER_AGENT);
            setConnectionHeaders(connection);

            connection.setDoOutput(true);
            connection.setDoInput(true);

            BufferedOutputStream outStream = new BufferedOutputStream(connection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outStream, "UTF-8"));
            writer.write(payload);
            writer.flush();
            writer.close();
            outStream.close();

            connection.connect();

            lastResponseCode = connection.getResponseCode();

            // Retrieve the response body as an InputStream.
            if (lastResponseCode == 200)
                inStream = connection.getInputStream();
            else
                inStream = connection.getErrorStream();

            receiveCookies(connection);

            if (lastResponseCode == 302)
                return "";

            if (inStream == null)
                throw new IOException("No response stream found.");

            String response = this.readStream(inStream);

            return response;
        } finally {
            if (connection != null)
                connection.disconnect();

            if (inStream != null)
                inStream.close();
        }
    }

    /**
     * Makes a GET request and
     * it parses the response as a json object
     */
    @NonNull
    public JSONObject getAsJSONObject(String path) throws IOException {
        try {
            String response = get(path);
            return new JSONObject(response);
        } catch (JSONException e) {
            throw new UnexpectedResponseException(
                "Response was not a json object as expected for '" + path + "'.", e
            );
        }
    }

    @NonNull
    public String post(String path, JSONObject json) throws IOException {
        setHeader("Content-Type", "application/json;charset=UTF-8");
        return post(path, json.toString());
    }

    /**
     * Makes a POST request with the request being a json object and
     * it parses the response as a json object
     */
    @NonNull
    public JSONObject postAsJSONObject(String path, JSONObject json) throws IOException {
        try {
            String response = post(path, json);
            return new JSONObject(response);
        } catch (JSONException e) {
            throw new UnexpectedResponseException(
                "Response was not a json object as expected for '" + path + "'.", e
            );
        }
    }

    @NonNull
    public String post(String path, Map<String, String> payload) throws IOException {
        StringBuilder sb = new StringBuilder();

        int index = 0;
        for (String key : payload.keySet()) {
            sb.append(URLEncoder.encode(key, "UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode(payload.get(key), "UTF-8"));

            if (index < payload.size() - 1)
                sb.append("&");
            index++;
        }

        setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        return post(path, sb.toString());
    }

    private void receiveCookies(HttpsURLConnection connection) {
        for (int i = 0;; i++) {
            String headerName = connection.getHeaderFieldKey(i);
            String headerValue = connection.getHeaderField(i);

            if (headerName == null && headerValue == null)
                break;

            if ("Set-Cookie".equalsIgnoreCase(headerName)) {
                cookies.header_setCookie(headerValue);
            }
        }
    }

    private String readStream(InputStream stream) throws IOException
    {
        Reader reader = new InputStreamReader(stream, "UTF-8");
        StringBuilder content = new StringBuilder();

        char[] buffer = new char[1024];
        int readChars;

        while ((readChars = reader.read(buffer)) != -1)
            content.append(buffer, 0, readChars);

        return content.toString();
    }
}
