package io.texio.networking;

import java.util.HashMap;
import java.util.Map;

/**
 * Very, very simple cookie store
 */
public class CookieStore {
    public Map<String, String> cookies = new HashMap<>();

    public void header_setCookie(String headerValue) {
        if (headerValue == null)
            return;

        String[] fields = headerValue.split(";\\s*");
        String[] parts = fields[0].split("=");

        if (parts.length != 2)
            return;

        cookies.put(parts[0].trim(), parts[1].trim());
    }

    public String header_cookie() {
        StringBuilder sb = new StringBuilder();

        int index = 0;
        for (String name : cookies.keySet()) {
            sb.append(name);
            sb.append("=");
            sb.append(cookies.get(name));
            if (index < cookies.size() - 1)
                sb.append("; ");
            index++;
        }

        return sb.toString();
    }

    public boolean isEmpty() {
        return cookies.isEmpty();
    }

    public String serialize() {
        return header_cookie();
    }

    public static CookieStore deserialize(String data) {
        CookieStore store = new CookieStore();

        String[] pairs = data.split(";");
        for (String pair : pairs) {
            String[] parts = pair.split("=");
            if (parts.length != 2)
                continue;
            store.cookies.put(parts[0].trim(), parts[1].trim());
        }

        return store;
    }
}
