package io.texio.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import io.texio.hyperlog.HyperLogExtended;

/**
 * Helper methods for working with files
 */
public class FileUtil {

    public static final String TAG = FileUtil.class.getSimpleName();

    /**
     * Reads file bytes into an array.
     * Returns null when a problem occurs (e.g. file may not exist)
     */
    public static byte[] readAllBytes(File file) {
        try {
            RandomAccessFile f = new RandomAccessFile(file.getAbsolutePath(), "r");
            byte[] data = new byte[(int) f.length()];
            f.readFully(data);
            return data;
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            HyperLogExtended.exceptionWithTrace(
                TAG, "Exception while reading file '" + file.getAbsolutePath() +"'.", e
            );
            return null;
        }
    }

    /**
     * Write bytes into a file
     * Returns false on failure
     */
    public static boolean writeAllBytes(File file, byte[] bytes) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bytes);
            fos.close();
            return true;
        } catch (IOException e) {
            HyperLogExtended.exceptionWithTrace(
                TAG, "Exception while writing into file '" + file.getAbsolutePath() +"'.", e
            );
            return false;
        }
    }

}
