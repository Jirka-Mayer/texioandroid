package io.texio.util;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

import io.texio.engine.text.TextProcessor;

public class Str
{
    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    /**
     * Turns a string into a slug
     */
    public static String slug(String input)
    {
        // taken from:
        // https://stackoverflow.com/questions/1657193/java-code-library-for-generating-slugs-for-use-in-pretty-urls

        // replace spaces with dashes
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");

        // separate accent marks from the glyphs č -> cˇ
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);

        // remove non-base glyph characters (accent marks)
        String slug = normalized.replaceAll("\\p{M}", "");

        // lowercase
        return slug.toLowerCase(Locale.ENGLISH);
    }
}
