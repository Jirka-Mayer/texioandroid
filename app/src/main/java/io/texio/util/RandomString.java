package io.texio.util;

import java.util.Random;

public class RandomString
{
    public static String generate(int length)
    {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++)
            if (generator.nextInt(2) == 0)
                randomStringBuilder.append((char)(generator.nextInt(26) + 65));
            else
                randomStringBuilder.append((char)(generator.nextInt(26) + 97));

        return randomStringBuilder.toString();
    }
}
