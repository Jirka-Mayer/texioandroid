package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.texio.db.receipt.ReceiptDate;

public class PrivacyPolicy
{
    public static final String KEY_PRIVACY_POLICY_ACCEPTED = "pref_privacyPolicyAccepted";

    public static boolean hasBeenAccepted(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(KEY_PRIVACY_POLICY_ACCEPTED, null) != null;
    }

    public static void accept(Context context)
    {
        if (PrivacyPolicy.hasBeenAccepted(context))
            return;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(KEY_PRIVACY_POLICY_ACCEPTED, ReceiptDate.today().toString());
        editor.apply();
    }
}
