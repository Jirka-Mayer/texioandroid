package io.texio.networking;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@Ignore // because it makes network requests
public class HttpsClientTest {

    @Test
    public void itMakesGetRequests() throws IOException {
        HttpsClient client = new HttpsClient("google.com");
        String response = client.get("/");
        assertTrue(response.contains("google"));
    }

    @Test
    public void itMakesJsonPostRequests() throws IOException {
        HttpsClient client = new HttpsClient("ptsv2.com");
        client.setHeader("My-Custom-Header", "Lorem ipsum dolor");
        client.setHeader("Content-Type", "application/json;charset=UTF-8");
        client.post("/t/s4idd-1546874218/post", "{\"foo\": \"bar\"}");

        // to verify, check:
        // https://ptsv2.com/t/s4idd-1546874218
    }

    @Test
    public void itMakesPostRequests() throws IOException {
        HttpsClient client = new HttpsClient("ptsv2.com");
        Map<String, String> payload = new HashMap<>();
        payload.put("somekey", "value@example.com");
        payload.put("foo", "bar bar");
        client.post("/t/s4idd-1546874218/post", payload);

        // to verify, check:
        // https://ptsv2.com/t/s4idd-1546874218
    }
}
