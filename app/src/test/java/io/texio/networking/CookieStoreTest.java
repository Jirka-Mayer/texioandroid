package io.texio.networking;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CookieStoreTest {
    private CookieStore store;

    @Before
    public void SetUp() {
        store = new CookieStore();
    }

    @Test
    public void itStoresCookies() {
        store.header_setCookie("language=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/");
        store.header_setCookie("language=cs; expires=Wed, 23-Jan-2019 20:15:04 GMT; Max-Age=1296000; path=/");
        store.header_setCookie("language=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/");
        store.header_setCookie("language=cs; expires=Wed, 23-Jan-2019 20:15:04 GMT; Max-Age=1296000; path=/");
        store.header_setCookie("CORESESS=b5a74727fef5640caaa9d390ed976755; path=/; secure; HttpOnly");

        assertEquals("cs", store.cookies.get("language"));
        assertEquals("b5a74727fef5640caaa9d390ed976755", store.cookies.get("CORESESS"));
        assertEquals(2, store.cookies.size());

        String[] exp = new String[] {
            "CORESESS=b5a74727fef5640caaa9d390ed976755",
            "language=cs"
        };
        String[] act = store.header_cookie().split("; ");
        Arrays.sort(exp);
        Arrays.sort(act);
        assertArrayEquals(exp, act);

        // serialization
        store = CookieStore.deserialize(store.serialize());

        assertEquals("cs", store.cookies.get("language"));
        assertEquals("b5a74727fef5640caaa9d390ed976755", store.cookies.get("CORESESS"));
        assertEquals(2, store.cookies.size());
    }
}
