package io.texio;

import org.junit.Test;

import io.texio.db.other.CurrencyAmount;

import static org.junit.Assert.*;

public class CurrencyAmountTest
{
    @Test
    public void it_parses_amount()
    {
        assertEquals(10000, CurrencyAmount.parse("10"));
        assertEquals(10500, CurrencyAmount.parse("10.5"));
        assertEquals(10050, CurrencyAmount.parse("10.05"));
        assertEquals(10005, CurrencyAmount.parse("10.005"));

        assertEquals(10500, CurrencyAmount.parse("10.500"));
        assertEquals(10050, CurrencyAmount.parse("10.050"));

        assertEquals(0, CurrencyAmount.parse("1.2.3"));

        assertEquals(15000000, CurrencyAmount.parse("15 000"));
        assertEquals(15000230, CurrencyAmount.parse("15,000.23"));
    }

    @Test
    public void it_formats_amount()
    {
        assertEquals("10", CurrencyAmount.toString(10000));
        assertEquals("10,45", CurrencyAmount.toString(10450));
        assertEquals("10,045", CurrencyAmount.toString(10045));
        assertEquals("1 123 123,12", CurrencyAmount.toString(1123123120));

        assertEquals("1", CurrencyAmount.toString(1000));
        assertEquals("12", CurrencyAmount.toString(12000));
        assertEquals("123", CurrencyAmount.toString(123000));
        assertEquals("1 234", CurrencyAmount.toString(1234000));
        assertEquals("12 345", CurrencyAmount.toString(12345000));
        assertEquals("123 456", CurrencyAmount.toString(123456000));
        assertEquals("1 234 567", CurrencyAmount.toString(1234567000));
    }
}
