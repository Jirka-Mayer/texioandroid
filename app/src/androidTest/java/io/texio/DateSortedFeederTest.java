package io.texio;

import android.app.Instrumentation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.ui.receiptListView.DateSortedFeeder;
import io.texio.ui.receiptListView.LoaderListItem;
import io.texio.ui.receiptListView.ReceiptListAdapter;
import io.texio.ui.receiptListView.ReceiptListItem;

import static org.junit.Assert.*;

public class DateSortedFeederTest {

    private ReceiptListAdapter adapter;
    private DateSortedFeeder feeder;

    private ReceiptState unusedState;

    @Before
    public void setUp() {
        adapter = new ReceiptListAdapter();
        adapter.NOTIFY_RECYCLER_VIEW = false;

        feeder = new DateSortedFeeder(adapter);
        feeder.LOG_MISSING_CONTEXT = false;

        unusedState = ReceiptState.fromStateChanges(new ReceiptStateChange[0]);
    }

    @Test
    public void insertingNewReceiptCreatesHeaderItemAndReceiptItem() {
        Receipt receipt = ReceiptFactory.make();
        feeder.insertReceipt(receipt, unusedState);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        Assert.assertEquals(receipt, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void insertingTwoReceiptsWithDifferentMonthsCreatesTwoHeaders() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        b.date = ReceiptDate.parse("2019-01-24");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        assertEquals(4, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(2));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(3));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(b, ((ReceiptListItem)adapter.getItem(3)).getReceipt());
    }

    @Test
    public void insertingTwoReceiptsWithDifferentMonthsInReversedOrderCreatesTwoHeaders() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        b.date = ReceiptDate.parse("2019-01-24");
        feeder.insertReceipt(b, unusedState); // <-- swapped
        feeder.insertReceipt(a, unusedState); // <--

        assertEquals(4, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(2));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(3));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(b, ((ReceiptListItem)adapter.getItem(3)).getReceipt());
    }

    @Test
    public void insertingTwoReceiptsWithSameMonthCreatesOneHeader() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        b.date = ReceiptDate.parse("2019-02-20");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        assertEquals(3, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(2));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(b, ((ReceiptListItem)adapter.getItem(2)).getReceipt());
    }

    @Test
    public void nullDateReceiptGetsNoHeading() {
        Receipt receipt = ReceiptFactory.make();
        receipt.date = null;
        feeder.insertReceipt(receipt, unusedState);

        assertEquals(1, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(0));
    }

    @Test
    public void insertingAtTheEndWhileLoadingKeepsLoaderAtTheEnd() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        a.date = null;

        feeder.insertReceipt(a, unusedState);
        adapter.insertItem(1, new LoaderListItem());

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_LOADER, adapter.getItemViewType(1));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(0)).getReceipt());

        feeder.insertReceipt(b, unusedState);

        assertEquals(4, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(1));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(2));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_LOADER, adapter.getItemViewType(3));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(0)).getReceipt());
        assertEquals(b, ((ReceiptListItem)adapter.getItem(2)).getReceipt());
    }

    @Test
    public void receiptDoesNotGetInsertedTwice() {
        Receipt receipt = ReceiptFactory.make();

        feeder.insertReceipt(receipt, unusedState);
        feeder.insertReceipt(receipt, unusedState);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
    }

    @Test
    public void removingReceiptRemovesItsHeader() {
        Receipt receipt = ReceiptFactory.make();
        feeder.insertReceipt(receipt, unusedState);

        feeder.removeReceipt(receipt.id);

        assertEquals(0, adapter.getItemCount());
    }

    @Test
    public void removingFirstOfTwoSameKeepsTheHeader() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        feeder.removeReceipt(a.id);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void removingSecondOfTwoSameKeepsTheHeader() {
        Receipt a = ReceiptFactory.make();
        Receipt b = ReceiptFactory.make();
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        feeder.removeReceipt(b.id); // b here

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void removingNullReceiptDoesNotExplode() {
        Receipt receipt = ReceiptFactory.make();
        receipt.date = null;
        feeder.insertReceipt(receipt, unusedState);

        feeder.removeReceipt(receipt.id);

        assertEquals(0, adapter.getItemCount());
    }

    @Test
    public void removingFirstOfTwoDifferentRemovesTheHeader() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-01-24");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        feeder.removeReceipt(a.id);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void removingSecondOfTwoDifferentRemovesTheHeader() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-01-24");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        feeder.removeReceipt(b.id);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(ReceiptListAdapter.VIEW_TYPE_RECEIPT, adapter.getItemViewType(1));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void moving_AB_2_BA_byBinSameMonth() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-02-20");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        b.date = ReceiptDate.parse("2019-02-28");
        feeder.updateReceipt(b, unusedState);

        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(a, ((ReceiptListItem)adapter.getItem(2)).getReceipt());
    }

    @Test
    public void moving_AB_2_BA_byAinSameMonth() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-02-20");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        a.date = ReceiptDate.parse("2019-02-10");
        feeder.updateReceipt(a, unusedState);

        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(a, ((ReceiptListItem)adapter.getItem(2)).getReceipt());
    }

    @Test
    public void moving_HAHB_2_HAB_byB_removesHeading() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-01-24");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        b.date = ReceiptDate.parse("2019-02-10");
        feeder.updateReceipt(b, unusedState);

        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(b, ((ReceiptListItem)adapter.getItem(2)).getReceipt());
    }

    @Test
    public void moving_HAB_2_HAHB_byB_createsHeading() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-02-20");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        b.date = ReceiptDate.parse("2019-01-10");
        feeder.updateReceipt(b, unusedState);

        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(2));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(3)).getReceipt());
    }

    @Test
    public void moving_HAHB_2_HBHA_byB_createsAndRemovesHeading() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        Receipt b = ReceiptFactory.make();
        b.date = ReceiptDate.parse("2019-01-20");
        feeder.insertReceipt(a, unusedState);
        feeder.insertReceipt(b, unusedState);

        b.date = ReceiptDate.parse("2019-03-10");
        feeder.updateReceipt(b, unusedState);

        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(b, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(2));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(3)).getReceipt());
    }

    @Test
    public void changingReceiptDateInPlaceWorks() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        feeder.insertReceipt(a, unusedState);

        a.date = ReceiptDate.parse("2019-03-10");
        feeder.updateReceipt(a, unusedState);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void notChangingReceiptDateWorks() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        feeder.insertReceipt(a, unusedState);

        // no actual update
        feeder.updateReceipt(a, unusedState);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }

    @Test
    public void movingReceiptBackwardWithinAMonthWorks() {
        Receipt a = ReceiptFactory.make();
        a.date = ReceiptDate.parse("2019-02-24");
        feeder.insertReceipt(a, unusedState);

        a.date = ReceiptDate.parse("2019-02-23");
        feeder.updateReceipt(a, unusedState);

        assertEquals(2, adapter.getItemCount());
        assertEquals(ReceiptListAdapter.VIEW_TYPE_HEADING, adapter.getItemViewType(0));
        assertEquals(a, ((ReceiptListItem)adapter.getItem(1)).getReceipt());
    }
}
