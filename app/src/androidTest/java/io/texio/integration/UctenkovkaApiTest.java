package io.texio.integration;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import io.texio.CredentialStorage;
import io.texio.Encrypter;
import io.texio.db.models.Receipt;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptField;
import io.texio.db.receipt.ReceiptTime;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.integration.bazar.BazarApi;
import io.texio.integration.uctenkovka.InvalidEetDataException;
import io.texio.integration.uctenkovka.Uctenkovka;
import io.texio.integration.uctenkovka.UctenkovkaApi;

import static org.junit.Assert.*;

@Ignore
@RunWith(AndroidJUnit4.class)
public class UctenkovkaApiTest {

    /*
        NOTE: tests here must be run manually one by one against the uctenkovka server
        with some active data

        Tests are meant to be run during development of the API connection and not touched afterwards
     */

    /**
     * Access explicitCredentials if needed for testing
     */
    private final String EMAIL = "jirkamayer97@gmail.com";
    private final String PASSWORD = "";
    private final String TOKEN = "";

    /**
     * The API connection
     */
    private UctenkovkaApi api;

    /**
     * Clear credential storage if needed for testing
     */
    private CredentialStorage storage;

    @Before
    public void setUp() {
        // create a clear credential storage if needed
        SharedPreferences preferences = InstrumentationRegistry.getTargetContext().getSharedPreferences(
            "io.texio.FAKE_KEYS", Context.MODE_PRIVATE
        );
        preferences.edit().clear().apply();
        storage = new CredentialStorage(preferences, new Encrypter(Encrypter.generateAesKey()));
    }

    /////////////////
    // Login logic //
    /////////////////

    @Test
    public void itCanLoginWithToken() throws UnirestException, InvalidCredentialsException {
        api = new UctenkovkaApi(EMAIL, TOKEN);
    }

    @Test
    public void itThrowsWithInvalidTokenLogin() throws UnirestException {
        try {
            api = new UctenkovkaApi(EMAIL, TOKEN + "xxxx");
            fail("InvalidCredentialsException not thrown");
        } catch (InvalidCredentialsException e) { }
    }

    @Test
    public void itCanLoginWithCredentials() throws UnirestException, InvalidCredentialsException {
        api = new UctenkovkaApi(
            new CredentialStorage.CredentialPair(EMAIL, PASSWORD)
        );
    }

    @Test
    public void itThrowsWithInvalidCredentials() throws UnirestException {
        try {
            api = new UctenkovkaApi(
                new CredentialStorage.CredentialPair("asd", "asd")
            );
            fail("InvalidCredentialsException not thrown");
        } catch (InvalidCredentialsException e) { }
    }

    @Test
    public void itCanLoginWithCredentialStorage() throws Exception {
        storage.storePair(
            Uctenkovka.LOGIN_PAIR_CSK,
            new CredentialStorage.CredentialPair(EMAIL, PASSWORD)
        );
        api = new UctenkovkaApi(storage);
    }

    @Test
    public void itThrowsInvalidArgumentWithEmptyCredentialStorage() throws Exception {
        try {
            api = new UctenkovkaApi(storage);
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) { }
    }

    @Test
    public void itThrowsInvalidCredentialsWithStorage() throws Exception {
        storage.storePair(
            Uctenkovka.LOGIN_PAIR_CSK,
            new CredentialStorage.CredentialPair("asd", "asd")
        );
        try {
            api = new UctenkovkaApi(storage);
            fail("InvalidCredentialsException not thrown");
        } catch (InvalidCredentialsException e) { }
    }

    /////////////////
    // Other logic //
    /////////////////

    private void loginViaToken() {
        try {
            api = new UctenkovkaApi(EMAIL, TOKEN);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void itObtainsReceiptState() throws UnirestException {
        loginViaToken();

        // this has to be real existing receipt
        final long RECEIPT_UCTENKOVKA_ID = 234808089;
        final ReceiptStateEnum EXPECTED_STATE = ReceiptStateEnum.UCTENKOVKA_NOT_WINNING;

        assertEquals(
            EXPECTED_STATE,
            api.queryReceiptState(RECEIPT_UCTENKOVKA_ID)
        );
    }

    @Test
    public void itRegistersReceiptThatGetsRejected() throws Exception {
        loginViaToken();

        Receipt receipt = new Receipt();
        receipt.date = ReceiptDate.today();
        receipt.time = ReceiptTime.now();
        receipt.total = CurrencyAmount.parse("42");
        receipt.fik = "abababab-bbbb-4aaa";
        EetData data = EetData.fromReceipt(receipt, false);

        UctenkovkaApi.RegistrationResult result = api.registerReceipt(data);

        assertEquals(
            ReceiptStateEnum.UCTENKOVKA_REJECTED,
            result.getState()
        );

        assertTrue(result.getReceiptEetId() > 20000);
    }

    @Test
    public void itRegistersReceiptThatGetsVerified() throws Exception {
        loginViaToken();

        // Not-yet uploaded, valid receipt needed for this test!

        Receipt receipt = new Receipt();
        receipt.date = ReceiptDate.parse("2019-01-22");
        receipt.time = ReceiptTime.parse("07:50");
        receipt.total = CurrencyAmount.parse("41");
        receipt.fik = "da953f4b-a07c-4ac5";
        EetData data = EetData.fromReceipt(receipt, false);

        UctenkovkaApi.RegistrationResult result = api.registerReceipt(data);

        assertEquals(
            ReceiptStateEnum.UCTENKOVKA_VERIFIED,
            result.getState()
        );

        assertTrue(result.getReceiptEetId() > 20000);
    }

    @Test
    public void itRegistersReceiptThatGetsVIO111() throws Exception {
        /*
            No it doesn't, When you register a receipt that does not satisfy 1x1x1,
            you will get "VERIFIED" after registration. The state changes only later,
            once the receipt gets properly checked.
         */
    }

    @Test
    public void itRegistersReceiptWithMalformedBkpAndGetsAnError() throws Exception {
        loginViaToken();

        Receipt receipt = new Receipt();
        receipt.date = ReceiptDate.today();
        receipt.time = ReceiptTime.now();
        receipt.total = CurrencyAmount.parse("42");
        receipt.bkp = "lorem-ipsum";
        EetData data = EetData.fromReceipt(receipt, false);

        try {
            UctenkovkaApi.RegistrationResult result = api.registerReceipt(data);
            fail("Exception was not thrown");
        } catch (InvalidEetDataException e) {
            InvalidEetDataException.FieldError error = e.errors.get(0);

            assertEquals(ReceiptField.BKP, error.field);
            assertEquals("object.invalid", error.code);
        }
    }

    @Test
    public void itRegistersLongExpiredReceiptAndGetsAnError() throws Exception {
        loginViaToken();

        Receipt receipt = new Receipt();
        receipt.date = new ReceiptDate(2006, 2, 15);
        receipt.time = ReceiptTime.now();
        receipt.total = CurrencyAmount.parse("42");
        receipt.fik = "abababab-bbbb-4aaa";
        EetData data = EetData.fromReceipt(receipt, false);

        try {
            UctenkovkaApi.RegistrationResult result = api.registerReceipt(data);
            fail("Exception was not thrown");
        } catch (InvalidEetDataException e) {
            InvalidEetDataException.FieldError error = e.errors.get(0);

            assertEquals(ReceiptField.DATE, error.field);
            assertEquals("object.invalid", error.code);
        }
    }

    @Test
    public void itRegistersDuplicateReceiptAndGetsAnException() throws Exception {
        loginViaToken();

        // NOTE: has to be in-draw otherwise you get date error (receipt expired)

        Receipt receipt = new Receipt();
        receipt.date = ReceiptDate.parse("2019-01-22");
        receipt.time = ReceiptTime.parse("07:50");
        receipt.total = CurrencyAmount.parse("41");
        receipt.fik = "da953f4b-a07c-4ac5";
        EetData data = EetData.fromReceipt(receipt, false);

        try {
            UctenkovkaApi.RegistrationResult result = api.registerReceipt(data);
            fail("Exception was not thrown");
        } catch (DuplicateReceiptException e) {
            assertTrue(true);
        }
    }
}
