package io.texio.integration;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import io.texio.CredentialStorage;
import io.texio.Encrypter;
import io.texio.integration.bazar.Bazar;
import io.texio.integration.bazar.BazarApi;

import static org.junit.Assert.*;

//@Ignore
@RunWith(AndroidJUnit4.class)
public class BazarApiTest {

    // provide this for testing
    private final String SESSION = "CORESESS=ec8872525916a207d8ba6d0495991c89; CORETOKEN=b2201fa012eacc65b7d90de4%3A3e4ad8a48d60b483b9450bd950401aff";
    private final String EMAIL = "jirkamayer97@gmail.com";
    private final String PASSWORD = ""; // true value only needed for the credential login test

    private BazarApi api;
    private CredentialStorage storage;

    @Before
    public void setUp() {
        SharedPreferences preferences = InstrumentationRegistry.getTargetContext().getSharedPreferences(
            "io.texio.FAKE_KEYS", Context.MODE_PRIVATE
        );
        preferences.edit().clear().apply();
        storage = new CredentialStorage(preferences, new Encrypter(Encrypter.generateAesKey()));
    }

    private void loginViaSession() throws Exception {
        // credentials needed because there's a verification before session login
        storage.storePair(
            Bazar.LOGIN_PAIR_CSK,
            new CredentialStorage.CredentialPair(EMAIL, PASSWORD)
        );
        storage.store(Bazar.SESSION_CSK, SESSION);
        api = new BazarApi(storage);
    }

    @Test
    @Ignore // WARNING: Do not run too many times per hour, it's more like a one-time check
    public void testLoginViaCredentials() throws Exception {
        api = new BazarApi(new CredentialStorage.CredentialPair(EMAIL, PASSWORD));
    }

    @Test
    public void itQueriesUserEmail() throws Exception {
        loginViaSession();
        assertEquals(EMAIL, api.page_settings().getUserEmail());
    }
}
