package io.texio.activityTests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.ui.trash.TrashActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static io.texio.db.receipt.receiptState.ReceiptStateEnum.BAZAR_FOR_SALE;
import static io.texio.db.receipt.receiptState.ReceiptStateEnum.BAZAR_REMOVED;
import static io.texio.db.receipt.receiptState.ReceiptStateEnum.BAZAR_UPLOADED;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class TrashActivityTest {

    /**
     * Our database connection
     */
    private AppDatabase db;

    @Rule
    public ActivityTestRule<TrashActivity> activityRule
        = new ActivityTestRule<>(TrashActivity.class, false, false);

    @Before
    public void setUp() {
        db = AppDatabaseFactory.getInstance(InstrumentationRegistry.getTargetContext());
        db.clearAllTables();
    }

    public void launchActivity() {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), TrashActivity.class);
        activityRule.launchActivity(intent);
    }

    @Test
    public void itCanRestoreReceipts() throws Exception {
        Receipt[] receipts = new Receipt[] {
            ReceiptFactory.make(), ReceiptFactory.make(), ReceiptFactory.make()
        };
        receipts[0].store = "foo"; receipts[2].store = "bar";
        receipts[0].deletedNow(); receipts[1].deletedNow(); receipts[2].deletedNow();
        db.receiptDao().insert(receipts);
        launchActivity();

        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[0].store))))
            .perform(click());
        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[2].store))))
            .perform(click());

        onView(withId(R.id.action_restore)).perform(click());

        List<Receipt> r = db.receiptDao().all();
        assertNull(r.get(0).deletedAt);
        assertNotNull(r.get(1).deletedAt);
        assertNull(r.get(2).deletedAt);
    }

    @Test
    public void itCanPermanentlyDeleteReceipts() throws Exception {
        Receipt[] receipts = new Receipt[] {
            ReceiptFactory.make(), ReceiptFactory.make(), ReceiptFactory.make()
        };
        receipts[0].store = "foo"; receipts[2].store = "bar";
        receipts[0].deletedNow(); receipts[1].deletedNow(); receipts[2].deletedNow();
        db.receiptDao().insert(receipts);
        launchActivity();

        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[0].store))))
            .perform(click());
        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[2].store))))
            .perform(click());

        ((Toolbar)activityRule.getActivity().findViewById(R.id.selection_toolbar))
            .showOverflowMenu();
        Thread.sleep(500);
        onView(withText("Delete")).perform(click());

        assertEquals(1, db.receiptDao().all().size());
    }

    @Test
    public void itCanEmptyTheTrash() throws Exception {
        Receipt[] receipts = new Receipt[] {
            ReceiptFactory.make(), ReceiptFactory.make(), ReceiptFactory.make()
        };
        receipts[0].deletedNow(); receipts[1].deletedNow(); receipts[2].deletedNow();
        db.receiptDao().insert(receipts);
        launchActivity();

        // following not working probably because I have two toolbars
        // openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());

        ((Toolbar)activityRule.getActivity().findViewById(R.id.toolbar))
            .showOverflowMenu();
        Thread.sleep(500);
        onView(withText("Empty trash")).perform(click());

        assertEquals(0, db.receiptDao().all().size());
    }

    @Test
    public void receiptWithStateCanBeDeleted() throws Exception {
        Receipt r = ReceiptFactory.make();
        r.deletedNow();
        db.receiptDao().insert(r);
        db.receiptStateChangeDao().insert(
            new ReceiptStateChange(r, BAZAR_UPLOADED),
            new ReceiptStateChange(r, BAZAR_FOR_SALE),
            new ReceiptStateChange(r, BAZAR_REMOVED)
        );
        launchActivity();

        ((Toolbar)activityRule.getActivity().findViewById(R.id.toolbar))
            .showOverflowMenu();
        Thread.sleep(500);
        onView(withText("Empty trash")).perform(click());

        assertEquals(0, db.receiptDao().all().size());
    }
}
