package io.texio.activityTests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.DatePicker;
import android.widget.TimePicker;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.CurrencyAmount;
import io.texio.db.receipt.ReceiptDate;
import io.texio.db.receipt.ReceiptTime;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.ui.receiptDetail.ReceiptDetailActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import static io.texio.db.receipt.receiptState.ReceiptStateEnum.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ReceiptDetailActivityTest {

    /**
     * The receipt we are looking at
     */
    private Receipt receipt;

    /**
     * State changes that happend to the receipt
     * (to be saved to database on activity launch)
     */
    private List<ReceiptStateChange> stateChanges;

    /**
     * Our database connection
     */
    private AppDatabase db;

    @Rule
    public ActivityTestRule<ReceiptDetailActivity> activityRule
        = new ActivityTestRule<>(ReceiptDetailActivity.class, false, false);

    @Before
    public void setUp() {
        receipt = ReceiptFactory.make();
        stateChanges = new ArrayList<>();
        db = AppDatabaseFactory.getInstance(InstrumentationRegistry.getTargetContext());
        db.clearAllTables();
    }

    /**
     * Saves the receipt to database and launches the activity on it
     */
    public void launchActivity() {
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(stateChanges.toArray(new ReceiptStateChange[0]));

        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ReceiptDetailActivity.class);
        intent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, receipt.id);
        activityRule.launchActivity(intent);
    }

    /**
     * Saves the receipt and restarts the activity on the receipt
     */
    public void restartActivity() {
        db.receiptDao().update(receipt);
        db.receiptStateChangeDao().update(stateChanges.toArray(new ReceiptStateChange[0]));

        activityRule.finishActivity();

        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ReceiptDetailActivity.class);
        intent.putExtra(IntentExtras.EXTRA_RECEIPT_ID, receipt.id);
        activityRule.launchActivity(intent);
    }

    /**
     * Updates data in the receipt instance to reflect data in database
     */
    public void refreshReceiptFromDatabase() {
        receipt = db.receiptDao().find(receipt.id);
    }

    /////////////////
    // Store field //
    /////////////////

    @Test
    public void storeFieldDisplaysStoreName() {
        receipt.store = "My store";
        launchActivity();
        onView(withId(R.id.store_field)).check(matches(withText("My store")));
    }

    @Test
    public void storeFieldDisplaysNullStoreProperly() {
        receipt.store = null;
        launchActivity();
        onView(withId(R.id.store_field)).check(matches(withText("")));
    }

    @Test
    public void storeFieldCanBeChanged() {
        launchActivity();

        // change name
        onView(withId(R.id.store_group)).perform(click());
        onView(withId(R.id.text_field)).perform(ViewActions.replaceText("New store"));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check that it's been saved
        refreshReceiptFromDatabase();
        assertEquals("New store", receipt.store);

        // check it displays new data
        onView(withId(R.id.store_field)).check(matches(withText("New store")));
    }

    @Test
    public void storeFieldCanBeChangedIfReceiptInUctenkovka() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_VERIFIED));

        receipt.store = "My store";
        launchActivity();

        // change
        onView(withId(R.id.store_group)).perform(click());
        onView(withId(R.id.text_field)).perform(ViewActions.replaceText("New store"));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check that change happened
        refreshReceiptFromDatabase();
        assertEquals("New store", receipt.store);

        // check, that the state has remained unchagned
        ReceiptState currentState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertTrue(currentState.is(UCTENKOVKA_VERIFIED));
    }

    @Test
    public void settingStoreFieldToEmptySetsStoreToNull() {
        receipt.store = "My store";
        launchActivity();

        // change
        onView(withId(R.id.store_group)).perform(click());
        onView(withId(R.id.text_field)).perform(ViewActions.clearText());
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check
        refreshReceiptFromDatabase();
        assertNull(receipt.store);
    }

    ////////////////
    // Date field //
    ////////////////

    @Test
    public void dateFieldDisplaysDate() {
        ReceiptDate date = ReceiptDate.today();
        receipt.date = date;
        launchActivity();

        onView(withId(R.id.date_field))
            .check(matches(withText(date.formatHumanReadable())));
    }

    @Test
    public void dateFieldDisplaysNullProperly() {
        receipt.date = null;
        launchActivity();

        onView(withId(R.id.date_field))
            .check(matches(withText("")));
    }

    @Test
    public void dateFieldCanBeCleared() {
        receipt.date = ReceiptDate.today();
        launchActivity();

        // click clear
        onView(withId(R.id.clear_date_button)).perform(click());

        // check UI update
        onView(withId(R.id.date_field))
            .check(matches(withText("")));

        // check database
        refreshReceiptFromDatabase();
        assertNull(receipt.date);
    }

    @Test
    public void dateFieldCanBeChanged() {
        receipt.date = ReceiptDate.today();
        launchActivity();

        // update
        ReceiptDate newDate = new ReceiptDate(2006, 7, 14);
        onView(withId(R.id.date_group)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
            .perform(PickerActions.setDate(newDate.year, newDate.month + 1, newDate.day));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check UI update
        onView(withId(R.id.date_field))
            .check(matches(withText(newDate.formatHumanReadable())));

        // check database
        refreshReceiptFromDatabase();
        assertEquals(newDate, receipt.date);
    }

    @Test
    public void dateFieldCannotBeChangedWhenInBazar() {
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_FOR_SALE));

        receipt.date = ReceiptDate.today();
        launchActivity();

        // try to change
        onView(withId(R.id.date_group)).perform(click());

        // expect toast message
        onView(withText(R.string.receiptDetail_cannotEditField))
            .inRoot(RootMatchers.withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
            .perform(click());
    }

    @Test
    public void dateFieldCanBeEditedIfReceiptRemovedFromBazarAndStateGetsCleared() {
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_FOR_SALE));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_REMOVED));

        receipt.date = ReceiptDate.today();
        launchActivity();

        // change
        onView(withId(R.id.date_group)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName())))
            .perform(PickerActions.setDate(2006, 5, 12));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check the new state
        ReceiptState currentState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertTrue(currentState.is(UNUSED));

        // check that the state group disappears
        onView(withId(R.id.state_group))
            .check(matches(not(isDisplayed())));
    }

    ////////////////
    // Time field //
    ////////////////

    @Test
    public void timeFieldDisplaysTime() {
        ReceiptTime time = ReceiptTime.now();
        receipt.time = time;
        launchActivity();

        onView(withId(R.id.time_field))
            .check(matches(withText(time.formatHumanReadable())));
    }

    @Test
    public void timeFieldDisplaysNullProperly() {
        receipt.time = null;
        launchActivity();

        onView(withId(R.id.time_field))
            .check(matches(withText("")));
    }

    @Test
    public void timeFieldCanBeCleared() {
        receipt.time = ReceiptTime.now();
        launchActivity();

        // click clear
        onView(withId(R.id.clear_time_button)).perform(click());

        // check UI update
        onView(withId(R.id.time_field))
            .check(matches(withText("")));

        // check database
        refreshReceiptFromDatabase();
        assertNull(receipt.time);
    }

    @Test
    public void timeFieldCanBeChanged() {
        receipt.time = ReceiptTime.now();
        launchActivity();

        // update
        ReceiptTime newTime = new ReceiptTime(16, 20);
        onView(withId(R.id.time_group)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
            .perform(PickerActions.setTime(newTime.hour, newTime.minute));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check UI update
        onView(withId(R.id.time_field))
            .check(matches(withText(newTime.formatHumanReadable())));

        // check database
        refreshReceiptFromDatabase();
        assertEquals(newTime, receipt.time);
    }

    @Test
    public void timeFieldCannotBeChangedWhenInBazar() {
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_FOR_SALE));

        receipt.time = ReceiptTime.now();
        launchActivity();

        // try to change
        onView(withId(R.id.time_group)).perform(click());

        // expect toast message
        onView(withText(R.string.receiptDetail_cannotEditField))
            .inRoot(RootMatchers.withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
            .perform(click());
    }

    @Test
    public void timeFieldCanBeEditedIfReceiptRemovedFromBazarAndStateGetsCleared() {
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_FOR_SALE));
        stateChanges.add(new ReceiptStateChange(receipt, BAZAR_REMOVED));

        receipt.time = ReceiptTime.now();
        launchActivity();

        // change
        onView(withId(R.id.time_group)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName())))
            .perform(PickerActions.setTime(3, 15));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check the new state
        ReceiptState currentState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertTrue(currentState.is(UNUSED));

        // check that the state group disappears
        onView(withId(R.id.state_group))
            .check(matches(not(isDisplayed())));
    }

    /////////////////
    // Total field //
    /////////////////

    @Test
    public void totalFieldDisplaysTotalAmount() {
        receipt.currency = "CZK";
        receipt.total = CurrencyAmount.parse("24.5");
        launchActivity();
        onView(withId(R.id.total_field)).check(matches(withText("24,5 Kč")));
        // NOTE: actual amount formatting is tested elsewhere
    }

    @Test
    public void totalFieldDisplaysZeroAsEmpty() {
        receipt.total = 0;
        launchActivity();
        onView(withId(R.id.total_field)).check(matches(withText("")));
    }

    @Test
    public void totalFieldCanBeChanged() {
        receipt.currency = "CZK";
        receipt.total = CurrencyAmount.parse("120");
        launchActivity();

        // change
        onView(withId(R.id.total_group)).perform(click());
        onView(withId(R.id.text_field)).perform(ViewActions.replaceText("10.12"));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check that it's been saved
        refreshReceiptFromDatabase();
        assertEquals(CurrencyAmount.parse("10.12"), receipt.total);

        // check it displays new data
        onView(withId(R.id.total_field)).check(matches(withText("10,12 Kč")));
    }

    @Test
    public void totalFieldCannotBeChangedWhenInUctenkovka() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_VERIFIED));

        receipt.currency = "CZK";
        receipt.total = CurrencyAmount.parse("120");
        launchActivity();

        // try to change
        onView(withId(R.id.total_group)).perform(click());

        // expect toast message
        onView(withText(R.string.receiptDetail_cannotEditField))
            .inRoot(RootMatchers.withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
            .perform(click());
    }

    @Test
    public void editingTotalFieldWhenRejectedFromUctenkovkaResetsReceiptState() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_REJECTED));

        receipt.currency = "CZK";
        receipt.total = CurrencyAmount.parse("120");
        launchActivity();

        // change
        onView(withId(R.id.total_group)).perform(click());
        onView(withId(R.id.text_field)).perform(ViewActions.replaceText("10.12"));
        onView(withText("OK")).inRoot(isDialog()).perform(click());

        // check the new state
        ReceiptState currentState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertTrue(currentState.is(UNUSED));

        // check that the state group disappears
        onView(withId(R.id.state_group))
            .check(matches(not(isDisplayed())));
    }

    /////////////////
    // State field //
    /////////////////

    /*
        This section is also a test of the ReceiptStateBadge view
     */

    @Test
    public void freshReceiptIsMissingStateGroup() {
        launchActivity();

        onView(withId(R.id.state_group))
            .check(matches(not(isDisplayed())));
    }

    @Test
    public void explicitlyUnusedReceiptIsMissingStateGroup() {
        stateChanges.add(new ReceiptStateChange(receipt, UNUSED));
        launchActivity();

        onView(withId(R.id.state_group))
            .check(matches(not(isDisplayed())));
    }

    @Test
    public void itDisplaysVerifyingState() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_NEW)); // = verifying
        launchActivity();

        onView(withText(R.string.receiptStateBadge_verifying))
            .check(matches(isDisplayed()));
    }

    @Test
    public void itDisplaysVerifyingStateIfOnlyJustUploaded() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        launchActivity();

        onView(withText(R.string.receiptStateBadge_verifying))
            .check(matches(isDisplayed()));
    }

    @Test
    public void itDisplaysVerifiedState() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_NEW));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_VERIFIED));
        launchActivity();

        onView(withText(R.string.receiptStateBadge_verified))
            .check(matches(isDisplayed()));
    }

    @Test
    public void itDisplaysWinningState() {
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_UPLOADED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_NEW));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_VERIFIED));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_IN_DRAW));
        stateChanges.add(new ReceiptStateChange(receipt, UCTENKOVKA_WINNING));
        launchActivity();

        onView(withText(R.string.receiptStateBadge_winning))
            .check(matches(isDisplayed()));
    }
}
