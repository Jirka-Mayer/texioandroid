package io.texio.activityTests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Toolbar;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import io.texio.IntentExtras;
import io.texio.R;
import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.ui.receiptDetail.ReceiptDetailActivity;
import io.texio.ui.receiptList.ReceiptListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ReceiptListActivityTest {

    /**
     * Our database connection
     */
    private AppDatabase db;

    @Rule
    public ActivityTestRule<ReceiptListActivity> activityRule
        = new ActivityTestRule<>(ReceiptListActivity.class, false, false);

    @Before
    public void setUp() {
        db = AppDatabaseFactory.getInstance(InstrumentationRegistry.getTargetContext());
        db.clearAllTables();
    }

    public void launchActivity() {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ReceiptListActivity.class);
        activityRule.launchActivity(intent);
    }

    @Test
    public void receiptCanBeTrashed() throws Exception {
        Receipt[] receipts = new Receipt[] {
            ReceiptFactory.make(), ReceiptFactory.make(), ReceiptFactory.make()
        };
        receipts[0].store = "foo";
        receipts[2].store = "bar";
        db.receiptDao().insert(receipts);
        launchActivity();

        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[0].store))))
            .perform(click());
        onView(allOf(withId(R.id.thumbnail), hasSibling(withText(receipts[2].store))))
            .perform(click());

        onView(withId(R.id.action_trash)).perform(click());

        List<Receipt> r = db.receiptDao().all();
        assertNotNull(r.get(0).deletedAt);
        assertNull(r.get(1).deletedAt);
        assertNotNull(r.get(2).deletedAt);
    }
}
