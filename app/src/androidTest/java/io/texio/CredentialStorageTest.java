package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class CredentialStorageTest {

    private SharedPreferences preferences;
    private CredentialStorage storage;

    @Before
    public void setUp() {
        preferences = InstrumentationRegistry.getTargetContext().getSharedPreferences(
            "io.texio.FAKE_KEYS", Context.MODE_PRIVATE
        );
        preferences.edit().clear().apply();
        storage = new CredentialStorage(preferences, new Encrypter(Encrypter.generateAesKey()));
    }

    @Test
    public void itStoresAndLoadsCredentials() {
        assertNull(storage.load("foo"));
        assertTrue(storage.store("foo", "bar"));
        assertEquals("bar", storage.load("foo"));
    }

    @Test
    public void itForgetsCredentials() {
        storage.store("foo", "bar");
        assertTrue(storage.forget("foo"));
        assertNull(storage.load("foo"));
    }

    @Test
    public void storedPreferencesAreEncoded() {
        storage.store("foo", "bar");
        String storedValue = preferences.getString(CredentialStorage.KEY_PREFIX + "foo", null);
        assertNotNull(storedValue);
        assertNotEquals("bar", storedValue);
    }

    @Test
    public void itStoresCzechCharacters() {
        storage.store("foo", "ěščřžýáíéůúťň");
        assertEquals("ěščřžýáíéůúťň", storage.load("foo"));
    }

    @Test
    public void itStoresCredentialPair() {
        assertTrue(storage.storePair("foo", new CredentialStorage.CredentialPair("name", "secret")));
        CredentialStorage.CredentialPair pair = storage.loadPair("foo");
        assertNotNull(pair);
        assertEquals("name", pair.login);
        assertEquals("secret", pair.password);
    }

    public void itDoesNotLoadPartialPairs() {
        storage.store("foo" + CredentialStorage.LOGIN_SUFFIX, "name");
        // missing password
        assertNull(storage.loadPair("foo"));
    }
}
