package io.texio;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.texio.db.factories.ReceiptFactory;
import io.texio.db.models.Receipt;
import io.texio.db.receipt.ReceiptFileManager;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ReceiptFileManagerTest {

    private Receipt receipt;
    private ReceiptFileManager fileManager;

    @Before
    public void setUp() {
        receipt = ReceiptFactory.make();
        fileManager = new ReceiptFileManager(
            InstrumentationRegistry.getTargetContext(),
            receipt
        );
    }

    @After
    public void tearDown() {
        ReceiptFileManager fileManager = new ReceiptFileManager(
            InstrumentationRegistry.getTargetContext(),
            receipt
        );
        fileManager.deleteReceiptDirectory();
    }

    @Test
    public void itWritesAndReadsContent() {
        String written = "Lorem ipsum dolor.";

        assertEquals(true, fileManager.writeFileText("foo.txt", written));
        String read = fileManager.readFileText("foo.txt");

        assertEquals(written, read);
    }
}
