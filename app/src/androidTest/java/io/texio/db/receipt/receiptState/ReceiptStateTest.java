package io.texio.db.receipt.receiptState;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import io.texio.db.factories.ReceiptStateChangeFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.Metadata;
import io.texio.db.factories.ReceiptFactory;

import static io.texio.db.receipt.receiptState.ReceiptStateEnum.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ReceiptStateTest {

    private Receipt receipt = ReceiptFactory.make();

    @Test
    public void itIsUnusedForFreshReceipt() {
        ReceiptState state = ReceiptState.fromStateChanges(new ReceiptStateChange[] {});
        assertTrue(state.is(UNUSED));
    }

    @Test
    public void receiptHasBeenUploadedToUctenkovkaAndVerified() {
        ReceiptState state = ReceiptState.fromStateChanges(new ReceiptStateChange[] {
            ReceiptStateChangeFactory.make(receipt, UCTENKOVKA_UPLOADED,
                new Metadata().put("registererEmail", "john@doe.com")
            ),
            ReceiptStateChangeFactory.make(receipt, UCTENKOVKA_VERIFIED),
            ReceiptStateChangeFactory.make(receipt, UCTENKOVKA_IN_DRAW)
        });
        assertTrue(state.is(UCTENKOVKA_IN_DRAW));
        assertTrue(state.isUctenkovka());

        UctenkovkaReceiptState u = state.asUctenkovka();
        assertEquals("john@doe.com", u.getEmail());
    }
}
