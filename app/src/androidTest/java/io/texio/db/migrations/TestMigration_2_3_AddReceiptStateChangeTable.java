package io.texio.db.migrations;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.arch.persistence.room.testing.MigrationTestHelper;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;

import static org.junit.Assert.*;

/**
 * Testing migrations guide:
 * https://medium.com/androiddevelopers/testing-room-migrations-be93cdb0d975
 */
@RunWith(AndroidJUnit4.class)
public class TestMigration_2_3_AddReceiptStateChangeTable {

    public static final String TESTING_DATABASE = "testing-database";

    @Rule
    public MigrationTestHelper helper;

    public TestMigration_2_3_AddReceiptStateChangeTable() {
        helper = new MigrationTestHelper(
            InstrumentationRegistry.getInstrumentation(),
            AppDatabase.class.getCanonicalName(),
            new FrameworkSQLiteOpenHelperFactory()
        );
    }

    @Test
    public void itAddsDicIntoReceipts() throws IOException {
        SupportSQLiteDatabase db = helper.createDatabase(TESTING_DATABASE, 2);

        db.execSQL("INSERT INTO Receipt (`id`, `total`) VALUES ('id', 100);");
        db.close();

        db = helper.runMigrationsAndValidate(
            TESTING_DATABASE,
            3, true,
            AppDatabaseFactory.MIGRATION_2_3
        );

        Cursor c = db.query("SELECT * FROM Receipt WHERE `id` = 'id';");
        c.moveToFirst();
        int total = c.getInt(c.getColumnIndex("total"));
        String dic = c.getString(c.getColumnIndex("dic"));
        db.close();

        assertEquals(100, total);
        assertNull(dic);
    }

    @Test
    public void itHasReceiptStateChangeTable() throws IOException {
        SupportSQLiteDatabase db = helper.createDatabase(TESTING_DATABASE, 3);

        db.execSQL("INSERT INTO Receipt (`id`, `total`) VALUES ('rid', 100);");
        db.execSQL("INSERT INTO ReceiptStateChange (`receiptId`, `state`, `datetime`) " +
            "VALUES ('rid', 'st', 'dt');");

        Cursor c = db.query("SELECT * FROM ReceiptStateChange");
        c.moveToFirst();
        int id = c.getInt(c.getColumnIndex("id"));
        String rid = c.getString(c.getColumnIndex("receiptId"));
        String datetime = c.getString(c.getColumnIndex("datetime"));
        db.close();

        assertEquals(1, id);
        assertEquals("rid", rid);
        assertEquals("dt", datetime);
    }
}
