package io.texio.db.other;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DicTest {

    @Test
    public void itCanNormalizeDic() {
        assertEquals("12345678", Dic.normalize("CZ12345678"));
        assertEquals("123456789", Dic.normalize("CZ123456789"));
        assertEquals("1234567890", Dic.normalize("CZ1234567890"));
        assertEquals("12345678", Dic.normalize("cz12345678"));
        assertEquals("123456789", Dic.normalize("cz123456789"));
        assertEquals("1234567890", Dic.normalize("cz1234567890"));

        assertEquals("42", Dic.normalize("asdj a42 asd a"));
    }

    @Test
    public void itCanValidateDic() {
        assertTrue(Dic.isValid("cz12345678"));
        assertTrue(Dic.isValid("cz123456789"));
        assertTrue(Dic.isValid("cz1234567890"));
        assertTrue(Dic.isValid("CZ12345678"));

        assertFalse(Dic.isValid("cz123"));
        assertFalse(Dic.isValid("cz123456789123456789"));
    }
}
