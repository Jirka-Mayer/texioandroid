package io.texio.db.other;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DateTimeTest {

    @Test
    public void itParsesData() {
        String str = "2019-01-16T10:00:02.054Z";
        DateTime dt = new DateTime(str);

        assertEquals(2019, dt.getYear());
        assertEquals(1, dt.getMonth());
        assertEquals(16, dt.getDay());

        assertEquals(10, dt.getHour());
        assertEquals(0, dt.getMinute());
        assertEquals(2, dt.getSecond());
        assertEquals(54, dt.getMillisecond());

        assertEquals(str, dt.toString());
    }

    @Test
    public void millisecondsCanBeOmitted() {
        DateTime dt = new DateTime("2019-01-16T10:00:02Z");
        assertEquals(0, dt.getMillisecond());
    }

    @Test
    public void itCanAddDays() {
        DateTime dt = new DateTime("2019-01-16T10:00:02.000Z");
        assertEquals("2018-12-31T10:00:02.000Z", dt.addDays(-16).toString());
    }
}
