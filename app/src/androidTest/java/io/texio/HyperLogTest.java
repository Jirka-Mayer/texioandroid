package io.texio;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.hypertrack.hyperlog.HyperLog;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import io.texio.hyperlog.HyperLogExtended;
import io.texio.hyperlog.HyperLogManager;

@Ignore
@RunWith(AndroidJUnit4.class)
public class HyperLogTest {

    /*
        Tests are meant to be run manually during development
     */

    private String SERVER_URL = "http://10.175.72.177:8000/api/1.0/hyperlog/push/xxxxx";

    @Before
    public void setUp() {
        HyperLogManager.initializeHyperLog(InstrumentationRegistry.getTargetContext());
        HyperLog.deleteLogs();
    }

    @Test
    public void itSendsLogsToServer() {
        HyperLog.i("TAG", "Something informative happened.");
        HyperLog.e("TAG", "Something bad happened.");
        HyperLogExtended.exceptionWithTrace("TAG", new IOException("A fake IO exception."));
        HyperLogExtended.exceptionWithTrace(
            "TAG",
            "Something bad with exception happened.",
            new RuntimeException("Lorem ipsum.")
        );

        /*for (int i = 0; i < 11_000; i++) {
            HyperLog.i(null, Integer.toString(i));
        }*/

        HyperLogManager.pushLogsToServer(SERVER_URL);
    }
}
