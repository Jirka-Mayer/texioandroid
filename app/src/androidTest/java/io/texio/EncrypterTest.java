package io.texio;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.crypto.SecretKey;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class EncrypterTest {

    private SharedPreferences preferences;
    private KeyStore keyStore;

    @Before
    public void setUp() throws Exception {
        // clear database
        AppDatabase db = AppDatabaseFactory.getInstance(InstrumentationRegistry.getTargetContext());
        db.clearAllTables();

        // clear preferences
        preferences = InstrumentationRegistry.getTargetContext().getSharedPreferences(
            Encrypter.PREFERENCES_FILENAME, Context.MODE_PRIVATE
        );
        preferences.edit().clear().apply();

        // clear keystore
        keyStore = KeyStore.getInstance(Encrypter.ANDROID_KEY_STORE);
        keyStore.load(null);
        keyStore.deleteEntry(Encrypter.RSA_KEY_NAME);
    }

    @Test
    public void itEncryptsBytes() {
        Encrypter encrypter = new Encrypter(Encrypter.generateAesKey());

        byte[] writtenData = new byte[] { 2, 8, 42, 6 };
        byte[] buffer = encrypter.encryptBytes(writtenData);
        byte[] obtainedData = encrypter.decryptBytes(buffer);

        assertArrayEquals(writtenData, obtainedData);
        assertNotEquals(writtenData.length, buffer.length);
    }

    @Test
    public void itCreatesAllKeysOnFreshInstallation() throws Exception {
        Encrypter encrypter = Encrypter.getDefault(InstrumentationRegistry.getTargetContext());

        assertTrue(preferences.contains(Encrypter.AES_KEY_ENCRYPTED_NAME));
        assertTrue(keyStore.containsAlias(Encrypter.RSA_KEY_NAME));

        assertEquals("foo", encrypter.decryptString(encrypter.encryptString("foo")));
    }

    @Test
    public void itCreatesRsaKeysAndEncryptsAesKeyOnUpdate() throws Exception {
        SecretKey aesKey = Encrypter.generateAesKey();
        Encrypter oldEncrypter = new Encrypter(aesKey);

        String encodedKey = Base64.encodeToString(aesKey.getEncoded(), Base64.DEFAULT);
        preferences.edit().putString(Encrypter.AES_KEY_NAME, encodedKey).apply();

        byte[] oldCiphertext = oldEncrypter.encryptString("foo");

        Encrypter newEncrypter = Encrypter.getDefault(InstrumentationRegistry.getTargetContext());

        assertFalse(preferences.contains(Encrypter.AES_KEY_NAME));
        assertTrue(preferences.contains(Encrypter.AES_KEY_ENCRYPTED_NAME));
        assertTrue(keyStore.isKeyEntry(Encrypter.RSA_KEY_NAME));

        assertEquals("foo", newEncrypter.decryptString(oldCiphertext));
        assertNotEquals(encodedKey, preferences.getString(Encrypter.AES_KEY_NAME, null));
    }

    @Test
    public void itWorksOnAnAlreadyUpToDateInstallation() {
        Encrypter.getDefault(InstrumentationRegistry.getTargetContext()); // create up-to-date installation

        Encrypter encrypter = Encrypter.getDefault(InstrumentationRegistry.getTargetContext());

        assertEquals("foo", encrypter.decryptString(encrypter.encryptString("foo")));
    }

    @Test
    public void aesKeyAsEmptyStringWillBeConsideredAsMissing() {
        preferences.edit().putString(Encrypter.AES_KEY_ENCRYPTED_NAME, "").apply();

        Encrypter encrypter = Encrypter.getDefault(InstrumentationRegistry.getTargetContext());

        assertEquals("foo", encrypter.decryptString(encrypter.encryptString("foo")));

        // new key has been generated
        assertNotEquals(0, preferences.getString(Encrypter.AES_KEY_ENCRYPTED_NAME, "").length());
    }

    @Test
    public void oldAesKeyAsEmptyStringWillBeConsideredAsMissing() {
        preferences.edit().putString(Encrypter.AES_KEY_NAME, "").apply();

        Encrypter encrypter = Encrypter.getDefault(InstrumentationRegistry.getTargetContext());

        assertEquals("foo", encrypter.decryptString(encrypter.encryptString("foo")));

        // new key has been generated
        assertNotEquals(0, preferences.getString(Encrypter.AES_KEY_ENCRYPTED_NAME, "").length());
    }
}
