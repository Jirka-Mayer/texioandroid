package io.texio;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.texio.db.AppDatabase;
import io.texio.db.AppDatabaseFactory;
import io.texio.db.factories.ReceiptFactory;
import io.texio.db.factories.ReceiptStateChangeFactory;
import io.texio.db.models.Receipt;
import io.texio.db.models.ReceiptStateChange;
import io.texio.db.other.Metadata;
import io.texio.db.receipt.receiptState.ReceiptState;
import io.texio.db.receipt.receiptState.ReceiptStateEnum;
import io.texio.db.receipt.receiptState.UctenkovkaReceiptState;
import io.texio.integration.bazar.BazarApi;
import io.texio.integration.uctenkovka.UctenkovkaApi;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(AndroidJUnit4.class)
public class ReceiptStateUpdaterTest {

    private static final long RECEIPT_UCTENKOVKA_ID = 123456L;
    private static final long RECEIPT_BAZAR_ID = 654321L;

    private AppDatabase db;
    private UctenkovkaApi uctenkovkaApi;
    private BazarApi bazarApi;

    @Before
    public void setUp() {
        uctenkovkaApi = mock(UctenkovkaApi.class);
        bazarApi = mock(BazarApi.class);

        db = AppDatabaseFactory.getInstance(InstrumentationRegistry.getTargetContext());
        db.clearAllTables();
    }

    ////////////////
    // Uctenkovka //
    ////////////////

    @Test
    public void itUpdatesUploadedReceiptToInDraw() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata().put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, RECEIPT_UCTENKOVKA_ID)
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change);

        // stub
        when(uctenkovkaApi.queryReceiptState(RECEIPT_UCTENKOVKA_ID))
            .thenReturn(ReceiptStateEnum.UCTENKOVKA_IN_DRAW);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check new state
        ReceiptState newState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertEquals(ReceiptStateEnum.UCTENKOVKA_IN_DRAW, newState.getState());
    }

    @Test
    public void itUpdatesNewReceiptToInDraw() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata().put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, RECEIPT_UCTENKOVKA_ID)
        );
        ReceiptStateChange change2 = ReceiptStateChangeFactory.make(
            receipt, ReceiptStateEnum.UCTENKOVKA_NEW
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change, change2);

        // stub
        when(uctenkovkaApi.queryReceiptState(RECEIPT_UCTENKOVKA_ID))
            .thenReturn(ReceiptStateEnum.UCTENKOVKA_IN_DRAW);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check new state
        ReceiptState newState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertEquals(ReceiptStateEnum.UCTENKOVKA_IN_DRAW, newState.getState());
    }

    @Test
    public void itUpdatesVerifiedReceiptToInDraw() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata().put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, RECEIPT_UCTENKOVKA_ID)
        );
        ReceiptStateChange change2 = ReceiptStateChangeFactory.make(
            receipt, ReceiptStateEnum.UCTENKOVKA_VERIFIED
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change, change2);

        // stub
        when(uctenkovkaApi.queryReceiptState(RECEIPT_UCTENKOVKA_ID))
            .thenReturn(ReceiptStateEnum.UCTENKOVKA_IN_DRAW);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check new state
        ReceiptState newState = ReceiptState.fromStateChanges(
            db.receiptStateChangeDao().of(receipt.id)
        );
        assertEquals(ReceiptStateEnum.UCTENKOVKA_IN_DRAW, newState.getState());
    }

    @Test
    public void rejectedReceiptGetsIgnored() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata().put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, RECEIPT_UCTENKOVKA_ID)
        );
        ReceiptStateChange change2 = ReceiptStateChangeFactory.make(
            receipt, ReceiptStateEnum.UCTENKOVKA_REJECTED
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change, change2);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check that no API call performed
        verify(uctenkovkaApi, never()).queryReceiptState(RECEIPT_UCTENKOVKA_ID);
    }

    @Test
    public void receiptWithoutUctenkovkaIdGetsIgnored() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt, ReceiptStateEnum.UCTENKOVKA_UPLOADED
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check that no API call performed
        verify(uctenkovkaApi, never()).queryReceiptState(RECEIPT_UCTENKOVKA_ID);
    }

    @Test
    public void unusedReceiptGetsIgnored() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        db.receiptDao().insert(receipt);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // check that no API call performed
        verify(uctenkovkaApi, never()).queryReceiptState(RECEIPT_UCTENKOVKA_ID);
    }

    @Test
    public void verifiedReceiptStaysVerified() throws Exception {
        Receipt receipt = ReceiptFactory.make();
        ReceiptStateChange change = ReceiptStateChangeFactory.make(
            receipt,
            ReceiptStateEnum.UCTENKOVKA_UPLOADED,
            new Metadata().put(UctenkovkaReceiptState.METADATA_UCTENKOVKA_RECEIPT_ID, RECEIPT_UCTENKOVKA_ID)
        );
        ReceiptStateChange change2 = ReceiptStateChangeFactory.make(
            receipt, ReceiptStateEnum.UCTENKOVKA_VERIFIED
        );
        db.receiptDao().insert(receipt);
        db.receiptStateChangeDao().insert(change, change2);

        // stub
        when(uctenkovkaApi.queryReceiptState(RECEIPT_UCTENKOVKA_ID))
            .thenReturn(ReceiptStateEnum.UCTENKOVKA_VERIFIED);

        // update
        ReceiptStateUpdaterJobService.UpdateReceiptStates.performTheActualUpdate(
            db, uctenkovkaApi, bazarApi
        );

        // receipt change history does not get enlarged
        assertEquals(2, db.receiptStateChangeDao().of(receipt.id).size());
    }
}
